# MsAML-09-1 [[PDF](SS21-MsAML__09-1__Support_vector_machines.pdf)]

1. [Support vector machines](#support-vector-machines)
   * [Hard margin case](#hard-margin-case)
   * [Soft margin case](#soft-margin-case)
   * [Implementation](#implementation)

Back to [index](index.md).


## Support vector machines

Recall the discussion we started in the module [models of linear
classification](SS21-MsAML__05-1__Linear_classification_models.md). There, we
introduced the class of linear classification models parametrized by the
Adaline model as a basic representative for various choices of activation
function $\alpha(z)$ and loss function $L(y,y')$. The objective of this
discussion was to better understand how these choices affect:

1. the efficiency of training with prescribed training data;
2. and the generalization capabilities of the model to unseen data.

Regarding point 1. it was important to understand in which sense a minimization
of the empirical loss implies a low empirical error probability and how this
minimization procedure can be made more efficient, e.g., by avoiding the
saturation of the neuron. We furthermore discussed that not only the choice of
$\alpha(z)$ and $L(y,y')$ is important but also the preparation of the training
data as well as the choice of initial weights impacts the performance; not to
mention the purity of the manually annotated training data.

Regarding point 2., so far we only discussed that, in addition to the
requirement that a minimization of the empirical loss should imply a low
empirical error, we we could equip the empirical loss function $\widehat L(b,w)$
with a notion that influence the update rule directly, e.g., in order to
prefer certain updates over others. This allows to encode a variety of desired
generalization behavior for the respective model.  In this module we close our
discussion on linear classification models by introducing one of the most
simple but, as it turns out, also very effective and "industry-proof" notions
of generalization. The resulting model is the so-called *support vector
machine*, which beside the multi-layered Adaline will later in our course
provide a particular convenient way to tackle non-linear classification tasks.


### Hard margin case

Recall that, for linearly separable training data, we proved that the
Perceptron will succeed to find a separating hyperplane in finite time. Also we
argued that, depending on the learning parameter tuning, the Adaline will quite
efficiency find one -- although we were not able to give a general convergence 
guarantee to a separating hyperplane. However, in the typical case of finite
training data, the separating hyperplane is never unique. This would allow us
to choose one that has good chances to generalize better than others to unseen
data. As we discussed after the [no-free-lunch
theorem](SS21-MsAML__04-2__No_free_lunch_theorems.md), this is only possible if
certain regularities of the distribution of the test data are known a priori,
and this a priori knowledge has to be supplied in addition to the training
data. In the following, we shall explore such a simple notion that works well
in many settings:

As we have done before, let us consider binary classification with labels
$\mathcal Y=\{-1,+1\}$ and prescribed, linearly separable, and finite training
data. Among all possible separating hyperplanes, one could now choose the one
that maximizes the margin width of the separation as it is depicted here:

![Margin width in our setting.](09/maximum_margin.png)

Now, our task is to find an optimization program that would penalizes both, a
larger error probability as well as a larger margin width.

First, let us compute the distance between a feature vector of a training
datum $x^{(i)}\in\mathbb R^d$ and a separating hyperplane given by means of the
normal equation:
\begin{align}
  H_{(b,w)}
  :=
  \left\{
    x\in\mathbb R^d
    \,\big|\,
    \bar w\cdot\bar x = w\cdot x+b=0
  \right\}
\end{align}

In our $\bar w=(b,w)$ and $\bar x^{(i)}=(1,x^{(i)})$ notation, by definition,
$\bar w$ is orthogonal to vectors $\bar x=(1,x)\in H_{(b,w)}$ on the
hyperplane. Hence, the ray orthogonal to the hyperplane $H_{(b,w)}$ and emitted
at point $\bar x^{(i)}$ can be parametrized as follows:
\begin{align}
  \bar x^{(i)}(\lambda)
  =
  \begin{pmatrix}
    1\\
    x^{(i)}(\lambda)
  \end{pmatrix}
  =
  \begin{pmatrix}
    1\\
    x^{(i)}
  \end{pmatrix}
  +
  \lambda
  \begin{pmatrix}
    0\\
    w 
  \end{pmatrix},
  \qquad
  \lambda\in\mathbb R.
\end{align}
In turn, the ray intersects the hyperplane at $\lambda^*$ fulfilling
\begin{align}
  0 = \bar w\cdot\bar x^{(i)}(\lambda^*)
  &= b + w\cdot x^{(i)} + \lambda^* |w|^2\\
  &= \bar w\cdot \bar x^{(i)} + \lambda^* |w|^2.
\end{align}
Note that on the right-hand side the bars $|\cdot|$ denote the euclidean norm
and the overset bar $\bar{\cdot}$ on $w$ in the argument of the norm is
missing on purpose.

The intersection point is therefore given by $x^{(i)}(\lambda^*)$ for
\begin{align}
  \lambda^* = - \frac{\bar w \cdot \bar x^{(i)}}{|w|^2},
\end{align}
which allows to compute the minimal distance between $x^{(i)}$ and the
hyperplane as follows:
\begin{align}
  \operatorname{dist}_{(b,w)}(x^{(i)}) 
  &:= 
  \left| x^{(i)}-x^{(i)}(\lambda^*)\right|
  =
  \frac{\left| \bar\omega\cdot \bar x^{(k)}\right|}{|w|}.
  \label{eq_1}
  \tag{E1}
\end{align}

This term would already allow to formulate a new notion of optimization, i.e.,
among all separating hyperplanes $H_{(b,w)}$, i.e., those that achieve zero
classification errors on the training data, choose the one that maximizes the
distances 
\begin{align}
  \operatorname{dist}_{(b,w)}(x^{(k)}),
\end{align}
for the points $k\in\{1,\ldots,N\}$ closest to the hyperplane $H_{(b,w)}$.

In order to make this notion more concise, we may exploit the scalar freedom in the normal equations
\begin{align}
  \bar w\cdot \bar x = 0
  \qquad
  \Leftrightarrow
  \qquad
  \forall \gamma>0: \gamma\bar w\cdot\bar x = 0,
\end{align}
and choose $\gamma>0$ such that those vectors $x^{(k)}$ that lies closest to
$H_{(b,w)}$, the so-called *support vectors*, fulfill 
\begin{align}
  \bar w\cdot\bar x^{(k)}=\pm 1.
\end{align}
After the scaling, the distances $\eqref{eq_1}$ read
\begin{align}
  \operatorname{dist}_{(b,w)}(x^{(k)}) 
  = 
  \frac{1}{|w|}
\end{align}
in the new units, we find ourselves in the following scenario:

![A separating hyperplane after rescaling.](09/after_rescaling.png)

Finally, we can recast this into the equivalent optimization program:
\begin{align}
  \underset{\bar w=(b,w)\in\mathbb R^{d+1}}{\sup} \frac{1}{|w|}
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1\\
    &\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
\end{align}

In other words, during optimization, the objective is to maximize
$\frac{1}{|w|}$ while restricting to those hyperplanes that correctly separate
the training data, which is encoded in the constraints
\begin{align}
  y^{(i)}\,\bar w\cdot \bar x^{(i)}\geq 1,
  \qquad
  \text{for}
  \qquad
  i=1,\ldots,N.
\end{align}

If preferred, we can formulate the same maximization program as a minimization program:
\begin{align}
  \underset{\bar w=(b,w)\in\mathbb R^{d+1}}{\inf} |w|
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1\\
    &\text{for all } i=1,\ldots, N
  \end{cases}
  \,,
  \label{eq_2}
  \tag{E2}
\end{align}
which we shall refer to as the optimization program for the *hard margin
support vector machine*.

Note that compared to the previous Adaline optimization programs, for which we
simply attempted to minimize the empirical loss 
\begin{align}
  \underset{\bar w=(b,w)\in\mathbb R^{d+1}}{\inf} \widehat L(b,w),
\end{align}
in $\eqref{eq_2}$, we minimize with respect to constraints that enforce zero
misclassifications. After the discussion of the "soft margin case" we will
give a formulation of the minimization program in terms of an empirical loss
function as indicated in our introductory discussion above.

Of course, the generalization capabilities of the optimal hyperplane $H_{(b,w)}$
strongly depend on the question, whether the maximum margin is a good a priori
choice to fit the test data for the learning task at hand. The latter depends
on the regularities the underlying distribution of test data features. So far, we can
only say that, if the empirical mean of the training data sample is already a
good enough approximation of the actual mean and the test data typically
clusters closely around this mean with respect to the euclidean norm, the above
optimization program $\eqref{eq_2}$ is very likely a reasonable choice that
has good chances to generalize well.

Later in our discussion of the optimization program, we will see that the
optimal $w$ for program $\eqref{eq_2}$ will be given as a function of the
support vectors. Thus, $w$ is we exposed to the fluctuations of the training
data near the margin. In order to reduce this dependence, we extend the program
in the next section.


### Soft margin case

As a small generalization of the hard margin case, we could even allow for
margin violations and even some misclassification to some extend and attempt to
minimize them. Then, we cannot guarantee anymore to have zero
misclassifications, however, the reduced dependence on strong fluctuations in
the training data (or, e.g., even cope with some outlines of wrongly annotated
training data) may be beneficial for the generalization capabilities, while one 
may still achieve a good, though not perfect, performance on the classification
of the training data.

Say, a hyperplane $H_{(b,w)}$ was chosen and a training data feature vector
$x^{(i)}$ violates the margin, then

* either, we have only a violation within the margin width
  \begin{align}
    0 \leq y^{(i)} \, \bar w\cdot \bar x^{(i)} \leq 1,
  \end{align}
* or even a misclassification
  \begin{align}
    y^{(i)}\, \bar w\cdot \bar x^{(i)} < 0.
  \end{align}

Now, the setting looks as follows:

![The soft margin case.](09/soft_margin.png)

The "depth" $\eta^{(i)}$ of the margin violation can be computed again by
orthogonal projection of the feature vector of the training datum $x^{(i)}$
this time onto the respective margin boundaries 
\begin{align}
  \bar w\cdot \bar x = y^{(i)};
\end{align}
recall again that we work in the setting $\mathcal Y=\{-1,+1\}$. 

Again, in order to compute the depth of a potential margin violation,  we
parametrize the respective ray emitted at $x^{(i)}$ and lying orthogonal to the
margin boundary as follows:
\begin{align}
  \bar x^{(i)}(\lambda)
  =
  \begin{pmatrix}
    1\\
    x^{(i)}(\lambda)
  \end{pmatrix}
  =
  \begin{pmatrix}
    1\\
    x^{(i)}
  \end{pmatrix}
  +
  \lambda
  \begin{pmatrix}
    0\\
    w 
  \end{pmatrix},
  \qquad
  \lambda\in\mathbb R.
\end{align}
In consequence, the intersection of the ray and the margin boundary can be
computed by solving for $\lambda^*$ in equation
\begin{align}
  \bar w \cdot \bar x^{(i)}(\lambda^*) = y^{(i)},
\end{align}
which yields
\begin{align}
  \lambda^* 
  = 
  \frac{\left|y^{(i)}-\bar w\cdot \bar x^{(i)}\right|}{|w|^2}
  = 
  \frac{\left|1 - y^{(i)} \, \bar w\cdot \bar x^{(i)}\right|}{|w|^2}.
\end{align}

In case of the margin violation, the depth is then given by
\begin{align}
  \eta^{(i)}
  =
  \left|x^{(i)}-x^{(i)}(\lambda^*)\right|
  =
  \frac{\left|1 - y^{(i)} \, \bar w\cdot \bar x^{(i)}\right|}{|w|}.
\end{align}

In analogy to the hard margin case optimization program $\eqref{eq_2}$, we may
set up its generalization as follows:
\begin{align}
  \underset{\substack{\bar w=(b,w)\in\mathbb R^{d+1}\\\eta^{(i)}\geq 0, i=1,\ldots, N}}{\inf} 
  \left[
    |w| + \sum_{i=1}^N \eta^{(i)}
  \right]
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1 - |w|\eta^{(i)}\\
    &\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
\end{align}
Note that the constraints give $\eta^{(i)}$ a meaning as depth of margin
violation.

Again, exploiting the scalar freedom of the model, we may reformulate this equivalently as
\begin{align}
  \underset{\substack{\bar w=(b,w)\in\mathbb R^{d+1}\\\xi^{(i)}\geq 0, i=1,\ldots,N}}{\inf} 
  \left[
    |w|^2 + \sum_{i=1}^N \xi^{(i)}
  \right]
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1 - \xi^{(i)}\\
    &\text{for all } i=1,\ldots, N
  \end{cases}
  \label{eq_3}
  \tag{E3}
  \,,
\end{align}
which we shall refer to as the optimization program of the *soft margin
support vector machine*.


### Implementation

A first, maybe naive but working implementation can be made in the spirit of
the Adaline model, minimizing the following empirical loss function
\begin{align}
  \widehat L(b,w) = 
  \frac12|w|^2 
  + \frac\mu N \sum_{i=1}^N 
    \max\left\{0, 1- y^{(i)}\, \bar w\cdot \bar x^{(i)}\right\}.
    \label{eq_4}
    \tag{E4}
\end{align}
The factor $\frac12$ and the parameter $\mu\geq 0$, controlling the trade-off
between a large margin vs. having only few and small margin violations during
the updates, are introduced for convenience as done in many text-books. The
update rule can be formulated by means of gradient descent employing a
sub-differential for the $\max$ function.

There are far superior algorithms to carry out the optimization problem
$\eqref{eq_3}$, most notably as implemented in the C library
[LIBSVM](https://www.csie.ntu.edu.tw/~cjlin/libsvm/), which has many bindings
including Python-bindings through
[sklean](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html).

👀
: Adapt one of your Adaline implementations to minimize the empirical loss
$\eqref{eq_4}$ with the help of the gradient descent method employing a
reasonable sub-differential. Discuss for which distributions of test data the
model would indeed generalize well and in which situations it would not,
despite the effort of the maximization of the margin width.
