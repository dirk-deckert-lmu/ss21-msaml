# MsAML-14-1 [[PDF](SS21-MsAML__14-1__Kernel_SVMs.pdf)]

1. [Kernel support vector machines](#kernel-support-vector-machines)


Back to [index](index.md).


## Kernel support vector machines 

In the last module we have reformulated the soft-margin support vector machine as follows

\begin{align}
  &\max_{\alpha\in \mathbb R^N}
  \left(
    \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
    y^{(j)} \; x^{(i)} \cdot x^{(j)}.
  \right)
  \\
  &\qquad
  \text{subject to}
  \qquad
  \begin{cases}
    &0\leq \alpha_i\leq C,
    \\
    &\sum_{i=1}^N \alpha_i y^{(i)} = 0,
    \\
    &\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
  \label{eq_dSVM}
  \tag{dSVM}
\end{align}

which turns out to allow for a very efficient implementation of a non-linear classifier.

As discussed in our [module on non-linear
classification](SS21-MsAML__10-1__Non-linear_classification.md), a first means to turn our linear classifier into a non-linear one is the introduction of a non-linear map
\begin{align}
  \Phi:\mathcal X\to\mathcal X',
\end{align}
on feature space $\mathcal X=\mathbb R^d$ and, e.g., choose some $\mathcal
X'=\mathbb R^n$ for $n\gg d$ as codomain in order to map the training feature
vectors to a space of sufficiently high dimension in which the training data
becomes linearly separable.

Though adapting the implementation of the support vector machine by introducing
the map $\Phi$ is straight-forward, we have already argued that it can turn out 
computationally very costly. Recalling our last
[module](SS21-MsAML__13-1__Dual_formulation_SVM.md), in which have computed the
output hypothesis in terms of the training data, after the application of
$\Phi$ we find
\begin{align}
  h_\Phi(x) 
  &= 
  \text{sign} \left(w_\Phi\cdot \Phi(x)+b_\Phi\right)
  \\
  &= 
  \text{sign} \left(\sum_{i=1}^N\alpha_i y^{(i)}\, \Phi(x^{(i)})\cdot \Phi(x) + b\right)
\end{align}
and the computation of the inner product $\Phi(x^{(i)})\cdot\Phi(x)$ scales roughly
as $n$ in terms of complexity.

Therefore, we already mentioned the idea to replace these inner products 
\begin{align}
  \mathcal X\times\mathcal X\ni (x',x)\mapsto \Phi(x')\cdot\Phi(x)
\end{align}
by more efficient function evaluations of the type
\begin{align}
  \mathcal X\times\mathcal X\ni (x',x)\mapsto K(x',x).
\end{align}
In this module, we will look into properties that must be required from $K$ in
order to replace inner products on $\mathcal X'$. In this context, one refers
to $K$ as kernels and the corresponding program $\eqref{eq_kSVM}$ as kernel
support vector machines. Let us regard an example:

**Example:** An example of a kernel function is
\begin{align}
  K(x,y)=(x\cdot y+c)^p,
  \label{eq_1}
  \tag{E1}
\end{align}
where $x,y\in\mathcal X=\mathbb R^d$ and $c\in\mathbb
R,p\in\mathbb N$ are additional tuning parameters. Note that the
inner production in $\eqref{eq_1}$ is only on $\mathcal X=\mathbb R^d$ and not
on $\mathcal X'=\mathbb R^n$, and hence, still computationally efficient.

👀: Show that in the example above for $p=2$ and $d=2$ we find that
    \begin{align}
      \Phi(x)
      =
      \Phi\begin{pmatrix}
        x_1\\
        x_2
      \end{pmatrix}
      =
      \begin{pmatrix}
        x_1^2\\
        x_2^2\\
        \sqrt{2} x_1 x_2\\
        \sqrt{2c} x_1\\
        \sqrt{2c} x_2\\
        c
      \end{pmatrix}
    \end{align}
    fulfills
   \begin{align}
     \forall x',x\in\mathcal X: K(x',x)=\Phi(x')\cdot\Phi(x)
     \label{eq_2}
     \tag{E2}
   \end{align}
    Hence, in this case $K$ can be seen to internally implement a map $\Phi$ from
    $\mathbb R^2\to\mathbb R^6$, i.e., from $d=2$ to $n=6$ dimensions.

In the dual formulation of the support vector machine we can readily implemented 
the trick $\eqref{eq_2}$ to replace the inner products by means of $K$:
\begin{align}
  &\max_{\alpha\in \mathbb R^N}
  \left(
    \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
    y^{(j)} \; K(x^{(i)},x^{(j)}).
  \right)
  \\
  &\qquad
  \text{subject to}
  \qquad
  \begin{cases}
    &0\leq \alpha_i\leq C,
    \\
    &\sum_{i=1}^N \alpha_i y^{(i)} = 0,
    \\
    &\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
  \label{eq_kSVM}
  \tag{kSVM}
\end{align}

Next, let us collect the requirements for kernels $K$.  First, we observe that
any map $\Phi:\mathcal X\to\mathcal X'$ gives rise to a kernel $K$ such that
$\eqref{eq_2}$ holds. Hence, it is worthwhile to define:

Definition
: ##### (PDS kernels)
  A map $K:\mathcal X\times\mathcal X\to\mathbb R$ is called a positive
  semi-defined symmetric (PDS) kernel if, for all $k\in\mathbb N$ and
  $x_1,\ldots,x_k\in\mathcal X$, the matrix
  \begin{align}
    M=\left(K(x_i,x_j)\right)_{i,j=1,\ldots,k}
  \end{align}
  is symmetric and positive semi-definite, i.e., if
  \begin{align}
    M=M^T
    \qquad
    \wedge
    \qquad
    \forall v\in\mathbb R^k: \langle v, Mv\rangle\geq 0.
  \end{align}

This condition also ensures the convexity of the corresponding support vector
machine program $\eqref{eq_kSVM}$. 

Conversely, let us clarify in which case a kernel $K$ gives rise to a map
$\Phi$ such that $\eqref{eq_2}$ holds true. The latter relation is grasped by
the following theorem. As for all these discussion we are in need of an inner
product structure, it makes sense to restrict us to the case in which $\mathcal
X'$ is a Hilbert space, i.e., a complete normed space whose norm is induced by
an inner product. To emphasize this assumption in our notation, let us write
$\mathbb H=\mathcal X'$ and denote by $\langle,\cdot,\cdot\rangle:\mathbb
H\times\mathbb H\to\mathbb R$ the corresponding inner product in $\mathbb H$.

Theorem
: ##### (Reproducing kernel Hilbert space)
  Let $K:\mathcal X\times\mathcal X\to\mathbb R$ be a PDS kernel. Then, there
  is a Hilbert sapce $\mathbb H$ and a map $\Phi:\mathcal X\to\mathbb
  H,x\mapsto\Phi_x$ such that
  \begin{align}
    \forall x,x'\in\mathcal X: K(x,x')=\langle \Phi_{x'},\Phi_x\rangle
  \end{align}
  and such that for all $x\in\mathcal X$ and $f\in\mathbb H$ 
  \begin{align}
    f(x)=\langle f, \Phi_x\rangle=\langle f, K(x,\cdot)\rangle
  \end{align}
  holds true.

**Proof:** Given a PDS kernel $K$, define for all $x\in\mathcal X$
\begin{align}
  \Phi: \mathcal X &\to \mathbb R^{\mathcal X}\\
  x&\mapsto \Phi_x
\end{align}
such that for all $x'\in\mathcal X$
\begin{align}
  \Phi_x(x'):=K(x,x').
\end{align}
Furthermore, define
\begin{align}
  \mathbb H_\text{pre} := \left\{
    \sum_{i=1}^I a_i \Phi_{x_i}
    \,\Big|\,
    a_i\in\mathbb R,x_i\in\mathcal X, |I|<\infty
  \right\},
\end{align}
i.e., the space of all finite linear combinations of such functions
$\Phi_{x_i}$. We equip $\mathbb H_\text{pre}$ with a pairing 
\begin{align}
  \langle \cdot,\cdot\rangle: \mathbb H_\text{pre}\times\mathbb H_\text{pre}\to\mathbb R
\end{align}
such that for $f,g\in\mathbb H_\text{pre}$, denoted by
\begin{align}
  f=\sum_{i\in I} a_i\Phi_{x_i},
  \qquad
  g=\sum_{j\in J} b_j\Phi_{\bar x_j}
\end{align}
for respective index sets $I,J$ and coefficients $a_i,b_j$, the pairing is
defined as follows
\begin{align}
  \langle f, g\rangle_\text{pre}
  &:=
  \sum_{i\in I,j\in J} a_i b_j K(x_i,\bar x_j)\\
  &=
  \sum_{j\in J} b_j \sum_{i\in I} a_i \Phi_{x_i}(\bar x_j)\\
  &=
  \sum_{j\in J} b_j f(\bar x_j)
  \label{eq_3}
  \tag{E3}
  \\
  &=
  \sum_{i\in I} a_i g(x_i).
  \label{eq_4}
  \tag{E4}
\end{align}
The last step from $\eqref{eq_3}$ to $\eqref{eq_4}$ is owed to the symmetry of
the PDS kernel $K$. Hence, also $\langle\cdot,\cdot\rangle_\text{pre}$ is
symmetric. Furthermore, due to the same two equalities, it is independent of
the chosen representation in terms of linear combinations, and moreover, it is
bilinear in $f,g$. Exploiting the PDS kernel properties of $K$ once more, we
find
\begin{align}
  \langle f,f\rangle_\text{pre}
  &=
  \sum_{i,j\in I} a_i a_j K(x_i,x_j)
  \\
  &=
  a^T M a
  \\
  &\geq 0
\end{align}
for $a=(a_i)_{i\in I}$ and $M=(K(x_i,x_j))_{i,j\in I}$. More generally, for
\begin{align}
  F=\sum_{i=1}^k c_i f_i
\end{align}
with $f_i\in\mathbb H_\text{pre}$, $c_i\in\mathbb R$, $i=1,\ldots, k$ we find 
\begin{align}
  \langle F, F\rangle_\text{pre} = \sum_{i,j=1}^k c_i c_j \langle f_i, f_j\rangle_\text{pre}
  \geq 0.
\end{align}
Thus, $\langle\cdot,\cdot\rangle_\text{pre}$ is a PDS kernel on $\mathbb H_\text{pre}$.
In conclusion, our
pairing $\langle\cdot,\cdot\rangle_\text{pre}$ is a positive semi-definite
bilinear form.

By definition, for any $f=\sum_{i\in I}a_i\Phi_{x_i}$, we find for $x\in\mathcal X$:
\begin{align}
  f(x)=\sum_{i\in I}a_i\Phi_{x_i}(x)=\sum_{i\in I} a_i K(x_i,x)=\langle f, \Phi_x\rangle_\text{pre},
  \label{eq_5}
  \tag{E5}
\end{align}
which is called the reproducing property.

For $f\in\mathbb H_\text{pre}$, $x\in\mathcal X$, and $\Phi_x$ as defined above
we get
\begin{align}
  \left( \langle f, \Phi_x \rangle_\text{pre} \right)^2
  &=
  \langle f,f\rangle_\text{pre} 
  \langle \Phi_x,\Phi_x\rangle_\text{pre}
  \label{eq_6}
  \tag{E6}
\end{align}
by $\eqref{eq_5}$ and observing what may be called the Cauchy-Schwarz
inequality for PDS kernels:
\begin{align}
  K(x,x')^2\leq K(x,x)K(x',x').  
\end{align}
The latter is due to the positive semi-definiteness of $K$ as it implies a positive determinant:
\begin{align}
  \det
  \begin{pmatrix}
    K(x,x) & K(x, x')\\
    K(x',x) & K(x',x')
  \end{pmatrix}
  = K(x,x) K(x',x') - K(x,x')^2 \geq 0,
\end{align}
Collecting $\eqref{eq_5}$ and $\eqref{eq_6}$ we have proven
\begin{align}
  (f(x))^2 \leq \langle f,f\rangle_\text{pre} K(x,x)
\end{align}
for all $x\in\mathcal X$. The latter shows that our pairing
$\langle\cdot,\cdot\rangle_\text{pre}$ is also definite, and summing up, the
space $(\mathbb H_\text{pre},\langle\cdot,\cdot\rangle_\text{pre})$ is a
pre-Hilbert space. 

Let $\mathbb H$ denote the completion of $\mathbb H_\text{pre}$. Applying the
Cauchy-Schwarz inequality again, we find that, for $x\in\mathcal X$, the map
\begin{align}
  \mathbb H_\text{pre} & \to \mathbb R\\
  f &\mapsto \langle f, \Phi_x\rangle_\text{pre}
\end{align}
is Lipschitz-continuous so that, by the linear extension theorem, there is a
unique paring
\begin{align}
  \langle\cdot,\cdot \rangle: \mathbb H\times\mathbb H\to\mathbb R.
\end{align}
which equals $\langle\cdot,\cdot\rangle_\text{pre}$ on $\mathbb H_\text{pre}$.
Hence, $(\mathbb H,\langle\cdot,\cdot\rangle)$ is a Hilbert space.

Furthermore, the latter fact allows to lift the reproducing property $\eqref{eq_5}$ in $\mathbb H_\text{pre}$ to $\mathbb H$, i.e., for all $x\in\mathcal X$ and $f\in\mathbb H$
\begin{align}
  f(x)=\langle f, \Phi_x\rangle= \langle f, K(x,\cdot)\rangle
\end{align}

$\square$

Note that, the theorem only guarantees the existence of such a Hilbert space.
In general, it is not unique.

Exploiting the established relation between PDS kernels and maps $\Phi$ we can
encode in a particular choice for an a priori measure of similarity of feature
vectors in $\mathcal X$ in the choice of $K$ without the need to specify
$\Phi$. In order to efficiently model such notions and build more complex
kernels from elementary ones, the following computation, which we will not prove
here, are most helpful:

1. Normalization: For PDS kernel $K$ we can define a normalized version
   \begin{align}
     \bar K(x',x) = \begin{cases}
       \frac{K(x',x)}{\sqrt{K(x',x')K(x,x)}} & \text{if well-defined}\\
       0 & \text{otherwise}
     \end{cases}
   \end{align}
   which is again a PDS kernel.

2. Operations: PDS kernels are closed under the following operations:
   
   * Summation
   * Product
   * Tensor product
   * Point-wise limits, in particular, e.g., power series

👀: Prove the closeness under summation, products, and limits.

This allows to build several general purpose kernels such as, for instance:

1. The gaussian kernel
   \begin{align}
      K(x',x) = \exp\left(-\frac{|x-x'}{2\sigma^2}\right)
   \end{align}
   for $\sigma>0$, which are maybe among the most frequently use ones. They can
   be shown to be PDS kernels by employing the corresponding Euler series. The
   decision boundaries are therefore generalized from hyperplanes to more
   general ones shaped by the radial symmetry encoded in the gaussian kernel;
   see a comparison
   [here](https://scikit-learn.org/0.18/auto_examples/svm/plot_iris.html).

2. Sigmoid kernels
   \begin{align}
      K(x',x) = \tanh(a x'\cdot x + b)
   \end{align}
   for tuning parameters $a,b\geq 0$.

And there are of course many more; see, e.g.,
[here](https://www.kaggle.com/residentmario/kernels-and-support-vector-machine-regularization).
For a quick example of how to employ a kernel SVM, have a look at our
hand-written digit recognition challenge, for which a gaussian kernel SVM was
implemented as reference model. Note how well it performs without any tweaking.
This typically good baseline performance paired with the fast algorithms to
approximate optimal solutions made kernel SVMs an industry standard to some
extend which found many applications.

This module closes our discussion of the non-linear models. Obviously, this is
just the start. We have left out many interesting models and topics, such as
convolution layer architectures for neural networks, which would have been
fitting to our data challenge, recurrent networks such as LSTMs, generative
architectures such as autoencoders and GANs, and many more. However, we can
probably claim that we have covered most of the key ideas in our discussions
and now, depending on your interest, you should be well-prepared to continue
your own journey into the field. Enjoy!
