# MsAML-08-1 [[PDF](SS21-MsAML__08-1__Conditioning_and_tuning.pdf)]


1. [Further conditioning and tuning](#further-conditioning-and-tuning)
   * [Feature scaling](#feature-scaling)
   * [Random initial weights](#random-initial-weights)
   * [Regularization](#regularization)

Back to [index](index.md).


## Further conditioning and tuning

Before the last holiday we started to study the large class of linear
classification models that were parametrized by our respective choices in the
activation function $\alpha(z)$ and loss function $L(y,y')$. We have discussed
choices that, on the one hand, compress the activation output values onto the
interval $(0,1)$ while, on the other hand, circumvent the encountered
saturation of the model that leads to the discussed vanishing gradient problem.
In this module we briefly touch upon three other simply but effective methods
that help to better condition and tune the training cycle for the model.


### Feature scaling

As we have discussed, the update of our model parameters during training was
given by
\begin{align}
  \bar w 
  & \leftarrowtail 
  \bar w^\text{new} 
  := 
  \bar w - \eta \frac{\partial \widehat L(\bar w)}{\partial \bar w}.
\end{align}
It is ruled by:

* the learning rate $\eta$, which is entirely under our control,
* and the activation error, e.g., $|y^{(i)}-\alpha(\bar w\cdot\bar
  x^{(i)})|$, which depends on the nature of our
  prescribed training data, i.e., the scale and distribution of the feature
  vectors $x^{(i)}$ and labels $y^{(i)}$.

While the question, how to tune $\eta$ conveniently to allow for successful and
efficient training, will usually involve some engineering intuition, this
tuning is certainly dependent on the natural scale inherent in the prescribed
training data. In order to reduce this dependence, and therefore, to also
make training cycles for different training data and even learning tasks more
comparable, it makes sense to standardize the input features, e.g., by
\begin{align}
  x^{(i)} 
  & \leftarrowtail 
  x^{(i)}_\text{new} := \frac{x^{(i)}-\widehat \mu}{\widehat \sigma},
\end{align}
where the empirical expectation and standard deviation are given by
\begin{align}
  \widehat \mu 
  &:= 
  \frac{1}{N}\sum_{i=1}^N x^{(i)},
  \\
  \widehat \sigma
  &:=
  \sqrt{\frac{1}{N-1}\sum_{i=1}^N|x^{(i)}-\widehat \mu|^2}.
  \label{eq_1}
  \tag{E1}
\end{align}

👀
: For the purpose of conditioning the training cycle well, the $\frac{1}{N-1}$
  factor in the empirical standard deviation is not terribly important, provided
  $N$ is large.  Nevertheless, it might be an interesting to see why this factor makes sense.
  For this, consider that the training sample
  $(x^{(i)},y^{(i)})_{i=1,\ldots,N}$ is drawn randomly from i.i.d.-$P$ random
  variables $(X^{(i)},Y^{(i)})_{i=1,\ldots,N}$, and thanks to factor
  $\frac{1}{N-1}$ as given in $\eqref{eq_1}$, show that then 
  \begin{align}
    E\widehat \sigma = E|X^{(i)}-EX^{(i)}|. 
  \end{align}

After standardization, also the variance of the activation output
$\alpha(\bar w\cdot \bar x^{(i)})$ is reduced, and in turn, also the
dependence of the range of $\eta$'s that lead to a successful training.

The standardization is particularly useful for multi-dimensional training
data whose feature components exhibit different scales (e.g., physical
units).  Consider, for example, the case of
$(x^{(i)}_1,x^{(i)}_2)=x^{(i)}\in\mathbb R^2$ and the empirical variance of the
first component being a lot larger than the one of the second. At least in case
of the logistic activation and cross-entropy (or linear activation and
quadratic loss), the update rule reads
\begin{align}
  & \bar w = 
  \begin{pmatrix}
    b\\
    w_1\\
    w_2
  \end{pmatrix}\\
  & \leftarrowtail 
  \bar w^\text{new} 
  \begin{pmatrix}
    b^\text{new}\\
    w^\text{new}_1\\
    w^\text{new}_2
  \end{pmatrix}
  := 
  \begin{pmatrix}
    b\\
    w_1\\
    w_2
  \end{pmatrix}
   + \eta  
     \frac{1}{N}\sum_{i=1}^N
     \left(y^{(i)}-\alpha(\bar w\cdot \bar x^{(i)})\right)
     \begin{pmatrix}
       1\\
       x^{(i)}_1\\
       x^{(i)}_2
     \end{pmatrix}.
\end{align}
Hence, during the training and irrespectively of the choice of $\eta$,
tentatively larger updates in the $w^\text{new}_1$ component will be preferred
over those in $w^\text{new}_2$. This effect is substantially reduced by the
above described standardization.

There is another reason why standardization is a good practice as it reduces
the risk of the saturation of the model. The latter becomes particularly
important when multi-class Adaline neurons are layered to form deeper networks,
as we will discuss soon. However, standardization only attacks the problem of
saturation from the perspective of the training data. Saturation can still
occur if the initial model parameters are ill-conditioned. In this regard, 
we have a control to some extend by the means of the method described in the
next section.


### Random initial weights

As another mechanism to reduce the saturation (and potential initial bias) of
the model is to randomize its initial model parameters. Note that the larger 
the dimension $d$ of the input features $(x^{(i)}_1,\ldots,
x^{(i)}_d)=x^{(i)}\in\mathbb R^d$ is,
the more likely we might end up with a large activation input 
\begin{align}
  w\cdot x^{(i)}+b = \sum_{j=1}^d w_i x^{(i)}_j + b.
\end{align}
Depending on the choice of activation function, a large activation input
results either in a high activation output or a saturation of the model.

One way to work against such a behavior is to arrange for initial model
parameters that are random and normal distributed, e.g., according
\begin{align}
  w_i \sim \mathcal N\left(\mu=0,\sigma=\frac{1}{\sqrt{d}}\right),
\end{align}
where the arguments $\mu$ and $\sigma$ denote the mean and standard deviation.

For simplicity, let us regard the case in which the feature vector components
are i.i.d. with mean zero and variance one and the feature vectors and model
parameters $w=(w_1,\ldots, w_d)$ are independent. Then, we would find
\begin{align}
   E w\cdot x = 0, \qquad E|w\cdot x|^2 = 1.
   \label{eq_2}
   \tag{E2}
\end{align}
Thus, on average (over the samples of model parameters and feature vectors) a
high activation output or saturation can be avoided.

👀
: Check the expectation an variance in line $\eqref{eq_2}$.

Of course, most sensible learning tasks will surely result in features vector
components that are not i.i.d. However, by means of this simplified examples,
we can at least see how standardization may also allow to reduce the risk of a
saturating of the model.

The randomization of the initial weights becomes particularly important in
layered multi-class Adaline neurons. Even beside the problem of saturation, it
is required to allow different layers of the network to assume different
"subtasks" of the learning problem as we will see later once we will discuss
the multi-layer networks.


### Regularization

As discussed, the initial weights are prescribe by conditioning the training
cycle, e.g., by random initial values. But how can we keep some control on the
updates of the model parameters during training and avoid potential
disadvantageous ones?

Given a particular activation function, the nature of the update is driven by
the choice empirical loss function. Hence, we may use it to encode further
notions of how to conduct model parameter updates. This can be done by
introducing further terms in the loss function that measure undesired behavior
and add to the overall value of the loss function, hence, penalizing potentially
undesired behavior.

A common choice is the so-called $l_2$-regularization for which we replace the
original empirical loss function
\begin{align}
  \widehat L(\bar w) = \frac 1N \sum_{i=1}^N L(y^{(i)},\alpha(\bar w\cdot x^{(i)})
\end{align}
by the regularized one
\begin{align}
  \widehat L_\text{reg}(\bar w) = 
  \widehat L(\bar w) + \frac{\lambda}{2d} \|w\|^2_2
\end{align}
where $\|\cdot\|_2$ denotes the $l_2$, i.e., euclidean, norm of the model parameters
\begin{align}
  \|w\|_2=\sum_{i=1}^d w_i^2.
\end{align}

Minimizing with respect to $\widehat L_\text{reg}$ instead of $\widehat L$ now
enforces a $\lambda$-dependent trade-off between minimizing the empirical loss
$\widehat L(\bar w)$  over the parameter choices $\bar w$ and
minimizing the $l_2$-norm of the model parameters $w$. The
corresponding update rule then reads:
\begin{align}
  b 
  & \leftarrowtail 
  b^\text{new} 
  := 
  b - \eta \frac{\partial \widehat L(b,w)}{\partial b},
  \\
  w 
  & \leftarrowtail 
  w^\text{new} 
  := 
  w - \eta 
  \left(
    \frac{\partial \widehat L(b,w)}{\partial w} + \frac{\lambda}{d} w
  \right)
  \\
  & \phantom{\leftarrowtail w^\text{new} }
  =
  \left(1-\frac{\eta\lambda}{d}\right) w - \eta 
    \frac{\partial \widehat L(b,w)}{\partial w}.
\end{align}
The first summand on the right-hand side of the $w$-update attempts to drive
$w$ to zero and the second one, as we discussed already, attempts to drive $w$
towards a local minimum. The new ad hoc parameter $\lambda\geq 0$ now allows to
adjust the trade-off between both update directions:

* Large $\lambda$ will prefer a smaller $\|w\|_2$ over minimizing the empirical
  loss function $\widehat L(\bar w)$.
* Smaller $\lambda$ will prefer the minimization of the empirical loss function 
  $\widehat L(\bar w)$ over smaller $\|w\|_2$.

In summary, by means of the $l_2$-regularization technique one can temper
unexpectedly large increases of the model parameters $w$ during training.

Remark
: In view of our discussion in the [error decomposition
  module](SS21-MsAML__02-S1__Error_decomposition.md), a regularization can also
  help to fight against the overfitting because, now, smaller $w$
  are preferred, and thus, the richness of the hypotheses space of the algorithm
  is $\lambda$-dependently is adaptively reduced. In a sense, the adjustment of
  $\lambda$ can be seen as a tuning of the trade-off between bias and variance.

Of course, there are many other ways to introduce a regularization:

👀
: Consider the following choice of so-called $l_1$-regularization:
  \begin{align}
    \widehat L_\text{reg}(\bar w) = \widehat L(\bar w) + \frac{\lambda}{d}\|w\|_1
  \end{align}
  for
  \begin{align}
    \|w\|_1 = \sum_{i=1}^d |w_i|.
  \end{align}
  Note that, with this choice, the gradient is not defined everywhere. How can
  we nevertheless define a sensible update rule? What update behavior is
  encouraged by means of this regularization?

In summary, a successful minimization of the empirical loss function $\widehat
L(\bar w)$ will lead to a classifier that reproduces the labels of the
prescribed training data well. However, a good performance on the training data 
does not mean that the learned classifier generalizes well to unseen data.
Adding further terms to the empirical loss function, as the ones above, can
adapt the richness of the potential candidates for hypotheses in a sensible
way and help to enforce a selection of a classifier that may have certain 
generalization capabilities. A last model we will study before discussing
multi-layered networks, the so-called *support vector machine*, exploits this
degree of freedom very directly to implement a sensible notion of
generalization.

➢ Next session!

