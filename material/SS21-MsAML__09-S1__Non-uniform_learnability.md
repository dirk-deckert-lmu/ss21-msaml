# MsAML-09-S1 [[PDF](SS21-MsAML__09-S1__Non-uniform_learnability.pdf)]


1. [Non-uniform learnability](#non-uniform-learnability)
   * [Structural risk minimization](#structural-risk-minimization)

Back to [index](index.md).


## Non-uniform learnability 

As the [fundamental theorem of binary
classification](SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.md)
made precise, the key ingredient that allowed to give PAC learning guarantees
was the finite VC-dimension. Though this covers a lot of applications, there are
interesting cases for which the finiteness of the VC-dimension does not hold.
Clearly, in order to go beyond our previous results we will have to relax our
definition of PAC learnability. And there is a sensible way to do so.

So far all of our results were uniform in the distribution of the training data
as well as the hypotheses. As we naturally know little about the underlying
distribution of the learning task, it makes sense to relax the uniformity on the
relevant hypotheses in $\mathcal F\subseteq \mathcal H$ first.

Let us recall that, for PAC learnability, c.f., the [fundamental theorem of binary
classification](SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.md),
we required:

\begin{align}
  & \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  & \qquad \qquad 
  R(h_S) \leq R_{\mathcal F} + \epsilon \qquad \text{with probability of at least } 1-\delta.
\end{align}

One way of relaxing this condition is to allow hypothesis-dependent bounds on
the sample size $N$. In other words, we do not look for a uniform polynomial
$p$ for the entire class of hypotheses $\mathcal F$ anymore but for a family of
polynomials $(p_h)_{h\in\mathcal F}$ indexed by hypotheses $h\in\mathcal F$ and
try to bound the risk $R(h_S)$ by means of $R(h)$.

Definition
: ##### (Non-uniform PAC learnability)
\begin{align}
  & \exists \, \text{a family of polynomials $(p)_{h\in\mathcal F}$ and an
  algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, h \in\mathcal F:\\
  & \qquad \forall \, N \geq p_h\left(\frac1\epsilon,\frac1\delta\right):\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \qquad 
  R(h_S) \leq R(h) + \epsilon \qquad \text{with probability of at least } 1-\delta.
\end{align}

Note that, by definition of $R_{\mathcal F}$, PAC learnability implies 
\begin{align}
  & \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  & \qquad \forall \, h \in\mathcal F:\\
  & \qquad \qquad 
  R(h_S) \leq R(h) + \epsilon \qquad \text{with probability of at least } 1-\delta,
\end{align}
and hence, non-uniform PAC learnability.

So far the empirical risk minimization was our showcase algorithm for PAC
learnability. It will be convenient to have such a showcase for non-uniform PAC
learnability as well, which we shall introduce next.

### Structural risk minimization

Besides in the algorithm $\mathcal A$ itself, prior knowledge about the
learning task is encoded in the hypotheses space $\mathcal H$. The latter can
be made more explicit when introducing a preference of hypotheses. For this
purpose, let us assume that
\begin{align}
  \mathcal H = \bigcup_{n\in\mathbb N} \mathcal H_n,
\end{align}
i.e., a countable decomposition of $\mathcal H$, and in addition, a weight
function
\begin{align}
  \omega_{\cdot}: \mathbb N \to \mathbb R^+_0
  \qquad
  \text{such that}
  \qquad
  \sum_{n\in\mathbb N} \omega_n \leq 1,
\end{align}
that specifies our preference for certain hypotheses classes $\mathcal H_n$,
$n\in\mathbb N$, over others in this family.

Let us furthermore assume that, for all $n\in\mathbb N$, the space $\mathcal
H_n$ features the uniform convergence property, i.e.,
\begin{align}
  & \exists \, \text{polynomial } p_n \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \forall \, N \geq p_n\left(\frac1\epsilon,\frac1\delta\right)\\
  & \qquad \forall \, h\in\mathcal H_n:\\
  & \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon
  \qquad \text{with probability of at least } 1-\delta.
\end{align}

We may then define the smallest upper bound for given $N$ and $\delta$:
\begin{align}
  \epsilon_n(N,\delta) := \inf\left\{
    \epsilon\in (0,1]
    \,\big|\,
    p_n\left(\frac1\epsilon,\frac1\delta\right)\leq N
  \right\}
\end{align}
so that, thanks to the uniform convergence property,
\begin{align}
  & \qquad \forall \, n,N\in\mathbb N\\
  & \qquad \forall \, \delta \in (0,1]\\
  & \qquad \forall \, h\in\mathcal H_n:\\
  & \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon_n(N,\delta) \\
  & \qquad\qquad \quad \text{with probability of at least } 1-\delta,
\end{align}
holds true.

Replacing $\delta \leftarrowtail \delta_n:=\delta w_n$ yields
\begin{align}
  & \qquad \forall \, n,N\in\mathbb N\\
  & \qquad \forall \, \delta \in (0,1]\\
  & \qquad \forall \, h\in\mathcal H_n:\\
  & \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon_n(N,\delta \omega_n)
  \label{eq_1}
  \tag{E1}
  \\
  & \qquad \qquad \quad \text{with probability of at least } 1-\sum_{n\in\mathbb N}\omega_n \leq 1 -\delta.
\end{align}

If, for $h\in\mathcal H$, we pick
\begin{align}
  n(h) := \min\left\{
    n\in\mathbb N
    \,\big|\,
    h\in\mathcal H_n
  \right\},
\end{align}
i.e., the smallest index $n$ of hypotheses class $\mathcal H_n$ that contains $h$, our bound in $\eqref{eq_1}$ implies
\begin{align}
  & \qquad \forall \, N\in\mathbb N\\
  & \qquad \forall \, \delta \in (0,1]\\
  & \qquad \forall \, h\in\mathcal H:\\
  & \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon_{n(h)}(N,\delta \omega_{n(h)}) 
  \label{eq_2}
  \tag{E2}
  \\
  & \qquad \qquad \quad \text{with probability of at least } 1 -\delta.
\end{align}

Contrary to empirical risk minimization, which only focusses on the minimality
of $\widehat R_S(h)$, we may use bound $\eqref{eq_2}$ to trade some of our bias
towards "smallness of $\widehat R_S(h)$" for "smallness of
$\epsilon_{n(h)}(N,\delta\omega_{n(h)})$", in case this already results in an
overall smaller error estimate in the bound of $R(h)$. This motivates the
so-called *structural risk minimization* algorithm $\mathcal A^\text{SRM}$,
which attempts to find a hypothesis $h^\text{SRM}_S = \mathcal A^\text{SRM}(S)$
such that
\begin{align}
  h^\text{SRM}_S \in \underset{h\in\mathcal H}{\operatorname{argmin}}
  \left\{\widehat R_S(h) + \epsilon_{n(h)}(N,\delta \omega_{n(h)})\right\}.
\end{align}

In practice, one usually arranges the hypotheses classes such that
\begin{align}
  \mathcal H_n \subseteq \mathcal H_{n+1},
  \qquad n\in\mathbb N.
\end{align}
A good example is again the least square fitting algorithm of a polynomial of
degree $n$ to sample points of the graph of an unknown function. In such
settings, we may conduct our search iteratively and, at step $n\in\mathbb N$,
though empirical risk minimization restrict to the hypotheses in $\mathcal
H_n\setminus \mathcal H_{n-1}$ that yields
\begin{align}
  h_S^{\text{ERM},n} \in 
  \underset{h\in\mathcal H_n\setminus\mathcal H_{n-1}}{\operatorname{argmin}}
  \widehat R_S(h)
\end{align}
monitor the growth of the two competing terms $\widehat R_S(h^{\text{ERM},n})$
and $\epsilon_{n}(N,\delta\omega_{n})$ in order to select $h^\text{SRM}_S$.

This algorithm allows to give the following learning grantee.

Lemma
: ##### (First non-uniform learning guarantee)
  Let $(\mathcal H_n)_{n\in\mathbb N}$ be a family of hypotheses spaces
  $\mathcal H_n$ fulfilling the uniform convergence property and weights
  $\omega_n:=\frac{6}{(\pi n)^2}$, $n\in\mathbb N$. Then,
  \begin{align}
    \mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n
  \end{align}
  is non-uniformly learnable by means of the structural risk minimization
  algorithm $\mathcal A^\text{SRM}$ with a sample bound for $h\in\mathcal H$
  of:
  \begin{align}
    p_h\left(\frac1\epsilon,\frac1\delta\right)
    \leq
    p_{n(h)}\left(\frac2\epsilon,\frac{(\pi n(h))^2}{3\delta}\right).
  \end{align}

**Proof:** Let $h\in\mathcal H$, $\epsilon,\delta\in (0,1]$, $N\in\mathbb N$,
so that
\begin{align}
  R(h) \leq \widehat R_S(h) + \epsilon_{n(h)}\left(N,\delta\omega_{n(h)}\right)
\end{align}
with probability of at least $1-\sum_{n\in\mathbb N}\frac{6}{(\pi
n)^2}\delta=1-\delta$.

Choosing
\begin{align}
  N 
  > 
  p_{n(h)}\left(
    \frac{1}{\frac12\epsilon},
    \frac{1}{\frac12\delta \omega_{n(h)}},
  \right) 
  =
  p_{n(h)}\left(
    \frac{2}{\epsilon},
    \frac{(\pi n(h))^2}{3\delta},
  \right) 
  =: 
  p_h\left(\frac1\epsilon,\frac1\delta\right)
\end{align}
we get for $h^\text{SRM}_S=\mathcal A^\text{SRM}(S)$ that
\begin{align}
  R\left(
  h^\text{SRM}_S
  \right)
  &\leq
  \inf_{\tilde h\in\mathcal H}
  \left[
    \widehat R_S(\tilde h)
    +
    \epsilon_{n(\tilde h)}(N,\delta\omega_{n(\tilde h)})
  \right]
  \\
  &\leq
  \widehat R_S(h)
    +
    \epsilon_{n(h)}(N,\delta\omega_{n(h)})
  \\
  &\leq
  \widehat R_S(h)
    +
  \frac\epsilon2
\end{align}
with probability of at least $1-\frac\delta2$.

Furthermore, for $N>p_h\left(\frac1\epsilon,\frac1\delta\right)$, by the
uniform convergence property of $\mathcal H_n$:
\begin{align}
  P_S\left(
    \left|
      \widehat R_S(h)-R(h)
    \right|
    > \frac\epsilon2
  \right)
  <
  \frac\delta2.
\end{align}

In turn, we can employ the union bound to infer
\begin{align}
  P_S\left(
    R\left(
    h^\text{SRM}_S
    \right)
    >
    \widehat R_S(h)
      +
    \frac\epsilon2
    \,
    \vee
    \,
    \left|
      \widehat R_S(h)-R(h)
    \right|
    > \frac\epsilon2
  \right)
  <
  \delta,
\end{align}
or equivalently
\begin{align}
  P_S\left(
    R\left(
    h^\text{SRM}_S
    \right)
    \leq
    \widehat R_S(h)
      +
    \frac\epsilon2
    \,
    \wedge
    \,
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \leq \frac\epsilon2
  \right)
  \geq
  1-\delta.
\end{align}
This implies
\begin{align}
    R\left(
      h^\text{SRM}_S
    \right)
    \leq R(h)+\epsilon
\end{align}
with probability of at least $1-\delta$. As a last step, we observe that the last bound was
independent of our choices in $h\in\mathcal H$, $\epsilon,\delta\in(0,1]$ and
distribution of $P$, which concludes our proof.

$\square$

To summarize, we have proven that every countable family of hypotheses spaces
$(\mathcal H_n)_{n\in\mathbb N}$ such that each $\mathcal H_n$ has the uniform
convergence property which gives rise to $\mathcal H=\bigcup_{n\in\mathbb
N}\mathcal H_n$ ensures that $\mathcal H$ is non-uniformly PAC learnable. 

With the help of the fundamental theorem of binary
classification, we can reformulate this statement into: $\mathcal
H=\bigcup_{n\in\mathbb N}\mathcal H_n$ with $\forall n\in\mathbb N:
d_n:=\dim_\text{VC}\mathcal H_n<\infty$ is non-uniformly PAC learnable.

Even the converse holds, so that we again gain a precise characterization of
non-uniform PAC learnability that we could add to our fundamental theorem of binary
classification:

Theorem
: ##### (Characterization of non-uniform learnability)
  A hypotheses class $\mathcal H$ is non-uniformly PAC learnable if and only if
  there is a family $(\mathcal H_n)_{n\in\mathbb N}$ of PAC learnable $\mathcal
  H_n$ such that $\mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n$.

**Proof:** "$\Leftarrow$" is an application of the fundamental theorem of
binary classification.

"$\Rightarrow$": If $\mathcal H$ is non-uniformly learnable, define
\begin{align}
  \mathcal H_n:=\left\{
    h\in\mathcal H
    \,\big|\,
    p_h\left(\frac{1}{\frac14},\frac{1}{\frac14}\right)
    \leq n
  \right\}
\end{align}
which implies $\mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n$.

For $n\in\mathbb N$ and $\epsilon=\frac14=\delta$, we then have
for all $N>n$:
\begin{align}
  \left|
    R\left(h^\text{SRM}_S\right)
    -\inf_{h\in\mathcal H_n} R(h)
  \right|
  > \frac 14
\end{align}
with probability less than $\frac14$.

Suppose we had $\dim_\text{VC}\mathcal H_n=\infty$. We may then conduct the same
argument as in the proof (S3) $\Rightarrow$ (S1) of the [fundamental theorem of binary
classification](SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.md)
to arrive at the contradiction $\dim_\text{VC}\mathcal H_n<\infty$, and
hence, $\mathcal H_n$ is PAC learnable.

$\square$
