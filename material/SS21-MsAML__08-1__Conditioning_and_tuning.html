<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__08-1__Conditioning_and_tuning</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-08-1-pdf">MsAML-08-1 [<a href="SS21-MsAML__08-1__Conditioning_and_tuning.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#further-conditioning-and-tuning">Further conditioning and tuning</a>
<ul>
<li><a href="#feature-scaling">Feature scaling</a></li>
<li><a href="#random-initial-weights">Random initial weights</a></li>
<li><a href="#regularization">Regularization</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="further-conditioning-and-tuning">Further conditioning and tuning</h2>
<p>Before the last holiday we started to study the large class of linear classification models that were parametrized by our respective choices in the activation function <span class="math inline">\(\alpha(z)\)</span> and loss function <span class="math inline">\(L(y,y&#39;)\)</span>. We have discussed choices that, on the one hand, compress the activation output values onto the interval <span class="math inline">\((0,1)\)</span> while, on the other hand, circumvent the encountered saturation of the model that leads to the discussed vanishing gradient problem. In this module we briefly touch upon three other simply but effective methods that help to better condition and tune the training cycle for the model.</p>
<h3 id="feature-scaling">Feature scaling</h3>
<p>As we have discussed, the update of our model parameters during training was given by <span class="math display">\[\begin{align}
  \bar w 
  &amp; \leftarrowtail 
  \bar w^\text{new} 
  := 
  \bar w - \eta \frac{\partial \widehat L(\bar w)}{\partial \bar w}.
\end{align}\]</span> It is ruled by:</p>
<ul>
<li>the learning rate <span class="math inline">\(\eta\)</span>, which is entirely under our control,</li>
<li>and the activation error, e.g., <span class="math inline">\(|y^{(i)}-\alpha(\bar w\cdot\bar x^{(i)})|\)</span>, which depends on the nature of our prescribed training data, i.e., the scale and distribution of the feature vectors <span class="math inline">\(x^{(i)}\)</span> and labels <span class="math inline">\(y^{(i)}\)</span>.</li>
</ul>
<p>While the question, how to tune <span class="math inline">\(\eta\)</span> conveniently to allow for successful and efficient training, will usually involve some engineering intuition, this tuning is certainly dependent on the natural scale inherent in the prescribed training data. In order to reduce this dependence, and therefore, to also make training cycles for different training data and even learning tasks more comparable, it makes sense to standardize the input features, e.g., by <span class="math display">\[\begin{align}
  x^{(i)} 
  &amp; \leftarrowtail 
  x^{(i)}_\text{new} := \frac{x^{(i)}-\widehat \mu}{\widehat \sigma},
\end{align}\]</span> where the empirical expectation and standard deviation are given by <span class="math display">\[\begin{align}
  \widehat \mu 
  &amp;:= 
  \frac{1}{N}\sum_{i=1}^N x^{(i)},
  \\
  \widehat \sigma
  &amp;:=
  \sqrt{\frac{1}{N-1}\sum_{i=1}^N|x^{(i)}-\widehat \mu|^2}.
  \label{eq_1}
  \tag{E1}
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd>For the purpose of conditioning the training cycle well, the <span class="math inline">\(\frac{1}{N-1}\)</span> factor in the empirical standard deviation is not terribly important, provided <span class="math inline">\(N\)</span> is large. Nevertheless, it might be an interesting to see why this factor makes sense. For this, consider that the training sample <span class="math inline">\((x^{(i)},y^{(i)})_{i=1,\ldots,N}\)</span> is drawn randomly from i.i.d.-<span class="math inline">\(P\)</span> random variables <span class="math inline">\((X^{(i)},Y^{(i)})_{i=1,\ldots,N}\)</span>, and thanks to factor <span class="math inline">\(\frac{1}{N-1}\)</span> as given in <span class="math inline">\(\eqref{eq_1}\)</span>, show that then <span class="math display">\[\begin{align}
E\widehat \sigma = E|X^{(i)}-EX^{(i)}|. 
  \end{align}\]</span>
</dd>
</dl>
<p>After standardization, also the variance of the activation output <span class="math inline">\(\alpha(\bar w\cdot \bar x^{(i)})\)</span> is reduced, and in turn, also the dependence of the range of <span class="math inline">\(\eta\)</span>’s that lead to a successful training.</p>
<p>The standardization is particularly useful for multi-dimensional training data whose feature components exhibit different scales (e.g., physical units). Consider, for example, the case of <span class="math inline">\((x^{(i)}_1,x^{(i)}_2)=x^{(i)}\in\mathbb R^2\)</span> and the empirical variance of the first component being a lot larger than the one of the second. At least in case of the logistic activation and cross-entropy (or linear activation and quadratic loss), the update rule reads <span class="math display">\[\begin{align}
  &amp; \bar w = 
  \begin{pmatrix}
    b\\
    w_1\\
    w_2
  \end{pmatrix}\\
  &amp; \leftarrowtail 
  \bar w^\text{new} 
  \begin{pmatrix}
    b^\text{new}\\
    w^\text{new}_1\\
    w^\text{new}_2
  \end{pmatrix}
  := 
  \begin{pmatrix}
    b\\
    w_1\\
    w_2
  \end{pmatrix}
   + \eta  
     \frac{1}{N}\sum_{i=1}^N
     \left(y^{(i)}-\alpha(\bar w\cdot \bar x^{(i)})\right)
     \begin{pmatrix}
       1\\
       x^{(i)}_1\\
       x^{(i)}_2
     \end{pmatrix}.
\end{align}\]</span> Hence, during the training and irrespectively of the choice of <span class="math inline">\(\eta\)</span>, tentatively larger updates in the <span class="math inline">\(w^\text{new}_1\)</span> component will be preferred over those in <span class="math inline">\(w^\text{new}_2\)</span>. This effect is substantially reduced by the above described standardization.</p>
<p>There is another reason why standardization is a good practice as it reduces the risk of the saturation of the model. The latter becomes particularly important when multi-class Adaline neurons are layered to form deeper networks, as we will discuss soon. However, standardization only attacks the problem of saturation from the perspective of the training data. Saturation can still occur if the initial model parameters are ill-conditioned. In this regard, we have a control to some extend by the means of the method described in the next section.</p>
<h3 id="random-initial-weights">Random initial weights</h3>
<p>As another mechanism to reduce the saturation (and potential initial bias) of the model is to randomize its initial model parameters. Note that the larger the dimension <span class="math inline">\(d\)</span> of the input features <span class="math inline">\((x^{(i)}_1,\ldots, x^{(i)}_d)=x^{(i)}\in\mathbb R^d\)</span> is, the more likely we might end up with a large activation input <span class="math display">\[\begin{align}
  w\cdot x^{(i)}+b = \sum_{j=1}^d w_i x^{(i)}_j + b.
\end{align}\]</span> Depending on the choice of activation function, a large activation input results either in a high activation output or a saturation of the model.</p>
<p>One way to work against such a behavior is to arrange for initial model parameters that are random and normal distributed, e.g., according <span class="math display">\[\begin{align}
  w_i \sim \mathcal N\left(\mu=0,\sigma=\frac{1}{\sqrt{d}}\right),
\end{align}\]</span> where the arguments <span class="math inline">\(\mu\)</span> and <span class="math inline">\(\sigma\)</span> denote the mean and standard deviation.</p>
<p>For simplicity, let us regard the case in which the feature vector components are i.i.d. with mean zero and variance one and the feature vectors and model parameters <span class="math inline">\(w=(w_1,\ldots, w_d)\)</span> are independent. Then, we would find <span class="math display">\[\begin{align}
   E w\cdot x = 0, \qquad E|w\cdot x|^2 = 1.
   \label{eq_2}
   \tag{E2}
\end{align}\]</span> Thus, on average (over the samples of model parameters and feature vectors) a high activation output or saturation can be avoided.</p>
<dl>
<dt>👀</dt>
<dd>Check the expectation an variance in line <span class="math inline">\(\eqref{eq_2}\)</span>.
</dd>
</dl>
<p>Of course, most sensible learning tasks will surely result in features vector components that are not i.i.d. However, by means of this simplified examples, we can at least see how standardization may also allow to reduce the risk of a saturating of the model.</p>
<p>The randomization of the initial weights becomes particularly important in layered multi-class Adaline neurons. Even beside the problem of saturation, it is required to allow different layers of the network to assume different “subtasks” of the learning problem as we will see later once we will discuss the multi-layer networks.</p>
<h3 id="regularization">Regularization</h3>
<p>As discussed, the initial weights are prescribe by conditioning the training cycle, e.g., by random initial values. But how can we keep some control on the updates of the model parameters during training and avoid potential disadvantageous ones?</p>
<p>Given a particular activation function, the nature of the update is driven by the choice empirical loss function. Hence, we may use it to encode further notions of how to conduct model parameter updates. This can be done by introducing further terms in the loss function that measure undesired behavior and add to the overall value of the loss function, hence, penalizing potentially undesired behavior.</p>
<p>A common choice is the so-called <span class="math inline">\(l_2\)</span>-regularization for which we replace the original empirical loss function <span class="math display">\[\begin{align}
  \widehat L(\bar w) = \frac 1N \sum_{i=1}^N L(y^{(i)},\alpha(\bar w\cdot x^{(i)})
\end{align}\]</span> by the regularized one <span class="math display">\[\begin{align}
  \widehat L_\text{reg}(\bar w) = 
  \widehat L(\bar w) + \frac{\lambda}{2d} \|w\|^2_2
\end{align}\]</span> where <span class="math inline">\(\|\cdot\|_2\)</span> denotes the <span class="math inline">\(l_2\)</span>, i.e., euclidean, norm of the model parameters <span class="math display">\[\begin{align}
  \|w\|_2=\sum_{i=1}^d w_i^2.
\end{align}\]</span></p>
<p>Minimizing with respect to <span class="math inline">\(\widehat L_\text{reg}\)</span> instead of <span class="math inline">\(\widehat L\)</span> now enforces a <span class="math inline">\(\lambda\)</span>-dependent trade-off between minimizing the empirical loss <span class="math inline">\(\widehat L(\bar w)\)</span> over the parameter choices <span class="math inline">\(\bar w\)</span> and minimizing the <span class="math inline">\(l_2\)</span>-norm of the model parameters <span class="math inline">\(w\)</span>. The corresponding update rule then reads: <span class="math display">\[\begin{align}
  b 
  &amp; \leftarrowtail 
  b^\text{new} 
  := 
  b - \eta \frac{\partial \widehat L(b,w)}{\partial b},
  \\
  w 
  &amp; \leftarrowtail 
  w^\text{new} 
  := 
  w - \eta 
  \left(
    \frac{\partial \widehat L(b,w)}{\partial w} + \frac{\lambda}{d} w
  \right)
  \\
  &amp; \phantom{\leftarrowtail w^\text{new} }
  =
  \left(1-\frac{\eta\lambda}{d}\right) w - \eta 
    \frac{\partial \widehat L(b,w)}{\partial w}.
\end{align}\]</span> The first summand on the right-hand side of the <span class="math inline">\(w\)</span>-update attempts to drive <span class="math inline">\(w\)</span> to zero and the second one, as we discussed already, attempts to drive <span class="math inline">\(w\)</span> towards a local minimum. The new ad hoc parameter <span class="math inline">\(\lambda\geq 0\)</span> now allows to adjust the trade-off between both update directions:</p>
<ul>
<li>Large <span class="math inline">\(\lambda\)</span> will prefer a smaller <span class="math inline">\(\|w\|_2\)</span> over minimizing the empirical loss function <span class="math inline">\(\widehat L(\bar w)\)</span>.</li>
<li>Smaller <span class="math inline">\(\lambda\)</span> will prefer the minimization of the empirical loss function <span class="math inline">\(\widehat L(\bar w)\)</span> over smaller <span class="math inline">\(\|w\|_2\)</span>.</li>
</ul>
<p>In summary, by means of the <span class="math inline">\(l_2\)</span>-regularization technique one can temper unexpectedly large increases of the model parameters <span class="math inline">\(w\)</span> during training.</p>
<dl>
<dt>Remark</dt>
<dd>In view of our discussion in the <a href="SS21-MsAML__02-S1__Error_decomposition.html">error decomposition module</a>, a regularization can also help to fight against the overfitting because, now, smaller <span class="math inline">\(w\)</span> are preferred, and thus, the richness of the hypotheses space of the algorithm is <span class="math inline">\(\lambda\)</span>-dependently is adaptively reduced. In a sense, the adjustment of <span class="math inline">\(\lambda\)</span> can be seen as a tuning of the trade-off between bias and variance.
</dd>
</dl>
<p>Of course, there are many other ways to introduce a regularization:</p>
<dl>
<dt>👀</dt>
<dd>Consider the following choice of so-called <span class="math inline">\(l_1\)</span>-regularization: <span class="math display">\[\begin{align}
\widehat L_\text{reg}(\bar w) = \widehat L(\bar w) + \frac{\lambda}{d}\|w\|_1
  \end{align}\]</span> for <span class="math display">\[\begin{align}
\|w\|_1 = \sum_{i=1}^d |w_i|.
  \end{align}\]</span> Note that, with this choice, the gradient is not defined everywhere. How can we nevertheless define a sensible update rule? What update behavior is encouraged by means of this regularization?
</dd>
</dl>
<p>In summary, a successful minimization of the empirical loss function <span class="math inline">\(\widehat L(\bar w)\)</span> will lead to a classifier that reproduces the labels of the prescribed training data well. However, a good performance on the training data does not mean that the learned classifier generalizes well to unseen data. Adding further terms to the empirical loss function, as the ones above, can adapt the richness of the potential candidates for hypotheses in a sensible way and help to enforce a selection of a classifier that may have certain generalization capabilities. A last model we will study before discussing multi-layered networks, the so-called <em>support vector machine</em>, exploits this degree of freedom very directly to implement a sensible notion of generalization.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
