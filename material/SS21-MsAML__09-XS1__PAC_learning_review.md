# Exercise 09-S1: PAC learning review [[PDF](SS21-MsAML__09-XS1__PAC_learning_review.pdf)]

a) Carry out the proof of the step $i)\to ii)$ in the fundamental theorem of binary classification explicitly using the results of the lectures and Exercise 06-S1.

b) Show further that the uniform convergence property $(ii)$ implies 

$$\lim_{N\to\infty}\sup_{h\in\mathcal{F}} E |\hat{R}_S(h)-R(h)| = 0,$$

which explains the name of the property. 