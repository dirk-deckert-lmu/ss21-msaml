<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__03-S1__A_simple_learning_setting</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-03-s1-pdf">MsAML-03-S1 [<a href="SS21-MsAML__03-S1__A_simple_learning_setting.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#a-simple-learning-setting">A first simple learning setting</a>
<ul>
<li><a href="#solutions">Solutions</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<p>Because we already have a several exercises concerning the Perceptron and Adaline, this module was written as part lecture and part exercise with most solutions included in the end.</p>
<h2 id="a-first-simple-learning-setting">A first simple learning setting</h2>
<p>In order to grasp the <em>learnability</em> more formally, let us regard a simple but non-trivial learning setting in the sense that it already features all important ingredients we will discuss in more general settings later on.</p>
<p>Suppose we have an algorithm <span class="math inline">\(\mathcal A\)</span> whose range is given by a finite space of hypotheses <span class="math inline">\(\mathcal F\)</span>. Let us assume that, given a training data sample <span class="math inline">\(s\)</span>, the algorithm always produces a hypothesis <span class="math inline">\(h_s=\mathcal A(s)\)</span>.</p>
<p>Furthermore, let us consider a setting such that <span class="math display">\[\begin{align}
  \exists h^*\in\mathcal F: \, P_{(X,Y)\sim P}(Y\neq h^*(X)) = 0,
\end{align}\]</span> a condition that is often called <em>realizability</em>. In other words, this means that there is a hypothesis <span class="math inline">\(h^*\)</span>, in principle reachable by the algorithm as <span class="math inline">\(h^*\in\mathcal F\)</span>, which correctly classifies all data points with probability <span class="math display">\[\begin{align}
  P_{(X,Y)\sim P}(Y=h^*(X)) = 1 - P_{(X,Y)\sim P}(Y\neq h^*(X)) = 1.
  \tag{P1}
  \label{eq_prob}
\end{align}\]</span></p>
<p>As the actual risk, e.g., given in terms of the error probability, <span class="math display">\[\begin{align}
  R(h^*)=E_{(X,Y)\sim P} 1_{Y\neq h(X)},
\end{align}\]</span> is not accessible without knowing <span class="math inline">\(P\)</span>, this is of course academic. Under our general assumption that the training data random variable <span class="math inline">\(S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}\)</span> is i.i.d.-<span class="math inline">\(P\)</span>, the corresponding empirical risk <span class="math display">\[\begin{align}
  \forall h\in\mathcal F: \quad \widehat R_S(h)=\frac1N\sum_{i=1}^N 1_{Y^{(i)}\neq h(X^{(i)})}
\end{align}\]</span> can however be seen to fulfill <span class="math display">\[\begin{align}
  \widehat R_S(h^*) = 0
\end{align}\]</span> with probability one (recall that it depends on random variable <span class="math inline">\(S\)</span>, and in turn, is a random variable), or equivalently <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left(\widehat R_S(h^*)=0\right)=1.
  \tag{P2}
  \label{eq_emp_risk}
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd><h5 id="task-1">Task 1</h5>
Prove <span class="math inline">\(\eqref{eq_emp_risk}\)</span> using the properties of i.i.d. random variables and assumption <span class="math inline">\(\eqref{eq_prob}\)</span>.
</dd>
</dl>
<p>Even though the last proposition <span class="math inline">\(\eqref{eq_emp_risk}\)</span> was about the more accessible empirical risk, the reachability of <span class="math inline">\(h^*\)</span> is still academic. Recall that <span class="math inline">\(\mathcal A\)</span> may only inspect the given realization <span class="math inline">\(s\)</span> of the random and i.i.d.-<span class="math inline">\(P\)</span> distributed training data <span class="math inline">\(S\)</span>. Hence, <span class="math inline">\(h^*\)</span> and <span class="math inline">\(h_s\)</span> may have little in common. Let us therefore assume further that <span class="math inline">\(\mathcal A\)</span> manages to complete the task of empirical risk minimization for any given training data sample <span class="math inline">\(s\)</span>, i.e., <span class="math display">\[\begin{align}
  h_s \in \underset{h\in\mathcal F}{\text{argmin}} \widehat R_s(h).
  \tag{A}
  \label{eq_argmin}
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd><h5 id="section"></h5>
Why is the right-hand side of <span class="math inline">\(\eqref{eq_argmin}\)</span> not empty?
</dd>
</dl>
<p>This assumption is sometimes referred to as <em>feasibility of the empirical risk minimization</em>. As a consequence, also <span class="math display">\[\begin{align}
  \widehat R_S(h_S)=0
\end{align}\]</span> holds with probability one, i.e., <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left(\widehat R_S(h_S)=0\right) = 1.
  \tag{P3}
  \label{eq_emp_h}
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd><h5 id="task-2">Task 2</h5>
Prove <span class="math inline">\(\eqref{eq_emp_h}\)</span> exploiting the existence of <span class="math inline">\(h^*\)</span> and <span class="math inline">\(\eqref{eq_argmin}\)</span>.
</dd>
</dl>
<p>Hence, we are in a advantageous setting, in which <span class="math inline">\(\mathcal A\)</span> succeeds to perform the empirical risk minimization with probability one. Next, it is natural to ask how well these found hypotheses <span class="math inline">\(h_S=\mathcal A(S)\)</span> generalize, or in other words, how their actual risk behaves. We should therefore regard the difference <span class="math display">\[\begin{align}
  R(h_S) - R(h^*),
\end{align}\]</span> which in our simple setting amounts to <span class="math inline">\(R(h_S)\)</span> as <span class="math inline">\(R(h^*)=0\)</span>; though, we will still keep the zero quantity <span class="math inline">\(R(h^*)\)</span> it in our notation as it helps the eye. Let us therefore analyze the probability <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)&gt;\epsilon\right)
  \tag{G1}
  \label{eq_gen}
\end{align}\]</span> for some <span class="math inline">\(\epsilon&gt;0\)</span>. This quantity is interesting as it formalizes how well the found hypotheses <span class="math inline">\(h_S\)</span> performs on unseen data. In fact, the above term is the probability of such hypotheses to generalize <span class="math inline">\(\epsilon\)</span>-badly. For our analysis, let us define the subset of hypotheses that, in our setting, can be considered <span class="math inline">\(\epsilon\)</span>-badly overfitted: <span class="math display">\[\begin{align}
  \mathcal F_\epsilon := \left\{h \in\mathcal F \,|\, R(h)&gt;\epsilon \right\}
\end{align}\]</span> Let us write the event inside the argument of the measure <span class="math inline">\(P\)</span> in <span class="math inline">\(\eqref{eq_gen}\)</span> as <span class="math display">\[\begin{align}
  \left\{S=s\,|\, R(h_s) &gt; \epsilon\right\},
\end{align}\]</span> using the notation <span class="math inline">\(S=s\)</span> to denote the realizations <span class="math inline">\(s\)</span> of the random variable <span class="math inline">\(S\)</span>. As <span class="math inline">\(h_s\)</span> is in the range of <span class="math inline">\(\mathcal A\)</span>, this event can only occur if there is a <span class="math inline">\(h\in \mathcal F_\epsilon \cap \text{range}\mathcal A\)</span>, i.e., <span class="math display">\[\begin{align}
  \left\{S=s\,\Big|\, R(h_s) &gt; \epsilon\right\}
  \subseteq
  \left\{S=s\,\Big|\, \exists h \in\mathcal F_\epsilon \wedge \widehat R_s(h) = 0\right\}.
\end{align}\]</span> This allows to find the bound <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)&gt;\epsilon\right)
  \leq 
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\widehat R_S(h)=0\right).
  \tag{G2}
  \label{eq_gen_2}
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd><h5 id="task-3">Task 3</h5>
Prove <span class="math inline">\(\eqref{eq_gen_2}\)</span> using the union bound.
</dd>
</dl>
<p>Of course, <span class="math inline">\(\eqref{eq_gen_2}\)</span> is only a coarse bound counting all the overfitted hypotheses. However, they are well-known to us in the sense that, whatever the unknown <span class="math inline">\(P\)</span> is, they fulfill <span class="math inline">\(R(h)&gt;0\)</span>. Now we have a chance to find a bound that is independent of the distribution <span class="math inline">\(P\)</span>, namely: <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)&gt;\epsilon\right)
  \leq 
  |\mathcal F| (1-\epsilon)^N.
  \tag{G3}
  \label{eq_gen_3}
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd><h5 id="task-4">Task 4</h5>
Prove <span class="math inline">\(\eqref{eq_gen_3}\)</span> by using the i.i.d.-<span class="math inline">\(P\)</span> propoerty of <span class="math inline">\(S\)</span>, the definition of <span class="math inline">\(\mathcal F_\epsilon\)</span>, and estimating the size <span class="math inline">\(|\mathcal  F_\epsilon|\)</span> even coarser by means of <span class="math inline">\(|\mathcal F|\)</span>.
</dd>
</dl>
<p>Morally, this bound informs us that the probability of a generalization error grows with the size of <span class="math inline">\(\mathcal F\)</span> while it decreases with the sample size <span class="math inline">\(N\)</span>. At least for this simple learning setting, our initial hope that we used to motivate the method of supervised learning is substantiated.</p>
<p>In a final step, let us rewrite <span class="math inline">\(\eqref{eq_gen_3}\)</span> to gain information on the magnitude of <span class="math inline">\(N\)</span>. For a given generalization error <span class="math inline">\(\epsilon\)</span> let us ask about the magnitude of <span class="math inline">\(N\)</span> to guarantee a given target probability of <span class="math inline">\(1-\delta\)</span>. Our bound <span class="math inline">\(\eqref{eq_gen_3}\)</span> implies:</p>
<ul>
<li><span class="math inline">\(\forall\delta &gt; 0\)</span></li>
<li><span class="math inline">\(\forall\epsilon &gt; 0\)</span></li>
<li><span class="math inline">\(\forall N &gt; \frac1\epsilon \log \frac{|\mathcal F|}{\delta}\)</span> <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left( R(h_S) - R(h^*) &lt; \epsilon\right) \geq 1 - \delta,
  \tag{G4}
  \label{eq_gen_4}
\end{align}\]</span> or in other words <span class="math display">\[\begin{align}
  R(h_S) - R(h^*)  &lt; \epsilon
\end{align}\]</span> at least with probability <span class="math inline">\(1-\delta\)</span>.</li>
</ul>
<dl>
<dt>👀</dt>
<dd><h5 id="task-5">Task 5</h5>
Prove <span class="math inline">\(\eqref{eq_gen_4}\)</span>.
</dd>
</dl>
<p>Even though, in actual implementations <span class="math inline">\(|\mathcal F|\)</span> is always finite due to the way computers are build, the size of the <span class="math inline">\(\mathcal F\)</span> can become exorbitantly large even for simple problems. The latter renders bounds as <span class="math inline">\(\eqref{eq_gen_3}\)</span> rather meaningless for applications since then, <span class="math inline">\(N\)</span> would have to be chosen extremely large as well. The latter is however not up to us as large samples of well-annotated training data are typically very costly to produce. In the next modules we will therefore go some distance to see how this simple learning setting can be generalized, in particular, how we can avoid a reliance on <span class="math inline">\(|\mathcal F|&lt;\infty\)</span>.</p>
<p>➢ Next session!</p>
<h3 id="solutions">Solutions</h3>
<p><strong>Task 1:</strong> Using the fact that <span class="math inline">\(S\)</span> is i.i.d.-<span class="math inline">\(P^N\)</span> we find <span class="math display">\[\begin{align}
  &amp; P_{S\sim P^N}\left(\widehat R_S(h^*)=0\right)
  \\
  &amp; = P_{S\sim P^N}\left(Y^{(1)}=h(X^{(1)})
                     \wedge Y^{(2)}=h(X^{(2)}) 
                     \wedge \ldots 
                     \wedge Y^{(N)}=h(X^{(N)})\right)
  \\
  &amp; = 
  \prod_{i=1}^N P_{S\sim P^N}(Y^{(i)}=h(X^{(i)}))
  \\
  &amp; =
  P_{(X,Y)\sim P}(Y=h(X))
  \\
  &amp; =
  1
\end{align}\]</span> by using independence, the identical distribution property, and <span class="math inline">\(\eqref{eq_prob}\)</span> in the last three steps, respectively.</p>
<p><strong>Task 2:</strong> <span class="math display">\[\begin{align}
  &amp;
  P_{S\sim P^N}\Big(\widehat R_S(h_S)=0\Big)
  \\
  &amp;=
  P_{S\sim P^N}\Big(\widehat R_S(\widetilde h)=0 
                     \text{ for } \widetilde h\in \underbrace{\underset{h\in\mathcal F}{\text{argmin}}\widehat R_S(h)}_{\ni h^*}
               \Big)
  \\
  &amp;=
  P_{S\sim P^N}\Big(\widehat R_S(h^*)=0\Big) \overset{\eqref{eq_emp_risk}}{=} 1
\end{align}\]</span></p>
<p><strong>Task 3:</strong> By the union bound we immediate get <span class="math display">\[\begin{align}
  &amp; P_{S\sim P^N}\left(R(h_S) - R(h^*)&gt;\epsilon\right)
  \\
  &amp;\leq
  P_{S\sim P^N}\left(\left\{S=s\,\Big|\, \exists h \in\mathcal F_\epsilon \wedge \widehat R(h_s) = 0\right\}\right)
  \\
  &amp;=
  P_{S\sim P^N}\left(\bigcup_{h \in\mathcal F_\epsilon}\left\{S=s\,\Big|\, \widehat R(h_s) = 0\right\}\right)
  \\
  &amp;\leq
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\left\{S=s\,\Big|\, \widehat R(h_s) = 0\right\}\right)
  \\
  &amp;=
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\widehat R(h_S) = 0\right).
\end{align}\]</span></p>
<p><strong>Task 4:</strong> Using <span class="math inline">\(\eqref{eq_gen_2}\)</span> we have <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)&gt;\epsilon\right)
  \leq 
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\widehat R_S(h)=0\right)
\end{align}\]</span> and we note <span class="math display">\[\begin{align}
  &amp; P_{S\sim P^N}\left(\widehat R_S(h)=0\right)
  \\
  &amp;= 
  P_{S\sim P^N}\left(\forall i=1,\ldots, N:\, Y^{(i)}=h(X^{(i)})\right).
\end{align}\]</span> Again, exploiting the i.i.d.-<span class="math inline">\(P\)</span> property, we find <span class="math display">\[\begin{align}
  &amp;P_{S\sim P^N}\left(R(h_S) - R(h^*)&gt;\epsilon\right)
  \\
  &amp;\leq 
  \sum_{h\in\mathcal F_\epsilon} 
  \prod_{i=1}^N 
  P_{(X^{(i)},Y^{(i)})\sim P}
  \left(Y^{(i)}=h(X^{(i)}) \right)
  \\
  &amp; = 
  \sum_{h\in\mathcal F_\epsilon} 
  \prod_{i=1}^N 
  P_{(X,Y)\sim P}
  \left(Y=h(X) \right).
\end{align}\]</span> Using the definition of <span class="math inline">\(\mathcal F_\epsilon\)</span> we find <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)&gt;\epsilon\right)
  &amp; \leq 
  \sum_{h\in\mathcal F_\epsilon} 
  \prod_{i=1}^N 
  (1-\epsilon)
  \\
  &amp; = |\mathcal F_\epsilon| (1-\epsilon)^N
  \\
  &amp; \leq |\mathcal F| (1-\epsilon)^N.
\end{align}\]</span></p>
<p><strong>Task 5:</strong> First, we observe <span class="math display">\[\begin{align}
  &amp; P_{S\sim P^N}\left( R(h_S) - R(h^*)\leq \epsilon\right) \geq 1-\delta
  \\
  &amp; \Leftrightarrow
  P_{S\sim P^N}\left( R(h_S) - R(h^*)&gt; \epsilon\right) \leq \delta.
\end{align}\]</span> But from <span class="math inline">\(\eqref{eq_gen_3}\)</span> we know already <span class="math display">\[\begin{align}
  P_{S\sim P^N}\left( R(h_S) - R(h^*) &gt; \epsilon\right) \leq |\mathcal F|(1-\epsilon)^N.
\end{align}\]</span> Hence, solving <span class="math display">\[\begin{align}
  |\mathcal F|\underbrace{(1-\epsilon)^N}_{\leq e^{-\epsilon N}} 
  \leq |\mathcal F|e^{-\epsilon N} 
  \leq \delta
\end{align}\]</span> for <span class="math inline">\(N\)</span> gives <span class="math display">\[\begin{align}
  N \geq \frac1\epsilon \log\frac{|\mathcal F|}{\delta}.
\end{align}\]</span></p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
