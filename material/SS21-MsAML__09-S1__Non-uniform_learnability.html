<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__09-S1__Non-uniform_learnability</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-09-s1-pdf">MsAML-09-S1 [<a href="SS21-MsAML__09-S1__Non-uniform_learnability.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#non-uniform-learnability">Non-uniform learnability</a>
<ul>
<li><a href="#structural-risk-minimization">Structural risk minimization</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="non-uniform-learnability">Non-uniform learnability</h2>
<p>As the <a href="SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.html">fundamental theorem of binary classification</a> made precise, the key ingredient that allowed to give PAC learning guarantees was the finite VC-dimension. Though this covers a lot of applications, there are interesting cases for which the finiteness of the VC-dimension does not hold. Clearly, in order to go beyond our previous results we will have to relax our definition of PAC learnability. And there is a sensible way to do so.</p>
<p>So far all of our results were uniform in the distribution of the training data as well as the hypotheses. As we naturally know little about the underlying distribution of the learning task, it makes sense to relax the uniformity on the relevant hypotheses in <span class="math inline">\(\mathcal F\subseteq \mathcal H\)</span> first.</p>
<p>Let us recall that, for PAC learnability, c.f., the <a href="SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.html">fundamental theorem of binary classification</a>, we required:</p>
<p><span class="math display">\[\begin{align}
  &amp; \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  &amp; \qquad \qquad 
  R(h_S) \leq R_{\mathcal F} + \epsilon \qquad \text{with probability of at least } 1-\delta.
\end{align}\]</span></p>
<p>One way of relaxing this condition is to allow hypothesis-dependent bounds on the sample size <span class="math inline">\(N\)</span>. In other words, we do not look for a uniform polynomial <span class="math inline">\(p\)</span> for the entire class of hypotheses <span class="math inline">\(\mathcal F\)</span> anymore but for a family of polynomials <span class="math inline">\((p_h)_{h\in\mathcal F}\)</span> indexed by hypotheses <span class="math inline">\(h\in\mathcal F\)</span> and try to bound the risk <span class="math inline">\(R(h_S)\)</span> by means of <span class="math inline">\(R(h)\)</span>.</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="non-uniform-pac-learnability">(Non-uniform PAC learnability)</h5>
<span class="math display">\[\begin{align}
  &amp; \exists \, \text{a family of polynomials $(p)_{h\in\mathcal F}$ and an
  algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, h \in\mathcal F:\\
  &amp; \qquad \forall \, N \geq p_h\left(\frac1\epsilon,\frac1\delta\right):\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \qquad 
  R(h_S) \leq R(h) + \epsilon \qquad \text{with probability of at least } 1-\delta.
\end{align}\]</span>
</dd>
</dl>
<p>Note that, by definition of <span class="math inline">\(R_{\mathcal F}\)</span>, PAC learnability implies <span class="math display">\[\begin{align}
  &amp; \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  &amp; \qquad \forall \, h \in\mathcal F:\\
  &amp; \qquad \qquad 
  R(h_S) \leq R(h) + \epsilon \qquad \text{with probability of at least } 1-\delta,
\end{align}\]</span> and hence, non-uniform PAC learnability.</p>
<p>So far the empirical risk minimization was our showcase algorithm for PAC learnability. It will be convenient to have such a showcase for non-uniform PAC learnability as well, which we shall introduce next.</p>
<h3 id="structural-risk-minimization">Structural risk minimization</h3>
<p>Besides in the algorithm <span class="math inline">\(\mathcal A\)</span> itself, prior knowledge about the learning task is encoded in the hypotheses space <span class="math inline">\(\mathcal H\)</span>. The latter can be made more explicit when introducing a preference of hypotheses. For this purpose, let us assume that <span class="math display">\[\begin{align}
  \mathcal H = \bigcup_{n\in\mathbb N} \mathcal H_n,
\end{align}\]</span> i.e., a countable decomposition of <span class="math inline">\(\mathcal H\)</span>, and in addition, a weight function <span class="math display">\[\begin{align}
  \omega_{\cdot}: \mathbb N \to \mathbb R^+_0
  \qquad
  \text{such that}
  \qquad
  \sum_{n\in\mathbb N} \omega_n \leq 1,
\end{align}\]</span> that specifies our preference for certain hypotheses classes <span class="math inline">\(\mathcal H_n\)</span>, <span class="math inline">\(n\in\mathbb N\)</span>, over others in this family.</p>
<p>Let us furthermore assume that, for all <span class="math inline">\(n\in\mathbb N\)</span>, the space <span class="math inline">\(\mathcal H_n\)</span> features the uniform convergence property, i.e., <span class="math display">\[\begin{align}
  &amp; \exists \, \text{polynomial } p_n \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \forall \, N \geq p_n\left(\frac1\epsilon,\frac1\delta\right)\\
  &amp; \qquad \forall \, h\in\mathcal H_n:\\
  &amp; \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon
  \qquad \text{with probability of at least } 1-\delta.
\end{align}\]</span></p>
<p>We may then define the smallest upper bound for given <span class="math inline">\(N\)</span> and <span class="math inline">\(\delta\)</span>: <span class="math display">\[\begin{align}
  \epsilon_n(N,\delta) := \inf\left\{
    \epsilon\in (0,1]
    \,\big|\,
    p_n\left(\frac1\epsilon,\frac1\delta\right)\leq N
  \right\}
\end{align}\]</span> so that, thanks to the uniform convergence property, <span class="math display">\[\begin{align}
  &amp; \qquad \forall \, n,N\in\mathbb N\\
  &amp; \qquad \forall \, \delta \in (0,1]\\
  &amp; \qquad \forall \, h\in\mathcal H_n:\\
  &amp; \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon_n(N,\delta) \\
  &amp; \qquad\qquad \quad \text{with probability of at least } 1-\delta,
\end{align}\]</span> holds true.</p>
<p>Replacing <span class="math inline">\(\delta \leftarrowtail \delta_n:=\delta w_n\)</span> yields <span class="math display">\[\begin{align}
  &amp; \qquad \forall \, n,N\in\mathbb N\\
  &amp; \qquad \forall \, \delta \in (0,1]\\
  &amp; \qquad \forall \, h\in\mathcal H_n:\\
  &amp; \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon_n(N,\delta \omega_n)
  \label{eq_1}
  \tag{E1}
  \\
  &amp; \qquad \qquad \quad \text{with probability of at least } 1-\sum_{n\in\mathbb N}\omega_n \leq 1 -\delta.
\end{align}\]</span></p>
<p>If, for <span class="math inline">\(h\in\mathcal H\)</span>, we pick <span class="math display">\[\begin{align}
  n(h) := \min\left\{
    n\in\mathbb N
    \,\big|\,
    h\in\mathcal H_n
  \right\},
\end{align}\]</span> i.e., the smallest index <span class="math inline">\(n\)</span> of hypotheses class <span class="math inline">\(\mathcal H_n\)</span> that contains <span class="math inline">\(h\)</span>, our bound in <span class="math inline">\(\eqref{eq_1}\)</span> implies <span class="math display">\[\begin{align}
  &amp; \qquad \forall \, N\in\mathbb N\\
  &amp; \qquad \forall \, \delta \in (0,1]\\
  &amp; \qquad \forall \, h\in\mathcal H:\\
  &amp; \qquad \qquad 
  \left|\widehat R_S(h)-R(h)\right| \leq \epsilon_{n(h)}(N,\delta \omega_{n(h)}) 
  \label{eq_2}
  \tag{E2}
  \\
  &amp; \qquad \qquad \quad \text{with probability of at least } 1 -\delta.
\end{align}\]</span></p>
<p>Contrary to empirical risk minimization, which only focusses on the minimality of <span class="math inline">\(\widehat R_S(h)\)</span>, we may use bound <span class="math inline">\(\eqref{eq_2}\)</span> to trade some of our bias towards “smallness of <span class="math inline">\(\widehat R_S(h)\)</span>” for “smallness of <span class="math inline">\(\epsilon_{n(h)}(N,\delta\omega_{n(h)})\)</span>”, in case this already results in an overall smaller error estimate in the bound of <span class="math inline">\(R(h)\)</span>. This motivates the so-called <em>structural risk minimization</em> algorithm <span class="math inline">\(\mathcal A^\text{SRM}\)</span>, which attempts to find a hypothesis <span class="math inline">\(h^\text{SRM}_S = \mathcal A^\text{SRM}(S)\)</span> such that <span class="math display">\[\begin{align}
  h^\text{SRM}_S \in \underset{h\in\mathcal H}{\operatorname{argmin}}
  \left\{\widehat R_S(h) + \epsilon_{n(h)}(N,\delta \omega_{n(h)})\right\}.
\end{align}\]</span></p>
<p>In practice, one usually arranges the hypotheses classes such that <span class="math display">\[\begin{align}
  \mathcal H_n \subseteq \mathcal H_{n+1},
  \qquad n\in\mathbb N.
\end{align}\]</span> A good example is again the least square fitting algorithm of a polynomial of degree <span class="math inline">\(n\)</span> to sample points of the graph of an unknown function. In such settings, we may conduct our search iteratively and, at step <span class="math inline">\(n\in\mathbb N\)</span>, though empirical risk minimization restrict to the hypotheses in <span class="math inline">\(\mathcal H_n\setminus \mathcal H_{n-1}\)</span> that yields <span class="math display">\[\begin{align}
  h_S^{\text{ERM},n} \in 
  \underset{h\in\mathcal H_n\setminus\mathcal H_{n-1}}{\operatorname{argmin}}
  \widehat R_S(h)
\end{align}\]</span> monitor the growth of the two competing terms <span class="math inline">\(\widehat R_S(h^{\text{ERM},n})\)</span> and <span class="math inline">\(\epsilon_{n}(N,\delta\omega_{n})\)</span> in order to select <span class="math inline">\(h^\text{SRM}_S\)</span>.</p>
<p>This algorithm allows to give the following learning grantee.</p>
<dl>
<dt>Lemma</dt>
<dd><h5 id="first-non-uniform-learning-guarantee">(First non-uniform learning guarantee)</h5>
Let <span class="math inline">\((\mathcal H_n)_{n\in\mathbb N}\)</span> be a family of hypotheses spaces <span class="math inline">\(\mathcal H_n\)</span> fulfilling the uniform convergence property and weights <span class="math inline">\(\omega_n:=\frac{6}{(\pi n)^2}\)</span>, <span class="math inline">\(n\in\mathbb N\)</span>. Then, <span class="math display">\[\begin{align}
\mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n
  \end{align}\]</span> is non-uniformly learnable by means of the structural risk minimization algorithm <span class="math inline">\(\mathcal A^\text{SRM}\)</span> with a sample bound for <span class="math inline">\(h\in\mathcal H\)</span> of: <span class="math display">\[\begin{align}
p_h\left(\frac1\epsilon,\frac1\delta\right)
\leq
p_{n(h)}\left(\frac2\epsilon,\frac{(\pi n(h))^2}{3\delta}\right).
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> Let <span class="math inline">\(h\in\mathcal H\)</span>, <span class="math inline">\(\epsilon,\delta\in (0,1]\)</span>, <span class="math inline">\(N\in\mathbb N\)</span>, so that <span class="math display">\[\begin{align}
  R(h) \leq \widehat R_S(h) + \epsilon_{n(h)}\left(N,\delta\omega_{n(h)}\right)
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\sum_{n\in\mathbb N}\frac{6}{(\pi n)^2}\delta=1-\delta\)</span>.</p>
<p>Choosing <span class="math display">\[\begin{align}
  N 
  &gt; 
  p_{n(h)}\left(
    \frac{1}{\frac12\epsilon},
    \frac{1}{\frac12\delta \omega_{n(h)}},
  \right) 
  =
  p_{n(h)}\left(
    \frac{2}{\epsilon},
    \frac{(\pi n(h))^2}{3\delta},
  \right) 
  =: 
  p_h\left(\frac1\epsilon,\frac1\delta\right)
\end{align}\]</span> we get for <span class="math inline">\(h^\text{SRM}_S=\mathcal A^\text{SRM}(S)\)</span> that <span class="math display">\[\begin{align}
  R\left(
  h^\text{SRM}_S
  \right)
  &amp;\leq
  \inf_{\tilde h\in\mathcal H}
  \left[
    \widehat R_S(\tilde h)
    +
    \epsilon_{n(\tilde h)}(N,\delta\omega_{n(\tilde h)})
  \right]
  \\
  &amp;\leq
  \widehat R_S(h)
    +
    \epsilon_{n(h)}(N,\delta\omega_{n(h)})
  \\
  &amp;\leq
  \widehat R_S(h)
    +
  \frac\epsilon2
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\frac\delta2\)</span>.</p>
<p>Furthermore, for <span class="math inline">\(N&gt;p_h\left(\frac1\epsilon,\frac1\delta\right)\)</span>, by the uniform convergence property of <span class="math inline">\(\mathcal H_n\)</span>: <span class="math display">\[\begin{align}
  P_S\left(
    \left|
      \widehat R_S(h)-R(h)
    \right|
    &gt; \frac\epsilon2
  \right)
  &lt;
  \frac\delta2.
\end{align}\]</span></p>
<p>In turn, we can employ the union bound to infer <span class="math display">\[\begin{align}
  P_S\left(
    R\left(
    h^\text{SRM}_S
    \right)
    &gt;
    \widehat R_S(h)
      +
    \frac\epsilon2
    \,
    \vee
    \,
    \left|
      \widehat R_S(h)-R(h)
    \right|
    &gt; \frac\epsilon2
  \right)
  &lt;
  \delta,
\end{align}\]</span> or equivalently <span class="math display">\[\begin{align}
  P_S\left(
    R\left(
    h^\text{SRM}_S
    \right)
    \leq
    \widehat R_S(h)
      +
    \frac\epsilon2
    \,
    \wedge
    \,
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \leq \frac\epsilon2
  \right)
  \geq
  1-\delta.
\end{align}\]</span> This implies <span class="math display">\[\begin{align}
    R\left(
      h^\text{SRM}_S
    \right)
    \leq R(h)+\epsilon
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\delta\)</span>. As a last step, we observe that the last bound was independent of our choices in <span class="math inline">\(h\in\mathcal H\)</span>, <span class="math inline">\(\epsilon,\delta\in(0,1]\)</span> and distribution of <span class="math inline">\(P\)</span>, which concludes our proof.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>To summarize, we have proven that every countable family of hypotheses spaces <span class="math inline">\((\mathcal H_n)_{n\in\mathbb N}\)</span> such that each <span class="math inline">\(\mathcal H_n\)</span> has the uniform convergence property which gives rise to <span class="math inline">\(\mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n\)</span> ensures that <span class="math inline">\(\mathcal H\)</span> is non-uniformly PAC learnable.</p>
<p>With the help of the fundamental theorem of binary classification, we can reformulate this statement into: <span class="math inline">\(\mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n\)</span> with <span class="math inline">\(\forall n\in\mathbb N: d_n:=\dim_\text{VC}\mathcal H_n&lt;\infty\)</span> is non-uniformly PAC learnable.</p>
<p>Even the converse holds, so that we again gain a precise characterization of non-uniform PAC learnability that we could add to our fundamental theorem of binary classification:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="characterization-of-non-uniform-learnability">(Characterization of non-uniform learnability)</h5>
A hypotheses class <span class="math inline">\(\mathcal H\)</span> is non-uniformly PAC learnable if and only if there is a family <span class="math inline">\((\mathcal H_n)_{n\in\mathbb N}\)</span> of PAC learnable <span class="math inline">\(\mathcal  H_n\)</span> such that <span class="math inline">\(\mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n\)</span>.
</dd>
</dl>
<p><strong>Proof:</strong> “<span class="math inline">\(\Leftarrow\)</span>” is an application of the fundamental theorem of binary classification.</p>
<p>“<span class="math inline">\(\Rightarrow\)</span>”: If <span class="math inline">\(\mathcal H\)</span> is non-uniformly learnable, define <span class="math display">\[\begin{align}
  \mathcal H_n:=\left\{
    h\in\mathcal H
    \,\big|\,
    p_h\left(\frac{1}{\frac14},\frac{1}{\frac14}\right)
    \leq n
  \right\}
\end{align}\]</span> which implies <span class="math inline">\(\mathcal H=\bigcup_{n\in\mathbb N}\mathcal H_n\)</span>.</p>
<p>For <span class="math inline">\(n\in\mathbb N\)</span> and <span class="math inline">\(\epsilon=\frac14=\delta\)</span>, we then have for all <span class="math inline">\(N&gt;n\)</span>: <span class="math display">\[\begin{align}
  \left|
    R\left(h^\text{SRM}_S\right)
    -\inf_{h\in\mathcal H_n} R(h)
  \right|
  &gt; \frac 14
\end{align}\]</span> with probability less than <span class="math inline">\(\frac14\)</span>.</p>
<p>Suppose we had <span class="math inline">\(\dim_\text{VC}\mathcal H_n=\infty\)</span>. We may then conduct the same argument as in the proof (S3) <span class="math inline">\(\Rightarrow\)</span> (S1) of the <a href="SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.html">fundamental theorem of binary classification</a> to arrive at the contradiction <span class="math inline">\(\dim_\text{VC}\mathcal H_n&lt;\infty\)</span>, and hence, <span class="math inline">\(\mathcal H_n\)</span> is PAC learnable.</p>
<p><span class="math inline">\(\square\)</span></p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
