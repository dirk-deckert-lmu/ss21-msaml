# Exercise 02-XS1: Linear regression model [[PDF](SS21-MsAML__02-XS1__Linear_regression_model.pdf)]

Assume the following data model:

$$x\in \mathbb{R}^d, \ x \sim g(\cdot),$$ 
$$y = f(x) + \epsilon \in \mathbb{R}, \ \epsilon \sim \mathcal{N}(0,\sigma_\epsilon^2)$$
where $g(\cdot)$ is the density function of input, $f(\cdot)$ is the regression function and $\epsilon_i$ is random noise. 

Assume that we have a dataset of $N$ points sampled i.i.d from this model:

$$(x_i,y_i) - \text{i.i.d}, i \in \bar{1,N},$$

$$X = [x_1^T, \dots ,x_N^T]^T\in \mathbb{R}^{N\times d}, Y = [y_1,\dots, y_N]^T\in \mathbb{R}^N,$$
where $X$ is the features matrix and $Y$ is the vector of outputs.

**(a)** Find an analytical expression for the solution of linear regression with mean-squared error, i.e. parameters $\hat{\beta} \in \mathbb{R}^d$, such that

$$\hat{\beta} = argmin_\beta\|X \beta- Y\|^2_2$$

**(b)** Consider an estimator of the following structure:

$$\forall x_0 \in \mathbb{R}^d, \ \hat{f}(x_0) = \sum_{i=1}^N \ell_i(x_0,X)y_i,$$
where weights $\ell_i(x_0,X)$ do not depend on $Y$ but can depend on the whole dataset $X$ and input $x_0$.

Show that linear regression with squared loss is a member of this class of estimators. Describe the weights $\ell_i(x_0,X)$ explicitly.

**(c)** Decompose the *conditional* mean-squared error 

$$E_{Y|X}\bigl[\bigl(f(x_0)-\hat{f}(x_0)\bigr)^2\bigr]$$
into *conditional* squared bias and variance components. (Here $E_{Y|X}$ denotes the expectation when features matrix $X$ is given, i.e. $X$ is a deterministic variable.)

**(d)** Decompose the *unconditional* mean-squared error

$$E_{Y,X}\bigl[\bigl(f(x_0)-\hat{f}(x_0)\bigr)^2\bigr]$$
into squared bias and variance components. (Now $X$ is a random variable.) 

**(e)** Can we find explicit expressions (i.e. calculate the expectations) for squared biases and variances in the two cases above? In case, provide the expressions. 
