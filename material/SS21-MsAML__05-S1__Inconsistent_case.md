# MsAML-05-S1 [[PDF](SS21-MsAML__05-1__Inconsistent_case.pdf)]

1. [Inconsistent case](#inconsistent-case)

Back to [index](index.md).


## Inconsistent case

For more general cases, we may not hold on to our consistency assumption
anymore, i.e., that for any concept $c$ and training data $s$ the algorithm
$\mathcal A$ returns a consistent hypothesis $h_s$, i.e., one fulfilling
$\widehat R_s(h_s)=0$.

One may rightfully expect that a failure of consistency will even typically be
the case. First, before we can utilize the computer as optimizer we need to
discretized the data which may lead to discrepancies. Next, the range of the
algorithm $\mathcal A$ may only approximate a concept but not contain it.
Furthermore, the optimization task carried out by the algorithm $\mathcal A$
might be rather complex and the particulars of the algorithm $\mathcal A$ may 
not guarantee a successful risk minimization; see our Adaline update rule for
an example.

In turn, the actual risk $R(h_S)$ may even be bounded away from zero due to the constraints above. Nevertheless, an inconsistent hypothesis having a small number of classification errors can still be useful, and under certain assumptions we may still prove learning guarantees that allow to estimate the difference between actual and empirical risk. Here is a first example in the setting of last module but without requiring consistency:

Theorem
: ##### (General case for finite hypothesis spaces)
  Let $\mathcal H$ be any finite hypothesis set. Then, for any $\delta>0$, the
  following inequality holds true with probability of a t least $1-\delta$:
  \begin{align}
    \forall h\in\mathcal H:
    \left|\widehat R_S(h)-R(h)\right|
    < 
    \sqrt{
      \frac{
        \log\frac{2|\mathcal H|}{\delta}}
      {2N}
    }.
  \end{align}

**Proof:** Since $\mathcal H$ is finite, we may label the hypotheses, for example like so $\mathcal H=\{h_1, h_2, h_3,\ldots,h_M\}$ such that $|\mathcal H|=M$.

For an $\epsilon>0$, let us regard the expression
\begin{align}
  &P_{S\sim P^N}\left(
    \exists h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \geq
    \epsilon
  \right)
  \\
  &= P_{S\sim P^N}
  \left(
    \bigcup_{j=1}^M
    \left\{
      S=s \,\big|\,
      \left|
        \widehat R_s(h_j)-R(h_j)
      \right|
      \geq \epsilon
    \right\}
  \right)
  \\
  &\leq
  \sum_{j=1}^M 
  P_{S\sim P^N}
  \left(
    \left|
      \widehat R_S(h_j)-R(h_j)
    \right|
    \geq \epsilon
  \right),
\end{align}
where we simply have rewrote the definition of the event in the argument of the
measure in the first line and then exploited the union bound. 

Now we recall the tool we have prepared in the last module about [concentration
inequalities](SS21-MsAML__04-S2__Concentration_inequalities.md), i.e.,
Hoeffding's inequality:

Theorem
: ##### (Hoeffding's inequality)
  Let $X_1,\ldots,X_N$ be independent random variables with
  $\text{range}X_i\subseteq [a_i,b_i]$ for reals $a_i<b_i$, $i=1,\ldots, N$.
  Then, for the empirical expectation $\widehat S_N=\frac1N\sum_{i=1}^N X_i$
  and any $\epsilon>0$ we find:
  \begin{align}
    P\left(\left| \widehat S_N- E\widehat S_N\right| \geq \epsilon\right) 
    \leq 
    2\exp\left(-\frac{2\epsilon^2N^2}{\sum_{i=1}^N(b_i-a_i)^2}\right).
  \end{align}

Setting $X_i\equiv 1_{Y^{(i)}\neq h(X^{(i)})}$, observing that the
corresponding range fulfills $\text{range}X_i\subseteq [0,1]$, and recalling
\begin{align}
  R(h)=E_{S\sim P^N}\widehat R_S(h),
\end{align}
we find the estimate
\begin{align}
  P_{S\sim P^N}
  \left(
    \left|
      \widehat R_S(h_j)-R(h_j)
    \right|
    \geq \epsilon
  \right)
  \leq 2e^{-2\epsilon^2 N}.
\end{align}

Hence, we may conclude
\begin{align}
  &P_{S\sim P^N}\left(
    \exists h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \geq
    \epsilon
  \right)
  \leq 2 |\mathcal H| e^{-2\epsilon^2 N}.
\end{align}

Setting the right-hand side to equal $\delta$ gives
\begin{align}
  2|\mathcal H|e^{-2\epsilon^2N} &= \delta \\
  \log2|\mathcal H|-2\epsilon^2N &= \log\delta,
\end{align}
from which we read off that
\begin{align}
  \epsilon = \sqrt{
    \frac{\log\frac{2|\mathcal H|}{\delta}}
    {2N}
  }.
\end{align}
This allows to conclude
\begin{align}
  &P_{S\sim P^N}\left(
    \exists h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \geq
    \sqrt{
      \frac{\log\frac{2|\mathcal H|}{\delta}}
      {2N}
    }
  \right)
  \leq \delta
\end{align}
or equivalently
\begin{align}
  &P_{S\sim P^N}\left(
    \forall h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    <
    \sqrt{
      \frac{\log\frac{2|\mathcal H|}{\delta}}
      {2N}
    }
  \right)
  \geq 1- \delta,
\end{align}
and in other words,
\begin{align}
  \forall h\in\mathcal H:
  \left|\widehat R_S(h)-R(h)\right|
  < 
  \sqrt{
    \frac{
      \log\frac{2|\mathcal H|}{\delta}}
    {2N}
  }
\end{align}
with probability of at least $1-\delta$.

$\square$

Recall that, in principle, we are still in the deterministic case discussed in
module [PAC learning framework](SS21-MsAML__04-S1__PAC_learning_framework.md)
so that $P$ is actually the product of a distribution $D$ on $\mathcal X$ and
the one corresponding to a Dirac-delta measure $\delta_{y=c(x)}$. This fact was
not used in the above proof and the result will also hold in the more general
case of $P$ having a distribution on $\mathcal X\times\mathcal Y$, the latter
of which are useful to model stochastic relations between the feature vectors
and labels.

Compare this bound to the generalization bound we found for the consistent
case. Here, as in the consistent case, we still observe the relevant ratio
\begin{align}
  \frac{\log\frac{|\mathcal H|}{\delta}}{N}.
\end{align}
However, this time under a square root. But this is not such a big price to
pay, as for fixed $\mathcal H$ and $\delta$, it means we need an only
quadratically larger training data set to achieve the same performance.

Observe further that there is again a trade-off involved in reducing the risk:

* A richer (and then potentially larger) hypothesis set may help to reduce the
  empirical risk as the algorithm can better approximate the concept underlying
  the learning task.
* But, for a similar empirical error, one should try to keep $\mathcal H$ as
  simple (and this means small) as possible.

In order to focus on the performance of the algorithm in perspective of its
capabilities it makes sense to slightly adapt our PAC learning definition.
Instead of comparing to zero risk, let us compare the risk to the best in class
risk considering only hypotheses in $\mathcal F$, e.g., in the range of the
algorithm $\mathcal A$.

Definition
: ##### (PAC-learnable in the inconsistent case)
  Let $\mathcal F$ be a relevant set of hypotheses and
  \begin{align}
    R_{\mathcal F} := \inf_{h\in\mathcal F}R(h).
  \end{align}
  A concept class $\mathcal C$ is said to be *$\mathcal
  F$-agnostic PAC-learnable*, if there exists an algorithm $\mathcal A$ and a
  polynomial $p$ such that
  \begin{align}
    \forall \quad &\epsilon, \delta\in (0,1]\\
    \forall \quad &\text{distributions $D$ on $\mathcal X$}\\
    \forall \quad &\text{concepts $c$ in $\mathcal C$}\\
    \forall \quad &N\geq p\left(\frac1\epsilon,\frac1\delta\right):\\
    &\\
    & P_{S\sim P^N}
    \left(
      R(h_S) - R_{\mathcal F}\leq \epsilon
    \right) \geq 1-\delta.
  \end{align}
  Furthermore, if $\mathcal A$ runs in $p(\frac1\epsilon,\frac1\delta)$
  polynomial time, then $\mathcal C$ is called *efficiently* $\mathcal
  F$-agnostic PAC-learnable. 

Note that, the larger $R_{\mathcal F}$, e.g., due to a too small or
ill-conditioned choice of $\mathcal F$, the weaker is the notion of $\mathcal
F$-agnostic PAC learnability and the less useful it is. We will briefly look at
two extreme examples in the next module. The learnability assertion is not
absolute but relative to the best in class performance $R_{\mathcal F}$ that
may be achieved by the algorithm under consideration.

👀
: Exploit the bounds of our [error
  decomposition](SS21-MsAML__02-S1__Error_decomposition.md) discussion together
  with the above theorem to formulate and show a first result for this notion of
  $\mathcal F$-agnostic PAC learnability: As long as $\mathcal F$ is finite and
  our algorithm is always successful in carrying out the empirical risk
  minimization on $\mathcal F$, then any concept class $\mathcal C\subseteq
  \mathcal F$ is efficiently $\mathcal F$-agnostic PAC learnable.

We have already mentioned that the theorem above does not depend on the
assumed property that, in the deterministic case, the distribution of $P$ was
given by a product of a distribution $D$ on $\mathcal X$ and the one
corresponding to a Dirac-delta measure $\delta_{y=c(x)}$. Hence, it is not be
much trouble to generalize PAC-learnability and state a similar first result
for general distributions of $P$, including also a stochastic dependence
between the feature vectors and labels. This we shall do next.
