% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-04-1-pdf}{%
\section{\texorpdfstring{MsAML-04-1
{[}\href{SS21-MsAML__04-1__Perceptron-Adaline_comparison.pdf}{PDF}{]}}{MsAML-04-1 {[}PDF{]}}}\label{msaml-04-1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{perceptron-adaline-comparison}{Perceptron-Adaline
  comparison}

  \begin{itemize}
  \tightlist
  \item
    \protect\hyperlink{onlineux2c-batchux2c-and-mini-batch-learning}{Online,
    batch, and mini-batch learning}
  \end{itemize}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{perceptron-adaline-comparison}{%
\subsection{Perceptron-Adaline
comparison}\label{perceptron-adaline-comparison}}

Our main motivation in replacing the Perceptron by the Adaline algorithm
were:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  For linearly separable training data, the Perceptron stops its updates
  right after the last incorrect classification is resolved by the last
  parameter update. Whether this last parameter update is in any sense
  optimal with respect to the generalization performance is not
  addressed by the algorithm, and there is no mechanism to even specify
  a notion for the possible generalization.
\item
  For ``almost'' linearly separable training data, the Perceptron jumps
  back an forth in its parameter updates depending on the found
  classification errors instead of nevertheless attempting to find an
  optimal hyperplane.
\end{enumerate}

In turn, the Adaline neuron allows to specify a notion of generalization
by means of the minima of the respective empirical loss \begin{align}
  \widehat L(b,w) = \frac1N \sum_{i=1}^N L(y^{(i)},h_{(b,w)}(x^{(i)}))
\end{align} for given loss function \(L(b,w)\) and activation output
\(h_{(b,w)}\) parametrized by \(w,b\).

\textbf{Ad 1. above:} Even in case the gradient descent algorithm of the
Adaline managed to find parameters \(w,b\) that imply a zero total
amount of classification errors, the empirical loss may not yet be close
to a local minimum. And therefore, a continued gradient descent may
bring the parameters \(w,b\) even closer to this empirical minimum.

For the choice of a square error loss one may encounter the following
scenario:

\begin{figure}
\centering
\includegraphics{04/Perceptron_Adaline_stops.png}
\caption{Perceptron-Adaline updates.}
\end{figure}

If the notion of generalization encoded in the loss function resembles
our learning tasks better the Adaline has a good chance to generalize
better. This is illustrated in the following figure, in which the
crosses denote the training data and the dots the unseen test data.

\begin{figure}
\centering
\includegraphics{04/Adaline_generalization.png}
\caption{Potential generalization performance of the Adaline.}
\end{figure}

Though both hyperplanes have no classification errors, the one on the
right-hand side seems to generalizes better -- at least judged by the
``unseen'' test data. Here it is important, not to be fooled by any
apparent symmetry as depicted in the figures as it may have been due to
the unfairly chosen batch of the training data.

\textbf{Ad 2. above:} Also in the case of training data that is not
linearly separable but there are hyperplanes that allow a classification
with only few errors, the Adaline has a good chance finding a compromise
between maximizing the about of correctly classified training data
points and minimizing the misclassifications. Again this strongly
depends on whether the notion of generalization supplied by the loss
functions resembles the learning task to some a sufficient extend.

The desirable generalization performance depends on several ingredients:

\begin{itemize}
\tightlist
\item
  The Adaline attempts to minimize the empirical loss
  \(\widehat L(b,w)\) and not the total number of classification errors.
  Therefore, \(L\) should be chosen in a way that the minimization of
  \(\widehat L(b,w)\) also enforces the minimization of the total number
  of classification errors. At best, it should encode as much a priori
  knowledge that we have at hand about the learning task.
\item
  Furthermore, \(L\) must be chosen sufficiently regular to allow for
  the gradient descent method to work. But even then we are not
  guaranteed to end up in a global minimum as the following discussion
  shows.
\end{itemize}

\textbf{Scenario 1:} For small learning rates \(\eta\) and model
parameters \(w,b\) close to a local minimum of the empirical loss, the
gradient descent might get stuck at a local minimum instead of finding a
potentially global one:

\begin{figure}
\centering
\includegraphics{04/Caught_in_local_min.png}
\caption{Gradient descent getting caught in local minimum due too a
small learning rate \(\eta\).}
\end{figure}

Even if we do not get stuck at a local minimum before descending to a
global one, a too small choice of \(\eta\) may require a large amount of
training epochs (iterations in the gradient descent algorithm).

\textbf{Scenario 2:} In turn, a too large learning rate \(\eta\) may
easily cause the gradient descent algorithm to overshoot even a global
minimum and potentially lead to an increases empirical loss as
illustrated here:

\begin{figure}
\centering
\includegraphics{04/Overshooting_a_minimum.png}
\caption{Overshooting a minimum due to a too large learning rate.}
\end{figure}

Both scenarios are the price for introducing an ad hoc constant learning
rate. As it could already be seen from the exercises, the success of the
gradient descent depends on the sign and magnitude of the Taylor
remainder beyond the linear correction of the empirical loss, which can
be estimated by the corresponding Hessian. Therefore, more sophisticated
algorithms typically rely on the second-order derivatives; e.g., most
prominently the Newton's method. Computing the second-order derivative
is of course not a problem for our single Adaline application. However,
later on, when considering networks of Adaline neuron, the computation
of second-order derivatives of the loss function becomes computationally
very costly.

Therefore, despite the discussed problems, gradient descent is the
method of choice for training larger networks of Adaline neurons and the
fine-tuning of learning rates \(\eta\) and loss functions \(L\) that
work together well will often require a lot of finesse and patience.

Furthermore, there are several improvements that can be made when
allowing an adaptive learning rate during the gradient descent
optimization. For example, in the \(t\)-th learning epoch we might
adjust the learning rate according to \begin{align}
  \eta = \frac{a}{b+t},\qquad a,b>0.
\end{align} This implies that \(\eta(t)\) is larger for early epochs
\(t\) and smaller for later ones -- then, it ultimately enforce a
convergence, though, not necessarily at a local minimum.

More sophisticated methods, e.g., implement a Newtonian dynamics for a
fictional body moving in parameter space of \((b,w)\), interpreting
\(\widehat L(b,w)\) as a ``potential'' that exerts a force proportional
to \(-\nabla_{(b,w)}\widehat L(b,w)\) on that body, and in addition,
introducing a friction that damps the body's velocity continuously. The
inherent inertia of the body then enables it to ``roll'' out of local
minima unless they are sufficiently deep for the friction to take over.
Others implement, in addition to the gradient descent, a random motion
with decreasing temperature. The latter allows to randomly ``jump'' out
of local minima unless they are sufficiently deep so that the remaining
temperature does not allow for a sufficiently large jump anymore in
order to escape.

You can find a great overview of some of these methods here:
\url{http://sebastianruder.com/optimizing-gradient-descent/}

In summary, while the success of a supervised training will always
depend on some finesse in the fine-tuning of the additional parameters
of our optimization algorithm of choice for minimizing the empirical
loss, a advantageous choice of loss function will require some a priori
knowledge that cannot be extracted ``for free'' from simply looking at
the known training alone. This fact will become soberingly clear in our
next module about the ``No free lunch'' theorems.

\hypertarget{online-batch-and-mini-batch-learning}{%
\subsubsection{Online, batch, and mini-batch
learning}\label{online-batch-and-mini-batch-learning}}

There is another noteworthy difference in the update rules of the
Perceptron and the Adaline neurons; at least in the way we have
introduced them.

\begin{itemize}
\tightlist
\item
  The Perceptron update rule is an \emph{online learner} as the model
  parameter updates occur right after inspection of a single training
  data points.
\item
  The Adaline update rule is a \emph{batch learner} as it first
  considers the whole batch of training data by computing the gradient
  of the empirical loss \(\widehat L(b,w)\) before then updating the
  model parameters.
\end{itemize}

While online learner typically introduce a stronger dependence on the
sequence of training data, batch learner are more computationally
expensive.

Clearly, also the Perceptron can be turned into a batch learner by
conducting only one average parameter update per epoch, i.e., after the
inspection of the entire batch of training data. Likewise, we may turn
our Adaline implementation into an online learner by performing for each
inspected training data point \((x^{(i)},y^{(i)})\) an immediate
parameter update according to the loss function \(L\), i.e.,
\begin{align}
  w & \leftarrowtail w^\text{new} := w - \eta \frac{\partial L(y^{(i)},h_{(b,w)}(x^{(i)}))}{\partial w}\\
  b & \leftarrowtail b^\text{new} := b - \eta \frac{\partial L(y^{(i)},h_{(b,w)}(x^{(i)}))}{\partial b},
\end{align} instead of the using the empirical loss \(\widehat L(b,w)\)
of entire batch.

A compromise between these two extremes are the so-called
\emph{mini-batch} learners, which, per learning epoch, divide their
training data \(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\) into disjoint
mini-batches \(s_k\), \(k=1,\ldots,M\), each containing a similar amount
of training data samples and execute the update rules after inspection
of each such mini-batch \(s_k\). In order to reduce the dependence on
the sequence, this division can be done randomly. This is also today's
most common method of training also larger Adaline networks. When the
gradient descent algorithm is used in combination of such a randomized
mini-batch approach, it is often referred to as \emph{stochastic
gradient descent}.

\end{document}
