<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__05-1__Linear_classification_models</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-05-1-pdf">MsAML-05-1 [<a href="SS21-MsAML__05-1__Linear_classification_models.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#models-of-linear-classification">Models of linear classification</a>
<ul>
<li><a href="#activation-functions">Activation functions</a></li>
<li><a href="#loss-functions">Loss functions</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<p>In order, to give you enough time to explore the corresponding changes in your implementation of the Adaline neuron, this time the lecture notes are rather short.</p>
<h2 id="models-of-linear-classification">Models of linear classification</h2>
<p>In stepping from the Perceptron to the Adaline neuron we gained two advantages:</p>
<ul>
<li>We can make direct use of regular optimization theory, which, e.g., for the method of gradient descent, requires a differentiable loss function.</li>
<li>We can more directly influence the intended notion of generalization by the choice of loss <span class="math inline">\(L(y,y&#39;)\)</span> and activation <span class="math inline">\(\alpha(z)\)</span> functions.</li>
</ul>
<p>These degree of freedom already lead to a rich class of linear classification models parametrized by <span class="math inline">\(L\)</span> and <span class="math inline">\(\alpha\)</span>, which we shall discuss more closely in the following chapters.</p>
<p>In this module, we look a bit closer at potential choices and constraints to keep in mind for:</p>
<ol type="1">
<li>The choice of activation functions.</li>
<li>The choice of loss functions.</li>
</ol>
<h3 id="activation-functions">Activation functions</h3>
<p>So far, for our binary classification models with, e.g., <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span>, we have been using a linear activation <span class="math display">\[\begin{align}
  \alpha(z)=z
\end{align}\]</span> and quadratic loss <span class="math display">\[\begin{align}
  L(y,y&#39;)=\frac12|y-y&#39;|^2.
\end{align}\]</span> This gave rise to the activation output <span class="math display">\[\begin{align}
  y&#39;=\alpha(w\cdot x+b)=w\cdot x+b
\end{align}\]</span> and the hypothesis <span class="math display">\[\begin{align}
  h^\alpha_{(b,w)}(x)=\text{sign}\alpha(w\cdot x+b)=\text{sign}(w\cdot x+b).
\end{align}\]</span> The intuition behind the quadratic loss was to somehow measure the distance between the actual class label <span class="math inline">\(y\in\mathcal Y\)</span> and the produced activation output <span class="math inline">\(y&#39;\)</span> corresponding to input features <span class="math inline">\(x\in\mathcal X\)</span>.</p>
<p>Typically, as in our example of binary classification (but also for many regression tasks), the labels <span class="math inline">\(y\)</span> in space <span class="math inline">\(\mathcal Y\)</span> are bounded. The activation output <span class="math inline">\(y&#39;\)</span> in the case of linear activation, however, is unbounded. This potential mismatch in order of magnitude between <span class="math inline">\(y\)</span> and <span class="math inline">\(y&#39;\)</span> causes the resulting empirical loss function <span class="math display">\[\begin{align}
  \widehat L(b,w) = \frac1N\sum_{i=1}^NL(y^{(i)},h^\alpha_{(b,w)}(x^{(i)}))
  = \frac1N \sum_{i=1}^N \frac12\left|y^{(i)}-(w\cdot x^{(i)}+b)\right|^2
  \tag{E1}
  \label{eq_1}
\end{align}\]</span> to behave much different from the empirical error <span class="math display">\[\begin{align}
  \frac1N \sum_{i=1}^N 1_{y^{(i)}\neq h^\alpha_{(b,w)}(x^{(i)})}
  =\frac1N \sum_{i=1}^N 1_{y^{(i)}\neq \text{sign}(w\cdot x^{(i)}+b)}.
  \tag{E2}
  \label{eq_2}
\end{align}\]</span></p>
<p>In our first implementation of the Adaline neuron we did not encounter a particular problem despite this mismatch of activation function and loss function; the main reason being that the Adaline neuron has an undetermined length scale at its expense <span class="math display">\[\begin{align}
  w\cdot x+b \quad \substack{\geq\\&lt;} \quad 0 \qquad \Leftrightarrow \qquad \lambda w\cdot x+\lambda b  \quad \substack{\geq\\&lt;} \quad 0,
  \qquad
  \lambda\in\mathbb R^+.
\end{align}\]</span> As we will discuss later on, the resulting, due to the potentially large difference <span class="math inline">\(y^{(i)}-(w\cdot x+b)\)</span>, the in turn potentially large parameter updates <span class="math display">\[\begin{align}
  w \leftarrowtail w^\text{new} 
  &amp;:= w - \eta \frac{\partial L(y^{(i)},h^\alpha_{(b,w)}(x^{(i)}))}{\partial w}\\
  &amp;= w + \eta \sum_{i=1}^N \left[y^{(i)}-(w\cdot x^{(i)}+b)\right]x^{(i)}\\
  b \leftarrowtail b^\text{new} 
  &amp;:= b - \eta \frac{\partial L(y^{(i)},h^\alpha_{(b,w)}(x^{(i)}))}{\partial b}\\
  &amp;=  w + \eta \sum_{i=1}^N \left[y^{(i)}-(w\cdot x^{(i)}+b)\right]\\
\end{align}\]</span> were to some extend even advantageous for the training speed in case of ill-conditioned initial parameters. However, it turns out that when generalization the Adaline neuron to multi-dimensional output, i.e., <span class="math inline">\(y&#39;\in\mathbb R^c\)</span> with <span class="math inline">\(c&gt;1\)</span>, and building a network of such Adaline neurons by connecting the outputs components of the preceding neuron to inputs of the succeeding one, such large changes somewhere in the network turn out to be rather problematic during training, and therfore, should be avoided by making the order of magnitudes of <span class="math inline">\(y\)</span> and <span class="math inline">\(y&#39;\)</span> more comparable.</p>
<p>As a general idea, in order to make the empirical loss <span class="math inline">\(\eqref{eq_1}\)</span> more comparable to the empirical error <span class="math inline">\(\eqref{eq_2}\)</span>, one may saturate the activation output in a similar way as the <span class="math inline">\(\text{sign}(\cdot)\)</span> function does, while retaining the differentiability. Good candidates for such activation functions are the so-called <em>signmoid</em> functions:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="sigmoid-function">(Sigmoid function)</h5>
We call a map <span class="math inline">\(\sigma:\mathbb R\to\mathbb R\)</span> a sigmoid function if it is a bounded differential function with a non-negative derivative.
</dd>
</dl>
<p>The rational behind this choice the following one:</p>
<dl>
<dt>👀</dt>
<dd>Show that any sigmoid has horizontal asymptotes for <span class="math inline">\(z\to\pm \infty\)</span>.
</dd>
</dl>
<p>Two frequent examples are:</p>
<ul>
<li>The logistic function <span class="math display">\[\begin{align}
  \alpha:\mathbb R&amp;\to[0,1]\\
  z&amp;\mapsto \frac{1}{1+e^{-z}};
\end{align}\]</span></li>
<li>The hyperbolic tangent: <span class="math display">\[\begin{align}
  \alpha:\mathbb R&amp;\to[-1,1]\\
  z&amp;\mapsto \tanh(z)=\frac{e^z-e^{-z}}{e^z+e^{-z}}.
\end{align}\]</span></li>
</ul>
<p>In case of binary classification, the logistic function is well suited for the label representation <span class="math inline">\(\mathcal Y=\{0,1\}\)</span>, while the <span class="math inline">\(\tanh\)</span> for <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span>. Obviously, by mapping the labels bijectively between <span class="math inline">\(\{0,1\}\)</span> and <span class="math inline">\(\{-1,+1\}\)</span>, both choices can be use for binary classification problems and we switch between both in some of the implementations.</p>
<dl>
<dt>Remark.</dt>
<dd>Historically, sigmoids were discussed already early on as they model a saturation of the activation signal. Later on we will discuss another of their advantages in more detail, namely that these sigmoids “slip in” a non-linearity into our yet linear classification models. When forming multiple layers of such non-linear Adaline neurons with multi-dimentional outputs by sucessively connecting their output and inputs, we will show that these non-linearities give the resulting network classification capabilities well beyond only the linear separable case.
</dd>
</dl>
<p>Let us experiment with the activation functions in our implementation:</p>
<dl>
<dt>👀</dt>
<dd>Adapt our Adaline implementation, in particular, the corresponding update rule, so that it can use the hyperbolic tangent or the logistic function (the latter requires the mentioned change of labels) as activation.
</dd>
</dl>
<h3 id="loss-functions">Loss functions</h3>
<p>Replacing the activation function by a sigmoid in our Adaline neuron implementation and adapting the update rule correspondingly will work out-of-the-box for initial model parameters close to <span class="math inline">\(w=0,b=0\)</span>.</p>
<p>However, one may realize that, for extreme choices of model parameters <span class="math inline">\(|w|,b\gg 1\)</span>, it will require many more epochs of training to achieve a similar empirical error as compared to the same setting with a linear activation function.</p>
<p>This training phenomenon is due to the fact, with a sigmoid function <span class="math inline">\(\alpha(z)\)</span>, the Adaline neuron can now easily be driven in regions of saturation, i.e., in regions in which even large changes of <span class="math inline">\(z\)</span> do not imply correspondingly large changes in <span class="math inline">\(\alpha(z)\)</span>.</p>
<figure>
<img src="05/sigmoid_saturation.png" alt="Saturation of the activation output due to a sigmoid function." /><figcaption aria-hidden="true">Saturation of the activation output due to a sigmoid function.</figcaption>
</figure>
<p>Suppression of large output signals by introducing a bounded activation <span class="math inline">\(\alpha(z)\)</span> introduces the so-called problem of <em>slow learning</em> for Adaline neurons in regions of saturation.</p>
<p>To understand this better, let us consider the case of binary classification with labels <span class="math inline">\(\mathcal Y=\{0,1\}\)</span> and the logistic function <span class="math display">\[\begin{align}
  \alpha(z)=\frac{1}{1+e^{-z}}
\end{align}\]</span> as activation. Correspondingly, we find for the update rule <span class="math display">\[\begin{align}
  w &amp; \leftarrowtail w^\text{new} 
  := w + \eta \sum_{i=1}^N \left[y^{(i)}-\alpha(w\cdot x^{(i)}+b)\right]\alpha&#39;(w\cdot x+b)x^{(i)}\\
  b &amp; \leftarrowtail b^\text{new} 
  := b + \eta \sum_{i=1}^N \left[y^{(i)}-\alpha(w\cdot x^{(i)}+b)\right]\alpha&#39;(w\cdot x+b),
\end{align}\]</span> where the derivative of the activation function is given by <span class="math display">\[\begin{align}
  \alpha&#39;(z)=\frac{e^{-z}}{(1+e^{-z})^2}=\alpha(z)(1-\alpha(z)).
\end{align}\]</span></p>
<p>Clearly, for large values of <span class="math inline">\(z\)</span>, the derivative <span class="math inline">\(\alpha&#39;(z)\)</span> becomes fairly small. In turn, the respective relative updates <span class="math inline">\((w^\text{new}-w)\)</span> and <span class="math inline">\((b^\text{new}-b)\)</span> are small and it takes the Adaline neuron many epochs of training to leave regions of saturation. The problem of too small <span class="math inline">\(\alpha&#39;(z)\)</span> appears in supervised learning in several situation and is also referred to as <em>vanishing gradient problem</em>.</p>
<p>This effect can be best observed when plotting the empirical loss ageist the training epochs. Typical plots may look like:</p>
<figure>
<img src="05/slow_learning.png" alt="Effect of slow learning for saturated Adaline neurons." /><figcaption aria-hidden="true">Effect of slow learning for saturated Adaline neurons.</figcaption>
</figure>
<p>Ideally, it would be nice to maintain the order of magnitude of the “fast” updates that we found for the case of linear activation also in the case of sigmoid functions. Regarding the derivative of the empirical loss function from which our general Adaline update rule computes its parameters updates: <span class="math display">\[\begin{align}
  \frac{\partial \widehat L_{(\bar w)}}{\partial \bar w}
  =\frac1 N \sum_{i=1}^N 
  \frac{\partial L(y^{(i)},z)}{\partial z}\Big|_{z=\alpha(\bar w\cdot \bar x^{(i)})}
  \alpha&#39;(\bar w\cdot \bar x^{(i)}) \bar x^{(i)},\\
\end{align}\]</span> where <span class="math display">\[\begin{align}
  \bar w=(b,w)^T,\qquad \bar x^{(i)}=(1,x^{(i)})^T,
\end{align}\]</span> we observe that for this to happen, the potentially small factor <span class="math inline">\(\alpha&#39;(z)\)</span> would then need to be compensated. In other words, the loss function <span class="math inline">\(L(y,y&#39;)\)</span> ideally needs to be adapted such that <span class="math display">\[\begin{align}
  \frac{\partial L(y^{(i)},z)}{\partial z} \sim \frac{1}{\alpha&#39;(z)}.
  \tag{E3}
  \label{eq_3}
\end{align}\]</span></p>
<p>There are at least two routes to proceed:</p>
<ol type="1">
<li>Guess a good candidate <span class="math inline">\(L(y,y&#39;)\)</span>, simply by integrating <span class="math inline">\(\eqref{eq_3}\)</span>.</li>
<li>Argue theoretically for a more meaningful expression for <span class="math inline">\(L(y,y&#39;)\)</span> than our ad hoc choice of quadratic loss in the case of binary classification.</li>
</ol>
<p>Route 1. will make up for a good task that we will look at in the exercises:</p>
<dl>
<dt>👀</dt>
<dd>Integrate <span class="math inline">\(\eqref{eq_3}\)</span> to find a good choice of <span class="math inline">\(L(y,y&#39;)\)</span> and adapt your Adaline implementation accordingly. Observe that the slow learning problem is absent even for ill-conditioned initial model parameters <span class="math inline">\(|w|,b\gg 1\)</span>.
</dd>
</dl>
<p>And route 2. will invite us for a small excursion to information entropy measures.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
