<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__08-S1__Computational_tools_VC-dimension</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-08-s1-pdf">MsAML-08-S1 [<a href="SS21-MsAML__08-S1__Computational_tools_VC-dimension.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#computational-tools-for-the-vc-dimension">Computational tools for the VC-dimension</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="computational-tools-for-the-vc-dimension">Computational tools for the VC-dimension</h2>
<p>In the following we will take a look at two results that help to compute the VC-dimension. Let us jump right in:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="vc-dimension-for-linear-spaces">(VC-dimension for linear spaces)</h5>
Let <span class="math inline">\(\mathcal G\)</span> be a finite-dimensional <span class="math inline">\(\mathbb R\)</span>-vector space of functions of the form <span class="math inline">\(g:\mathcal X\to\mathbb R\)</span>, and let <span class="math inline">\(\phi:\mathcal  X\to\mathbb R\)</span> be an additional function. Then, the space of hypotheses <span class="math display">\[\begin{align}
\mathcal F := \left\{
  x\mapsto \text{sign}\left(\phi(x)+g(x)\right)
  \,\big|\,
  g\in\mathcal G
\right\}
\subseteq \{-1,+1\}^{\mathcal X}
  \end{align}\]</span> fulfills <span class="math display">\[\begin{align}
\dim_\text{VC} \mathcal F = \dim_{\mathbb R}\mathcal G.
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> Part 1) Let us start by showing <span class="math display">\[\begin{align}
  \dim_\text{VC} \mathcal F \leq \dim_{\mathbb R} \mathcal G.
\end{align}\]</span> We shall argue by contradiction. For this purpose let us set <span class="math inline">\(k=\dim_{\mathbb R}\mathcal G+1\)</span> and assume <span class="math display">\[\begin{align}
  \dim_\text{VC} \mathcal F \geq k.
\end{align}\]</span> Then, there is a tuple of points <span class="math inline">\(P=(x_1,\ldots,x_k)\subset \mathcal X\)</span> that is shattered by <span class="math inline">\(\mathcal F\)</span>, or equivalently <span class="math display">\[\begin{align}
  \left|\mathcal F|_P\right| = 2^{|P|} = 2^k
\end{align}\]</span> or in order words, <span class="math inline">\(\mathcal F|_P\)</span> contains every bit pattern in <span class="math inline">\(\{-1,+1\}^{k}\)</span>.</p>
<p>Now, the set <span class="math display">\[\begin{align}
  \mathcal G|_P := \left\{
    \big(g(x_1),\ldots, g(x_k)\big)
    \,\big|\,
    g\in\mathcal G
  \right\}
  \subset \mathbb R^k
\end{align}\]</span> is a <span class="math inline">\(\mathbb R\)</span>-vector space of dimension <span class="math display">\[\begin{align}
  \dim_{\mathbb R}\mathcal G|_P
  =
  \dim_{\mathbb R}\mathcal G
  =k-1.
\end{align}\]</span></p>
<p>Hence, there exists a <span class="math inline">\(v\in\mathbb R^k\)</span> with at least one negative component <span class="math inline">\(v_j\in\mathbb R^-\)</span> such that for all <span class="math inline">\(g\in\mathcal G\)</span> <span class="math display">\[\begin{align}
  0
  =
  \sum_{i=1}^k v_i g(x_i)
  =: \langle
    v, (g(x_1),
    \ldots,
    g(x_k)
  \rangle
  ,
\end{align}\]</span> which then implies that for all <span class="math inline">\(g\in\mathcal G\)</span> <span class="math display">\[\begin{align}
  \Big\langle
    v, \big( 
      g(x_1)+\phi(x_1),
      \ldots,
      g(x_k)+\phi(x_k)
    \big)
  \Big\rangle
  =
  \langle
    v, (\phi(x_1),\ldots,\phi(x_k)
  \rangle
  \label{eq_1}
  \tag{E1},
\end{align}\]</span> or put differently, the term is <span class="math inline">\(g\)</span>-independent.</p>
<p>But given such a vector <span class="math inline">\(v\)</span>, and recalling that <span class="math inline">\(\mathcal F|_P\)</span> comprises all bit patterns in <span class="math inline">\(\{-1,+1\}^k\)</span>, there must exist <span class="math inline">\(g^+,g^-\in\mathcal G\)</span> such that <span class="math display">\[\begin{align}
  \text{sign}\left(g^\pm(x_i)+\phi(x_i)\right) = \pm \text{sign}(v_i),
  \qquad
  i=1,\ldots, k,
\end{align}\]</span> using our convention <span class="math display">\[\begin{align}
  \text{sign}(z)=
  \begin{cases}
    +1 &amp; \text{for } x\geq 0\\
    -1 &amp; \text{otherwise}
  \end{cases}
  \,.
  \label{eq_2}
  \tag{E2}
\end{align}\]</span></p>
<p>This implies <span class="math display">\[\begin{align}
  \left\langle
    v,
    \left(
      g^\pm(x_1)+\phi(x_1),
      \ldots,
      g^\pm(x_k)+\phi(x_k)
    \right)
  \right\rangle
  &amp;=
  \pm\sum_{i=1}^k |v_i| 
  \left|
    g^\pm(x_i)+\phi(x_i)
  \right|.
\end{align}\]</span> However, there is at least one negative component <span class="math inline">\(v_j&lt;0\)</span> and the term <span class="math inline">\(|g^\pm(x_j)+\phi(x_j)|\)</span> cannot be zero for both cases <span class="math inline">\(+\)</span> and <span class="math inline">\(-\)</span> due to <span class="math inline">\(\eqref{eq_2}\)</span>. This contradicts <span class="math inline">\(\eqref{eq_1}\)</span>.</p>
<p>Part 2) Next we shall show <span class="math display">\[\begin{align}
  \dim_\text{VC}\mathcal F\geq \dim_{\mathbb R}\mathcal G.
\end{align}\]</span> Therefore, it suffices to show that <span class="math display">\[\begin{align}
  &amp; \forall k\leq \dim_{\mathbb R}\mathcal G \, \exists x_1,\ldots, x_k\in\mathcal X\\
  &amp; \qquad \forall y\in\mathbb R^k \, \exists g\in\mathcal G \\
  &amp; \qquad \qquad \forall i=1,\ldots, k: y_i=g(x_i).
\end{align}\]</span></p>
<p>For any choice of <span class="math inline">\(k\)</span> linear independent functions <span class="math inline">\((g_i)_{i=1,\ldots,k}\)</span> in <span class="math inline">\(\mathcal G\)</span> we have <span class="math display">\[\begin{align}
  \text{span}_{x\in\mathcal X} 
  \begin{pmatrix}
   g_1(x)\\
   \vdots\\
   g_k(x)
  \end{pmatrix}
  =
  \mathbb R^k
\end{align}\]</span> and there is a tuple <span class="math inline">\(x_1,\ldots, x_k\)</span> in <span class="math inline">\(\mathcal X\)</span> such that <span class="math display">\[\begin{align}
  \begin{pmatrix}
   g_1(x_1)\\
   \vdots\\
   g_k(x_1)
  \end{pmatrix},
  \ldots,
  \begin{pmatrix}
   g_1(x_k)\\
   \vdots\\
   g_k(x_k)
  \end{pmatrix}
\end{align}\]</span> are linearly independent. In consequence, the matrix <span class="math display">\[\begin{align}
  G=\left(g_j(x_i)\right)_{1\leq i,j\leq k}
\end{align}\]</span> is invertible. Thus, for any given <span class="math inline">\(y\in\mathbb R^k\)</span>, we can find a vector of coefficients <span class="math inline">\(\lambda\in\mathbb R^k\)</span> such that <span class="math display">\[\begin{align}
  y = G\lambda 
  \qquad :\Leftrightarrow \qquad
  \forall i=1,\ldots,k: y_i=\sum_{j=1}^k \lambda_j g_j(x_i),
\end{align}\]</span> namely <span class="math display">\[\begin{align}
  \lambda = G^{-1}y,
\end{align}\]</span> which in turn identifies a element in <span class="math inline">\(\mathcal G\)</span> <span class="math display">\[\begin{align}
  g=\sum_{j=1}^k \lambda_j g_j,
\end{align}\]</span> which then fulfills <span class="math display">\[\begin{align}
  \forall i=1,\ldots, k: y_i=g(x_i). 
\end{align}\]</span></p>
<p><span class="math inline">\(\square\)</span></p>
<p>This allows to compute the VC-dimension for our linear classification models with ease:</p>
<dl>
<dt>Corollary:</dt>
<dd><h5 id="vc-dimension-of-linear-classification-models">(VC-dimension of linear classification models)</h5>
For <span class="math inline">\(\mathcal X=\mathbb R^d\)</span>, <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span>, and <span class="math display">\[\begin{align}
\mathcal F = \left\{
  h(x)=\text{sign}(w\cdot x+b)
  \,\big|\,
  w\in\mathbb R^d, b\in\mathbb R.
\right\},
  \end{align}\]</span> the relation <span class="math display">\[\begin{align}
\dim_\text{VC} \mathcal F = d+1
  \end{align}\]</span> holds true.
</dd>
</dl>
<p><strong>Proof:</strong> Define <span class="math display">\[\begin{align}
  \phi(x) &amp;:= 0,\\
  g_i(x) &amp;:= x_i, \qquad i=1,\ldots, d,\\
  g_{d+1}(x) &amp;:= 1,
\end{align}\]</span> This allows to express the activation input for any model parameters <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in\mathbb R\)</span> as <span class="math display">\[\begin{align}
  \forall x\in\mathbb R^d:
  \qquad
  w\cdot x+b
  =
  \phi(x)+\sum_{i=1}^{d+1} \lambda_i g_i(x)
\end{align}\]</span> employing coefficients <span class="math inline">\(\lambda\in\mathbb R^{d+1}\)</span> that fulfill <span class="math display">\[\begin{align}
  \lambda_{d+1} = b,
  \qquad
  \lambda_i = w_i, \quad  i=1,\ldots, d.
\end{align}\]</span></p>
<p>Hence, we can apply our theorem above to the <span class="math inline">\((d+1)\)</span>-dimensional <span class="math inline">\(\mathbb R\)</span>-vector space of functions <span class="math display">\[\begin{align}
  \mathcal G:=
  \left\{
    g(x) = \sum_{i=1}^{d+1}\lambda_i g_i(x)
    \,\big|\,
    \lambda\in\mathbb R^{d+1}
  \right\}.
\end{align}\]</span></p>
<p><span class="math inline">\(\square\)</span></p>
<p>After these computations one may be tempted to think that the VC-dimension is already determined by the dimension of the parameter space that parametrize the hypotheses in <span class="math inline">\(\mathcal F\)</span>. While this is always the case, it does not hold in general as the following example shows:</p>
<p><strong>Example:</strong> Let us consider the hypotheses space <span class="math display">\[\begin{align}
  \mathcal F := \left\{
    h(x)=\text{sign}\left(\sin(\alpha x)\right)
    \,\big|\,
    \alpha\in\mathbb R
  \right\}
\end{align}\]</span> and take the tuple of points <span class="math display">\[\begin{align}
  P = \left(
    x_k:=2^{-k}
    \,\big|\,
    k=1,\ldots, k
  \right)
\end{align}\]</span> together with a bit pattern <span class="math display">\[\begin{align}
  \big(c(x_1),\ldots,c(x_n)\big) \in \{-1,+1\}^n
\end{align}\]</span> for some concept <span class="math inline">\(c:\mathbb R\to \{-1,+1\}\)</span>.</p>
<p>Now, choosing <span class="math display">\[\begin{align}
  \alpha = \pi\left(
    1 + \sum_{k=1}^n \, \underbrace{\frac{1-c(x_k)}{2}}_{\in \{0,1\}} \, 2^k
  \right)
\end{align}\]</span> implies for <span class="math inline">\(l=1,\ldots,n\)</span>, <span class="math display">\[\begin{align}
  \alpha x_l \mod 2\pi
  &amp;=
  \pi\left(
    1 + \sum_{k=1}^n \, \underbrace{\frac{1-c(x_k)}{2}}_{\in \{0,1\}} \, 2^k
  \right) 2^{-l} \mod 2\pi \\
  &amp;=
  \pi \frac{1-c(x_l)}{2}
  +
  \pi
  \underbrace{
  \left(
    2^{-l}+\sum_{k=1}^{l-1} \, \frac{1-c(x_k)}{2} \, 2^{k-l}
  \right) 
  }_{\in (0,1)},
\end{align}\]</span> and therefore, <span class="math display">\[\begin{align}
  \text{sign}(\sin(\alpha x_l)) = c(x_l).
\end{align}\]</span> In other words, <span class="math inline">\(P\)</span> is shattered by <span class="math inline">\(\mathcal F\)</span>.</p>
<p>As this argument holds for any <span class="math inline">\(n\)</span>, we get <span class="math display">\[\begin{align}
  \forall n\in\mathbb N: \quad \dim_\text{VC}\mathcal F \geq n,
\end{align}\]</span> or in other words, <span class="math inline">\(\dim_\text{VC}\mathcal F=\infty\)</span>.</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
