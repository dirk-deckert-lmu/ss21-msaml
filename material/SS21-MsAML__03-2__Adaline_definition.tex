% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-03-2-pdf}{%
\section{\texorpdfstring{MsAML-03-2
{[}\href{SS21-MsAML__03-2__Adaline_definition.pdf}{PDF}{]}}{MsAML-03-2 {[}PDF{]}}}\label{msaml-03-2-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{adaline-definition}{Adaline definition}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{adaline-definition}{%
\subsection{Adaline definition}\label{adaline-definition}}

The main drawbacks of the Perceptron algorithm are:

\begin{itemize}
\tightlist
\item
  The update rule stops updating the model parameters \(w\) and \(b\) as
  soon as linear separability is reached, even if there might be
  separating hyperplanes that have more potential to generalize.
\item
  In case of ``almost'' linear separability, the update rule causes the
  model parameters to jump back and forth due to the remaining incorrect
  classifications instead of searching for an optimal hyperplane --
  e.g., one with the lowest number of violations.
\end{itemize}

It will turn out that a minor modification allows to address these
shortcomings to some extend. In particular, it will allow to specify
better a notion of optimality in a way that allows to apply regular
optimization theory.

In the Perceptron algorithm, training data feeds back information to the
update rule as follows:

\begin{figure}
\centering
\includegraphics{03/Perceptron_feedback_loop.png}
\caption{Illustration of the Perceptron feedback loop.}
\end{figure}

This illustration depicts the various steps carried out during an
update. The feature signals in form of the feature vector
\(x\in\mathbb R^d\) are fed into the weighted sum \(w\cdot x\). The
produced ``signal'' of the artificial neuron is then lifted by means of
the bias term \(b\) to result in the activation signal \(w\cdot x+b\).
Afterwards, it is ``cleaned'' to enforce an output signal
\(y\in\{-1,+1\}\). In case of a misclassification, it is the cleaned,
and therefore discrete, output signal that is fed back to the update
rule in order to adjust the weights in \(w\) and the bias \(b\).

Next, we will implement a tiny adaptation of this feedback loop that
will turn out to have a quite big effect:

\begin{figure}
\centering
\includegraphics{03/Adaline_feedback_loop.png}
\caption{Illustration of the Adaline feedback loop.}
\end{figure}

This architecture is called the \emph{ada}ptive \emph{line}ar neuron, or
Adaline neuron for short. The main difference is that we will change the
algorithm to continuously feed information about the activation signal
back before making the signal discrete by means of the \(\text{sign}\)
function.

The activation signal will be computed from \(w\cdot x+b\) by applying
another function \begin{align}
  \alpha:\mathbb R\to\mathbb R,
\end{align} the so-called \emph{activation function} whose value we will
call \emph{activation output}. A sufficiently regular choice in this
function will be the first ingredient to apply regular optimization
theory.

Note that the activation input \(w\cdot x+b\) is unbounded, which could
render a comparison to \(y^{(i)}\) ill-posed as the activation input and
the actual labels live on different scales. However, we could simulate
the discrete \(\text{sign}\) function by a smooth version of it. For
example for \(\mathcal Y=\{-1,+1\}\), we could choose the so-called
\emph{sigmoid} function \begin{align}
  \alpha(z):=\tanh(z).
\end{align} Now \(y{(i)}\) and \(h^\alpha_{(b,w)}\) are again on the
``same'' scale, and furthermore, \(h^\alpha_{(b,w)}\) is as smooth as
\(\alpha\).

Next, let us introduce a loss function to formalize the comparison
between actual labels \(y^{(i)}\) and activation output
\(h^\alpha_{(b,w)}\). As for \(\alpha\), we will discuss various choices
in the coming modules, but for now let us assume that our label space
\(\mathcal Y\) comprises only the numbers \(-1\) and \(+1\) and start
with the simple choice: \begin{align}
  L: \mathcal Y \times \mathcal Y &\to\mathbb R
  \\
  (y,y') &\mapsto \frac12|y-y'|^2,
\end{align} which for training data point \((x^{(i)},y^{(i)})\) equals
half the square distance between the actual class label \(y^{(i)}\). The
factor \(\frac12\) is only kept for convenience as we will mainly be
working with the corresponding derivative. This allows to define the
loss between actual label \(y^{(i)}\) and activation output as response
to input \(x^{(i)}\) by \begin{align}
  L\left(y^{(i)},h^\alpha_{(b,w)}(x^{(i)})\right),
  \qquad
  h^\alpha_{(b,w)}(x^{(i)}) := \alpha(w\cdot x+b).
\end{align} Furthermore, we accumulate these losses and define the
empirical risk of the activation output hypothesis \(h^\alpha_{(b,w)}\)
as \begin{align}
  \widehat R(h^\alpha_{(b,w)})=\frac{1}{N}\sum_{i=1}^N L\left(y^{(i)},h^\alpha_{(b,w)}(x^{(i)})\right).
\end{align} which, to emphasize it once more, is as smooth as \(\alpha\)
and \(L\).

\begin{description}
\tightlist
\item[Remark.]
Note however that the hypothesis used for assigning the predicted class
labels is not given by the activation output \(h^\alpha_{(b,w)}\) but by
the ``cleaned'' hypothesis \begin{align}
h_{(b,w)}(x) := \text{sign}(h^\alpha_{(b,w)}(x))
  \end{align} that projects the activation output to the discrete label
space \(\mathcal Y\). Hence, although closely related, the empirical
risk of the activation output \(h^\alpha_{(b,w)}\) will differ from the
empirical risk of the classifier \(h_{(b,w)}\) \begin{align}
\widehat R(h_{(b,w)})=\frac{1}{N}\sum_{i=1}^N L\left(y^{(i)},h_{(b,w)}(x^{(i)})\right),
  \end{align} and likewise, of the error probability \begin{align}
\widehat{\text{Err}}(h_{(b,w)})=\frac{1}{N}\sum_{i=1}^N 1_{y^{(i)}\neq h_{(b,w)}(x^{(i)})}.
  \end{align}
\end{description}

It is the goal, of course, to minimize the empirical risk
\(\widehat R(h_{(b,w)})\). However, due to the \(\text{sign}\) function,
the latter is not continuous, not to mention differentiable, no matter
how smooth \(\alpha\) or \(L\) was chosen; and the same holds for the
empirical error. This makes it impossible to apply regular optimization
methods. Therefore, instead of attempting to minimize the empirical risk
directly we will focus on the closely related empirical risk of the
activation output for sufficiently smooth \(\alpha\) and \(L\).

The introduction of \(\alpha\) and \(L\) will help us to quip the model
with a mathematically precise notion of priori knowledge about:

\begin{itemize}
\tightlist
\item
  How close the model is to make an accurate classification of the
  training data;
\item
  and how optimal a potential hypothesis is.
\end{itemize}

Especially, for the latter, \(L\) has to be chosen with much care.

To keep our notation short, let us introduce the abbreviation
\begin{align}
  \widehat L(b,w) = \widehat R(h^\alpha_{(b,w)})
\end{align} and let us for a moment assume that the model parameters
\(w\) and \(b\) are already sufficiently close to a local minimum of
\(\widehat R(h^\alpha_{(b,w)})\).

How should the model parameters \(w\) and \(b\) be updated \begin{align}
  w & \leftarrowtail w^\text{new} := w + \delta w \\
  b & \leftarrowtail b^\text{new} := b + \delta b
\end{align} to get even closer to the minimum, i.e., such that
\begin{align}
  \widehat L(w^\text{new})\leq \widehat L(w)
  \tag{min}
  \label{eq_min}
\end{align} is fulfilled?

Let us consider a heuristic argument and employ the notation
\(\bar w=(b,w)\): \begin{align}
  \widehat L(\bar w^\text{new}) - \widehat L(\bar w)
  &=
  \widehat L(\bar w+\delta \bar w) - \widehat L(\bar w)
  \\
  &=
  \frac{\partial \widehat L(\bar w)}{\partial \bar w} \delta\bar w
  +
  O_{|\delta \bar w|\to 0}(|\delta \bar w|^2),
\end{align} where \(\frac{\partial}{\partial\bar w}\) denotes the
gradient in \(\mathbb R^{d+1}\) with respect to \(w\in\mathbb R^d\) and
\(b\in\mathbb R\). Hence, choosing \begin{align}
  \delta\bar w = -\eta \frac{\partial \widehat L(\bar w)}{\partial \bar w}
  \tag{negative gradient}
  \label{eq_negative_gradient}
\end{align} for a learning rate \(\eta>0\), results in \begin{align}
  \widehat L(\bar w^\text{new}) - \widehat L(\bar w)
  &=
  \widehat L(\bar w+\delta \bar w) - \widehat L(\bar w)
  \\
  &=
  -\eta
  \left|\frac{\partial \widehat L(\bar w)}{\partial \bar w} w\right|^2
  +
  O_{\eta \to 0}(\eta^2).
\end{align} Note how our choice \(\eqref{eq_negative_gradient}\) in
taking the negative gradient of \(\widehat L\) at \(\bar w\) fixes the
sign of the linear order term. In case the higher order corrections,
whose modulus can be estimated by means of the Taylor remainder, in our
case, involving the Hesse matrix of \(\widehat L(\bar w)\), does not
outweigh this negativity of first order term, the new parameters
\(\bar w+\delta\bar w\) brought us even closer to the minimum, i.e.,
\(\eqref{eq_min}\). This is the core idea behind a procedure that is
referred to as \emph{gradient descent}, owing its name to
\(\eqref{eq_negative_gradient}\).

\begin{description}
\tightlist
\item[👀]
Is there a bound on the higher orders which is accessible in
applications?
\end{description}

Of course, this heuristic discussion only scratches the surface of
search policies to find a minimum of \(\widehat L\) in regular
optimization theory. Under which conditions do we converge? And if we
do, to which local minimum? And will this local minimum be a global one?
What to do to not get stuck in local minima that still feature a high
value of \(\widehat L\)? And there are many more sophisticated
algorithms, like the Newton's method you might recall from your analysis
lectures to mention a simple one. Nevertheless, it will turn out that,
for application, the gradient descent will perform reasonably well
without relying on second order derivatives that are very expensive to
compute once the model parameters become large.

In summary, we may formalize the Adaline update rule as follows:

\textbf{Update rule:} The component-wise Adaline update rule is given by
\begin{align}
  w & \leftarrowtail w^\text{new}_j := w_j -\eta \frac{\partial \widehat L(b,w)}{\partial w_j} \\
  b & \leftarrowtail b^\text{new} := b -  \eta \frac{\partial \widehat L(b,w)}{\partial b}
\end{align} for components \(j=1,\ldots,d\).

Furthermore, we specify the first version of the Adaline algorithm by:

\begin{description}
\item[Definition] ~ 
\hypertarget{adaline}{%
\subparagraph{(Adaline)}\label{adaline}}

The realization (as it is actually a random variable when defined as
below) of the Adaline algorithm is a map \begin{align}
\mathcal A:\cup_{N\in\mathbb N} (\mathbb R^d,\{-1,1\})^N\to \mathcal H
  \end{align} which is specified by means of the computation steps
below:
\end{description}

\textbf{Input for \(\mathcal A\):} Training data \begin{align}
  (x^{(i)},y^{(i)})_{i=1,\ldots,N}.
\end{align} Furthermore, we assume a given choice of activation function
\(\alpha\).

\textbf{Step 1:} Initialize \(w\in\mathbb R^d\) and \(b\in \mathbb R\)
with random coefficients which defines the activation output
\begin{align}
  h^\alpha_{(b,w)}(x) := \alpha(w\cdot x +b).
\end{align} and hypothesis \begin{align}
  h_{(b,w)}(x) := \text{sign}(h^\alpha_{(b,w)}(x)).
\end{align}

\textbf{Step 2:} Until a maximum total number of repetitions is reached:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Compute the empirical activation loss \(\widehat L(b,w)\).
\item
  Conduct the \textbf{Update rule} above to infer the parameter updates:
  \begin{align}
   w & \leftarrowtail w^\text{new}\\
   b & \leftarrowtail b^\text{new}
      \end{align}
\end{enumerate}

and return to \textbf{Step 2}.

\textbf{Output of \(\mathcal A\):} The adjusted parameters
\(w\in\mathbb R^d\) and \(b\in\mathbb R\) and therefore a ``hopefully''
better hypothesis \begin{align}
  h_{(b,w)} \in\mathcal H.
\end{align}

Each iteration in the above algorithm is usually called a \emph{training
epoch}. In the next module we will compare the Perceptron and Adaline
algorithms a bit closer and discuss some useful variations.

➢ Next session!

\begin{description}
\item[👀] ~ 
\hypertarget{section}{%
\subparagraph{}\label{section}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Prove that, even for linearly separable data, the Adaline algorithm
  for constant \(\eta>0\) may not converge, no many how many epochs we
  allow. This will even typically be the case. Argue why the Adaline may
  nevertheless behave better than the Perceptron in view of our initial
  motivation above.
\item
  Consider a linear activation function \(\alpha(z)=z\) and the square
  loss \(L(y,y')=\frac12|y-y'|^2\) as above. Given training data
  \(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\), does a global minimum of
  \(\widehat  L(b,w)\) exist? Can we enforce convergence by allowing the
  Adaline algorithm, e.g., to decrease \(\eta\), after each epoch of
  training? Discuss the trade-off between speed of convergence and
  accuracy for smaller \(\eta\).
\end{enumerate}
\end{description}

\end{document}
