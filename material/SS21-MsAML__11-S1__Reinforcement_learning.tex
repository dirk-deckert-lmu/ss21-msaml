% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-11-s1-pdf}{%
\section{\texorpdfstring{MsAML-11-S1
{[}\href{SS21-MsAML__11-S1__Reinforcement_learning.pdf}{PDF}{]}}{MsAML-11-S1 {[}PDF{]}}}\label{msaml-11-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{reinforcement-learning}{Reinforcement learning}
\end{enumerate}

Back to \href{index.pdf}{index}.

Since we are getting towards the end of our lecture, we can allow
ourselves some playtime with a more specialized topic of supervised
learning, namely \emph{reinforcement learning}. Since the content of the
next modules may be comparably bigger as compared to the average module
sizes so far, we will regard the lecture notes as both lectures and
exercises, and we will try to reduce the workload of the exercises
correspondingly. As always, we encourage you even more to go through the
lecture notes with pencil and paper

\hypertarget{reinforcement-learning}{%
\subsection{Reinforcement learning}\label{reinforcement-learning}}

Until now, we have worked under the premise that we were given a
sufficiently large set of high quality training data in order to conduct
the training of our supervised learners. In many practical applications,
such gold standard training data is simply not available, very costly to
compile, and there are even scenarios in which the a priori complexity
of the learning tasks appears as so high that any endeavor to generate
an correspondingly sufficient amount of training data may seem futile.
This is often the case when not only one task but several intermediate
tasks, e.g., carried out successively in time, are required to achieve
an \emph{overall goal}. Take, for example the games of chess and Go. The
total number of combinations of possible \emph{states}, e.g., the
positions in the games, and possible \emph{actions}, e.g., the valid
moves are extremly large; even only the number of positions in the Go
game is already exorbitantly high, see for instance
\href{https://en.wikipedia.org/wiki/Go_and_mathematics\#Complexity_of_certain_Go_configurations}{here}.

Another typical example would be an \emph{agent}, e.g., an autonomous
vehicle operating in an unknown \emph{environment} and trying to get
from point A to point B under certain conditions such as maneuverability
of the terrain, battery power, etc. Luckily, in those situations, the
effectiveness of the \emph{actions} taken by the agent can often be
evaluated in perspective of an overall goal to some extend during the
process. If such an online evaluation is possible, it delivers high
quality annotated training data ``for free'', which can then be employed
to optimize the learners future choices in order to achieve the overall
goal.

Each newly taken action of the agent may involve both
\emph{exploitation} of previous acquired knowledge about the learning
task and \emph{exploration} by, e.g., once in a while carrying out a
random action to learn from its consequences. As it turns out, the
balance between exploration and exploitation is the key to an efficient
training of the agent.

Furthermore, this approach addresses the problem of the possibly large
number of combinations of state and action pairs. Since the training
data is generated along the way, it is much more likely to be generated
with a beneficial distribution reflecting the a priori unknown actual
distribution that one picks up in typical paths through the state-action
space. The agent therefore naturally focuses more on these common paths,
e.g., the common openings of a chess game, instead of all theoretically
possible ones.

Reinforcement learning provides a framework for this type of learning
tasks. For the sake of generality, let us abstract the learning
scenarios and from now on employ the jargon that we have already
introduced above:

\begin{itemize}
\tightlist
\item
  The learner is typically referred to as an ``agent''.
\item
  The agent carries out actions which allows it to interact with an
  ``environment''.
\item
  Each action may change the ``state'' of the agent or both the agent
  and the environment and the environment provides feedback, usually
  referred to as ``reward'', which allows the agent to evaluate its
  actions in perspective to an ``overall goal''.
\end{itemize}

In summary:

\begin{figure}
\centering
\includegraphics{11/reinforcement_learning.png}
\caption{Reinforcement learning scenario.}
\end{figure}

Instead of passively receiving the annotated training data at once
before training, as in the case of our previously discussed supervised
learners, the agent collects training data in a continuous manner
through the course of its interaction with its environment. In response
of each action, the agent receives two types of information. The new
agent's state and a reward in perspective of the overall goal. Note that
it requires a priori knowledge in order to interpret the reward in an
effective way in order to improve the learning task in perspective of
the overall goal.

In our settings we will assume the reward to be real-valued and model
the overall goal of the agent to maximize the overall accumulated
reward. An agent's strategy to optimize the overall accumulated reward
is usually referred to as a \emph{policy}. Note that a strategy such as
greedily choosing those actions that maximize the reward in the very
next step may not result in a maximum accumulated reward. For instance,
though traveling from A to be B as the crow flies might result in the
agent's maximal reward per time step, but its success streak might
abruptly come to an end at a cliff due to the lack of wings. The
difficulty is to find a good strategy that also incorporates which local
detours may be necessary to maximize accumulated reward globally --
regarding our climate policies, this is often a hard task even for
humans.

So in contrast to our previously discussed supervised learning
scenarios, first, the distribution with respect to which training data
is ``drawn'' is now policy-dependent, and therefore, not fixed, and
second, training and testing data are somewhat intertwined during the
whole training process.

There are two main settings for reinforcement learning. One in which the
model of the environment is known to the agent, and one in which it is
not. The chess and Go games are examples of the former kind and the
autonomous vehicle in an unknown environment of the latter one. Even if
the environment ist known completely, maneuvering in it in perspective
of an overall goal might still be a highly non-trivial learning task for
the agent as, e.g., in a Go games, a brute force search for an optimal
path is simply infeasible.

The underlying mathematical model describing the states, actions, and
rewards in time which we will use is the so-called \emph{Markov decision
process}:

\begin{description}
\item[Definition] ~ 
\hypertarget{markov-decision-process}{%
\subparagraph{(Markov decision process)}\label{markov-decision-process}}

Given the triple \((\mathcal S,\mathcal A,\mathcal P)\) comprising a set
of possible states \(\mathcal S\), a set of possible actions
\(\mathcal A\), and a family of transition probabilities \begin{align}
P=\left((p(s'|s,a)\right)_{\substack{s,s'\mathcal S\\a\in\mathcal A}}
  \end{align} we call a sequence of random variables \begin{align}
(S_t,A_t)_{t\in\mathbb N_0},
\qquad
\forall t\in\mathbb N_0: S_t\in\mathcal S,A_t\in\mathcal A,
  \end{align} fulfilling for all \(t\in\mathbb N_0\),
\(s's,s_t,\ldots, s_0\in\mathcal S\), and
\(a,a_t,\ldots, a_0\in\mathcal A\) \begin{align}
&P(S_{t+1}=s'|S_t=s_t,A_t=a_t,\ldots,S_0=s_0,A_0=a_0)
\\
&=:p(s'|s_t,a_t)
\label{eq_1}
\tag{E1}
  \end{align} a Markov decision process (MDP).
\end{description}

The quantity \(p(s'|s,a)\) reflects how likely a transition from state
\(s\) to state \(s'\) as consequence of action \(a\) occurs.

By definition, the process \((S_t,A_t)_{t\in\mathbb N_0}\) has the
Markov property \begin{align}
  &P(S_{t+1}=s'|S_t=s_t,A_t=a_t,\ldots,S_0=s_0,A_0=a_0)\\
  &=
  P(S_{t+1}=s'|S_t=s_t,A_t=a_t)\\
  &=p(s'|s_t,a_t),
\end{align} and hence, the name MDP.

The properties of the discreteness in the steps as well as the Markov
property are of course only model assumptions which can and, depending
on the setting, must be generalized. We will however stick to them.

It is important to observe that \(\eqref{eq_1}\) must be supplied with
an initial distribution of \(S_0\) and \(A_0\) together with a mechanism
how to choose \(A_{t+1}\) on the basis of \(S_{t+1}\) in order to
construct a realization of the process. As this mechanism shall be
informed by the reward we introduce it next:

\begin{description}
\item[Definition] ~ 
\hypertarget{reward-function}{%
\subparagraph{(Reward function)}\label{reward-function}}

A reward function is a measurable map of the form: \begin{align}
R:\mathcal S\times\mathcal A\times\mathcal S &\to \mathbb R\\
(s,a,s') &\mapsto R(s,a,s')
  \end{align}
\end{description}

Again, more generally, it might make sense to consider more general
mechanism that also depends on the whole history of the process.
Moreover, the reward of the environment could even be stochastic, i.e.,
it could be given in terms of a process \((R_t)_{t\in\mathbb N}\).
Again, we will only stick to the above deterministic one.

\textbf{Example:} For a Tic-Tac-Toe game, the state space \(\mathcal S\)
could be modeled as reflecting all possible game board positions by
means of \begin{align}
  \mathcal S=\{-1,0,+1\}^{3\times 3}.
\end{align} For example:

\begin{figure}
\centering
\includegraphics{11/tic-tac-toe.png}
\caption{Representation example of a game board position.}
\end{figure}

In this example \(-1\) denotes an X, \(+1\) an O, and zero an empty
slot.

Let us imagine player 1 as the environment placing the X's and the
player 2 being the agent placing the O's. The possible actions of the
player could be modeled by \begin{align}
  \mathcal A=\{1,2,\ldots,9\},
\end{align} each number denoting a placement of an O in the enumerated
slot according to the table:

\begin{figure}
\centering
\includegraphics{11/tic-tac-toe_actions.png}
\caption{Representation of actions.}
\end{figure}

Depending on the action \(A_t\) chosen by the agent following the game
policy that also may incorporate restrictions, as for example occupied
slots cannot receive another O, the state \(S_t\) changes to \(S_{t+1}\)
as the game either stops or a new slot will be occupied by an X of
player 1, in other words, in response of the environment and according
to an unknown transition probability \(p(s'|s,a)\) for \(s=S_t\) and
\(a=A_t\).

In scenarios in which the environment comprises other players, it is
often convenient to let more than one MDP play against each other by
interpreting the actions of each player as the environment of the
respective others.

As discussed, the reward function must be chosen to be meaningful with
respect to the overall goal, and hence, involves a priori knowledge. Our
choice for the Tic-Tac-Toe game could be as follows:

\begin{itemize}
\tightlist
\item
  For every triple
  \((s,a,s')\in\mathcal S\times\mathcal A\times\mathcal S\) such that
  \(s'\) implies a victory of the game, our agent receives a
  \(r(s,a,s')=+1\).
\item
  For \(s'\) implying a loss of the game, our agent receives a
  \(r(s,a,s')=-1\).
\item
  In every other case, i.e., a tie or a continuing game, the agent
  receives \(r(s,a,s')=0\).
\end{itemize}

Adapting the reward function to \(r(s,a,s')=-\frac 12\) in case of a tie
would drive the agent's policy towards avoiding ties as long as it does
not mean a loss of the game since the latter receives an even lower
reward \(r(s,a,s')=-1\). As it maybe known to the reader, in our case,
this choice does not play a big role for the game, as the complexity is
so low that there is an algorithm how to avoid a loss. However, it
illustrates how the choice of a local reward may influence the search of
the agent for an optimal policy that maximizes the overall accumulate
reward.

After this example, let us make the term policy precise, which shall
drive the mechanism by means of which the agent chooses actions
\(A_{t+1}\in\mathcal A\) based on the state \(S_{t+1}\):

\begin{description}
\item[Definition:] ~ 
\hypertarget{policy}{%
\subparagraph{(Policy)}\label{policy}}

We call a map \begin{align}
\pi:\mathcal S\to\mathcal A
  \end{align} a deterministic policy (or sometimes strategy) and say
that an MDP \((S_t,A_t)_{t\in\mathbb N_0}\) follows a policy \(\pi\) if
\begin{align}
\forall t\in\mathbb N: \qquad A_t=\pi(S_t)
  \end{align}
\end{description}

This notion can be extended to depend on the entire history of the
process or model actions stochastically, in which case \(\pi\) takes
values in the probability distributions over actions in \(\mathcal A\)
and the probability that the agents chooses an action \(a\in\mathcal A\)
given a state \(s\in\mathcal S\) is then given by \((\pi(s))(a)\).
Adding further generality, the policy may depend on time
\(t\in\mathbb N_0\), i.e., \((\pi_t(s))(a)\). It turns out that MDPs
that run only for finite times \(t\in\{0,1,\ldots,T\}\) often even
require a time-dependency of the policy in order to be optimal. We will
however focus on the idealization of an ``infinite time horizon'', i.e.,
\(t\in\mathbb N_0\), and stick to deterministic policies.

In order to evaluate a policy in view of the accumulated overall reward
we define its value:

\begin{description}
\item[Definition] ~ 
\hypertarget{policy-value}{%
\subparagraph{(Policy value)}\label{policy-value}}

Let \(\gamma\in[0,1]\), \((S_t,A_t)_{t\in\mathbb N_0}\) an MDP, and
\(\pi\) a policy. We define the policy value for the MDP started at a
state \(s\in\mathcal S\) according to \begin{align}
V_\pi(s) := E\left(\sum_{t=0}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1})\bigg|S_0=s\right),
\label{eq_2}
\tag{E2}
  \end{align} in other words, the expected accumulated reward weighted
with factors \(\gamma^t\).
\end{description}

Should it be well-defined, one can quickly recast the right-hand side of
\(\eqref{eq_2}\) into the form, which will later be useful:
\begin{align}
  V_\pi(s)=E\left(
    R(s,\pi(s),S_1) + \sum_{t=1}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1}) \bigg| S_0=s
  \right).
\end{align}

Note that the value of a policy depends on all future states. The
parameter \(\gamma\) therefore fine-tunes how much influence possible
future states may have. In a probabilistic setting, it could be
problematic to set \(\gamma=1\) as, e.g., due to an aggressive policy of
the agent's opponent in the game, most of these states may never be
reached. In contrast, \(\gamma=0\) would neglect all possible future
rewards and only give value to the immediate reward. For practical
purposes, the value of \(\gamma\) is usually tuned close to but below 1.

Of course, one may ask why the weight was chosen in this special form of
the decay \(\gamma^t\) for \(t\to\infty\). The simple answer is that in
great parts our strategy of proofs will require a certain decay in time
\(t\). Slower decays may be possible will require more technology in the
proofs and are often also practically not as easily implemented.

In a similar fashion one defines another helpful quantity, namely the
state-action value:

\begin{description}
\item[Definition] ~ 
\hypertarget{state-action-value}{%
\subparagraph{(State-action value)}\label{state-action-value}}

Let \(\gamma\in[0,1]\), \((S_t,A_t)_{t\in\mathbb N_0}\) an MDP, and
\(\pi\) a policy. We define the state-action value for the MDP started
at a state \(s\in\mathcal S\) with action \(a\in\mathcal A\) according
to \begin{align}
Q_\pi(s,a):=E\left(
  R(s,a,S_1) + \sum_{t=1}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1}) \bigg| S_0=s, A_0=a
\right).
  \end{align}
\end{description}

Again presuming the well-undefinedness, a useful observation is that for
all \(s\in\mathcal S\) we have \begin{align}
  V_\pi(s)=Q_\pi(s,\pi(s)).
\end{align} Hence, both function contain the information regarding the
accumulated reward. However, \(Q_\pi(s,a)\) will prove convenient to
understand how policy values should be adapted, e.g., how to choose
another \(a\in\mathcal A\) rather than \(a=\pi(s)\) as a next action in
order to optimize the strategy.

Ok, so far that was only definitions and vocabulary but we are over the
hump now. Our program will be to construct the value function (next
module), and afterwards, discuss optimality of a policy, and algorithms
that approximate an optimal policy.

➢ Next session!

\end{document}
