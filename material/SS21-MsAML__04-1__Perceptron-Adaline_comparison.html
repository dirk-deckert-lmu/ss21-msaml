<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__04-1__Perceptron-Adaline_comparison</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-04-1-pdf">MsAML-04-1 [<a href="SS21-MsAML__04-1__Perceptron-Adaline_comparison.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#perceptron-adaline-comparison">Perceptron-Adaline comparison</a>
<ul>
<li><a href="#online,-batch,-and-mini-batch-learning">Online, batch, and mini-batch learning</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="perceptron-adaline-comparison">Perceptron-Adaline comparison</h2>
<p>Our main motivation in replacing the Perceptron by the Adaline algorithm were:</p>
<ol type="1">
<li>For linearly separable training data, the Perceptron stops its updates right after the last incorrect classification is resolved by the last parameter update. Whether this last parameter update is in any sense optimal with respect to the generalization performance is not addressed by the algorithm, and there is no mechanism to even specify a notion for the possible generalization.</li>
<li>For “almost” linearly separable training data, the Perceptron jumps back an forth in its parameter updates depending on the found classification errors instead of nevertheless attempting to find an optimal hyperplane.</li>
</ol>
<p>In turn, the Adaline neuron allows to specify a notion of generalization by means of the minima of the respective empirical loss <span class="math display">\[\begin{align}
  \widehat L(b,w) = \frac1N \sum_{i=1}^N L(y^{(i)},h_{(b,w)}(x^{(i)}))
\end{align}\]</span> for given loss function <span class="math inline">\(L(b,w)\)</span> and activation output <span class="math inline">\(h_{(b,w)}\)</span> parametrized by <span class="math inline">\(w,b\)</span>.</p>
<p><strong>Ad 1. above:</strong> Even in case the gradient descent algorithm of the Adaline managed to find parameters <span class="math inline">\(w,b\)</span> that imply a zero total amount of classification errors, the empirical loss may not yet be close to a local minimum. And therefore, a continued gradient descent may bring the parameters <span class="math inline">\(w,b\)</span> even closer to this empirical minimum.</p>
<p>For the choice of a square error loss one may encounter the following scenario:</p>
<figure>
<img src="04/Perceptron_Adaline_stops.png" alt="Perceptron-Adaline updates." /><figcaption aria-hidden="true">Perceptron-Adaline updates.</figcaption>
</figure>
<p>If the notion of generalization encoded in the loss function resembles our learning tasks better the Adaline has a good chance to generalize better. This is illustrated in the following figure, in which the crosses denote the training data and the dots the unseen test data.</p>
<figure>
<img src="04/Adaline_generalization.png" alt="Potential generalization performance of the Adaline." /><figcaption aria-hidden="true">Potential generalization performance of the Adaline.</figcaption>
</figure>
<p>Though both hyperplanes have no classification errors, the one on the right-hand side seems to generalizes better – at least judged by the “unseen” test data. Here it is important, not to be fooled by any apparent symmetry as depicted in the figures as it may have been due to the unfairly chosen batch of the training data.</p>
<p><strong>Ad 2. above:</strong> Also in the case of training data that is not linearly separable but there are hyperplanes that allow a classification with only few errors, the Adaline has a good chance finding a compromise between maximizing the about of correctly classified training data points and minimizing the misclassifications. Again this strongly depends on whether the notion of generalization supplied by the loss functions resembles the learning task to some a sufficient extend.</p>
<p>The desirable generalization performance depends on several ingredients:</p>
<ul>
<li>The Adaline attempts to minimize the empirical loss <span class="math inline">\(\widehat L(b,w)\)</span> and not the total number of classification errors. Therefore, <span class="math inline">\(L\)</span> should be chosen in a way that the minimization of <span class="math inline">\(\widehat L(b,w)\)</span> also enforces the minimization of the total number of classification errors. At best, it should encode as much a priori knowledge that we have at hand about the learning task.</li>
<li>Furthermore, <span class="math inline">\(L\)</span> must be chosen sufficiently regular to allow for the gradient descent method to work. But even then we are not guaranteed to end up in a global minimum as the following discussion shows.</li>
</ul>
<p><strong>Scenario 1:</strong> For small learning rates <span class="math inline">\(\eta\)</span> and model parameters <span class="math inline">\(w,b\)</span> close to a local minimum of the empirical loss, the gradient descent might get stuck at a local minimum instead of finding a potentially global one:</p>
<figure>
<img src="04/Caught_in_local_min.png" alt="Gradient descent getting caught in local minimum due too a small learning rate \eta." /><figcaption aria-hidden="true">Gradient descent getting caught in local minimum due too a small learning rate <span class="math inline">\(\eta\)</span>.</figcaption>
</figure>
<p>Even if we do not get stuck at a local minimum before descending to a global one, a too small choice of <span class="math inline">\(\eta\)</span> may require a large amount of training epochs (iterations in the gradient descent algorithm).</p>
<p><strong>Scenario 2:</strong> In turn, a too large learning rate <span class="math inline">\(\eta\)</span> may easily cause the gradient descent algorithm to overshoot even a global minimum and potentially lead to an increases empirical loss as illustrated here:</p>
<figure>
<img src="04/Overshooting_a_minimum.png" alt="Overshooting a minimum due to a too large learning rate." /><figcaption aria-hidden="true">Overshooting a minimum due to a too large learning rate.</figcaption>
</figure>
<p>Both scenarios are the price for introducing an ad hoc constant learning rate. As it could already be seen from the exercises, the success of the gradient descent depends on the sign and magnitude of the Taylor remainder beyond the linear correction of the empirical loss, which can be estimated by the corresponding Hessian. Therefore, more sophisticated algorithms typically rely on the second-order derivatives; e.g., most prominently the Newton’s method. Computing the second-order derivative is of course not a problem for our single Adaline application. However, later on, when considering networks of Adaline neuron, the computation of second-order derivatives of the loss function becomes computationally very costly.</p>
<p>Therefore, despite the discussed problems, gradient descent is the method of choice for training larger networks of Adaline neurons and the fine-tuning of learning rates <span class="math inline">\(\eta\)</span> and loss functions <span class="math inline">\(L\)</span> that work together well will often require a lot of finesse and patience.</p>
<p>Furthermore, there are several improvements that can be made when allowing an adaptive learning rate during the gradient descent optimization. For example, in the <span class="math inline">\(t\)</span>-th learning epoch we might adjust the learning rate according to <span class="math display">\[\begin{align}
  \eta = \frac{a}{b+t},\qquad a,b&gt;0.
\end{align}\]</span> This implies that <span class="math inline">\(\eta(t)\)</span> is larger for early epochs <span class="math inline">\(t\)</span> and smaller for later ones – then, it ultimately enforce a convergence, though, not necessarily at a local minimum.</p>
<p>More sophisticated methods, e.g., implement a Newtonian dynamics for a fictional body moving in parameter space of <span class="math inline">\((b,w)\)</span>, interpreting <span class="math inline">\(\widehat L(b,w)\)</span> as a “potential” that exerts a force proportional to <span class="math inline">\(-\nabla_{(b,w)}\widehat L(b,w)\)</span> on that body, and in addition, introducing a friction that damps the body’s velocity continuously. The inherent inertia of the body then enables it to “roll” out of local minima unless they are sufficiently deep for the friction to take over. Others implement, in addition to the gradient descent, a random motion with decreasing temperature. The latter allows to randomly “jump” out of local minima unless they are sufficiently deep so that the remaining temperature does not allow for a sufficiently large jump anymore in order to escape.</p>
<p>You can find a great overview of some of these methods here: <a href="http://sebastianruder.com/optimizing-gradient-descent/" class="uri">http://sebastianruder.com/optimizing-gradient-descent/</a></p>
<p>In summary, while the success of a supervised training will always depend on some finesse in the fine-tuning of the additional parameters of our optimization algorithm of choice for minimizing the empirical loss, a advantageous choice of loss function will require some a priori knowledge that cannot be extracted “for free” from simply looking at the known training alone. This fact will become soberingly clear in our next module about the “No free lunch” theorems.</p>
<h3 id="online-batch-and-mini-batch-learning">Online, batch, and mini-batch learning</h3>
<p>There is another noteworthy difference in the update rules of the Perceptron and the Adaline neurons; at least in the way we have introduced them.</p>
<ul>
<li>The Perceptron update rule is an <em>online learner</em> as the model parameter updates occur right after inspection of a single training data points.</li>
<li>The Adaline update rule is a <em>batch learner</em> as it first considers the whole batch of training data by computing the gradient of the empirical loss <span class="math inline">\(\widehat L(b,w)\)</span> before then updating the model parameters.</li>
</ul>
<p>While online learner typically introduce a stronger dependence on the sequence of training data, batch learner are more computationally expensive.</p>
<p>Clearly, also the Perceptron can be turned into a batch learner by conducting only one average parameter update per epoch, i.e., after the inspection of the entire batch of training data. Likewise, we may turn our Adaline implementation into an online learner by performing for each inspected training data point <span class="math inline">\((x^{(i)},y^{(i)})\)</span> an immediate parameter update according to the loss function <span class="math inline">\(L\)</span>, i.e., <span class="math display">\[\begin{align}
  w &amp; \leftarrowtail w^\text{new} := w - \eta \frac{\partial L(y^{(i)},h_{(b,w)}(x^{(i)}))}{\partial w}\\
  b &amp; \leftarrowtail b^\text{new} := b - \eta \frac{\partial L(y^{(i)},h_{(b,w)}(x^{(i)}))}{\partial b},
\end{align}\]</span> instead of the using the empirical loss <span class="math inline">\(\widehat L(b,w)\)</span> of entire batch.</p>
<p>A compromise between these two extremes are the so-called <em>mini-batch</em> learners, which, per learning epoch, divide their training data <span class="math inline">\(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\)</span> into disjoint mini-batches <span class="math inline">\(s_k\)</span>, <span class="math inline">\(k=1,\ldots,M\)</span>, each containing a similar amount of training data samples and execute the update rules after inspection of each such mini-batch <span class="math inline">\(s_k\)</span>. In order to reduce the dependence on the sequence, this division can be done randomly. This is also today’s most common method of training also larger Adaline networks. When the gradient descent algorithm is used in combination of such a randomized mini-batch approach, it is often referred to as <em>stochastic gradient descent</em>.</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
