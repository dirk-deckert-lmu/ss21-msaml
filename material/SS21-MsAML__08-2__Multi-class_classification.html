<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__08-2__Multi-class_classification</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-08-2-pdf">MsAML-08-2 [<a href="SS21-MsAML__08-2__Multi-class_classification.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#multi-class-classification">Multi-class classification</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="multi-class-classification">Multi-class classification</h2>
<p>Before we continue our discussion of how to best influence the generalization properties of the considered class of models to unseen data, let us introduce also the long announced family of multi-class Adline neurons, i.e., linear classification models that can cope with <span class="math inline">\(|\mathcal Y|&gt;2\)</span>.</p>
<p>For this purpose, let us regard <span class="math inline">\(d\)</span>-dimensional features and <span class="math inline">\(c\)</span> distinct class labels encoded by numbers <span class="math inline">\(1,2,\ldots, c\)</span>, in other words: <span class="math display">\[\begin{align}
  \mathcal X=\mathbb R^d, \qquad \mathcal Y=\{1,2,...,\ldots,c\}.
\end{align}\]</span></p>
<p>Recall our Adaline neuron setup for binary classification:</p>
<figure>
<img src="08/Binary_Adaline.png" alt="The Adaline neuron for binary classification." /><figcaption aria-hidden="true">The Adaline neuron for binary classification.</figcaption>
</figure>
<p>and note the allowing small adaptions in yellow:</p>
<figure>
<img src="08/Multi-class_Adaline.png" alt="The multi-class Adaline neuron." /><figcaption aria-hidden="true">The multi-class Adaline neuron.</figcaption>
</figure>
<p>Now, the weight parameters are not given by a vector <span class="math inline">\(w\)</span> in <span class="math inline">\(\mathbb R^d\)</span> but by a matrix <span class="math inline">\(W\)</span> in <span class="math inline">\(\mathbb R^{c\times d}\)</span>. Hence, the vector <span class="math inline">\(W\cdot x\in\mathbb R^c\)</span> is given by the matrix product <span class="math inline">\(\cdot\)</span> of <span class="math inline">\(W\)</span> and the input feature vector <span class="math inline">\(x\in\mathbb R^d\)</span>. Naturally, the bias is also given by a vector <span class="math inline">\(b\in\mathbb R^c\)</span>.</p>
<p>The predicted class is then given by the output of the hypothesis <span class="math display">\[\begin{align}
  \mathbb R^d &amp;\to \{1,\ldots, c\}\\
  x &amp;\mapsto h(x):=\text{argmax&#39;}\, \alpha(W\cdot x+b).
\end{align}\]</span> The heuristics behind the <span class="math inline">\(\text{argmax}\)</span> function is simply that we might think of the component <span class="math inline">\(k\in\{1,\ldots,c\}\)</span> of the activation output vector <span class="math inline">\(\alpha(W\cdot x+b)\in\mathbb R^c\)</span> as some kind of confidence. If that was the case, the largest component <span class="math inline">\(k\)</span> would indicate that the correct label should indeed be the <span class="math inline">\(k\)</span>-th one. In the end, this is only a mechanism to discretized the activation output and chose one of the <span class="math inline">\(c\)</span> classes. If the largest component appears more than once in the components of <span class="math inline">\(\alpha(W\cdot x+b)\)</span>, the return value of the <span class="math inline">\(\text{argmax}\)</span> function is a set of all such indices <span class="math inline">\(k\)</span>, and the Adaline neuron has to implement an additional rule to disambiguate the output, e.g., by choosing the one with the smallest index <span class="math inline">\(k\)</span>. To remind us about this additional rule we wrote <span class="math inline">\(\text{argmax&#39;}\)</span> with an apostrophe.</p>
<p>Also the quadratic loss generalizes with ease to the multi-class setting when employing, e.g., the so-called <em>one-hot</em> encoding of the labels in <span class="math inline">\(\mathcal Y\)</span> by means of <span class="math display">\[\begin{align}
  \tilde\cdot:\{1,\ldots, c\} &amp;\to 
  \mathbb R^c\\
  y&amp;\mapsto \tilde y(y):=\widehat e_y
\end{align}\]</span> where <span class="math inline">\(\widehat e_i\)</span>, <span class="math inline">\(i=1,\ldots, c\)</span>, is the unit vector comprising zero components except for the component <span class="math inline">\(i\)</span> which equals one, i.e., <span class="math inline">\((\widehat e_i)_j=\delta_{ij}\)</span>. Similarly, we use the tilde notation to write the prediction of our classifier one-hot encoded: <span class="math display">\[\begin{align}
  \tilde h(x) = \widehat e_{h(x)}.
\end{align}\]</span></p>
<p>The loss function then takes the form <span class="math display">\[\begin{align}
  L:\mathbb R^c\times\mathbb R^c &amp;\to\mathbb R_0^+\\
  (\tilde y,\tilde y&#39;) &amp;\mapsto L(\tilde y,\tilde y&#39;) = \frac12 \left\|\tilde y - \tilde y&#39;\right\|_2^2,
\end{align}\]</span> where <span class="math inline">\(\|\cdot\|_2\)</span> is the <span class="math inline">\(l_2\)</span>-norm on <span class="math inline">\(\mathbb R^c\)</span> and the overset tilde again remind us that the <span class="math inline">\(\tilde y\)</span> is not the actual label in <span class="math inline">\(\mathcal Y\)</span> but the one-hot encoded label in <span class="math inline">\(\widehat e_y\)</span> and the activation output <span class="math inline">\(\tilde y&#39;\)</span> to which it is compared is also a <span class="math inline">\(\mathbb R^c\)</span> vector.</p>
<p>The empirical loss function can then be expressed as <span class="math display">\[\begin{align}
  \widehat L(b,W) = \frac1N \sum_{i=1}^N L(\tilde y^{(i)},\alpha(W\cdot x^{(i)}+b))
\end{align}\]</span> again using the one-hot encoding <span class="math inline">\(\tilde y^{(i)}=\widehat e_{y^{(i)}}\)</span>.</p>
<p>With this choice also the update rule follows with ease: <span class="math display">\[\begin{align}
  b_{j} 
  &amp; \leftarrowtail 
  b^\text{new}_{j}
  := 
  b_{j}- \eta \frac{\partial \widehat L(b,W)}{\partial b_{j}},
  \\
  W_{jk} 
  &amp; \leftarrowtail 
  W^\text{new}_{jk}
  := 
  W_{jk}- \eta \frac{\partial \widehat L(b,W)}{\partial W_{jk}},
\end{align}\]</span> where, e.g., <span class="math inline">\(W_{jk}\)</span> denotes the coefficient in row <span class="math inline">\(j\)</span> and column <span class="math inline">\(k\)</span> of the matrix <span class="math inline">\(W\in\mathbb R^{c\times d}\)</span>.</p>
<dl>
<dt>👀</dt>
<dd>Compute the explicit update rule.
</dd>
</dl>
<p>Also for other choices of activation and loss function one readily finds generalizations to the multi-class setting. Let us, e.g., consider the generalization to logistic activation with cross-entropy loss, which we shall introduce with a heuristic argument.</p>
<p>In this case, we would assign the activation function <span class="math display">\[\begin{align}
  \alpha_i(z) = \frac{1}{1+e^{-z_i}}, \qquad z=(z_1,\ldots, z_c)
\end{align}\]</span> and think of <span class="math display">\[\begin{align}
  p^{(i)}_j = \alpha_j(W\cdot x^{(i)}+b), \qquad i=1,\ldots, d, \quad \text{and} \quad j=1,\ldots, c,
\end{align}\]</span> as “probability”. The “probability” for a correct classification would thus read <span class="math display">\[\begin{align}
  P\left(\tilde y_j^{(i)}=\tilde h(x^{(i)})_j\right)
  =
  \left(p^{(i)}_j\right)^{y^{(i)}}
  \left(1-p^{(i)}_j\right)^{1-y^{(i)}}.
\end{align}\]</span> When assuming “independence” we could express the overall “probability” of no classification error on the training set by means of <span class="math display">\[\begin{align}
  P\left(\forall i=1,\ldots,N: y^{(i)}=h(x^{(i)})\right)
  &amp;=
  \prod_{i=1}^N 
  P\left(y^{(i)}=h(x^{(i)})\right)
  \\
  &amp; =
  \prod_{i=1}^N 
  P\left(\tilde y^{(i)}=\tilde h(x^{(i)})\right)
  \\
  &amp;=
  \prod_{i=1}^N\prod_{j=1}^c P\left(\tilde y^{(i)}_j=\tilde h(x^{(i)})_j\right).
\end{align}\]</span> To maximize this “probability” we could regard the empirical loss function of the form <span class="math display">\[\begin{align}
  \widehat L(b,W)
  &amp;=-\frac{1}{N} \log\left(
    P\left(\forall i=1,\ldots,N: y^{(i)}=h(x^{(i)})\right)
  \right)
  \\
  &amp;= -\frac{1}{N} \sum_{i=1}^N\sum_{j=1}^c \left[
    y^{(i)}_j \log p^{(i)}_j
    +
    (1-y^{(i)}_j) \log (1-p^{(i)}_j),
  \right]
\end{align}\]</span> which is an old acquaintance, i.e., the cross-entropy, now only for the multi-class setting.</p>
<p>Note however that we put the words “probability” and “independence” in quotation marks as these words were just used for the heuristics. There is no mechanism in the model that would justify to consider the <span class="math inline">\(p^{(i)}_j\)</span> as relative frequencies or even probabilities. These coefficients are simple the <span class="math inline">\(j\)</span>-th component of the activation output for the <span class="math inline">\(i\)</span>-th training datum. The only thing that the <span class="math inline">\(p^{(i)}_j\)</span>’s have in common with a probability is that they are between 0 and 1.</p>
<p>In order to make the components <span class="math inline">\(j\)</span> of the output activation <span class="math inline">\(\alpha(W\cdot x+b)\)</span> look even more like a probability distribution we could employ the following so-called <em>soft-max</em> activation <span class="math display">\[\begin{align}
  \alpha(z) := \left(\frac{e^{z_j}}{\sum_{k=1}^c e^{z_k}}\right)_{j=1,\ldots, c}.
\end{align}\]</span> If we set again <span class="math display">\[\begin{align}
  p^{(i)}_j = \alpha(W\cdot x^{(i)}+b)_j
\end{align}\]</span> for this new activation function <span class="math inline">\(\alpha(z)\)</span>, we readily have <span class="math display">\[\begin{align}
  \sum_{j=1}^c p^{(i)}_j = 1,
\end{align}\]</span> so that <span class="math inline">\((p^{(i)}_j)_{j=1,\ldots,c}\)</span> can be regarded a discrete “probability” distribution.</p>
<p>The latter activation function is called soft-max because, by its definition, scale differences in the <span class="math inline">\(z_j\)</span> for <span class="math inline">\(j=1,\ldots,c\)</span> are exponentiated so that the corresponding output activation is close to a unit vector. Although it is computationally more costly is is known to be rather well-behaved during training.</p>
<dl>
<dt>👀</dt>
<dd>Does the Adaline with soft-max activation and cross-entropy loss suffer from the vanishing gradient problem?
</dd>
</dl>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
