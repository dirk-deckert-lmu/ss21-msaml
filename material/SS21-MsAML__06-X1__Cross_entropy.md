# Exercise 06-1: Cross entropy [[PDF](SS21-MsAML__06-X1__Cross_entropy.pdf)]

a) Given a discrete probability space $(\Omega,P)$ and another measure $Q$ on $\Omega$, prove 

$$H(P,Q)\geq H(P)$$

b) Show why the Adaline with $\alpha(z)=\tanh(z)$ and cross-entropy loss avoids the vanishing gradients problem. 
