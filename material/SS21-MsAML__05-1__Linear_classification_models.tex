% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-05-1-pdf}{%
\section{\texorpdfstring{MsAML-05-1
{[}\href{SS21-MsAML__05-1__Linear_classification_models.pdf}{PDF}{]}}{MsAML-05-1 {[}PDF{]}}}\label{msaml-05-1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{models-of-linear-classification}{Models of linear
  classification}

  \begin{itemize}
  \tightlist
  \item
    \protect\hyperlink{activation-functions}{Activation functions}
  \item
    \protect\hyperlink{loss-functions}{Loss functions}
  \end{itemize}
\end{enumerate}

Back to \href{index.pdf}{index}.

In order, to give you enough time to explore the corresponding changes
in your implementation of the Adaline neuron, this time the lecture
notes are rather short.

\hypertarget{models-of-linear-classification}{%
\subsection{Models of linear
classification}\label{models-of-linear-classification}}

In stepping from the Perceptron to the Adaline neuron we gained two
advantages:

\begin{itemize}
\tightlist
\item
  We can make direct use of regular optimization theory, which, e.g.,
  for the method of gradient descent, requires a differentiable loss
  function.
\item
  We can more directly influence the intended notion of generalization
  by the choice of loss \(L(y,y')\) and activation \(\alpha(z)\)
  functions.
\end{itemize}

These degree of freedom already lead to a rich class of linear
classification models parametrized by \(L\) and \(\alpha\), which we
shall discuss more closely in the following chapters.

In this module, we look a bit closer at potential choices and
constraints to keep in mind for:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  The choice of activation functions.
\item
  The choice of loss functions.
\end{enumerate}

\hypertarget{activation-functions}{%
\subsubsection{Activation functions}\label{activation-functions}}

So far, for our binary classification models with, e.g.,
\(\mathcal Y=\{-1,+1\}\), we have been using a linear activation
\begin{align}
  \alpha(z)=z
\end{align} and quadratic loss \begin{align}
  L(y,y')=\frac12|y-y'|^2.
\end{align} This gave rise to the activation output \begin{align}
  y'=\alpha(w\cdot x+b)=w\cdot x+b
\end{align} and the hypothesis \begin{align}
  h^\alpha_{(b,w)}(x)=\text{sign}\alpha(w\cdot x+b)=\text{sign}(w\cdot x+b).
\end{align} The intuition behind the quadratic loss was to somehow
measure the distance between the actual class label \(y\in\mathcal Y\)
and the produced activation output \(y'\) corresponding to input
features \(x\in\mathcal X\).

Typically, as in our example of binary classification (but also for many
regression tasks), the labels \(y\) in space \(\mathcal Y\) are bounded.
The activation output \(y'\) in the case of linear activation, however,
is unbounded. This potential mismatch in order of magnitude between
\(y\) and \(y'\) causes the resulting empirical loss function
\begin{align}
  \widehat L(b,w) = \frac1N\sum_{i=1}^NL(y^{(i)},h^\alpha_{(b,w)}(x^{(i)}))
  = \frac1N \sum_{i=1}^N \frac12\left|y^{(i)}-(w\cdot x^{(i)}+b)\right|^2
  \tag{E1}
  \label{eq_1}
\end{align} to behave much different from the empirical error
\begin{align}
  \frac1N \sum_{i=1}^N 1_{y^{(i)}\neq h^\alpha_{(b,w)}(x^{(i)})}
  =\frac1N \sum_{i=1}^N 1_{y^{(i)}\neq \text{sign}(w\cdot x^{(i)}+b)}.
  \tag{E2}
  \label{eq_2}
\end{align}

In our first implementation of the Adaline neuron we did not encounter a
particular problem despite this mismatch of activation function and loss
function; the main reason being that the Adaline neuron has an
undetermined length scale at its expense \begin{align}
  w\cdot x+b \quad \substack{\geq\\<} \quad 0 \qquad \Leftrightarrow \qquad \lambda w\cdot x+\lambda b  \quad \substack{\geq\\<} \quad 0,
  \qquad
  \lambda\in\mathbb R^+.
\end{align} As we will discuss later on, the resulting, due to the
potentially large difference \(y^{(i)}-(w\cdot x+b)\), the in turn
potentially large parameter updates \begin{align}
  w \leftarrowtail w^\text{new} 
  &:= w - \eta \frac{\partial L(y^{(i)},h^\alpha_{(b,w)}(x^{(i)}))}{\partial w}\\
  &= w + \eta \sum_{i=1}^N \left[y^{(i)}-(w\cdot x^{(i)}+b)\right]x^{(i)}\\
  b \leftarrowtail b^\text{new} 
  &:= b - \eta \frac{\partial L(y^{(i)},h^\alpha_{(b,w)}(x^{(i)}))}{\partial b}\\
  &=  w + \eta \sum_{i=1}^N \left[y^{(i)}-(w\cdot x^{(i)}+b)\right]\\
\end{align} were to some extend even advantageous for the training speed
in case of ill-conditioned initial parameters. However, it turns out
that when generalization the Adaline neuron to multi-dimensional output,
i.e., \(y'\in\mathbb R^c\) with \(c>1\), and building a network of such
Adaline neurons by connecting the outputs components of the preceding
neuron to inputs of the succeeding one, such large changes somewhere in
the network turn out to be rather problematic during training, and
therfore, should be avoided by making the order of magnitudes of \(y\)
and \(y'\) more comparable.

As a general idea, in order to make the empirical loss \(\eqref{eq_1}\)
more comparable to the empirical error \(\eqref{eq_2}\), one may
saturate the activation output in a similar way as the
\(\text{sign}(\cdot)\) function does, while retaining the
differentiability. Good candidates for such activation functions are the
so-called \emph{signmoid} functions:

\begin{description}
\item[Definition] ~ 
\hypertarget{sigmoid-function}{%
\subparagraph{(Sigmoid function)}\label{sigmoid-function}}

We call a map \(\sigma:\mathbb R\to\mathbb R\) a sigmoid function if it
is a bounded differential function with a non-negative derivative.
\end{description}

The rational behind this choice the following one:

\begin{description}
\tightlist
\item[👀]
Show that any sigmoid has horizontal asymptotes for \(z\to\pm \infty\).
\end{description}

Two frequent examples are:

\begin{itemize}
\tightlist
\item
  The logistic function \begin{align}
    \alpha:\mathbb R&\to[0,1]\\
    z&\mapsto \frac{1}{1+e^{-z}};
  \end{align}
\item
  The hyperbolic tangent: \begin{align}
    \alpha:\mathbb R&\to[-1,1]\\
    z&\mapsto \tanh(z)=\frac{e^z-e^{-z}}{e^z+e^{-z}}.
  \end{align}
\end{itemize}

In case of binary classification, the logistic function is well suited
for the label representation \(\mathcal Y=\{0,1\}\), while the \(\tanh\)
for \(\mathcal Y=\{-1,+1\}\). Obviously, by mapping the labels
bijectively between \(\{0,1\}\) and \(\{-1,+1\}\), both choices can be
use for binary classification problems and we switch between both in
some of the implementations.

\begin{description}
\tightlist
\item[Remark.]
Historically, sigmoids were discussed already early on as they model a
saturation of the activation signal. Later on we will discuss another of
their advantages in more detail, namely that these sigmoids ``slip in''
a non-linearity into our yet linear classification models. When forming
multiple layers of such non-linear Adaline neurons with
multi-dimentional outputs by sucessively connecting their output and
inputs, we will show that these non-linearities give the resulting
network classification capabilities well beyond only the linear
separable case.
\end{description}

Let us experiment with the activation functions in our implementation:

\begin{description}
\tightlist
\item[👀]
Adapt our Adaline implementation, in particular, the corresponding
update rule, so that it can use the hyperbolic tangent or the logistic
function (the latter requires the mentioned change of labels) as
activation.
\end{description}

\hypertarget{loss-functions}{%
\subsubsection{Loss functions}\label{loss-functions}}

Replacing the activation function by a sigmoid in our Adaline neuron
implementation and adapting the update rule correspondingly will work
out-of-the-box for initial model parameters close to \(w=0,b=0\).

However, one may realize that, for extreme choices of model parameters
\(|w|,b\gg 1\), it will require many more epochs of training to achieve
a similar empirical error as compared to the same setting with a linear
activation function.

This training phenomenon is due to the fact, with a sigmoid function
\(\alpha(z)\), the Adaline neuron can now easily be driven in regions of
saturation, i.e., in regions in which even large changes of \(z\) do not
imply correspondingly large changes in \(\alpha(z)\).

\begin{figure}
\centering
\includegraphics{05/sigmoid_saturation.png}
\caption{Saturation of the activation output due to a sigmoid function.}
\end{figure}

Suppression of large output signals by introducing a bounded activation
\(\alpha(z)\) introduces the so-called problem of \emph{slow learning}
for Adaline neurons in regions of saturation.

To understand this better, let us consider the case of binary
classification with labels \(\mathcal Y=\{0,1\}\) and the logistic
function \begin{align}
  \alpha(z)=\frac{1}{1+e^{-z}}
\end{align} as activation. Correspondingly, we find for the update rule
\begin{align}
  w & \leftarrowtail w^\text{new} 
  := w + \eta \sum_{i=1}^N \left[y^{(i)}-\alpha(w\cdot x^{(i)}+b)\right]\alpha'(w\cdot x+b)x^{(i)}\\
  b & \leftarrowtail b^\text{new} 
  := b + \eta \sum_{i=1}^N \left[y^{(i)}-\alpha(w\cdot x^{(i)}+b)\right]\alpha'(w\cdot x+b),
\end{align} where the derivative of the activation function is given by
\begin{align}
  \alpha'(z)=\frac{e^{-z}}{(1+e^{-z})^2}=\alpha(z)(1-\alpha(z)).
\end{align}

Clearly, for large values of \(z\), the derivative \(\alpha'(z)\)
becomes fairly small. In turn, the respective relative updates
\((w^\text{new}-w)\) and \((b^\text{new}-b)\) are small and it takes the
Adaline neuron many epochs of training to leave regions of saturation.
The problem of too small \(\alpha'(z)\) appears in supervised learning
in several situation and is also referred to as \emph{vanishing gradient
problem}.

This effect can be best observed when plotting the empirical loss ageist
the training epochs. Typical plots may look like:

\begin{figure}
\centering
\includegraphics{05/slow_learning.png}
\caption{Effect of slow learning for saturated Adaline neurons.}
\end{figure}

Ideally, it would be nice to maintain the order of magnitude of the
``fast'' updates that we found for the case of linear activation also in
the case of sigmoid functions. Regarding the derivative of the empirical
loss function from which our general Adaline update rule computes its
parameters updates: \begin{align}
  \frac{\partial \widehat L_{(\bar w)}}{\partial \bar w}
  =\frac1 N \sum_{i=1}^N 
  \frac{\partial L(y^{(i)},z)}{\partial z}\Big|_{z=\alpha(\bar w\cdot \bar x^{(i)})}
  \alpha'(\bar w\cdot \bar x^{(i)}) \bar x^{(i)},\\
\end{align} where \begin{align}
  \bar w=(b,w)^T,\qquad \bar x^{(i)}=(1,x^{(i)})^T,
\end{align} we observe that for this to happen, the potentially small
factor \(\alpha'(z)\) would then need to be compensated. In other words,
the loss function \(L(y,y')\) ideally needs to be adapted such that
\begin{align}
  \frac{\partial L(y^{(i)},z)}{\partial z} \sim \frac{1}{\alpha'(z)}.
  \tag{E3}
  \label{eq_3}
\end{align}

There are at least two routes to proceed:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Guess a good candidate \(L(y,y')\), simply by integrating
  \(\eqref{eq_3}\).
\item
  Argue theoretically for a more meaningful expression for \(L(y,y')\)
  than our ad hoc choice of quadratic loss in the case of binary
  classification.
\end{enumerate}

Route 1. will make up for a good task that we will look at in the
exercises:

\begin{description}
\tightlist
\item[👀]
Integrate \(\eqref{eq_3}\) to find a good choice of \(L(y,y')\) and
adapt your Adaline implementation accordingly. Observe that the slow
learning problem is absent even for ill-conditioned initial model
parameters \(|w|,b\gg 1\).
\end{description}

And route 2. will invite us for a small excursion to information entropy
measures.

➢ Next session!

\end{document}
