# MsAML-08-2 [[PDF](SS21-MsAML__08-2__Multi-class_classification.pdf)]

1. [Multi-class classification](#multi-class-classification)

Back to [index](index.md).


## Multi-class classification

Before we continue our discussion of how to best influence the generalization
properties of the considered class of models to unseen data, let us introduce
also the long announced family of multi-class Adline neurons, i.e., linear
classification models that can cope with $|\mathcal Y|>2$.

For this purpose, let us regard $d$-dimensional features and $c$ distinct class
labels encoded by numbers $1,2,\ldots, c$, in other words:
\begin{align}
  \mathcal X=\mathbb R^d, \qquad \mathcal Y=\{1,2,...,\ldots,c\}.
\end{align}

Recall our Adaline neuron setup for binary classification:

![The Adaline neuron for binary classification.](08/Binary_Adaline.png)

and note the allowing small adaptions in yellow:

![The multi-class Adaline neuron.](08/Multi-class_Adaline.png)

Now, the weight parameters are not given by a vector $w$ in $\mathbb R^d$ but
by a matrix $W$ in $\mathbb R^{c\times d}$. Hence, the vector $W\cdot
x\in\mathbb R^c$ is given by the matrix product $\cdot$ of $W$ and the input feature
vector $x\in\mathbb R^d$. Naturally, the bias is also given by a vector
$b\in\mathbb R^c$.

The predicted class is then given by the output of the hypothesis
\begin{align}
  \mathbb R^d &\to \{1,\ldots, c\}\\
  x &\mapsto h(x):=\text{argmax'}\, \alpha(W\cdot x+b).
\end{align}
The heuristics behind the $\text{argmax}$ function is simply that we might
think of the component $k\in\{1,\ldots,c\}$ of the activation output vector
$\alpha(W\cdot x+b)\in\mathbb R^c$ as some kind of confidence. If that was the
case, the largest component $k$ would indicate that the correct label should
indeed be the $k$-th one. In the end, this is only a mechanism to discretized
the activation output and chose one of the $c$ classes. If the largest
component appears more than once in the components of $\alpha(W\cdot x+b)$, the
return value of the $\text{argmax}$ function is a set of all such indices $k$, and
the Adaline neuron has to implement an additional rule to disambiguate the
output, e.g., by choosing the one with the smallest index $k$.  To remind us about 
this additional rule we wrote $\text{argmax'}$ with an apostrophe.

Also the quadratic loss generalizes with ease to the multi-class setting
when employing, e.g., the so-called *one-hot* encoding of the labels in
$\mathcal Y$ by means of 
\begin{align}
  \tilde\cdot:\{1,\ldots, c\} &\to 
  \mathbb R^c\\
  y&\mapsto \tilde y(y):=\widehat e_y
\end{align}
where $\widehat e_i$, $i=1,\ldots, c$, is the unit vector comprising zero components except for
the component $i$ which equals one, i.e., $(\widehat e_i)_j=\delta_{ij}$.
Similarly, we use the tilde notation to write the prediction of our classifier one-hot encoded:
\begin{align}
  \tilde h(x) = \widehat e_{h(x)}.
\end{align}

The loss function then takes the form 
\begin{align}
  L:\mathbb R^c\times\mathbb R^c &\to\mathbb R_0^+\\
  (\tilde y,\tilde y') &\mapsto L(\tilde y,\tilde y') = \frac12 \left\|\tilde y - \tilde y'\right\|_2^2,
\end{align}
where $\|\cdot\|_2$ is the $l_2$-norm on $\mathbb R^c$ and the overset
tilde again remind us that the $\tilde y$ is not the actual label in $\mathcal
Y$ but the one-hot encoded label in $\widehat e_y$ and the activation output
$\tilde y'$ to which it is compared is also a $\mathbb R^c$ vector.

The empirical loss function can then be expressed as
\begin{align}
  \widehat L(b,W) = \frac1N \sum_{i=1}^N L(\tilde y^{(i)},\alpha(W\cdot x^{(i)}+b))
\end{align}
again using the one-hot encoding $\tilde y^{(i)}=\widehat e_{y^{(i)}}$.

With this choice also the update rule follows with ease:
\begin{align}
  b_{j} 
  & \leftarrowtail 
  b^\text{new}_{j}
  := 
  b_{j}- \eta \frac{\partial \widehat L(b,W)}{\partial b_{j}},
  \\
  W_{jk} 
  & \leftarrowtail 
  W^\text{new}_{jk}
  := 
  W_{jk}- \eta \frac{\partial \widehat L(b,W)}{\partial W_{jk}},
\end{align}
where, e.g., $W_{jk}$ denotes the coefficient in row $j$ and column $k$ of the matrix
$W\in\mathbb R^{c\times d}$.

👀
: Compute the explicit update rule.

Also for other choices of activation and loss function one readily finds
generalizations to the multi-class setting. Let us, e.g., consider the
generalization to logistic activation with cross-entropy loss, which we shall
introduce with a heuristic argument.

In this case, we would assign the activation function
\begin{align}
  \alpha_i(z) = \frac{1}{1+e^{-z_i}}, \qquad z=(z_1,\ldots, z_c)
\end{align}
and think of 
\begin{align}
  p^{(i)}_j = \alpha_j(W\cdot x^{(i)}+b), \qquad i=1,\ldots, d, \quad \text{and} \quad j=1,\ldots, c,
\end{align}
as "probability". The "probability" for a correct classification would thus read
\begin{align}
  P\left(\tilde y_j^{(i)}=\tilde h(x^{(i)})_j\right)
  =
  \left(p^{(i)}_j\right)^{y^{(i)}}
  \left(1-p^{(i)}_j\right)^{1-y^{(i)}}.
\end{align}
When assuming "independence" we could express the overall "probability" of no classification error on the training set by means of
\begin{align}
  P\left(\forall i=1,\ldots,N: y^{(i)}=h(x^{(i)})\right)
  &=
  \prod_{i=1}^N 
  P\left(y^{(i)}=h(x^{(i)})\right)
  \\
  & =
  \prod_{i=1}^N 
  P\left(\tilde y^{(i)}=\tilde h(x^{(i)})\right)
  \\
  &=
  \prod_{i=1}^N\prod_{j=1}^c P\left(\tilde y^{(i)}_j=\tilde h(x^{(i)})_j\right).
\end{align}
To maximize this "probability" we could regard the empirical loss function of the form
\begin{align}
  \widehat L(b,W)
  &=-\frac{1}{N} \log\left(
    P\left(\forall i=1,\ldots,N: y^{(i)}=h(x^{(i)})\right)
  \right)
  \\
  &= -\frac{1}{N} \sum_{i=1}^N\sum_{j=1}^c \left[
    y^{(i)}_j \log p^{(i)}_j
    +
    (1-y^{(i)}_j) \log (1-p^{(i)}_j),
  \right]
\end{align}
which is an old acquaintance, i.e., the cross-entropy, now only for the
multi-class setting. 

Note however that we put the words "probability" and "independence" in
quotation marks as these words were just used for the heuristics.  There is no
mechanism in the model that would justify to consider the $p^{(i)}_j$ as
relative frequencies or even probabilities. These coefficients are simple the
$j$-th component of the activation output for the $i$-th training datum. The
only thing that the $p^{(i)}_j$'s have in common with a probability is that
they are between 0 and 1.

In order to make the components $j$ of the output activation $\alpha(W\cdot
x+b)$ look even more like a probability distribution we could employ the
following so-called *soft-max* activation
\begin{align}
  \alpha(z) := \left(\frac{e^{z_j}}{\sum_{k=1}^c e^{z_k}}\right)_{j=1,\ldots, c}.
\end{align}
If we set again
\begin{align}
  p^{(i)}_j = \alpha(W\cdot x^{(i)}+b)_j
\end{align}
for this new activation function $\alpha(z)$, we readily have
\begin{align}
  \sum_{j=1}^c p^{(i)}_j = 1,
\end{align}
so that $(p^{(i)}_j)_{j=1,\ldots,c}$ can be regarded a discrete "probability"
distribution.

The latter activation function is called soft-max because, by its definition,
scale differences in the $z_j$ for $j=1,\ldots,c$ are exponentiated so that the
corresponding output activation is close to a unit vector. Although it is
computationally more costly is is known to be rather well-behaved during training.

👀
: Does the Adaline with soft-max activation and cross-entropy loss suffer from
the vanishing gradient problem?
