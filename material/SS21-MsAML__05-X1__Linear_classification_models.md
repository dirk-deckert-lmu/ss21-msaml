# Exercise 05-1: Linear classification models [[PDF](SS21-MsAML__05-X1__Linear_classification_models.pdf)]

(a) Show that $\forall$ sigmoid function $\sigma: \mathbb{R}\to\mathbb{R}^{+}, \ \exists A^{+},A^{-}\in\mathbb{R}$ with $\lim_{z\to\pm\infty}\sigma(z)=A^{\pm}$ for $+$ and $-$ s.t. $A^{-}\leq A^{+}$.

(b) Adapt our Adaline implementation to use two new activation functions: hyperpolic tangent $\alpha(z) = \tanh(z)$ and logistic function $\alpha(z) = \frac{1}{1+e^{-z}}$. Note that for the latter we need to adapt the class labels $Y=\{0,1\}$ instead of $\{-1,1\}$.

(c) Demonstrate the problem of vanishing gradients numerically by a direct comparison of the loss per epoch in case of $\alpha(z)=z$ and $\alpha(z)=\tanh(z)$ (or the logistic function) with a similar plot as the one in the lecture notes.

(d) Find a better loss function than the quadratic one to cure the vanishing gradients problem for  $\alpha(z) = \frac{1}{1+e^{-z}}$ by demanding

$$\frac{dL(y,\alpha(z))}{dz}= \frac{\partial L(y,r)}{\partial r}\Bigl|_{r=\alpha(z) }\cdot \alpha^{'}(z) = - (y - \alpha(z))$$ 

so that the partial derivative of $L$ already compensates the small derivative of $\alpha$.