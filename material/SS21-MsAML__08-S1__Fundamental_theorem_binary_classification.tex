% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-08-s2-pdf}{%
\section{\texorpdfstring{MsAML-08-S2
{[}\href{SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.pdf}{PDF}{]}}{MsAML-08-S2 {[}PDF{]}}}\label{msaml-08-s2-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{the-fundamental-theorem-of-binary-classification}{The
  fundamental theorem of binary classification}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{the-fundamental-theorem-of-binary-classification}{%
\subsection{The fundamental theorem of binary
classification}\label{the-fundamental-theorem-of-binary-classification}}

We have come a long way in treating also the setting of infinite
hypotheses spaces, at least for binary classification, and it is a good
time to harvest all our results and summarize them by means of one
central theorem.

Let us recall the setting of binary classification:

\begin{itemize}
\tightlist
\item
  Binary labels: \(\mathcal Y=\{-1,+1\}\),
\item
  Relevant hypotheses:
  \(\mathcal F\subseteq {\mathcal Y}^{\mathcal X}\),
\item
  I.i.d. training data: \(S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}\),
\item
  Loss function, counting the misclassifications:
  \(L(y,y')=1_{y\neq y'}\).
\end{itemize}

\begin{description}
\item[Theorem:] ~ 
\hypertarget{fundamental-theorem-of-binary-classification}{%
\subparagraph{(Fundamental theorem of binary
classification)}\label{fundamental-theorem-of-binary-classification}}

In the above setting, the following statements (S1)-(S4) are equivalent.
\end{description}

\textbf{Statement (S1):} Finite VC-dimension of \(\mathcal F\):
\(\dim_\text{VC}\mathcal F < \infty\).

\textbf{Statement (S2):} Uniform convergence property of \(\mathcal F\):
\begin{align}
  & \exists \, \text{polynomial } p \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  & \qquad \qquad 
  P_S\left(\exists h\in\mathcal F: \left|\widehat R_S(h)-R(h)\right| > \epsilon \right) < \delta.
\end{align}

\textbf{Statement (S3):} PAC learnability of \(\mathcal F\):
\begin{align}
  & \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  & \qquad \qquad 
  P_S\left(\left|R(h_S)-R_{\mathcal F}\right| > \epsilon \right) < \delta.
\end{align}

\textbf{Statement (S4):} PAC learnability of \(\mathcal F\) by means of
empirical risk minimization (ERM): \begin{align}
  & \exists \, \text{polynomial $p$ and empirical risk minimizer $\mathcal A^{\text{ERM}}:s\mapsto h_s^{ERM}\in\mathcal F$} \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  & \qquad \qquad 
  P_S\left(\left|R(h_S)-R_{\mathcal F}\right| > \epsilon \right) < \delta.
\end{align}

\textbf{Proof:} (S1) \(\Rightarrow\) (S2) holds thanks to the last
corollary in \href{SS21-MsAML__07-S2__VC-Dimension.pdf}{VC-dimension
module}.

\begin{description}
\tightlist
\item[👀]
Check the reference, recall the proof of the corollary, and convince
yourself that it holds.
\end{description}

(S2) \(\Rightarrow\) (S4): Given (S2) there is a polynomial \(p\) such
that for all \(\epsilon,\delta\in(0,1]\), distributions \(P\),
\(N\geq p\left(\frac2\epsilon,\frac1\delta\right)\), and all
\(h\in\mathcal F\) \begin{align}
  |\widehat R_S(h)-R(h)|\leq \epsilon 
\end{align} with probability of a t least \(1-\delta\).

Recalling our \href{SS21-MsAML__02-S1__Error_decomposition.pdf}{error
decomposition module}, for empirical risk minimizers \(h^\text{ERM}_S\)
we get \begin{align}
  |R(h^\text{ERM}_S)-R_{\mathcal F}| \leq 2 \sup_{h\in\mathcal F}|\widehat R_S(h)-R(h)| \leq 2\cdot \frac \epsilon 2 = \epsilon.
\end{align}

\begin{description}
\tightlist
\item[👀]
Check the reference and convince yourself that it holds.
\end{description}

(S4) \(\Rightarrow\) (S3) holds as (S4) gives an example for such an
algorithm.

(S3) \(\Rightarrow\) (S1): This is the only new assertion that was not
discussed before. Say, (S3) holds true, then \begin{align}
  & \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  & \qquad \forall \, \epsilon,\delta \in (0,1]\\
  & \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  & \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  & \qquad \qquad 
  P_S\left(\left|R(h_S)-R_{\mathcal F}\right| > \epsilon \right) < \delta.
\end{align} We shall now prove (S1) by contradiction and assume
\begin{align}
  \dim_\text{VC}\mathcal F = \infty.
\end{align} This implies that for any \(k\in\mathbb N\) there is a tuple
\(\mathfrak X=(x_1,\ldots, x_k)\) of points in \(\mathcal X\) that is
shattered by \(\mathcal F\), i.e., \(|\mathcal F|_{\mathfrak X}|=2^k\).
Let us think of \(\mathfrak X\) as feature space, and by abuse of
notation also denote the set of points in \(\mathfrak X\) by the same
symbol \(\mathfrak X\). So put differently, for every bit pattern in
\(\{-1,+1\}^k\) there is a concept \(c:\mathfrak X\to\{-1,+1\}\), given
by a hypothesis \(h\in\mathcal F\) through \(c(x_i)=h(x_i)\),
\(i=1,\ldots, k\), such that the bit pattern can be expressed by
\(\big(c(x_1),\ldots, c(x_k)\big)\).

Applying our
\href{SS21-MsAML__04-2__No_free_lunch_theorems.pdf}{no-free-lunch
theorem} in which we replace \(\mathcal X\) by the set of points
\(\mathfrak X\), we find for any algorithm \(\mathcal A\), producing
\(h_s=\mathcal A(s)\): \begin{align}
  &\frac12\left(1-\frac{N}{k}\right) 
  \leq E_C E_{X^N} P_X\left( h_S(X)\neq C(X)\right).
  \label{eq_1}
  \tag{E1}
\end{align} The right-hand side can be bounded further by \begin{align}
  \eqref{eq_1}
  \leq E_C \Bigg[
  &
  1\cdot P_S\Big(
           P_X(h_S(X)\neq C(X)) > \epsilon
         \Big)
  \\
  &+ \epsilon \cdot \Big[
    1 - P_S\Big(
          P_X(h_S(X)\neq C(X) > \epsilon)
        \Big)
    \Big]
  \Bigg]
\end{align} since the error probability is less or equal one.

Hence, choosing \(\epsilon=\frac14\), for any \(k\in\mathbb N\) and
\(N<k\), there must be a concept \(c:\mathfrak X\to\{-1,+1\}\) such that
\begin{align}
  P_S\Big(
          P_X(h_S(X)\neq C(X) > \epsilon)
        \Big) 
  \geq \frac13 - \frac23 \frac N k.
\end{align}

However, choosing further \(\delta=\frac 14\) together with the
distribution \begin{align}
  P(X=x\wedge Y=y) = \frac{1}{k} 1_{x\in\mathfrak X} 1_{y=c(x)},
\end{align} (S3) implies that for
\(N>p(\frac1\epsilon=4,\frac1\delta=4)\) \begin{align}
  \frac 14 
  &> 
  P_S\left(|R(h_S)-\underbrace{R_{\mathcal F}}_{=0 \text{ as }c\in\mathcal F}|>\frac 1 4\right)
  \\
  &=
  P_S\left(R(h_S)>\frac 1 4\right)
  \\
  &=
  P_X\left(P_X(h_S(X)\neq c(X))>\frac 1 4\right)
\end{align} This bound and the one of \(\eqref{eq_1}\) render the
following scenario:

\begin{figure}
\centering
\includegraphics{08/bounds.png}
\caption{Illustration of the bounds on
\(p=P_X\left(P_X(h_S(X)\neq c(X))>\frac 1 4\right)\).}
\end{figure}

For sufficiently large \(k\), this leads to a contradiction.

\(\square\)

As you can imagine, there are various generalizations to the case
\(|\mathcal Y|>2\), maybe most notably the graph and Natarajan
dimension, which allow for a similar type of generalization bounds.
Furthermore, there are similar complexity measures for regression tasks,
i.e., in the continuous case, such as the fat-shattering dimension. For
quadratic loss functions one can find an analogue of the above theorem
employing the fat-shattering dimension in case of regression.

In view of our time constraints, we will however not proceed towards
those generalizations but will rather consider the non-uniform
learnability concept and related structural risk minimization and
boosting methods, which led to many applications in machine learning.

➢ Next session!

\end{document}
