% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{ss21-msaml-01-3-pdf}{%
\section{\texorpdfstring{SS21-MsAML 01-3
{[}\href{SS21-MsAML__01-3__Supervised_learning_setting.pdf}{PDF}{]}}{SS21-MsAML 01-3 {[}PDF{]}}}\label{ss21-msaml-01-3-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{supervised-learning-setting}{Supervised learning
  setting}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{supervised-learning-setting}{%
\subsection{Supervised learning
setting}\label{supervised-learning-setting}}

We start our general discussion on supervised machine learning with the
help of a traditional machine learning data set, the Iris flow data set:

\begin{itemize}
\tightlist
\item
  \url{http://archive.ics.uci.edu/ml/datasets/iris}
\end{itemize}

This data set comprises measurements of three types of Iris flower
species
(\href{https://en.wikipedia.org/wiki/Iris_flower_data_set}{Source}):

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth,height=\textheight]{https://upload.wikimedia.org/wikipedia/commons/5/56/Kosaciec_szczecinkowaty_Iris_setosa.jpg}
\caption{Iris setosa}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth,height=\textheight]{https://upload.wikimedia.org/wikipedia/commons/4/41/Iris_versicolor_3.jpg}
\caption{Iris versicolor}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth,height=\textheight]{https://upload.wikimedia.org/wikipedia/commons/9/9f/Iris_virginica.jpg}
\caption{Iris virginica}
\end{figure}

In the following we will look at the measurements of sepal length and
width:

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth,height=\textheight]{./01/Iris_sepal_width_length.png}
\caption{Iris virginica}
\end{figure}

The following shows a scatter plot of the two dimensional data tuple of
sepal length and width:

\begin{figure}
\centering
\includegraphics{./01/iris_plot.svg}
\caption{Iris data scatter plot of sepal width and length.}
\end{figure}

Each data point is annotated by a color that encodes the Iris flower
species from which the measurement had been taken.

Suppose we want an algorithm to learn the classification of a Iris
species type by means of given examples of sepal length and width data
which are already annotated by means of the Iris species type. This is a
typical machine learning which, more abstractly, can be cast into the
following form:

\begin{description}
\item[Goal]
We seek an algorithm \(\mathcal A\) with the following specification:

\textbf{Input:} Training data \(s=(x^{(i)},y^{(i)})_{i=1\ldots N}\) of
length \(N\) where we call:

\begin{itemize}
\tightlist
\item
  \(x^{(i)}\) a feature in feature space \(\mathcal X\);
\item
  \(y^{(i)}\) a label in label space \(\mathcal Y\);
\end{itemize}

\textbf{Output:} A map \(h:\mathcal X\to\mathcal Y\) called hypothesis
that, for any \(x\in\mathcal X\) produces a prediction of a label
\(h(x)\in\mathcal Y\).
\end{description}

In our example of the Iris data set, it would be convenient to choose

\begin{align}
  \mathcal X=\mathbb R^2
\end{align}

to represent our data tuples as vectors:

\begin{align}
  x^{(i)}
  =
  \begin{pmatrix}
    x^{(i)}_1 \\
    x^{(i)}_2
  \end{pmatrix}
  \quad
  \begin{array}{l}
    \leftarrow \text{sepal length}\\
    \leftarrow \text{sepal width}
  \end{array}
\end{align}

and a label space

\begin{align}
  \mathcal Y = \{0, 1, 2\},
\end{align}

where the integers 0, 1, 2 encode the species ``Iris setosa'', ``Iris
versicolor'', ``Iris virginica'', respectively.

For other tasks we would have to adapt feature and label spaces
\(\mathcal X, \mathcal Y\) accordingly. To imagine other example, let us
consider the task of object recognition from images. For this purpose
the feature space could be chosen consist of matrices \begin{align}
  \mathcal X = [0,1]^{m\times n}
\end{align} representing gray scale images of \(m\times n\) pixels.
Furthermore, the label space may comprise strings encoding the object
classes, e.g., \begin{align}
  \mathcal Y = \{\text{human}, \text{cat}, \text{car}, \ldots\}.
\end{align}

\begin{description}
\tightlist
\item[In this lecture]
we will mainly discuss and implement such algorithms \(\mathcal A\) and
analyze how they perform. Note that the algorithm itself can be viewed
as a map from sets of training data \(s\) to a ``learned'' hypothesis
\begin{align}
  h_s\in\mathcal Y^{\mathcal X}
\end{align} or, more precisely, \begin{align}
  \mathcal A: \bigcup_{N\in\mathbb N} \left(\mathcal X\times \mathcal Y\right)^N 
  & \to 
  \mathcal Y^{\mathcal X}
  \\
  s
  & \mapsto 
  h_s.
\end{align} Here, the \(\mathcal Y^{\mathcal X}\) is short for all maps
\(\mathcal X\to\mathcal Y\).
\end{description}

Before discussing ideas of how such algorithms can ``learn'' a
hypothesis to predict labels for unseen samples from a set of known
training data, let us consider what we would expect from the ``learned''
hypothesis:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  It should make as few errors as possible when classifying the known
  training data.
\item
  It should \emph{generalize} well to previously unseen samples.
\end{enumerate}

Especially the second point is naturally very difficult to control as
little to nothing might be known about the unseen samples. In order to
proceed and study such learning algorithms systematically we need to
define the ``goal of learning''.

➢ Next session!

\end{document}
