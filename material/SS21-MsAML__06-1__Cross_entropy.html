<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__06-1__Cross_entropy</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-06-1-pdf">MsAML-06-1 [<a href="SS21-MsAML__06-1__Cross_entropy.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#entropy-measures">Entropy measures</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="entropy-measures">Entropy measures</h2>
<p>In the last module, we have found a good hint for a new type of loss function, i.e., we identified its derivative that would allow to compensate the slow learning problem in case the Adaline neuron is in a region of saturation. We could simply leave it at that and accept the new form of loss function as a good choice in combination with the logistic activation function (and also the <span class="math inline">\(\tanh\)</span> when converting the labels correspondingly). However, in this module, we will take it as excuse to make a brief excursion into information theory in order to better understand under which conditions the new type of loss function can be expected to work and also reflect the misclassification errors well.</p>
<p>For sake of simplicity, let us restrict our discussion to a discrete probability space <span class="math inline">\((\Omega,P)\)</span> and consider the following communication setting between Alice and Bob:</p>
<p>Suppose, in their communication, Alice and Bob want to distinguish a number of <span class="math inline">\(|\Omega|=n\)</span> events using a communication channel that allows to send only one bit per communication exchange. In order to reduce the number of communication exchanges between Alice and Bob, they would need to find a coding scheme, i.e., an encoding table relating bit sequences to the respective events, which still allows to encode all the <span class="math inline">\(N\)</span> events but at least on average reduces the number of communications exchanges. As an analogy, if Bob intends to report to Alice the color of each car passing by a highway intersection, it would be advantageous that the word “blue” has less letters than “magenta”, in case, there are more blue than magenta cars passing by, and there is no need to use the same amount of letters for each word denoting a color. Clearly, to achieve this, the more frequent events <span class="math inline">\(\omega\in\Omega\)</span>, i.e., those with respectively large <span class="math inline">\(P(\{\omega\})\)</span>, would need to be assigned a smaller bit sequence than the less frequent ones.</p>
<p>Such a strategy provides a gain in communication efficiency only if there are events that are indeed more frequent than others. In order to predict the average minimum number of communication exchanges required by Alice and Bob for any encoding scheme given the probability measure <span class="math inline">\(P\)</span>, one defines the so-called <em>information entropy</em>:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="entropy">(Entropy)</h5>
<span class="math display">\[\begin{align}
H(P) := \sum_{\substack{\omega\in\Omega\\
                        P(\{\omega\})&gt;0}}
        P(\{\omega\}) 
        (-1)\log_2 P(\{\omega\})
  \end{align}\]</span>
</dd>
</dl>
<p>The term <span class="math inline">\((-1)\log_2 P(\{\omega\})\)</span> can be interpreted as number of bits that shall be allocated in order to encode the event <span class="math inline">\(\omega\)</span> – few bits for events that are likely to occur and more for those that will occur less likely. In this computation we can, of course, drop those events that surely do not occur, i.e., with <span class="math inline">\(P(\{\omega\})=0\)</span>, as they do not have to be encoded at all and we will do this for convenience as otherwise the <span class="math inline">\(\log_2\)</span> is not always well-defined.</p>
<p>To get familiar with this quantity, let us look at a few examples:</p>
<p><strong>Example 1) A fair coin:</strong></p>
<p>To model a fair coin, we set <span class="math display">\[\begin{align}
  \Omega=\{0,1\} \qquad \text{and} \qquad P(\{0\})=\frac12=P(\{1\})
\end{align}\]</span> and find <span class="math display">\[\begin{align}
  H(P) = -\log_2\frac12=1.
\end{align}\]</span> Hence, on average, we will need one bit for each event as we might already expect. For example, we could represent <span class="math inline">\(\omega=0\)</span> by bit value 0 and <span class="math inline">\(\omega=1\)</span> by bit value <span class="math inline">\(1\)</span>.</p>
<p><strong>Example 2) An unfair coin:</strong></p>
<p>Let us load the coin, e.g., such that <span class="math display">\[\begin{align}
  \Omega=\{0,1\}, \qquad P(\{0\})=\frac14, \qquad \text{and} \qquad P(\{1\})=\frac34.
\end{align}\]</span> Then the entropy evaluates to <span class="math display">\[\begin{align}
  H(P) = \frac14 \log_2 4 + \frac34 \log_2\frac43 \approx 0.81\ldots .
\end{align}\]</span> Of course, the number of bits, or number of communication exchanges, in our example of Alice and Bob is an integer. Even though, <span class="math inline">\(H(P)\)</span> indicates that some events are much frequent than others, we would still need <span class="math inline">\(1\)</span> bit per event. A reduction in communication exchanges can therefore only be achieved in case of more events.</p>
<p><strong>Example 3) A fair and unfair dice:</strong></p>
<p>To model a fair dice, we set <span class="math display">\[\begin{align}
  \Omega=\{1,2,3,4,5,6\}, \qquad \text{and} \qquad \forall \omega\in\Omega: P(\{w\})=\frac16,
\end{align}\]</span> which results in <span class="math display">\[\begin{align}
  H(P) \approx 2.85\ldots .
\end{align}\]</span> This implies that we would have to use a <span class="math inline">\(3\)</span> bit coding scheme, which would even suffice to encode <span class="math inline">\(8=2^3\)</span> events. However, for a loaded dice, e.g., <span class="math display">\[\begin{align}
  P(\{1\})=\frac14, \qquad P(\{2\}),\ldots,P(\{5\})=\frac{1}{16},\qquad P(\{6\})=\frac12,
\end{align}\]</span> we find <span class="math display">\[\begin{align}
  H(P)=2,
\end{align}\]</span> which implies that, in an optimal coding scheme, on average only <span class="math inline">\(2\)</span> bits would be need to be send per event.</p>
<dl>
<dt>👀</dt>
<dd>Find such a coding scheme.
</dd>
</dl>
<p>In general, <span class="math inline">\(H(P)\)</span> is larger the more uniform <span class="math inline">\(P\)</span> is distributed and for a uniform distributed <span class="math inline">\(P\)</span> one finds <span class="math inline">\(H(P)=\log_2 N\)</span>, i.e., the minimum number of bits to encode <span class="math inline">\(N\)</span> events with bit sequences of same length.</p>
<p>Given <span class="math inline">\(P\)</span>, one could now assign bit sequences to events to approximate the optimal coding scheme bandwith (taking in account that the number of bits per communication exchange sequence is an integer, of course).</p>
<p>However, in the typical situation in statistics, the measure <span class="math inline">\(P\)</span> is unknown and the objective is to find a good “estimated” measure <span class="math inline">\(Q\)</span> based on the empirical evidence. A possible means to evaluate how good a “guess” <span class="math inline">\(Q\)</span> actually is, when compared to the actual measure <span class="math inline">\(P\)</span>, is to compute the so-called <em>cross-entropy</em>:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="cross-entropy">(Cross entropy)</h5>
Given another measure <span class="math inline">\(Q\)</span> on <span class="math inline">\(\Omega\)</span> we define: <span class="math display">\[\begin{align}
H(P,Q) = \sum_{\omega \in\Omega}
         P(\{\omega\})(-1)\underbrace{\log_2 Q(\{\omega\})}_{\text{convention: $\log_2 0 = -\infty$}}
  \end{align}\]</span>
</dd>
</dl>
<p>One may now interpret <span class="math inline">\(H(P,Q)\)</span> as follows: According our guess <span class="math inline">\(Q\)</span> of how likely any event <span class="math inline">\(\omega\)</span> occurs, we would optimally assign a bit sequence of length <span class="math inline">\(\log_2 Q(\{\omega\})\)</span> to encode event <span class="math inline">\(\omega\)</span>. Now, the cross-entropy <span class="math inline">\(H(P,Q)\)</span> averages these assigned bit-lengths over the actual distribution <span class="math inline">\(P\)</span> of events <span class="math inline">\(\omega\in\Omega\)</span>. If, according to <span class="math inline">\(Q\)</span>, we would enforce a sub-optimal coding scheme with respect to <span class="math inline">\(P\)</span>, we will on average assign longer bit sequences as would be required per event:</p>
<dl>
<dt>Theorem</dt>
<dd>The cross-entropy is bounded from below by the entropy, i.e., <span class="math display">\[\begin{align}
H(P,Q)\geq H(P).
  \end{align}\]</span>
</dd>
</dl>
<p>We leave the proof as an exercise.</p>
<dl>
<dt>👀</dt>
<dd>Proof the above theorem. One strategy could be to regard the difference <span class="math inline">\(H(P,Q)-H(P)\)</span>, confirm the inequality <span class="math inline">\(\log x \leq x -1\)</span> for <span class="math inline">\(x&gt;0\)</span>, and apply it to show that the former difference is bounded below by zero.
</dd>
</dl>
<p>This property qualifies <span class="math inline">\(H(P,Q)\)</span> to function as distance between potential guess <span class="math inline">\(Q\)</span> and actual <span class="math inline">\(P\)</span>.</p>
<h3 id="application-to-our-adaline-neuron">Application to our Adaline neuron</h3>
<p>Next, we will line out an analogy to this optimal encoding question for the Adaline setting of binary classification that motivates the use of the cross-entropy as a loss function, at least in the case of the logistic activation function <span class="math display">\[\begin{align}
  \alpha(z) = \frac{1}{1+e^{-z}}.
\end{align}\]</span> In such a setting, the Adaline neuron is sometimes referred to as <em>logistic Adaline</em> neuron.</p>
<p>Given the usual training data for binary classification <span class="math display">\[\begin{align}
  s=(x^{(i)},y^{(i)})_{i=1,\ldots, N} \in (\mathcal X\times\mathcal Y)^N,
  \qquad \mathcal Y=\{0,1\},
\end{align}\]</span> let us consider the following sample space <span class="math display">\[\begin{align}
  \Omega=\left\{ \{x^{(i)}\text{ drawn and }y^{(i)}=y\}\,\Big|\, i=1,\ldots, N, y\in\{0,1\}
         \right\}
\end{align}\]</span> with the following probability distribution <span class="math display">\[\begin{align}
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=1
    \right\}
  \right)
  &amp;=
  \frac{y^{(i)}}{N},\\
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=0
    \right\}
  \right)
  &amp;=
  \frac{1-y^{(i)}}{N}.
\end{align}\]</span> Note that this actual gives rise to a probability measure because <span class="math display">\[\begin{align}
  P(\Omega)
  =
  \sum_{\omega \in\Omega} P(\{\omega\})
  =
  \sum_{i=1}^N\sum_{y=0,1}
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=y
    \right\}
  \right)
  =1.
\end{align}\]</span></p>
<p>Hence, <span class="math inline">\((\Omega,P)\)</span> models the probability of picking a feature vector <span class="math inline">\(x^{(i)}\)</span> of the training data with the correct label at random with uniform distribution.</p>
<p>Now the Adaline’s confidence for the same event to have one label of another could be modeled by the following measure <span class="math inline">\(Q\)</span>, setting <span class="math display">\[\begin{align}
  Q\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=1
    \right\}
  \right)
  &amp;=
  \frac{\alpha(w\cdot x^{(i)}+b)}{N},\\
  Q\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=0
    \right\}
  \right)
  &amp;=
  \frac{1-\alpha(w\cdot x^{(i)}+b)}{N}.
\end{align}\]</span> Note that also <span class="math inline">\(Q\)</span> is a probability measure as <span class="math inline">\(\text{range}\alpha\in(0,1)\)</span> and <span class="math inline">\(Q(\Omega)=1\)</span>. Here, the emphasis must lie on “a measure” as the nature of the activation obviously does not need to reflect a relative frequency, not to mention a probability – although there are interesting classification models that try to enforce this nature of relative frequencies.</p>
<p>Computing the respective cross entropy yields <span class="math display">\[\begin{align}
  &amp;H(P,Q)
  =
  -\sum_{\omega\in\Omega} \log_2 Q(\{\omega\})P(\{\omega\})\\
  &amp;=
  -\sum_{i=1}^N \sum_{y=0,1} 
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=y
    \right\}
  \right)
  \\
  &amp;\qquad \qquad \qquad
  \times \log_2 
  Q\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=y
    \right\}
  \right)
  \\
  &amp;=
  -\sum_{i=1}^N \Big[
    P\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=1
      \right\}
    \right)
    \\
    &amp;\qquad\qquad\qquad\times
    \log_2
    Q\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=1
      \right\}
    \right)
    \\
    &amp;\qquad\qquad
    +
    P\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=0
      \right\}
    \right)
    \\
    &amp;\qquad\qquad\qquad\times
    \log_2
    Q\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=0
      \right\}
    \right)
  \Big]
  \\
  &amp;=
  -\sum_{i=1}^N
  \left[
    \frac{y^{(i)}}{N}\log_2\frac{\alpha(w\cdot x^{(i)}+b)}{N}
    +
    \frac{1-y^{(i)}}{N}\log_2\frac{1-\alpha(w\cdot x^{(i)}+b)}{N}
  \right]
  \\
  &amp;=
  \log_2 N - \frac1N
  \sum_{i=1}^N 
  \Big[
    y^{(i)}\log_2 \alpha(w\cdot x^{(i)}+b)
    \\
    &amp;\qquad\qquad\qquad\qquad\qquad
    +
    (1-y^{(i)})\log_2(1-\alpha(w\cdot x^{(i)}+b))
  \Big].
\end{align}\]</span> The constant <span class="math inline">\(\log_2 N\)</span>, as discussed, reflects the worst case average for the case of a uniform distribution. Dropping this constant (and also the base <span class="math inline">\(2\)</span>), which is irrelevant for the task of optimization, this computation motivates the loss function <span class="math display">\[\begin{align}
  L_{\text{cross-entropy}}(y,y&#39;)
  =
  -\left(y\log y&#39; + (1-y)\log (1-y&#39;)\right)
\end{align}\]</span> and corresponding empirical loss <span class="math display">\[\begin{align}
  \widehat L_{\text{cross-entropy}}(b,w)
  =
  \frac1N \sum_{i=1}^N L_{\text{cross-entropy}}\left(y^{(i)}, \alpha(w\cdot x^{(i)}+b)\right).
\end{align}\]</span></p>
<p>The gradient needed for the corresponding update rule can be computed as follows: <span class="math display">\[\begin{align}
  \frac{\partial \widehat L(\bar w)}{\partial\bar w}
  &amp;= 
  -\frac1N\sum_{i=1}^N\left[
    \frac{y^{(i)}}{\alpha(\bar w\cdot \bar x^{(i)})}
    -
    \frac{1-y^{(i)}}{1-\alpha(\bar w\cdot \bar x^{(i)})}
  \right]
  \alpha&#39;(\bar w\cdot \bar x^{(i)})\bar x^{(i)}\\
  &amp;=
  -\frac1N
  \sum_{i=1}^N \left[y^{(i)}-\alpha(\bar w\cdot \bar x^{(i)})\right]
  \bar x^{(i)},
\end{align}\]</span> recalling <span class="math display">\[\begin{align}
  \alpha&#39;(z)=\alpha(z)(1-\alpha(z))
\end{align}\]</span> and using our short-hand notation <span class="math display">\[\begin{align}
  \bar w=(b,w)^T \qquad\text{and}\qquad \bar x=(1,x)^T.
\end{align}\]</span> This allows to specify the update rule by means of: <span class="math display">\[\begin{align}
  \bar w &amp; \leftarrowtail \bar w^\text{new} := \bar w + \eta \frac1N
  \sum_{i=1}^N \left[y^{(i)}-\alpha(\bar w\cdot \bar x^{(i)})\right] \bar x^{(i)}.
\end{align}\]</span></p>
<p>In comparison to a setting of an logistic Adaline with a mean square error loss, we observe the absence of the potentially small <span class="math inline">\(\alpha&#39;(z)\)</span> despite the logistic activation function, as discussed in the last module.</p>
<dl>
<dt>Remark</dt>
<dd>As a rule of thumb, the Adaline neuron with logistic or <span class="math inline">\(tanh\)</span> activation function <span class="math inline">\(\alpha(z)\)</span> will in general be easier to train with respect to a cross-entropy than a mean square error loss, in particular, for “unfortunate” choice of initial model parameters. That is, unless the vanishing gradient effect is desired to model a certain inertial during training. The latter turns out to be useful in the case of deep networks of many layered Adaline neurons, in particular, for the so-called <em>convolution layers</em>. In their case, a popular choice for an activation function is given by the rectified linear or ReLu unit: <img src="06/ReLu_activation.png" alt="Rectified linear (ReLu) unit activation." />
</dd>
</dl>
<p>If time permits, we will discuss these convolution layers and the usefulness of the ReLu activation in more detail towards the end of the course. For now, the take-aways message is that, often, the choice of the activation functions and loss function have to be carefully adapted to each other.</p>
<dl>
<dt>👀</dt>
<dd>Since the ReLu is not differentiable. How could be define a sensible update rule?
</dd>
</dl>
<p>In the next model, we will touch upon another important topic in conditioning the Adaline model well for optimal learning performance and later generalize it to the case of more than two classes, which can be seen as the first step towards deep networks.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
