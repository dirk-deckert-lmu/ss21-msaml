% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-08-s1-pdf}{%
\section{\texorpdfstring{MsAML-08-S1
{[}\href{SS21-MsAML__08-S1__Computational_tools_VC-dimension.pdf}{PDF}{]}}{MsAML-08-S1 {[}PDF{]}}}\label{msaml-08-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{computational-tools-for-the-vc-dimension}{Computational
  tools for the VC-dimension}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{computational-tools-for-the-vc-dimension}{%
\subsection{Computational tools for the
VC-dimension}\label{computational-tools-for-the-vc-dimension}}

In the following we will take a look at two results that help to compute
the VC-dimension. Let us jump right in:

\begin{description}
\item[Theorem] ~ 
\hypertarget{vc-dimension-for-linear-spaces}{%
\subparagraph{(VC-dimension for linear
spaces)}\label{vc-dimension-for-linear-spaces}}

Let \(\mathcal G\) be a finite-dimensional \(\mathbb R\)-vector space of
functions of the form \(g:\mathcal X\to\mathbb R\), and let
\(\phi:\mathcal  X\to\mathbb R\) be an additional function. Then, the
space of hypotheses \begin{align}
\mathcal F := \left\{
  x\mapsto \text{sign}\left(\phi(x)+g(x)\right)
  \,\big|\,
  g\in\mathcal G
\right\}
\subseteq \{-1,+1\}^{\mathcal X}
  \end{align} fulfills \begin{align}
\dim_\text{VC} \mathcal F = \dim_{\mathbb R}\mathcal G.
  \end{align}
\end{description}

\textbf{Proof:} Part 1) Let us start by showing \begin{align}
  \dim_\text{VC} \mathcal F \leq \dim_{\mathbb R} \mathcal G.
\end{align} We shall argue by contradiction. For this purpose let us set
\(k=\dim_{\mathbb R}\mathcal G+1\) and assume \begin{align}
  \dim_\text{VC} \mathcal F \geq k.
\end{align} Then, there is a tuple of points
\(P=(x_1,\ldots,x_k)\subset \mathcal X\) that is shattered by
\(\mathcal F\), or equivalently \begin{align}
  \left|\mathcal F|_P\right| = 2^{|P|} = 2^k
\end{align} or in order words, \(\mathcal F|_P\) contains every bit
pattern in \(\{-1,+1\}^{k}\).

Now, the set \begin{align}
  \mathcal G|_P := \left\{
    \big(g(x_1),\ldots, g(x_k)\big)
    \,\big|\,
    g\in\mathcal G
  \right\}
  \subset \mathbb R^k
\end{align} is a \(\mathbb R\)-vector space of dimension \begin{align}
  \dim_{\mathbb R}\mathcal G|_P
  =
  \dim_{\mathbb R}\mathcal G
  =k-1.
\end{align}

Hence, there exists a \(v\in\mathbb R^k\) with at least one negative
component \(v_j\in\mathbb R^-\) such that for all \(g\in\mathcal G\)
\begin{align}
  0
  =
  \sum_{i=1}^k v_i g(x_i)
  =: \langle
    v, (g(x_1),
    \ldots,
    g(x_k)
  \rangle
  ,
\end{align} which then implies that for all \(g\in\mathcal G\)
\begin{align}
  \Big\langle
    v, \big( 
      g(x_1)+\phi(x_1),
      \ldots,
      g(x_k)+\phi(x_k)
    \big)
  \Big\rangle
  =
  \langle
    v, (\phi(x_1),\ldots,\phi(x_k)
  \rangle
  \label{eq_1}
  \tag{E1},
\end{align} or put differently, the term is \(g\)-independent.

But given such a vector \(v\), and recalling that \(\mathcal F|_P\)
comprises all bit patterns in \(\{-1,+1\}^k\), there must exist
\(g^+,g^-\in\mathcal G\) such that \begin{align}
  \text{sign}\left(g^\pm(x_i)+\phi(x_i)\right) = \pm \text{sign}(v_i),
  \qquad
  i=1,\ldots, k,
\end{align} using our convention \begin{align}
  \text{sign}(z)=
  \begin{cases}
    +1 & \text{for } x\geq 0\\
    -1 & \text{otherwise}
  \end{cases}
  \,.
  \label{eq_2}
  \tag{E2}
\end{align}

This implies \begin{align}
  \left\langle
    v,
    \left(
      g^\pm(x_1)+\phi(x_1),
      \ldots,
      g^\pm(x_k)+\phi(x_k)
    \right)
  \right\rangle
  &=
  \pm\sum_{i=1}^k |v_i| 
  \left|
    g^\pm(x_i)+\phi(x_i)
  \right|.
\end{align} However, there is at least one negative component \(v_j<0\)
and the term \(|g^\pm(x_j)+\phi(x_j)|\) cannot be zero for both cases
\(+\) and \(-\) due to \(\eqref{eq_2}\). This contradicts
\(\eqref{eq_1}\).

Part 2) Next we shall show \begin{align}
  \dim_\text{VC}\mathcal F\geq \dim_{\mathbb R}\mathcal G.
\end{align} Therefore, it suffices to show that \begin{align}
  & \forall k\leq \dim_{\mathbb R}\mathcal G \, \exists x_1,\ldots, x_k\in\mathcal X\\
  & \qquad \forall y\in\mathbb R^k \, \exists g\in\mathcal G \\
  & \qquad \qquad \forall i=1,\ldots, k: y_i=g(x_i).
\end{align}

For any choice of \(k\) linear independent functions
\((g_i)_{i=1,\ldots,k}\) in \(\mathcal G\) we have \begin{align}
  \text{span}_{x\in\mathcal X} 
  \begin{pmatrix}
   g_1(x)\\
   \vdots\\
   g_k(x)
  \end{pmatrix}
  =
  \mathbb R^k
\end{align} and there is a tuple \(x_1,\ldots, x_k\) in \(\mathcal X\)
such that \begin{align}
  \begin{pmatrix}
   g_1(x_1)\\
   \vdots\\
   g_k(x_1)
  \end{pmatrix},
  \ldots,
  \begin{pmatrix}
   g_1(x_k)\\
   \vdots\\
   g_k(x_k)
  \end{pmatrix}
\end{align} are linearly independent. In consequence, the matrix
\begin{align}
  G=\left(g_j(x_i)\right)_{1\leq i,j\leq k}
\end{align} is invertible. Thus, for any given \(y\in\mathbb R^k\), we
can find a vector of coefficients \(\lambda\in\mathbb R^k\) such that
\begin{align}
  y = G\lambda 
  \qquad :\Leftrightarrow \qquad
  \forall i=1,\ldots,k: y_i=\sum_{j=1}^k \lambda_j g_j(x_i),
\end{align} namely \begin{align}
  \lambda = G^{-1}y,
\end{align} which in turn identifies a element in \(\mathcal G\)
\begin{align}
  g=\sum_{j=1}^k \lambda_j g_j,
\end{align} which then fulfills \begin{align}
  \forall i=1,\ldots, k: y_i=g(x_i). 
\end{align}

\(\square\)

This allows to compute the VC-dimension for our linear classification
models with ease:

\begin{description}
\item[Corollary:] ~ 
\hypertarget{vc-dimension-of-linear-classification-models}{%
\subparagraph{(VC-dimension of linear classification
models)}\label{vc-dimension-of-linear-classification-models}}

For \(\mathcal X=\mathbb R^d\), \(\mathcal Y=\{-1,+1\}\), and
\begin{align}
\mathcal F = \left\{
  h(x)=\text{sign}(w\cdot x+b)
  \,\big|\,
  w\in\mathbb R^d, b\in\mathbb R.
\right\},
  \end{align} the relation \begin{align}
\dim_\text{VC} \mathcal F = d+1
  \end{align} holds true.
\end{description}

\textbf{Proof:} Define \begin{align}
  \phi(x) &:= 0,\\
  g_i(x) &:= x_i, \qquad i=1,\ldots, d,\\
  g_{d+1}(x) &:= 1,
\end{align} This allows to express the activation input for any model
parameters \(w\in\mathbb R^d\) and \(b\in\mathbb R\) as \begin{align}
  \forall x\in\mathbb R^d:
  \qquad
  w\cdot x+b
  =
  \phi(x)+\sum_{i=1}^{d+1} \lambda_i g_i(x)
\end{align} employing coefficients \(\lambda\in\mathbb R^{d+1}\) that
fulfill \begin{align}
  \lambda_{d+1} = b,
  \qquad
  \lambda_i = w_i, \quad  i=1,\ldots, d.
\end{align}

Hence, we can apply our theorem above to the \((d+1)\)-dimensional
\(\mathbb R\)-vector space of functions \begin{align}
  \mathcal G:=
  \left\{
    g(x) = \sum_{i=1}^{d+1}\lambda_i g_i(x)
    \,\big|\,
    \lambda\in\mathbb R^{d+1}
  \right\}.
\end{align}

\(\square\)

After these computations one may be tempted to think that the
VC-dimension is already determined by the dimension of the parameter
space that parametrize the hypotheses in \(\mathcal F\). While this is
always the case, it does not hold in general as the following example
shows:

\textbf{Example:} Let us consider the hypotheses space \begin{align}
  \mathcal F := \left\{
    h(x)=\text{sign}\left(\sin(\alpha x)\right)
    \,\big|\,
    \alpha\in\mathbb R
  \right\}
\end{align} and take the tuple of points \begin{align}
  P = \left(
    x_k:=2^{-k}
    \,\big|\,
    k=1,\ldots, k
  \right)
\end{align} together with a bit pattern \begin{align}
  \big(c(x_1),\ldots,c(x_n)\big) \in \{-1,+1\}^n
\end{align} for some concept \(c:\mathbb R\to \{-1,+1\}\).

Now, choosing \begin{align}
  \alpha = \pi\left(
    1 + \sum_{k=1}^n \, \underbrace{\frac{1-c(x_k)}{2}}_{\in \{0,1\}} \, 2^k
  \right)
\end{align} implies for \(l=1,\ldots,n\), \begin{align}
  \alpha x_l \mod 2\pi
  &=
  \pi\left(
    1 + \sum_{k=1}^n \, \underbrace{\frac{1-c(x_k)}{2}}_{\in \{0,1\}} \, 2^k
  \right) 2^{-l} \mod 2\pi \\
  &=
  \pi \frac{1-c(x_l)}{2}
  +
  \pi
  \underbrace{
  \left(
    2^{-l}+\sum_{k=1}^{l-1} \, \frac{1-c(x_k)}{2} \, 2^{k-l}
  \right) 
  }_{\in (0,1)},
\end{align} and therefore, \begin{align}
  \text{sign}(\sin(\alpha x_l)) = c(x_l).
\end{align} In other words, \(P\) is shattered by \(\mathcal F\).

As this argument holds for any \(n\), we get \begin{align}
  \forall n\in\mathbb N: \quad \dim_\text{VC}\mathcal F \geq n,
\end{align} or in other words, \(\dim_\text{VC}\mathcal F=\infty\).

\end{document}
