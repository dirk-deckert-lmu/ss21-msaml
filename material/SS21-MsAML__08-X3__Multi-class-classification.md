# Exercise 08-3: Multi-class classification [[PDF](SS21-MsAML__08-X3__Multi-class-classification.pdf)]

a) Use the Iris dataset that comprises three classes to implement a multi-class classifier and train it. For this, compute the update rule, e.g. for the soft-max Adaline with cross-entropy loss.

b) Does your implementation suffer from the vanishing gradients problem?