# Multi-layered network playground 

This is our first implementation of a multi-layered neural network that employs
the backpropagation algorithm we have discussed in Exercise 11-1.

It is ready to be applied to the [MNIST](http://yann.lecun.com/exdb/mnist/)
hand-written digits dataset.

As the implementation is a bit bigger than suitable for a Jupyter notebook we decided to implement it directly in Python but attempted to take great care to separate the different functionalities needed.

Let us start by inspecting the Python files and modules one by one.


## `download.py`

This is the script that downloads the MNIST training data and stores them into 
the `./MNIST` directory.

Simply execute

    python3 download.py

and it will automatically download the MNIST test and training data, uncompress
it, and store in the subdirectory mentioned above.

Run it once.


## `test_network_mnist.py`

This is the main Python script. After you have downloaded the MNIST data you can run this script to set up a neural network and conduct the training by

    python3 test_network_mnist.py

or

    python3 test_network_mnist.py > log.txt

if you would like to redirect the output directly into a file for later use.

The output is of the form:

    > Epoch=9/10 (90.00%), Dt=5.886761s, Avg=5.599596s, Acc: 0.910200/0.904333, Obj: 0.092003/0.094889, Weight: 0.609769
    | Epoch=9, Batch=1/60 (1.67%), Dt=0.090841s, Avg=0.090841s, ETA: 10.959204s|

| Abbreviation   | Meaning                                                        |
| -              | -                                                              |
| Epoch=m/M (p%) | m-th epoch of training for M many which amounts to p percent.  |
| Dt             | Time taken per last training epoch.                            |
| Avg            | Average Time taken per training epoch.                         |
| Acc e/f        | Accuracy on training and test data.                          |
| Obj            | Objective (loss) function evaluated on training and test data. |
| Weight         | Current weight matrix regularization weight.                   |
| Batch n/N (p%) | Current batch n of N many which amounts to p percent.          |
| Dt             | Time taken for last batch.                                     |
| Avg            | Average time taken for last batch.                             |
| ETA            | Estimated total amount of total time to complete the training  |

At the heart of the implementation are the code blocks that build
the neural network:

1) Create an instance of the neural network class by
    ```python
    # Build neural network with:
    # * as many input neurons as pixels = `data.rows * data.cols`
    # * with `argmax` function as interpretation of the one-hot encoded labels
    # * using the quadratic loss as objective function

    nn = NN(data.rows * data.cols,
                interpreters.ArgMax,
                objectives.L2)
    ```
    As the comment reads, this create a neural network instance defining a
    certain number of input neurons, and interpreter functions of the output of
    the neural network, and a objective (or loss) function.

2) Add hidden layers to the neural network instance.
    ```python
    # Add a `FullyConencted` hidden layer with:
    # * of `nw*nw` number of layer neurons (for convenient plotting of the weight
    #   matrix components in the monitoring functions)
    # * sigmoid activation.
    # * learning rate `eta`
    # * L2 regularization weight type
    # * factor `omega` to scale the regularization weight in loss function
    # * and initialize the weight matrix and bias terms with a normal distribution

    nw = 4
    nn.add_layer(num=nw*nw,
                layer=FullyConnected_Diag,
                activation=activations.Sigmoid,
                train='gradient',
                eta=3,
                weight=weights.L2,
                omega=0.1,
                init="norm_adapt"
                )
    ```
    As the comment reads, with the method `add_layer()` you can add the
    dedicated layers one by one to your neural network instance. Each time you
    can specify the number hidden layer neurons, their activation function,
    learning rate, regularization weight, regularization learning parameter,
    and routine to initialize the weight matrices and biases.

    Add further layers by repetitive invocation of the `add_layer()` method with
    your parameters of choice.

3) Finally add the last layer of output neurons by:
    ```python
    # Add an output layer of 10 output neurons that allow to encode the 10
    # different digit labels
    nn.add_layer(num=10,
                layer=FullyConnected,
                activation=activations.Sigmoid,
                train='gradient',
                eta=3,
                weight=weights.L2,
                omega=0.1,
                init="norm_adapt"
                )
    ```
    In principle, there is no difference to step 2), except that is is common to use a one-hot encoding for the labels in terms of unit vectors (recall
    our lecture on multi-class classification where we mapped the labels 0, 1,
    2, ..., c to the first, second, third, ..., c-th unit vectors of Euclidean
    space), and therefore, the number of output neurons should be equal to the
    number of labels that we want the network to learn to distinguish -- in our
    case 10.

## Neural network implementation

We have pre-defined most of the options that we also have discused in the
lectures. You can find:

* Interpreter functions in `interpreters.py`
* Activation functions in `activations.py` (only the `argmax` at the moment)
* Loss functions in: `objectives.py`
* Regularization weights in: `weights.py`
* Layer types: `layers.py` (only fully connected or dense layer at the moment)

and, if you are interested, you are of course encouraged to add your own.
The interfaces of the functions are documented and hopefully quite easy to
understand. Always keep it vectorization friendly by using `Numpy` as the
input and output are typically `Numpy Arrays`.

Loading of the MNIST images is done with the helper module: `mnist.py`
which you can safely ignore for a moment.

The core implementation of the neural network class is contained in the 
`multilayernet.py`. Here the most important methods to understand are training
methods that in interaction with the layer class defined in `layers.py`
implements the backpropagation algorithm. Compare the implementation with the
description of the algorithm as we have discussed in Exercise 11-1.

Finally, `dignostics.py` is a class that adds several additional monitoring and
plotting functions which may at first be ignored.

## Results of the first run

The base configuration of the neural network in `test_network_mnist.py` is a
network with as many input neurons as pixels in an image, a hidden layer of
only 16 neurons, and an output layer of 10 neuron that encode the 10 labels
through the one-hot encoding. Such a narrow hidden layer of course only offers
limited possibilities to introduce enough non-linearity, and thus, limited
efficiency. Therefore, you may want to try larger network
architectures. Nevertheless, let us look at the results of this base
configuration, which is trained with only 10 epochs and a batch size of 1000
images per batch according to the code block in
`test_network_mnist.py`:

```python
epochs = 10
batch_size = 1000
```
The results of the training are visualized as follows:

![](network_activations.png)

The upper plot shows the accuracy of the network as applied to training and
test data in terms of a relative error plotted against the epoch in training.
After 10 epochs, the network reaches the accuracy:

Model                                                                                                  | Accuracy   |
-----------------------------------------------------------                                            | ------------ |
Neural Network with (28*28) x 16 x 10 neurons, sigmoid activations, scaled normal init, quadratic loss, L2 regularization | 0.912000     |

The second half of the plots shows density plots of the of the weight matrices.
The first 16 weight matrices matrices show parts of the first weight matrix
$`(W^{(1)}_{jk})_{k=1,\dots,28*28}`$ for hidden layer neurons $`j=1,\dots,16`$.
The $`k`$ parameters is plotted with the same stride length as the input image
with, i.e., 28 pixels. This results in a nice visualization of how activations
of the input neurons lead to an activation of the 16 hidden layer neurons. If
you look closely you will see shadows of parts of the hand-written digits, and
in other words how the network neurons of the second layer specialize in
detecting certain features of the input image. The last density plot shows the 16x10 matrix which has to carry out the remaining logic to map the activations of the hidden layer neurons to the respective 10 output neurons.

The following illustration shows some of the misclassified images:

![](error_candidates.png)

On the top row following "#" is the image ID, "l=" specifies the actual label,
and "p=" the predicted label. Have a look at the top left, for example, for
which one may be easily comprehensible that an un-trained eye might misclassify
the digit.

## Ways to improve

Are the results we obtained in the first run good or bad? Accuracy of 91% on a 10-class problem is definitely way better than random, so the model managed to learn a lot about the data. However, if we want the model to make decisions automatically without human supervision, i.e. for industrial purposes, an error in 10% of cases is rather unacceptable in most applications. 

Real machine learning models typically fight for fractions of percent in accuracy, as they are significant when we intend to apply the model to very large numbers of samples. You can have a look e.g. at the history of winning results of ImageNet competition (which deals with much more complex data and classification problem than MNIST):

![](imagenet.png)
(Figure from this [blogpost](http://www.videantis.com/deep-learning-in-five-and-a-half-minutes.html))

How can we achieve better results in our problem then? Here are some potential ways to improve without changing the given implementation much:

* **Architecture:** As mentioned above, the network architecture of the base configuration is not
very favorably chosen for the task. Funneling the 28*28=784 input neurons
immediately down to 16 hidden neurons in only one layer is too big of a step
that does not leverage the potential non-linearity that can be introduced by
broader hidden layers. Therefore, we encourage you to play with different
architectures, i.e., different number of layers and neurons in  a layer.
* **Hyperparameters:** You can try various acctivation functions and optimization objectives (including ones with regularization) that we considered in the course. You should also ensure that your choice of optimization hyperparameters, e.g. learning rate or regularization constants, is reasonable.
* **Initialization:** You can try to change the distribution of random initialization of parameters.

If you are interested and have time, you can also try more advanced methods, which require extending our implementaion:

* **New layer types:** A breakthrough technique in object
recognition was the usage of [convolution
layers](https://cs231n.github.io/convolutional-networks/) which have much
sparser connections than the fully connected layers we have discussed so far. You can try
to extend the `layers.py` with an additional convolution layer class using the above description.
* **New optimization methods:** Deep learning frameworks typically provide a variety of optimization methods except for gradient descent, many of which tend to yield better results and require fewer iterations. In particular, you can have a look into Adam algorithm, described briefly e.g. in this [blogpost](https://towardsdatascience.com/adam-latest-trends-in-deep-learning-optimization-6be9a291375c).
* **Data augumentation:** You can try to extend your dataset with new images created by transformation of the given ones. An overview of such techniques can be found e.g. [here](https://nanonets.com/blog/data-augmentation-how-to-use-deep-learning-when-you-have-limited-data-part-2/).

Using all of the above, it is possible to achieve test accuracy above 99% for MNIST.

## `svm_mnist.py`

Finally, it is always good to have a reference to compare to. In this Python
script we set up a Support Vector Machine, using the module `sklearn` which is
in default setting, i.e., using a Gaussian kernel.

For the interested, read up on the SVM implementation of `sklearn` here:

https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

and see

https://scikit-learn.org/stable/modules/svm.html#svm-kernels

for a detailed description of the kernel parameters if you want to fine-tune.

With the default setting it achieves accuracy of

Model                                                       | Accuracy   |
----------------------------------------------------------- | ------------ |
SVM with Gaussian kernel $`K(x,x')=\exp(-\gamma (x-x')^2)`$ | 0.941700     |


