'''
Library of interpreters
-----------------------

The output of the neural network may be multi-dimensional, e.g., c many
classes. For example, for the purpose of classification, we have usually used
an encoding of the different class labels in terms of c-dimensional unit
vectors -- such an encoding is usually referred to as "one-hot" encoding as the
unit vector have only one component equal 1 and the rest equal zero. The neural
network is then trained to approximate this encoding, and therefore, its output
is a c-dimensional vector. In order to interpret this output in terms of
discreet class labels 0, 1, 2, ..., (c-1) we need an interpreter function such
as `argmax`. For example, for the purpose of regression there are, of course,
be other ways to interpret the network output.

Currently, only `argmax` is implemented. If you are interested and have time
over the semester break, add more to this library.
'''


import numpy as np


# interpreter classes

class ArgMax:
    ''' Implementation of the `argmax` function and its "inverse", the one-hot
    encoding. '''

    def val(self, x):
        '''
        Compute the `argmax` of the argument `x`. '''
        return np.argmax(x)

    def inv(self, num_out, z):
        '''
        Returns the one-hot encoding of a label `z` with `num_out` dimensional
        unit vectors (one-hot encoding).
        '''
        ret = np.zeros((num_out,1))
        ret[z] = 1.0
        return ret
