import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import multilayernet
import layers


# Extensions of the `FullyConnected` and `NN` classes for the purpose of
# diagnostics and plotting

class FullyConnected_Diag(layers.FullyConnected):
    ''' Extens the `FullyConnected` layer class of module `layer` with
    additional diagnostic functions that allow to inquire about the weights and
    parameters.  '''

    def params_weight(self):
        ''' Return parameter weight of matrix W. '''
        return self.params_weight_funct.val(self.W)

    def get_params(self):
        ''' Return matrix W. '''
        return self.W


class NN_Diag(multilayernet.NN):
    ''' Extends the neural network class `NN` of module `multilayernet` with
    diagnostic and plotting functions. '''

    def sum_params_weight(self):
        ''' Sum parameter weights of all layers. '''
        c = 0
        for l in self.layers:
            c += l.params_weight()
        return c

    def epoch_monitor(self, **kwargs):
        ''' Issues a monitor message and corresponding plots after the training
        of each epoch. If `kwargs` contains the optional `fig` parameter, in
        addition to the console message a monitor plot is generated using this
        figure handle.'''

        epoch_dts = kwargs['epoch_dts']
        batch_dts = kwargs['batch_dts']
        epochs = kwargs['epochs']
        batches = kwargs['batches']
        batch_size = kwargs['batch_size']
        train_data = kwargs['train_data']
        test_data = kwargs['test_data']

        test_objective, test_errors = self.test_network(test_data)
        test_eff = 1 - len(test_errors) / len(test_data)

        train_objective, train_errors = self.test_network(train_data)
        train_eff = 1 - len(train_errors) / len(train_data)

        params_weight = self.sum_params_weight()

        epoch_num = len(epoch_dts)
        epoch_progress = float(epoch_num) / epochs

        print('> Epoch=%d/%d (%.2f%%), Dt=%fs, '
              'Avg=%fs, Acc: %f/%f, Obj: %f/%f, Weight: %f'
              % (epoch_num,
                 epochs,
                 epoch_progress * 100,
                 epoch_dts[-1],
                 np.mean(epoch_dts),
                 test_eff, train_eff,
                 test_objective, train_objective,
                 params_weight))

        if 'fig' in kwargs['kwargs']:
            # a figure handle is know, so generate the plots
            fig = kwargs['kwargs']['fig']
            plots_mon(fig, self, epoch_num, test_eff, train_eff)


# Functions required to initialize and end the plotting, plot the monitor
# graphs, and display typical candidate image

def plots_init(nn, epochs, nw):
    ''' Prepare for interactive plotting of monitor graphs for neural network object `nn` for maximum number of epochs `epochs`
    and return figure handle. '''

    fig, _ = plt.subplots(sharex=True, sharey=True, figsize=(24, 12))
    hr = [ 1 ] * (1 + nw + len(nn.layers[1:]))
    hr[0] = 2
    wr = [ 1 ] * (1 + nw)
    wr[0] = .1
    G = gridspec.GridSpec(1 + nw + len(nn.layers[1:]), 1 + nw,
                         width_ratios=wr, height_ratios=hr)

    plt.subplot(G[0, :])
    plt.title('Accuracy Monitor')
    plt.axis([0.5, epochs+0.5, 0, 1])

    a = 0
    b = 0
    for i in range(min(nn.layers[0].num_out, nw*nw)):
        ax = plt.subplot(G[1 + b, 1 + a])
        ax.xaxis.set_ticklabels([])
        ax.yaxis.set_ticklabels([])
        a += 1
        if a >= nw:
            a = 0
            b += 1
        plt.title('%d' % i)

    for i in range(len(nn.layers[1:])):
        plt.subplot(G[1 + nw + i, 1:])
        plt.title('W^%d' % (i+1))

    ax = plt.subplot(G[1:, 0])
    sm = plt.cm.ScalarMappable(norm=plt.Normalize(vmin=-1, vmax=1))
    sm._A = []
    plt.colorbar(sm, cax=ax)

    G.tight_layout(fig)
    plt.ion()
    plt.show()

    return fig


def plots_end():
    ''' Tidy up after interactive plotting. '''

    plt.ioff()
    plt.show()


def plots_mon(fig, nn, epoch, test_eff, train_eff):
    ''' Draw interactive monitor plots. '''

    plt.sca(fig.axes[0])
    plt.scatter(epoch, train_eff, label='train', c='b', marker='o')
    plt.scatter(epoch, test_eff, label='test', c='r', marker='x')
    plt.legend(['training', 'test'], loc=4)

    W = nn.layers[0].get_params()
    for i in range(nn.layers[0].num_out):
        ax = plt.sca(fig.axes[1+i])
        im = plt.imshow(W[i][:28*28].reshape(28,28))

    for i in range(1, len(nn.layers)):
        W = nn.layers[i].get_params()
        plt.sca(fig.axes[nn.layers[0].num_out+i])
        plt.imshow(W)

    fig.canvas.draw()
    # unfortunate workaround to show the plots simultaneous to the training
    plt.pause(0.00001)

def show_error_candidates(nn, data):
    ''' Plot a couple of candidates that were predicted wrongly. '''

    print('Showing some error candidates...')

    _, error_list = nn.test_network(data)

    fig = plt.figure('Errors', figsize=(10,10))
    num = 1
    for i, l, p in error_list[:6*6]:
        plt.subplot(6, 6, num)
        num += 1
        plt.imshow(data[i][0].reshape(28,28),cmap='Blues')
        plt.title('#%d: l=%d, p=%d' % (i, l, p))

    fig.tight_layout()
    plt.show()

    print('Done.')
