''' Library of weight functions for the purpose of regularization.  '''


import numpy as np

# regularization weight classes

class NoWeight:
    '''
    No weight at all.
    '''

    def val(self, W):
        ''' Compute weight of matrix `W`.'''
        return 0

    def diff(self, W):
        ''' Derivative of weight of matrix `W`.'''
        return 0


class L1:
    '''
    L1 weight and its derivative.
    '''

    def val(self, W):
        ''' Compute weight of matrix `W`.'''
        return np.abs(W).sum() / W.size

    def diff(self, W):
        ''' A sub-derivative of weight of matrix `W`.'''
        return np.sign(W) / W.size


class L2:
    '''
    L2 weight and its derivative.
    '''

    def val(self, W):
        ''' Compute weight of matrix `W`.'''
        return 0.5 * np.square(W).sum() / W.size

    def diff(self, W):
        ''' Derivative of weight of matrix `W`.'''
        return W / W.size
