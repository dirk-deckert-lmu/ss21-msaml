'''
Library of types of layers
--------------------------

In this current implementation only the Adaline type, as discussed in the
lecture, was implemented. If you are interested and have the time, try to
implement a convolution layer in the semester break.
'''


import numpy as np

import activations as activations
import weights as weights


# layer classes

class FullyConnected:
    '''
    Implementation of a fully connected layer = Adaline type layer, as
    discussed in the lecture.
    '''

    def __init__(self, num_in, num_out, args):
        ''' Initialize the neural network layer with `num_in` many input
        and `num_out` many output neurons. Additional parameters are specified
        in `args`:
            * `activation` to specify the activation class
            * `eta` to specify the learning rate
            * `weight` to specify a regularization weight class
            * `omega` to specify the learning rate for the regularization
              weight in the loss function
            * `init` to specify how to initialize the weight matrix and bias
              components: use `norm` for a standard normal distribution and
              `norm_adapt` in order to adapt the variance as discussed in the
              lecture
        '''

        # number of in and out neurons
        self.num_in = num_in
        self.num_out = num_out

        # set activation function
        if 'activation' in args:
            self.activation = args['activation']()
        else:
            self.activation = activations.Sigmoid()

        # set learning rate
        if 'eta' in args:
            self.eta = args['eta']
        else:
            # some default value
            self.eta = 0.1

        # set parameter weight function
        if 'weight' in args:
            self.params_weight_funct = args['weight']()
        else:
            # use L2 weight as default
            self.params_weight_funct = weights.L2()

        # set regularization weight learning rate
        if 'weight' in args:
            self.params_omega = args['omega']
        else:
            # some default value
            self.params_omega = 0.1

        # initialize weights to zero
        self.W = np.zeros((num_out, num_in))
        self.b = np.zeros((num_out, 1))
        # or according to the choices
        if 'init' in args:
            if args['init'] == "norm":
                # initialize weights with normal random numbers
                self.W = np.random.randn(num_out, num_in)
                self.b = np.random.randn(num_out, 1)
            elif args['init'] == "norm_adapt":
                # initialize weights with normal random numbers
                # but reduce the variance in order not to saturate
                # the neurons
                self.W = np.random.randn(num_out, num_in) / np.sqrt(num_in)
                self.b = np.random.randn(num_out, 1)

        self.reset_delta_params()
        self.da = np.zeros( (self.b.size, self.b.size) )

    def reset_delta_params(self):
        self.delta_W = np.zeros_like(self.W)
        self.delta_b = np.zeros_like(self.b)

    def val(self, x):
        ''' Compute the activation of all output neurons. '''
        # activation input, i.e., before applying the activation function
        self.z = np.dot(self.W, x) + self.b
        # activation output, i.e., after applying the activation function
        self.x = self.activation.val(self.z)
        return self.x

    def compute_delta(self, W_next, delta_next):
        '''Compute backpropagated error delta^l from delta^l+1 as in Exercise 11-1'''
        self.delta = np.dot(W_next.T,delta_next)*self.activation.diff(self.z)
        return self.delta

    def add_grad(self, x_prev):
        '''Add gradients w.r.t the layer parameters
            for a given input x to accumulated gradients for the batch'''
        self.delta_W = self.delta_W + np.outer(self.delta, x_prev)
        self.delta_b = self.delta_b + self.delta

    def update_params(self, reps):
        ''' Update rule for this layer. '''
        avg_W = self.delta_W / reps
        avg_b = self.delta_b / reps
        # the weight matrix update incorporate both the gradient update as well
        # as the update coming from the weight regularization
        self.W = self.W \
            - (self.params_omega * self.params_weight_funct.diff(self.W) \
               + self.eta * avg_W )
        # the bias terms are only subject to a gradient update
        self.b = self.b \
            - self.eta * avg_b
