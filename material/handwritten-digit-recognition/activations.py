''' Library of activation functions.  '''


import numpy as np


# activation classes

class Sigmoid:
    ''' Logistic function and its derivative. '''

    def val(self, y):
        ''' Returns the value at `y`. '''
        return 1.0 / ( 1.0 + np.exp(-y) )

    def diff(self, y):
        a = self.val(y)
        return a * ( 1 - a ) 


class Tanh:
    ''' Tanh activation and its derivative. '''

    def val(self, y):
        ''' Returns the value at `y`. '''
        return np.tanh(y)

    def diff(self, y):
        ''' Returns the value of the derivative at `y`. '''
        return 1 / np.square( np.cosh(y) )


class Relu:
    ''' Rectified linear unit activation and a sub-derivative. '''

    def val(self, y):
        ''' Returns the value at `y`. '''
        return np.maximum(0, y)

    def diff(self, y):
        ''' Returns the value of the derivative at `y`. '''
        return ( 1.0 + np.sign(y) ) / 2.0 


class Linear:
    ''' Linear activation and its derivative. '''

    def val(self, y):
        ''' Returns the value at `y`. '''
        return y

    def diff(self, y):
        ''' Returns the value of the derivative at `y`. '''
        return 1.


