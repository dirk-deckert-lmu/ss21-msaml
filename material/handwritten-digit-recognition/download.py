''' Download the MNIST data from the Internet. '''

import urllib.request
import os
import gzip
import shutil


# CONSTANTS ###################################################################

# source URL
MNIST_URL = "https://storage.googleapis.com/cvdf-datasets/mnist/"

# source files to download
MNIST_FILES = ('train-images-idx3-ubyte.gz',
               'train-labels-idx1-ubyte.gz',
               't10k-images-idx3-ubyte.gz',
               't10k-labels-idx1-ubyte.gz')

# target directory
MNIST_DIR = os.path.dirname(os.path.realpath(__file__))
MNIST_DIR = os.path.join(MNIST_DIR, 'MNIST')

# MAIN ########################################################################

if not os.path.isdir(MNIST_DIR):
    print(f'Creating directory {MNIST_DIR}.')
    os.mkdir(MNIST_DIR)

print(f'Downloading MNIST training and test data from `{MNIST_URL}`:')
for fn in MNIST_FILES:

    url_file_name = MNIST_URL + fn
    gz_file_name = os.path.join(MNIST_DIR, fn)
    file_name = os.path.splitext(gz_file_name)[0]

    print(f'  * downloading `{url_file_name}`...', end=' ')
    urllib.request.urlretrieve(url_file_name, gz_file_name)
    print('done.')

    print(f'    * uncompressing `{gz_file_name}`...', end=' ')
    gz_f = gzip.open(gz_file_name)
    print('done.')

    print(f'    * storing `{file_name}`...', end=' ')
    f = open(file_name, 'wb')
    shutil.copyfileobj(gz_f, f)
    gz_f.close()
    f.close()
    print('done.')

print(f'All files stored at: `{MNIST_DIR}`.')
