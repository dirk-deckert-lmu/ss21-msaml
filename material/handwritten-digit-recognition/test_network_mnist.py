import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# our own neural network classes
import multilayernet
import objectives
import weights
import interpreters
import activations
import layers
import mnist

# our own class extension for monitoring
from diagnostics import *


## MAIN ##########################################

# Load the MNIST data.
data = mnist.MNIST('./MNIST/')
print('%d test images' % len(data.test_imgs))
print('%d training images' % len(data.train_imgs))

# Prepare the data, normalizing the pixel value between 0 and 255 to 0 and 1,
# and reshaping the pixel matrix to allow direct input for our neural network.
train_data = [(data.train_imgs[i].reshape(data.rows * data.cols, 1) / 255.0, \
               data.train_labels[i]) for i in range(len(data.train_imgs))]
test_data = [(data.test_imgs[i].reshape(data.rows * data.cols, 1) / 255.0, \
              data.test_labels[i]) for i in range(len(data.test_imgs))]

# Build neural network with:
# * as many input neurons as pixels = `data.rows * data.cols`
# * with `argmax` function as interpretation of the one-hot encoded labels
# * using the quadratic loss as objective function

nn = NN_Diag(data.rows * data.cols,
             interpreters.ArgMax,
             objectives.L2)

# Add a `FullyConencted_Diag` hidden layer with:
# * of `nw*nw` number of layer neurons (for convenient plotting of the weight
#   matrix components in the monitoring functions)
# * sigmoid activation.
# * learning rate `eta`
# * L2 regularization weight type
# * factor `omega` to scale the regularization weight in loss function
# * and initialize the weight matrix and bias terms with a normal distribution

nw = 4
nn.add_layer(num=nw*nw,
             layer=FullyConnected_Diag,
             activation=activations.Sigmoid,
             train='gradient',
             eta=3,
             weight=weights.L2,
             omega=0.1,
             init="norm_adapt"
            )

# Add an output layer of 10 output neurons that allow to encode the 10
# different digit labels
nn.add_layer(num=10,
             layer=FullyConnected_Diag,
             activation=activations.Sigmoid,
             train='gradient',
             eta=3,
             weight=weights.L2,
             omega=0.1,
             init="norm_adapt"
            )

# Train the network

epochs = 10
batch_size = 1000
fig = plots_init(nn, epochs, nw)
nn.batch_training(train_data, test_data, epochs, batch_size, fig=fig)

# show the error candidates
show_error_candidates(nn, test_data)

plots_end()
