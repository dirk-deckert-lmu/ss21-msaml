''' Library of objective functions, e.g., our empirical loss functions which
we will try to minimize during training. '''


import numpy as np


# objective (or loss) functions and their derivatives

class L2:
    ''' Quadratic loss value and its derivative. '''

    def val(self, xN, xBar):
        ''' Loss for prediction `xN` and encoded labels `xBar` (needs to be a
        vector, e.g., one-hot encoded of the same dimension as the prediction).
        '''
        return .5 * np.square(xN - xBar).sum()

    def diff(self, xN, xBar):
        ''' Derivative of loss for prediction `xN` and encoded labels `xBar`
        (needs to be a vector, e.g., one-hot encoded of the same dimension as
        the prediction).  '''
        return xN - xBar


class CrossEntropy:
    ''' Cross-entropy loss value and its derivative. '''

    def val(self, xN, xBar):
        ''' Loss for prediction `xN` and encoded labels `xBar` (needs to be a
        vector, e.g., one-hot encoded of the same dimension as the prediction).
        '''
        return - np.nan_to_num( np.dot(xBar.T, np.log(xN)) \
                               + np.dot((1-xBar).T, np.log(1-xN)) )

    def diff(self, xN, xBar):
        ''' Derivative of loss for prediction `xN` and encoded labels `xBar`
        (needs to be a vector, e.g., one-hot encoded of the same dimension as
        the prediction).  '''
        return np.nan_to_num( ( (xN - xBar) / (xN * (1-xN) ) ).T )
