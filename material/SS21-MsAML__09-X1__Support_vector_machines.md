# Exercise 09-1: Support vector machines [[PDF](SS21-MsAML__09-X1__Support_vector_machines.pdf)]

a) Adapt our Adaline implementation to minimize the SVM empirical loss using a subderivative for the gradient.

b) Discuss the notion of generalization of the hard margin SVM, in particular regarding the dependence on support vectors, and compare to other possible notions.