# MsAML-12-S1 [[PDF](SS21-MsAML__12-S1__Value_function.pdf)]

1. [Construction of the value function](#construction-of-the-value-function)

Back to [index](index.md).


## Construction of the value function

Recall how our definitions of the policy and state-action value functions
depended on the future rewards. It should be possible to compute expectations
of such accumulated quantities on the basis of the transition probabilities
$\mathcal P$ of the MDP.  Indeed, based on our definition of the policy and
state-action value, we find the so-called *Bellmann equation* given
additionally the reward function $R$, policy $\pi$, and state-action pair $(s,a)$:
\begin{align}
  V_\pi(s)
  &=
  E\left(
    \sum_{t=0}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1})
    \,\Big|\, 
    S_0=s
    \right)
  \\
  &=
  E\left(
    R(s,\pi(s),S_1)+\gamma V_\pi(S_1) 
    \,\Big|\, 
    S_0=s
  \right)
  \\
  &=
  \sum_{s'\in\mathcal S}
  p(s'|s,\pi(s))
  \left[
    R(s,\pi(s),s')+\gamma V_\pi(s')
  \right],
  \label{eq_B1}
  \tag{B1}
\end{align}
and similarly,
\begin{align}
  Q_\pi(s,a)
  &=
  \sum_{s'\in\mathcal S}
  p(s'|s,a)
  \left[
    R(s,a,s')+\gamma Q_\pi(s',\pi(s'))
  \right].
  \label{eq_B2}
  \tag{B2}
\end{align}
As before, observe the simple relation $V_\pi(s)=Q_\pi(s,\pi(s))$.

It will strike the eye that the Bellman equations $\eqref{eq_B1}$ and
$\eqref{eq_B2}$ are of a fixed-point form. Therefore, tuning $\gamma<1$ one may
construct solutions with the help of Banach's fixed point argument.

For this purpose, consider the sets
\begin{align}
  \mathcal V
  &:= 
  \left\{
    V:\mathcal S\to\mathcal R
    \,\big|\,
    V \text{ measurable}
  \right\},\\
  \mathcal Q
  &:= 
  \left\{
    Q:\mathcal S\times \mathcal A\to\mathcal R
    \,\big|\,
    Q \text{ measurable}
  \right\}
\end{align}
on which we define the following self-maps in vein of $\eqref{eq_B1}$ and
$\eqref{eq_B2}$:
\begin{align}
  T_\pi^{\mathcal V}: \mathcal V &\to \mathcal V
  \\
  V &\mapsto 
  \left(
    s\mapsto \sum_{s'\in\mathcal S}
    p(s'|s,\pi(s))
    \left[
      R(s,\pi(s),s')+\gamma V(s')
    \right]
  \right),
  \\
  T_\pi^{\mathcal Q}: \mathcal Q &\to \mathcal Q
  \\
  Q &\mapsto 
  \left(
    (s,a)\mapsto
    \sum_{s'\in\mathcal S}
    p(s'|s,a)
    \left[
      R(s,a,s')+\gamma Q(s',\pi(s'))
    \right]
  \right).
\end{align}
Recursively, for every $k\in\mathbb N$, $V\in\mathcal V$, $Q\in\mathcal Q$, we
define the repetitive application of these self-maps as
\begin{align}
  (T_\pi^{\mathcal V})^k(V)
  &:=
  T^{\mathcal V}_\pi\left((T_\pi^{\mathcal V})^{k-1}(V)\right),
  \\
  T_\pi^{\mathcal Q})^k(Q)
  &:=
  T^{\mathcal Q}_\pi\left((T_\pi^{\mathcal Q})^{k-1}(Q)\right)
\end{align} 
together with $(T^{\mathcal V}_\pi)^0(V):=V$ and $(T^{\mathcal Q}_\pi)^0(Q):=Q$.

The fixed-points of these self-maps are exactly the policy and state-action value functions as the following theorem shows.

Theorem
: ##### (Construction of the policy and state-action functions) 
  Let state space $\mathcal S$ and action space $\mathcal A$ be finite and
  $\gamma\in[0,1)$. Then, for all $V\in\mathcal V$ and $Q\in\mathcal Q$, the
  equalities
  \begin{align}
    \lim_{k\to\infty} \max_{s\in\mathcal S} 
    \left|
    V_\pi(s)-(T_\pi^{\mathcal V})^k(V)
    \right|
    &=
    0,\\
    \lim_{k\to\infty} \max_{s\in\mathcal S,a\in\mathcal A} 
    \left|
    Q_\pi(s,a)-(T_\pi^{\mathcal Q})^k(Q)(s,a)
    \right|
    &=
    0
  \end{align}
  hold true.

**Proof:** We only prove the convergence to the policy value function $V_\pi$.
The convergence of the state-action value function
$Q_\pi$ can be shown analogously.

Since $\mathcal S$ is finite, for any $V_1,V_2\in
\mathcal V$, the following map is well-defined 
\begin{align}
  d(V_1,V_2) := \max_{s\in\mathcal S}\left|V_1(s)-V_2(s)\right|.
\end{align}
and gives rise to a metric $d$ on $\mathcal V$. 

Moreover, $(\mathcal V,d)$ is a complete metric space. To see this, take a
Cauchy sequence $(V_n)_{n\in\mathbb N}$ in $(\mathcal V,d)$, and observe that,
thanks to the uniformity in $\mathcal S$ of the metric $d$, we find for
each $s\in\mathcal S$ that $(V_n(s))_{n\in\mathbb N}$ is a Cauchy sequence in
$(\mathbb R,|\cdot|)$. But $(\mathbb R,|\cdot|)$ is complete, which implies
that there exists a limit $V(s):=\lim_{n\to\infty}\mathcal V_n(s)$. Exploiting
the finiteness of $\mathcal S$ again, we find
\begin{align}
  d(V,V_n)
  =
  \max_{s\in\mathcal S}\left|V(s)-V_n(s)\right| 
  \leq 
  \sum_{s\in\mathcal S}\left|V(s)-V_n(s)\right|  
  \underset{n\to\infty}{\to} 0
\end{align}
and also that $V$ is measurable, hence $V\in\mathcal V$. In conclusion,
$(\mathcal V,d)$ is a complete metric space.

Given a $V\in\mathcal V$ we introduce the short-hand notation
\begin{align}
  \qquad V_k:=(T_\pi^{\mathcal V})^k(V)
  \qquad
  \text{for} \qquad k\in\mathbb N_0
\end{align}
and turn to studying the possible convergence properties of this sequence in
$k$.

Based on the definition of $V_{k+1}$ and $V_k$ and the fact that $\mathcal P$
are probability distributions, we infer
\begin{align}
  d(V_{k+1},V_k)
  &=
  \max_{s\in\mathcal S}\left|V_{k+1}(s)-V_k(s)\right|
  \\
  &=
  \max_{s\in\mathcal S}\left|
    \sum_{s'\in\mathcal S}
    p(s'|s,\pi(s)) \gamma \left[V_k(s')-V_{k-1}(s')\right]
  \right|
  \\
  &\leq
  \gamma   
  \max_{s\in\mathcal S} \left|V_k(s)-V_{k-1}(s)\right|
  \;
  \underbrace{\sum_{s'\in\mathcal S} p(s'|s,\pi(s))}_{=1}
  \\
  &\leq
  \gamma
  \;
  d(V_k,V_{k-1}).
\end{align}
In other words, the self-map $T^{\mathcal V}_\pi$ is $\gamma$-contractive
on $(\mathcal V,d)$.

By induction, we find for all $k\in\mathbb N$
\begin{align}
  d(V_{k+1},V_k) \leq \gamma^k\; d(V_1,V_0)
\end{align}
and, with Banach's standard argument, conclude the $(\mathcal V,d)$-Cauchy
property of the sequence $(V_n)_{n\in\mathbb N_0}$ as follows. By exploiting
the properties of the metric $d$, for all $n>m\in\mathbb N$ we get
\begin{align}
  d(V_n,V_m) 
  &\leq 
  \sum_{k=m}^{n-1} d(V_{k+1},V_k)
  \\
  &\leq 
  d(V_{1},V_0) \sum_{k=m}^{\infty} \gamma^k
  \underset{m\to\infty}{\to} 0
\end{align}
as the geometric sequence is convergent. Thanks to completeness of $(\mathcal
V,d)$ and the properties of the limit, there exists a unique
$V^*\in\mathcal V$ such that
\begin{align}
  \lim_{n\to\infty}d(V^*,V_n) = 0.
\end{align}

Lastly, we need to ensure that this limit point also has the correct
properties. Take $n\in\mathbb N,s\in\mathcal S$ and consider
\begin{align}
  V_{n+1}(s)
  &=
  T_\pi^{\mathcal V}(V_n)(s)
  \\
  &=
  \sum_{s'\in\mathcal S}
  p(s'|s,\pi(s)) \left[R(s,\pi(s),s')+\gamma V_n(s')\right].
\end{align}
We can now take the limits of the left- and right-hand side to conclude
\begin{align}
  V^*(s)
  =
  \sum_{s'\in\mathcal S}
  p(s'|s,\pi(s)) \left[R(s,\pi(s),s')+\gamma V^*(s')\right].
\end{align}
This implies $V^*=V_\pi^{\mathcal V}$, or in other words, the fixed-point $V^*$
is exactly our policy value function $V_\pi^{\mathcal V}$.

$\square$

Note that the above theorem does not only guarantee the existence and
uniqueness of $V_\pi$ and $Q_\pi$ given a policy $\pi$ but also specifies a
construction of these functions. The ingredients of the construction comprise
the transition amplitudes $\mathcal P$ of the MDP, the reward functions $R$,
and the policy $\pi$.

Obviously, our long-term goal is to maximize $V_\pi$ for as many states
$s\in\mathcal S$ as possible. The question is how to learn a convenient
strategy $\pi$ to accomplish this task. Typically, although finite, the
combinatorial complexity does not allow a brute force search -- recall the
discussion about the number of position in a Go game. Another complication is
that the transition probabilities $\mathcal P$ are typically unknown to us. 

As next, steps we will therefore define optimal strategies and prove their
existence. In a final step we will then attempt to approximate them by means of
algorithms that, e.g., update given initial policies conveniently, and come back to the question of the unknown transition probabilities $\mathcal P$.

➢ Next session!
