<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__13-1__Dual_formulation_SVM</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-13-1-pdf">MsAML-13-1 [<a href="SS21-MsAML__13-1__Dual_formulation_SVM.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#dual-formulation-of-the-svm">Dual formulation of the SVM</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="dual-formulation-of-the-svm">Dual formulation of the SVM</h2>
<p>Given a Lagrangian <span class="math display">\[\begin{align}
  \mathcal L:D\times(\mathbb R^+_0)^N &amp;\to \mathbb R\\
  (x,\alpha) &amp;\mapsto \mathcal L(x,\alpha):=f(x)+\sum_{i=1}^N\alpha_i g_i(x)
\end{align}\]</span> for the <a href="SS21-MsAML__12-1__Short_review_optimization.html">previously defined</a> general program <span class="math display">\[\begin{align}
  \min_{x\in D} f(x)
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    g_i(x)\leq 0\\
    i=1,\ldots, N
  \end{cases}
  \,,
  \label{eq_P}
  \tag{P}
\end{align}\]</span> we may associate a so-called <em>Lagrange dual</em> function by defining <span class="math display">\[\begin{align}
  \mathcal D:(\mathbb R^+_0)^N &amp;\to \mathbb R\\
  \alpha &amp;\mapsto \mathcal D(\alpha):=\inf_{x\in D}\mathcal L(x,\alpha).
\end{align}\]</span></p>
<p>Due to the linearity of <span class="math inline">\(\mathcal L\)</span> in <span class="math inline">\(\alpha\)</span> and the properties of the infimum, <span class="math inline">\(\mathcal D\)</span> is always concave.</p>
<p>For every <span class="math inline">\(\tilde x\in D\)</span> fulfilling the constraints, we have <span class="math display">\[\begin{align}
  \forall \alpha\in (\mathbb R^+_0)^N: \, \mathcal L(\tilde x,\alpha) \leq f(\tilde x).
\end{align}\]</span> And in turn, for all <span class="math inline">\(\alpha\in (\mathbb R^+_0)^N\)</span>, we find <span class="math display">\[\begin{align}
  \mathcal D(\alpha) = \inf_{x\in D} \mathcal L(x,\alpha)
  \leq
  \mathcal L(\tilde x,\alpha)
  \leq f(\tilde x)
\end{align}\]</span> so that also the optimal value <span class="math inline">\(p^*\)</span> of <span class="math inline">\(\eqref{eq_P}\)</span> we have <span class="math display">\[\begin{align}
  \forall \alpha\in(\mathbb R^+_0)^N:\, \mathcal D(\alpha)\leq p^*.
  \label{eq_1}
  \tag{E1}
\end{align}\]</span></p>
<p><strong>Example:</strong> As a fictitious example (taken from the boook <em>Convex optimization</em> of Boyd &amp; Vandenberghe), let us consider only one constraint <span class="math inline">\(g(x)\leq 0\)</span>, i.e., <span class="math inline">\(N=1\)</span> and <span class="math inline">\(g=g_i\)</span>. The following figure illustrates a possible objective function <span class="math inline">\(f\)</span>, constraint function <span class="math inline">\(g\)</span>, and a family of plots of the Lagrange function <span class="math inline">\(\mathcal L(x,\alpha)\)</span> for different choices of <span class="math inline">\(\alpha\)</span>:</p>
<figure>
<img src="13/objetive_constraint_lagrange.png" alt="Objective, constraint, and Lagrange functions for the example of one constraint." /><figcaption aria-hidden="true">Objective, constraint, and Lagrange functions for the example of one constraint.</figcaption>
</figure>
<p>Although neither the objective function <span class="math inline">\(f\)</span>, nor the constraint function <span class="math inline">\(g\)</span> is convex, the Lagrange dual <span class="math inline">\(\mathcal D\)</span> shown in the next plot is concave:</p>
<figure>
<img src="13/dual.png" alt="Lagrange dual function in the example above." /><figcaption aria-hidden="true">Lagrange dual function in the example above.</figcaption>
</figure>
<p>The Lagrange dual <span class="math inline">\(\mathcal D\)</span> naturally leads to another optimization problem:</p>
<p><span class="math display">\[\begin{align}
  \max_{\alpha\in\mathbb R^N} \mathcal D(\alpha)
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    \alpha_i\leq 0\\
    i=1,\ldots, N
  \end{cases}
  \,.
  \label{eq_D}
  \tag{D}
\end{align}\]</span></p>
<p>One usually refers to <span class="math inline">\(\eqref{eq_P}\)</span> as the <em>primal</em> and <span class="math inline">\(\eqref{eq_D}\)</span> as the corresponding <em>dual program</em>.</p>
<p>Note that thanks to concavity of <span class="math inline">\(\mathcal D\)</span>, the dual program is always a convex optimization problem. For <span class="math inline">\(d^*\)</span> denoting the optimal value of <span class="math inline">\(\eqref{eq_D}\)</span>, relation <span class="math inline">\(\eqref{eq_1}\)</span> implies <span class="math display">\[\begin{align}
  d^*\leq p^*
\end{align}\]</span> and the difference <span class="math inline">\((p^*-d^*)\)</span> is called duality gap.</p>
<p>It can be shown that a convex program fulfilling the Slater condition also fulfills <span class="math display">\[\begin{align}
  d^*=p^*.
\end{align}\]</span> As a proof of this fact is the content of an optimization course, we omit it here and refer the interest reader, e.g., to the <a href="SS21-MsAML__12-1__Short_review_optimization.html">already mentioned</a> standard literature.</p>
<p>Coming back to the minimization program of the support vector machine, <span class="math display">\[\begin{align}
  \begin{split}
    &amp;\inf_{\bar w=(b,w)\in\mathbb R^{d+1}}
    \left[
      \frac12 |w|^2 + C\sum_{i=1}^N \xi^{(i)}
    \right]
    \\
    &amp;\qquad \text{subject to}
    \quad
    \begin{cases}
      &amp;y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1 - \xi^{(i)},\\
      &amp;\xi^{(i)} \geq 0,\\
      &amp;\text{for all } i=1,\ldots, N
    \end{cases}
    \,,
  \end{split}
  \label{eq_SVM}
  \tag{SVM}
\end{align}\]</span> here in the soft-margin case with an additional constant <span class="math inline">\(C\geq 0\)</span> to fine-tune the penalty for larger slack variables, we can now recast it into its dual formulation.</p>
<p>The Lagrangian reads <span class="math display">\[\begin{align}
  &amp;\mathcal L\left(
    \underbrace{w,b,\xi}_{\text{The &quot;$x$&quot; in previous notation.}},
    \underbrace{\alpha,\beta}_{\text{The &quot;$\alpha$&quot; in previous notation.}}
  \right)\\
  &amp;=
  \underbrace{\frac12 |w|^2+C\sum_{i=1}^N\xi^{(i)}}_{\text{The &quot;$f(x)$&quot; in previous notation.}}\\
  &amp;\qquad
  -\sum_{i=1} \underbrace{\alpha_i}_{\text{First part of the &quot;$\alpha$&quot; in previous notation.}}
  \quad
  \underbrace{\left(y^{(i)}(w\cdot x^{(i)}+b)-1+\xi^{(i)}\right)}_{\text{First type of constraints.}} 
  \\
  &amp;\qquad
  -\sum_{i=1}^N \underbrace{\beta_i}_{\text{Second part of the &quot;$\alpha$&quot; in previous notation.}}\quad \underbrace{\xi^{(i)}}_{\text{Second type of constraints.}}.
\end{align}\]</span></p>
<p>Following the same steps as in <a href="SS21-MsAML__12-X1__KKT_criterion.pdf">exercise 12-1</a>, in which the hard-margin case was treated, we can apply the KKT criterion to find <span class="math display">\[\begin{align}
  \nabla_w\mathcal L = 0 &amp;\Rightarrow w=\sum_{i=1}^N\alpha_i y^{(i)}x^{(i)},
  \label{eq_2}
  \tag{E2}
  \\
  \nabla_b\mathcal L = 0 &amp;\Rightarrow \sum_{i=1}^N \alpha_i y^{(i)} = 0,
  \label{eq_3}
  \tag{E3}
  \\
  \nabla_{\xi^{(i)}}\mathcal L = 0 &amp;\Rightarrow \alpha_i+\beta_i = C.
  \label{eq_4}
  \tag{E4}
\end{align}\]</span> by means of <span class="math inline">\(\fbox{KKT1}\)</span> and in addition for all <span class="math inline">\(i=1,\ldots,N\)</span> <span class="math display">\[\begin{align}
  \alpha_i\left(
    y^{(i)}(w\cdot x^{(i)}+b)-1+\xi^{(i)}
  \right) 
  &amp;= 0
  \\
  \beta_i\xi^{(i)} &amp;= 0.
\end{align}\]</span> based on <span class="math inline">\(\fbox{KKT2-3}\)</span>. It may strike the eye that the KKT criterion for soft-margin case does not differ that much from the hard-margin case.</p>
<p>In order to compute the Lagrange dual function for the soft-margin support vector machine, we need to find the infimum in <span class="math inline">\((w,b,\xi)\)</span> of the Lagrange function. This can be done by exploiting Fermat’s theorem, where <span class="math inline">\(\eqref{eq_2}\)</span>, <span class="math inline">\(\eqref{eq_3}\)</span>, and <span class="math inline">\(\eqref{eq_4}\)</span> come in handy. In the first step, we plug in <span class="math inline">\(\eqref{eq_2}\)</span> and observe <span class="math display">\[\begin{align}
  \mathcal L\Big|_{w=\sum_{i=1}^N\alpha_i y^{(i)} x^{(i)}}
  =
  &amp;\frac12 \left|\sum_{i=1}^N \alpha_i y^{(i)} x^{(i)}\right|^2
  \\
  &amp;+
  C\sum_{i=1}^N \xi^{(i)}
  \\
  &amp;-
  \sum_{i=1}^N \alpha_i y^{(i)}\sum_{j=1}^N \alpha_j y^{(j)} \; x^{(j)}\cdot x^{(i)}
  \\
  &amp;-
  \sum_{i=1}^N \alpha_i y^{(i)}b
  \\
  &amp;-
  \sum_{i=1}^N \alpha_i \xi^{(i)}
  \\
  &amp;+
  \sum_{i=1}^N \alpha_i 
\end{align}\]</span> In a second step, we make use of <span class="math inline">\(\eqref{eq_3}\)</span> and collect the many summands to find <span class="math display">\[\begin{align}
  \mathcal L\Big|_{w=\sum_{i=1}^N\alpha_i y^{(i)} x^{(i)}}
  =
  \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
  y^{(j)} \; x^{(i)} \cdot x^{(j)}.
\end{align}\]</span> Since this is now a function of only the Lagrange multipliers <span class="math inline">\((\alpha,\beta)\)</span>, notably even independent of <span class="math inline">\(\beta\)</span>, we have already found our dual Lagrange function <span class="math display">\[\begin{align}
  \mathcal D(\alpha, \beta) 
  = 
  \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
  y^{(j)} \; x^{(i)} \cdot x^{(j)}.
\end{align}\]</span></p>
<p>The dual formulation of corresponding to <span class="math inline">\(\eqref{eq_SVM}\)</span> therefore reads <span class="math display">\[\begin{align}
  &amp;\max_{\alpha\in \mathbb R^N}
  \left(
    \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
    y^{(j)} \; x^{(i)} \cdot x^{(j)}.
  \right)
  \\
  &amp;\qquad
  \text{subject to}
  \qquad
  \begin{cases}
    &amp;0\leq \alpha_i\leq C,
    \\
    &amp;\sum_{i=1}^N \alpha_i y^{(i)} = 0,
    \\
    &amp;\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
  \label{eq_dSVM}
  \tag{dSVM}
\end{align}\]</span></p>
<p>As mentioned in our general discussion, this dual program is concave. Furthermore, it is infinitely often differentiable and therefore a smooth quadratic optimization program for which very efficient numerical solvers are available. A very popular and efficient implementation can be found here <a href="https://en.wikipedia.org/wiki/LIBSVM">LIBSVM</a>, which has bindings to many languages including Python; see <a href="https://scikit-learn.org/0.15/modules/generated/sklearn.svm.libsvm.fit.html">scikit learn</a>.</p>
<p>Note that, given an optimal point <span class="math inline">\(\alpha^*\)</span>, we can directly compute the hypothesis <span class="math display">\[\begin{align}
  h(x) 
  &amp;= 
  \text{sign} \left(w\cdot x+b\right)
  \\
  &amp;= 
  \text{sign} \left(\sum_{i=1}^N\alpha_i y^{(i)}\, x^{(i)}\cdot x + b\right),
\end{align}\]</span> where the bias term <span class="math inline">\(b\)</span> can be inferred from any support vector <span class="math inline">\(x^{(s)}\)</span> with label <span class="math inline">\(y^{(s)}\)</span>, i.e., any <span class="math inline">\(s=1,\ldots,N\)</span> such that <span class="math inline">\(w\cdot x^{(i)}+b=y^{(i)}\)</span>, which results in <span class="math display">\[\begin{align}
  b = y^{(s)} - \sum_{i=1}^N \alpha_i y^{(i)}\; x^{(i)}\cdot x^{(s)}.
\end{align}\]</span></p>
<p>This dual formulation not only allows for efficient solvers but has another advantage, namely to readily generalize the support vector machine to the case of non-linear classification problems. Recall our initial discussion of <a href="SS21-MsAML__10-1__Non-linear_classification.html">non-linear classification problems</a>, where we introduced a non-linear map <span class="math inline">\(\Phi\)</span> on feature space <span class="math inline">\(\mathcal X\)</span> that maps to a higher-dimensional one <span class="math inline">\(\mathcal X&#39;\)</span> in order to render the corresponding image of the training data linearly separable. A drawback of this route was the fact that adding these further dimensions may increase the number of computation steps during training substantially. Taking a look at <span class="math inline">\(\eqref{eq_dSVM}\)</span>, it would require to replace the inner product <span class="math inline">\(x^{(i)}\cdot x^{(j)}\)</span> in <span class="math inline">\(\mathcal X\)</span> by <span class="math display">\[\begin{align}
  \Phi(x^{(i)})\cdot \Phi(x^{(j)})
\end{align}\]</span> in <span class="math inline">\(\mathcal X&#39;\)</span>, which roughly implies as many computation steps as dimensions in <span class="math inline">\(\mathcal X&#39;\)</span>. This is no news to us but now this inner product structure of <span class="math inline">\(\eqref{eq_dSVM}\)</span> allows for a short-cut. Instead of simply equipping <span class="math inline">\(\mathcal X&#39;\)</span> with the standard euclidean inner product, let us define the inner product by means of an evaluation of a function <span class="math inline">\(K:\mathcal X\times\mathcal X\to\mathbb R\)</span>, i.e., <span class="math display">\[\begin{align}
  \Phi(x^{(i)})\cdot \Phi(x^{(j)}) := K(x^{(i)},x^{(j)}).
\end{align}\]</span> Obviously, we need to choose <span class="math inline">\(K\)</span> carefully in order to end up defining an inner product on <span class="math inline">\(\mathcal X&#39;\)</span> and we will look into this particular class of so-called <em>kernel functions</em> in our last module. However, to get into this mode of thinking, why defining the inner product in terms of a kernel function is beneficial, consider, e.g., the popular choice <span class="math display">\[\begin{align}
  K(x,x&#39;) := \exp\left(-\frac{|x-x&#39;|^2}{2\sigma^2}\right)
\end{align}\]</span> for some with parameter <span class="math inline">\(\sigma&gt;0\)</span>. The latter is called <em>gaussian kernel</em>. Note that the argument of the exponential function involves only an inner product in <span class="math inline">\(\mathcal X\)</span> and therefore does not excessively blow up the computation steps. Roughly the only additional computational effort is to evaluate the exponential for which there are efficient algorithms and, in worst case, we could simply use a look-up table. On the other hand, recall how the simple non-linear classification example in <a href="SS21-MsAML__10-1__Non-linear_classification.html">non-linear classification</a> was made linearly separable by only adding a quadratic polynomial. The gaussian kernel already comprises a limit of polynomials of order <span class="math inline">\(N\)</span> for <span class="math inline">\(N\to\infty\)</span>, and in a sense corresponds to an infinite dimensional <span class="math inline">\(\mathcal X&#39;\)</span> in the above language. This justifies the hope for another very general way to approach non-linear classification by supplying a kernel function to the dual support vector machine program.</p>
<p>In the last module, let us use conclude our first round into of non-linear classification by a study of admissible kernel functions.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
