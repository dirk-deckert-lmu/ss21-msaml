% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-07-s2-pdf}{%
\section{\texorpdfstring{MsAML-07-S2
{[}\href{SS21-MsAML__07-S2__VC-Dimension.pdf}{PDF}{]}}{MsAML-07-S2 {[}PDF{]}}}\label{msaml-07-s2-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{the-vc-dimension}{The VC-dimension}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{the-vc-dimension}{%
\subsection{The VC-dimension}\label{the-vc-dimension}}

In the special case of binary classification, i.e., \(|\mathcal Y|=2\),
there is an interesting dichotomy in the behavior of the growth
function. At first, for \(N\) smaller or equal a particular number
\(d\in\mathbb N_0\), the function \(\Pi_{\mathcal F}(N)\) grows at
maximal rate, i.e., as \(2^N\), while for \(N>d\) its increase is
polynomially bounded. The transition point \(N=d\) is called the
\emph{Vapnik-Chervonenkis} dimension or VC-dimension of \(\mathcal F\).
The latter natural number will already prove sufficient to characterize
the ``richness'' of hypotheses in \(\mathcal F\) and provide respective
generalization bounds.

\begin{description}
\item[Definition] ~ 
\hypertarget{vapnik-chervonenkis-dimension}{%
\subparagraph{(Vapnik-Chervonenkis
dimension)}\label{vapnik-chervonenkis-dimension}}

Given a set of relevant hypotheses
\(\mathcal F\subseteq {\mathcal  Y}^{\mathcal X}\) for
\(|\mathcal Y|=2\), we define \begin{align}
\dim_\text{VC}\mathcal F := 
\begin{cases}
  \max\left\{
    n\in\mathbb N_0
    \,\big|\,
    \Pi_{\mathcal F}(N)=2^N
  \right\}
  & \text{if the maximum exists}\\
  \infty
  & \text{otherwise}
\end{cases}\,.
  \end{align}
\end{description}

Recall the definition \begin{align}
  \Pi_{\mathcal F}(N)=\max_{x_1,\ldots,x_N\in\mathcal X} 
  \Big|
    \mathcal F|_{x_1,\ldots, x_N}
  \Big|
\end{align} for the generated patterns \begin{align}
    \mathcal F|_{x_1,\ldots, x_N}
    =
    \left\{
      (h(x_1),\ldots, h(x_N)
      \,\big|\,
      h\in\mathcal F
    \right\}.
\end{align} Since \(\mathcal F|_{x_1,\ldots,x_N}\subseteq \mathcal Y^N\)
we already have the bound \begin{align}
  \Pi_{\mathcal F}(N) \leq 2^N,
\end{align} as we only consider the case of binary classification.

Therefore, if \(\dim_\text{VC}\mathcal F=d\), \(d\) is the largest
number such that there exist a vector \begin{align}
  (x_1,\ldots,x_d) \in \mathcal X^d
\end{align} with the property that \begin{align}
  \mathcal F|_{x_1,\ldots,x_d}
  =
  \mathcal Y^{x_1,\ldots, x_d}.
\end{align}

It is useful to introduce the notion of \emph{shattering}. We will say,
a set of \(N\in\mathbb N_0\) points
\(P=\{x_1,\ldots,x_N\}\subseteq \mathcal X\) is shattered by the
hypotheses in \(\mathcal F\), if \begin{align}
  \Big|\mathcal F|_P\Big| = 2^{|P|},
\end{align} or equivalently, \begin{align}
  \Big|\left\{(h(x_1),\ldots,h(x_N)\}
  \,\big|\, 
  h\in\mathcal F
  \right\}
  \Big|
  = 2^N,
\end{align} in other words, the amount of patterns
\((h(x_1),\ldots,h(x_N))\) generated by \(h\in\mathcal F\) equals the
maximum number of possible configurations \(2^N\) of ``bit'' sequences.

In order to get familiar with this new concept, let us regards two
examples:

\textbf{Example 1:} Consider \(\mathcal X=\mathbb R\) and the set of
threshold functions \begin{align}
  {\mathcal F}_\text{threshold} 
  :=
  \left\{
    h_b:x\mapsto \text{sign}(x-b)
    \,\big|\,
    b\in\mathbb R
  \right\}.
\end{align}

Recalling the definition \begin{align}
  \Pi_{\mathcal F}(N)=\max_{x_1,\ldots,x_N\in\mathcal X}
  \left|\left\{
    (h(x_1),\ldots,h(x_N))
    \,\big|\,
    h\in\mathcal F
  \right\}\right|,
\end{align} we compute:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Case \(N=1\): \begin{align}
    &\Pi_{{\mathcal F}_\text{threshold}}(1)
    =\left|\left\{(-1),(+1)\right\}\right|
    =2
  \end{align} because, depending on \(x\in\mathbb R\), one always finds
  a \(b\in\mathcal b\) such that \(h_b(x)\) is either \(-1\) or \(+1\).
\item
  Case \(N=2\): \begin{align}
    \Pi_{{\mathcal F}_\text{threshold}}(1)
    =\left|\left\{(-1,-1),(-1,1),(1,1)\right\}\right|
    =3
  \end{align} because, by virtue of the threshold functions, after the
  first classification of \(h_b(x)=1\) for some \(x\in\mathbb R\) we
  have \(h_b(x')=1\) for all \(x'\geq x\). Hence, the pattern
  \((+1,-1)\) cannot be generated.
\item
  Case \(N\in\mathbb N\): \begin{align}
    &\Pi_{{\mathcal F}_\text{threshold}}(N)
    \\
    &=\left|\left\{
      (\underbrace{-1,\ldots,-1}_{n\text{ many}},\underbrace{1,\ldots, 1}_{m\text{ many}})
      \,\big|\,
      n,m \in\mathbb N: n+m=N
    \right\}\right|
    \\
    &=N+1
  \end{align} because, as noted above, after the first classification of
  \(h_b(x)=1\) for some \(x\in\mathbb R\) we have \(h_b(x')=1\) for all
  \(x'\geq x\). Hence, the pattern \((+1,-1)\) cannot be generated.
\end{enumerate}

Thanks to this general formula, we can compute the VC-dimension by
solving \begin{align}
  N+1 = \Pi_{{\mathcal F}_{\text{threshold}}}(N)=2^N
\end{align} and taking the maximum, which in this simple example gives
\begin{align}
  \dim_\text{VC}{\mathcal F}_{\text{threshold}}=1. 
\end{align}

\textbf{Example 2:} Let \(\mathcal X=\mathbb R^2\),
\(\mathcal Y=\{0,1\}\), and \begin{align}
  {\mathcal F}_{\text{rectangle}}
  =
  \left\{
    h_R:x\mapsto 1_R(x)
    \,\big|\,
    R\subset \mathbb R^2
    \text{ being an axis-aligned rectangle}
  \right\}.
\end{align}

First, we notice that, for \(N=4\), there are \(2^4\) patterns produced
by the various choices of axis-aligned rectangle. For example, the
configuration of points \((x_1,x_2,x_3,x_4)\in\mathbb R^2\) such that

\begin{figure}
\centering
\includegraphics{07/rectangle_0000.png}
\caption{Pattern \((0,0,0,0)\) for four points.}
\end{figure}

produces the pattern \((0,0,0,0)\). Similarly, we find

\begin{figure}
\centering
\includegraphics{07/rectangle_others.png}
\caption{Other patterns for four points.}
\end{figure}

and likewise all permutations, which sum up to \(2^4\) possible
patterns. We may therefore conclude that \begin{align}
  \dim_\text{VC}{\mathcal F}_{\text{rectangle}}\geq 4.
\end{align} The first restriction shows up for \(N=5\) because there is
no axis-aligned rectangle that could produce the following
pattern/configuration:

\begin{figure}
\centering
\includegraphics{07/rectangle_nogo.png}
\caption{Pattern for a given configuration that cannot be generated by
axis-aligned rectangles.}
\end{figure}

It will even turn out that
\(\dim_\text{VC}{\mathcal F}_{\text{rectangle}}=4\) as the loss in the
number of possible patterns cannot be made up for for larger \(N\).

The central result about the growth function is the following bound:

\begin{description}
\item[Lemma] ~ 
\hypertarget{sauers-lemma}{%
\subparagraph{(Sauer's Lemma)}\label{sauers-lemma}}

Given \(|\mathcal Y|=2\) and hypotheses space \(\mathcal F\) with
\(\dim_\text{VC}=d\), we have \begin{align}
\Pi_{\mathcal F}(N)\leq \sum_{k=0}^d
\begin{pmatrix}
  N\\
  k
\end{pmatrix}.
\label{eq_1}
\tag{E1}
  \end{align}
\end{description}

\textbf{Proof:} We prove this by induction over \((N,d)\).

\emph{Case \((N,d)\in\{(1,0),(1,1)\}\):} As \(d\) is the largest number
in \(\mathbb N_0\) such that \begin{align}
  \max_{x_1,\ldots,x_d}\left|\mathcal F|_{x_1,\ldots,x_d}\right| = 2^d,
\end{align} and on the other hand, for all
\(n\in\mathbb N_0: |\mathcal F|_{x_1,\ldots,x_n}\leq 2^n\), we find
\begin{align}
  d=0 \Rightarrow \qquad &\forall x\in\mathcal X: |\mathcal F|_x|=1\\
  &\text{ so that } \Pi_{\mathcal F}(1)=1\leq \sum_{k=0}^0
  \begin{pmatrix}
    1\\
    k
  \end{pmatrix}
  =1,\\
  d=1 \Rightarrow \qquad & \Pi_{\mathcal F}(1)\leq 2=\sum_{k=0}^1
  \begin{pmatrix}
    1\\
    k
  \end{pmatrix}.
\end{align}

Now, let us assume that \(\eqref{eq_1}\) holds for
\((N,d)\in\{(N-1,d-1),(N-1,d)\}\). Let
\(P=\{x_1,\ldots,x_N\}\subset \mathcal X\) such that \(|P|=N\). Let
\(\mathcal G\) be a class of functions over \(\{x_1,\ldots, x_N\}\) such
that \begin{align}
 \mathcal G|_P=\mathcal F|_P. 
\end{align} Note that this implies \begin{align}
  \dim_\text{VC}\mathcal G\leq \dim_\text{VC}\mathcal F
\end{align} because for all possible \(\widetilde P\subseteq P\) that is
shattered by \(\mathcal G\), it is also shattered by \(\mathcal F\).

For each possible pattern \((h(x_1),\ldots,h(x_{N-1}))\) generated by a
\(h\in\mathcal G\) we add one representative to a previously empty set
\(\mathcal G_1\) and define \begin{align}
  \mathcal G_2 := \mathcal G\setminus \mathcal G_1.
\end{align}

Hence, for \(h\in\mathcal G_2\) there is a
\(\widetilde h\in\mathcal G_1\) such that \begin{align}
  \forall i=1,\ldots,N-1: h(x_i)=\widetilde h(x_i)
\end{align} while \begin{align}
  h(x_N)\neq \widetilde h(x_N).
\end{align}

Without loss of generality, the function representatives above can be
chosen such that \begin{align}
  h(x_N)=1,\qquad \widetilde h(x_N)=0,
  \qquad
  \text{and} 
  \qquad
  \forall h\in\mathcal G_2: h(x_N)=1.
\end{align}

By construction, we have \begin{align}
  \Big|
    \mathcal F|_P
  \Big|
  =
  \Big|
    \mathcal G|_P
  \Big|
  =
  \Big|
    \mathcal G_1|_P
  \Big|
  +
  \Big|
    \mathcal G_2|_P
  \Big|
\end{align} and, since \(\mathcal G_1\subseteq \mathcal G\) the estimate
\begin{align}
  \dim_\text{VC} \mathcal G_1\leq \dim_\text{VC}\mathcal G\leq \dim_\text{VC}\mathcal F=d
\end{align} holds true.

Furthermore, let \(\widetilde P=\{x_1,\ldots,x_{N-1}\}\), then
\begin{align}
  \Big|\mathcal G_1|_P\Big|
  =
  \Big|\mathcal G_1|_{\widetilde P}\Big|
\end{align} because, on the one hand, \begin{align}
  \Big|\mathcal G_1|_P\Big|
  \geq
  \Big|\mathcal G_1|_{\widetilde P}\Big|,
\end{align} and on the other hand, there is no \(h\) induced pattern of
\(\widetilde P\) such that both \begin{align}
  (h(x_1),\ldots, h(x_{N-1}),0)
  \qquad
  \text{and}
  \qquad
  (h(x_1),\ldots, h(x_{N-1}),1)
\end{align} are in \(\mathcal G_1|_P\), which implies \begin{align}
  \Big|\mathcal G_1|_P\Big|
  \leq
  \Big|\mathcal G_1|_{\widetilde P}\Big|.
\end{align}

Now we can exploit our induction hypothesis as follows \begin{align}
  \Big|\mathcal G_1|_P\Big|
  =
  \Big|\mathcal G_1|_{\widetilde P}\Big|
  \leq
  \Pi_{\mathcal G_1}(N-1)\leq \sum_{k=0}^d
  \begin{pmatrix}
    N-1\\
    k
  \end{pmatrix}.
\end{align} Moreover, if \(Q\subseteq P\) is shattered by
\(\mathcal G_2\), then \(x_N\notin P\) and \(Q\cup\{x_N\}\) is shattered
by \(\mathcal G\) because, for \(h\in\mathcal G_2\), there is a
\(\widetilde h\in\mathcal G_1\) such that
\(h(x_N)\neq \widetilde h(x_N)\).

This implies \begin{align}
  \dim_\text{VC}\mathcal G_2
  \leq
  \dim_\text{VC}\mathcal G - 1
  = d-1.
\end{align}

Finally, \begin{align}
  \Big|\mathcal G_2|_P\Big|
  =
  \Big|\mathcal G_2|_{\widetilde P}\Big|
\end{align} and, using the induction hypothesis again, gives
\begin{align}
  \Big|\mathcal G_2|_P\Big|
  =
  \Big|\mathcal G_2|_{\widetilde P}\Big|
  \leq
  \sum_{k=0}^{d-1}
  \begin{pmatrix}
    N-1\\
    k
  \end{pmatrix}.
\end{align}

In conclusion, we have shown \begin{align}
  \Big|\mathcal F|_P\Big|
  &=
  \Big|\mathcal G_1|_P\Big|
  +
  \Big|\mathcal G_2|_P\Big|
  \\
  &\leq
  \sum_{k=0}^{d}
  \begin{pmatrix}
    N-1\\
    k
  \end{pmatrix}
  +
  \sum_{k=0}^{d-1}
  \begin{pmatrix}
    N-1\\
    k
  \end{pmatrix}
  \\
  &=
  \sum_{k=0}^{d}
  \begin{pmatrix}
    N-1\\
    k
  \end{pmatrix},
\end{align} and by induction, for all \(N\) and \(d\): \begin{align}
  \Pi_{\mathcal F}(N)
  =
  \max_{x_1,\ldots,x_N\in\mathcal X}\Big|\mathcal F|_P\Big|
  \leq \sum_{k=0}^d
    \begin{pmatrix}
      N\\
      k
    \end{pmatrix}.
\end{align}

\(\square\)

With this result, we can now characterize the growth behavior of
\(\Pi_{\mathcal F}(N)\) in \(N\) by means of the VC-dimension as
follows:

\begin{description}
\item[Theorem] ~ 
\hypertarget{vc-dimension-based-growth-function-estimate}{%
\subparagraph{(VC-dimension based growth function
estimate)}\label{vc-dimension-based-growth-function-estimate}}

Let \(|\mathcal Y|=2\), \(\mathcal F\) a space of relevant hypotheses
with \(d=\dim_\text{VC}\mathcal F<\infty\). Then, for
\(N\in\mathcal N\), the relations \begin{align}
\Pi_{\mathcal F}(N)
\begin{cases}
  =2^N & \text{for }N\leq d\\
  \leq \left(\frac{eN}{d}\right)^d
  & \text{for }
  N>d
\end{cases}\,.
  \end{align} holds true.
\end{description}

\textbf{Proof:} For \(N\leq d\), by definition,
\(\Pi_{\mathcal F}(N)=2^N\).

For \(N>d\) we have \(0<\frac{d}{N}<1\), and hence, \begin{align}
  \left(\frac{d}{N}\right)^d
  \sum_{k=0}^d
  \begin{pmatrix}
    N\\
    k
  \end{pmatrix}
  &\leq
  \sum_{k=0}^d
  \begin{pmatrix}
    N\\
    k
  \end{pmatrix}
  \left(\frac{d}{N}\right)^k
  \\
  &\leq
  \sum_{k=0}^N
  \begin{pmatrix}
    N\\
    k
  \end{pmatrix}
  \left(\frac{d}{N}\right)^k
  \cdot 1^{N-k}
  \\
  = \left(1+\frac{d}{N}\right)^N
  \leq e^d.
\end{align}

\(\square\)

Coming back to our generalization bounds, the last result implies:

\begin{description}
\item[Corollay] ~ 
\hypertarget{vc-dimension-based-generalization-bound}{%
\subparagraph{(VC-dimension based generalization
bound)}\label{vc-dimension-based-generalization-bound}}

Let \(|\mathcal Y|=2\) and \(\mathcal F\subseteq \mathcal H\) a space of
relevant hypotheses with \(d=\dim_\text{VC}\mathcal F<\infty\). Then for
all \(\delta\in(0,1]\) and all \(h\in\mathcal F\): \begin{align}
R(h)\leq \widehat R_S(h) +
\sqrt{\frac{2\log\left(\frac{eN}{d}\right)^d}{N}} +
\sqrt{\frac{\log\frac2\delta}{2N}}.
  \end{align} with probability of at least \(1-\delta\).
\end{description}

We observe that our bound on \(|R(h)-\widehat R_S(h)|\) is then
crucially controlled by the ratio \begin{align}
  \frac{\log\frac{N}{d}}{\frac{N}{d}}.
\end{align} Hence, the approximation of \(R(h)\) through
\(\widehat R_S(h)\) is better for larger training sample sizes \(N\) and
smaller VC-dimensions, or in other words, less ``rich'' \(\mathcal F\).

\end{document}
