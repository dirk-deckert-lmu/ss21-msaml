% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{exercise-01-x1-set-up-a-python-development-environment-pdf}{%
\section{\texorpdfstring{Exercise 01-X1: Set up a Python development
environment
{[}\href{SS21-MsAML__01-X1__Set_up_a_Python_development_environment.pdf}{PDF}{]}}{Exercise 01-X1: Set up a Python development environment {[}PDF{]}}}\label{exercise-01-x1-set-up-a-python-development-environment-pdf}}

Depending on the platform macOS, Linux/Unix, Windows, etc. you are using
there are several options to set up a Python development environment.
There are several ingredients of a Python development environment:

\textbf{Essential ingredients:}

\begin{itemize}
\tightlist
\item
  A Python distribution including Python interpreter and the standard
  library of Python packages, each of which provides certain Python
  functionality;
\item
  Any kind of text editor to edit a \texttt{your\_script.py} text file
  to be interpreted by Python typically using the console command;
  \texttt{\textgreater{}\textgreater{}\ python\ your\_script.py}
\item
  The Python debugger \texttt{pdb} that is actually a Python package
  included in the distribution.
\end{itemize}

\textbf{Convenient ingredients:}

\begin{itemize}
\tightlist
\item
  An integrated development environment such as
  \href{https://www.jetbrains.com/de-de/pycharm/}{PyCharm},
  \href{https://code.visualstudio.com/}{VS Code}, etc. that allows to
  edit, run, debug Python scripts, inspect the console, features code
  completion and linting, helps to organize projects, employ version
  control, etc. without having to leave the application;
\end{itemize}

\textbf{Something light-weight in between:}

\begin{itemize}
\tightlist
\item
  \href{https://jupyter.org}{Jupyter Notebook} which is a web-based
  interactive development environment for
  \href{https://jupyter.org}{Jupyter Notebooks} that allow to create
  mixed documents of live code, equations, visualizations and narrative
  text.
\end{itemize}

All of the above require to install a Python distribution. For this
there are two approaches:

\begin{itemize}
\item
  \emph{Worry-free bundle:} Anaconda by Continuum Analytics is a free
  bundle of a Python distribution that, among other things, ships with
  the essential data science Python packages.

  \begin{itemize}
  \tightlist
  \item
    \href{https://www.anaconda.com/distribution/}{Download page}
  \item
    \href{https://docs.anaconda.com/anaconda/install/}{Install
    instructions}
  \item
    \href{https://docs.anaconda.com/anaconda/user-guide/getting-started/}{Quick
    start guide}
  \end{itemize}

  After a successful install, further Python packages can be installed
  using the console command

\begin{verbatim}
  conda install package_name_to_install
\end{verbatim}

  and update outdated Python packages

\begin{verbatim}
  conda update package_name_to_update
\end{verbatim}
\item
  \emph{Hands-on method:} While the above Anaconda bundle provides an
  easy install procedure and does not require much maintenance it is
  also quite rigid. For a slightly more difficult but more minimal and
  configurable installation you can install the Python distribution
  manually. The platform dependent installation procedures are well
  explained on the \href{https://www.python.org}{official Python site}
  and the latest version can be obtained here:

  \begin{itemize}
  \tightlist
  \item
    https://www.python.org/downloads/
  \end{itemize}

  Platforms such as macOS and Unix/Linux come with a pre-installed
  Python distribution, however, maybe with only the old Python 2 version
  in which case a Python 3 installation is necessary. Check the version
  by

\begin{verbatim}
>> python --version
\end{verbatim}

  Sometimes a parallel installation of a Python 3 interpreter carries
  the name \texttt{python3}, so also try:

\begin{verbatim}
>> python3 --version
\end{verbatim}

  Python 3 can also be conveniently installed using common software
  package manager. For example:

  \begin{itemize}
  \item
    \href{https://brew.sh/}{Homebrew} for macOS, once installed using
    the command

\begin{verbatim}
>> /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
\end{verbatim}

    allows to install the Python distribution simply by

\begin{verbatim}
brew install python
\end{verbatim}
  \item
    Similarly on Linux distributions such as Ubuntu you can use the
    standard software package manager to install Python as follows

\begin{verbatim}
sudo apt-get install python3 
\end{verbatim}
  \end{itemize}

  After a successful install, missing Python packages can be installed
  using the console. For example, some standard package we may used can
  be installed as follows:

\begin{verbatim}
  pip install numpy scipy matplotlib pandas jupyter
\end{verbatim}

  and they can be updated with

\begin{verbatim}
  pip install --upgrade package_name_to_update
\end{verbatim}
\end{itemize}

As a started kit, especially, if you have not yet used an integrated
development environment, I recommend you to install the Python
distribution with Anaconda and take a look at Jupyter Notebook or Lab
that is included. If you are working with VS Code the python extension
will also allow you to load Jupyter notebook and work with them
interactively as explained
\href{https://code.visualstudio.com/docs/python/jupyter-support}{here}
and
\href{https://code.visualstudio.com/docs/python/jupyter-support-py}{here}.

It has a focus on data science and suits our purposes well as it allows
for a quick and dynamic \emph{call and response} type of programming
mixing code with documentation that is even nicely rendered on GitLab --
though it is a terrible format for version tracking software. Once
installed, it can be launched by:

\begin{verbatim}
>> jupyter notebook
\end{verbatim}

to edit a single Jupyter notebook or

\begin{verbatim}
>> jupyter lab
\end{verbatim}

for the more sophisticated environment.

With an installation approach of your choice, set up a Python
development environment and write your first data science equivalent of
a hello world program:

\begin{itemize}
\tightlist
\item
  Hello world of data science
  \href{./01/hello_world_of_data_science.py}{Python Script},
  \href{./01/hello_world_of_data_science.ipynb}{Python Notebook}{]}
\end{itemize}

\end{document}
