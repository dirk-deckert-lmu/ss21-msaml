# Exercise 09-S2: Model selection [[PDF](SS21-MsAML__09-XS2__Model_selection.pdf)]

In case we have multiple models at hand for some learning task, we need criteria to decide which ones to choose. From the viewpoint of PAC learning, we would want to choose a model that  has a sufficient complexity to achieve a desired classification performance but not much more than needed as then the danger of overfitting increases.

a) For this problem we may use structural risk minimization as a tool to conduct training with multiple models without deciding in advance which model to use. Instead, we let the trade-off between bias and complexity decide which hypotheses of which model hypotheses space $\mathcal{H}_n$ to pick for a particular learning task.

Suppose $d_n:=\dim_{VC}(\mathcal{H}_n) < \infty$ for $n\in \mathbb{N}$. Show that with probability at least $1-\delta, n\in\mathbb{N}$ and $h\in\mathcal{H}_n$ we have 

$$R(h)\leq \hat{R}_S(h) + C \sqrt{\frac{g(d_n) + \log\frac{1}{\delta}+2\log n + 1}{N}}$$

for sufficiently large $N$ with constant $C$ and monotonically increasing $g$.

For given sample size $N$ and confidence $\delta$ the SRM algorithm will then search for $n\in\mathbb{N}$ and $h\in\mathcal{H}_n$ to minimize the right-hand side.

b) If access to training data is not an issue, we can use a more direct approach which also yields a more direct bound on the risk. 

Assume that in addition to the training sample $S$ we have a validation sample $V$ that consists of $M$ new i.i.d data samples. We train $K\in\mathbb{N}$ different algorithms on the training sample, which results in hypotheses $\{h_1,\dots,h_K\}$. Show that then with probability at least $1-\delta$ we have 

$$\forall k=1,\dots,K, \ |R(h_k)-\hat{R}_V(h_k)|\leq \sqrt{\frac{\log\frac{2K}{\delta}}{2M}}.$$