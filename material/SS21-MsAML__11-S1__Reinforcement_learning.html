<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__11-S1__Reinforcement_learning</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-11-s1-pdf">MsAML-11-S1 [<a href="SS21-MsAML__11-S1__Reinforcement_learning.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#reinforcement-learning">Reinforcement learning</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<p>Since we are getting towards the end of our lecture, we can allow ourselves some playtime with a more specialized topic of supervised learning, namely <em>reinforcement learning</em>. Since the content of the next modules may be comparably bigger as compared to the average module sizes so far, we will regard the lecture notes as both lectures and exercises, and we will try to reduce the workload of the exercises correspondingly. As always, we encourage you even more to go through the lecture notes with pencil and paper</p>
<h2 id="reinforcement-learning">Reinforcement learning</h2>
<p>Until now, we have worked under the premise that we were given a sufficiently large set of high quality training data in order to conduct the training of our supervised learners. In many practical applications, such gold standard training data is simply not available, very costly to compile, and there are even scenarios in which the a priori complexity of the learning tasks appears as so high that any endeavor to generate an correspondingly sufficient amount of training data may seem futile. This is often the case when not only one task but several intermediate tasks, e.g., carried out successively in time, are required to achieve an <em>overall goal</em>. Take, for example the games of chess and Go. The total number of combinations of possible <em>states</em>, e.g., the positions in the games, and possible <em>actions</em>, e.g., the valid moves are extremly large; even only the number of positions in the Go game is already exorbitantly high, see for instance <a href="https://en.wikipedia.org/wiki/Go_and_mathematics#Complexity_of_certain_Go_configurations">here</a>.</p>
<p>Another typical example would be an <em>agent</em>, e.g., an autonomous vehicle operating in an unknown <em>environment</em> and trying to get from point A to point B under certain conditions such as maneuverability of the terrain, battery power, etc. Luckily, in those situations, the effectiveness of the <em>actions</em> taken by the agent can often be evaluated in perspective of an overall goal to some extend during the process. If such an online evaluation is possible, it delivers high quality annotated training data “for free”, which can then be employed to optimize the learners future choices in order to achieve the overall goal.</p>
<p>Each newly taken action of the agent may involve both <em>exploitation</em> of previous acquired knowledge about the learning task and <em>exploration</em> by, e.g., once in a while carrying out a random action to learn from its consequences. As it turns out, the balance between exploration and exploitation is the key to an efficient training of the agent.</p>
<p>Furthermore, this approach addresses the problem of the possibly large number of combinations of state and action pairs. Since the training data is generated along the way, it is much more likely to be generated with a beneficial distribution reflecting the a priori unknown actual distribution that one picks up in typical paths through the state-action space. The agent therefore naturally focuses more on these common paths, e.g., the common openings of a chess game, instead of all theoretically possible ones.</p>
<p>Reinforcement learning provides a framework for this type of learning tasks. For the sake of generality, let us abstract the learning scenarios and from now on employ the jargon that we have already introduced above:</p>
<ul>
<li>The learner is typically referred to as an “agent”.</li>
<li>The agent carries out actions which allows it to interact with an “environment”.</li>
<li>Each action may change the “state” of the agent or both the agent and the environment and the environment provides feedback, usually referred to as “reward”, which allows the agent to evaluate its actions in perspective to an “overall goal”.</li>
</ul>
<p>In summary:</p>
<figure>
<img src="11/reinforcement_learning.png" alt="Reinforcement learning scenario." /><figcaption aria-hidden="true">Reinforcement learning scenario.</figcaption>
</figure>
<p>Instead of passively receiving the annotated training data at once before training, as in the case of our previously discussed supervised learners, the agent collects training data in a continuous manner through the course of its interaction with its environment. In response of each action, the agent receives two types of information. The new agent’s state and a reward in perspective of the overall goal. Note that it requires a priori knowledge in order to interpret the reward in an effective way in order to improve the learning task in perspective of the overall goal.</p>
<p>In our settings we will assume the reward to be real-valued and model the overall goal of the agent to maximize the overall accumulated reward. An agent’s strategy to optimize the overall accumulated reward is usually referred to as a <em>policy</em>. Note that a strategy such as greedily choosing those actions that maximize the reward in the very next step may not result in a maximum accumulated reward. For instance, though traveling from A to be B as the crow flies might result in the agent’s maximal reward per time step, but its success streak might abruptly come to an end at a cliff due to the lack of wings. The difficulty is to find a good strategy that also incorporates which local detours may be necessary to maximize accumulated reward globally – regarding our climate policies, this is often a hard task even for humans.</p>
<p>So in contrast to our previously discussed supervised learning scenarios, first, the distribution with respect to which training data is “drawn” is now policy-dependent, and therefore, not fixed, and second, training and testing data are somewhat intertwined during the whole training process.</p>
<p>There are two main settings for reinforcement learning. One in which the model of the environment is known to the agent, and one in which it is not. The chess and Go games are examples of the former kind and the autonomous vehicle in an unknown environment of the latter one. Even if the environment ist known completely, maneuvering in it in perspective of an overall goal might still be a highly non-trivial learning task for the agent as, e.g., in a Go games, a brute force search for an optimal path is simply infeasible.</p>
<p>The underlying mathematical model describing the states, actions, and rewards in time which we will use is the so-called <em>Markov decision process</em>:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="markov-decision-process">(Markov decision process)</h5>
Given the triple <span class="math inline">\((\mathcal S,\mathcal A,\mathcal P)\)</span> comprising a set of possible states <span class="math inline">\(\mathcal S\)</span>, a set of possible actions <span class="math inline">\(\mathcal A\)</span>, and a family of transition probabilities <span class="math display">\[\begin{align}
P=\left((p(s&#39;|s,a)\right)_{\substack{s,s&#39;\mathcal S\\a\in\mathcal A}}
  \end{align}\]</span> we call a sequence of random variables <span class="math display">\[\begin{align}
(S_t,A_t)_{t\in\mathbb N_0},
\qquad
\forall t\in\mathbb N_0: S_t\in\mathcal S,A_t\in\mathcal A,
  \end{align}\]</span> fulfilling for all <span class="math inline">\(t\in\mathbb N_0\)</span>, <span class="math inline">\(s&#39;s,s_t,\ldots, s_0\in\mathcal S\)</span>, and <span class="math inline">\(a,a_t,\ldots, a_0\in\mathcal A\)</span> <span class="math display">\[\begin{align}
&amp;P(S_{t+1}=s&#39;|S_t=s_t,A_t=a_t,\ldots,S_0=s_0,A_0=a_0)
\\
&amp;=:p(s&#39;|s_t,a_t)
\label{eq_1}
\tag{E1}
  \end{align}\]</span> a Markov decision process (MDP).
</dd>
</dl>
<p>The quantity <span class="math inline">\(p(s&#39;|s,a)\)</span> reflects how likely a transition from state <span class="math inline">\(s\)</span> to state <span class="math inline">\(s&#39;\)</span> as consequence of action <span class="math inline">\(a\)</span> occurs.</p>
<p>By definition, the process <span class="math inline">\((S_t,A_t)_{t\in\mathbb N_0}\)</span> has the Markov property <span class="math display">\[\begin{align}
  &amp;P(S_{t+1}=s&#39;|S_t=s_t,A_t=a_t,\ldots,S_0=s_0,A_0=a_0)\\
  &amp;=
  P(S_{t+1}=s&#39;|S_t=s_t,A_t=a_t)\\
  &amp;=p(s&#39;|s_t,a_t),
\end{align}\]</span> and hence, the name MDP.</p>
<p>The properties of the discreteness in the steps as well as the Markov property are of course only model assumptions which can and, depending on the setting, must be generalized. We will however stick to them.</p>
<p>It is important to observe that <span class="math inline">\(\eqref{eq_1}\)</span> must be supplied with an initial distribution of <span class="math inline">\(S_0\)</span> and <span class="math inline">\(A_0\)</span> together with a mechanism how to choose <span class="math inline">\(A_{t+1}\)</span> on the basis of <span class="math inline">\(S_{t+1}\)</span> in order to construct a realization of the process. As this mechanism shall be informed by the reward we introduce it next:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="reward-function">(Reward function)</h5>
A reward function is a measurable map of the form: <span class="math display">\[\begin{align}
R:\mathcal S\times\mathcal A\times\mathcal S &amp;\to \mathbb R\\
(s,a,s&#39;) &amp;\mapsto R(s,a,s&#39;)
  \end{align}\]</span>
</dd>
</dl>
<p>Again, more generally, it might make sense to consider more general mechanism that also depends on the whole history of the process. Moreover, the reward of the environment could even be stochastic, i.e., it could be given in terms of a process <span class="math inline">\((R_t)_{t\in\mathbb N}\)</span>. Again, we will only stick to the above deterministic one.</p>
<p><strong>Example:</strong> For a Tic-Tac-Toe game, the state space <span class="math inline">\(\mathcal S\)</span> could be modeled as reflecting all possible game board positions by means of <span class="math display">\[\begin{align}
  \mathcal S=\{-1,0,+1\}^{3\times 3}.
\end{align}\]</span> For example:</p>
<figure>
<img src="11/tic-tac-toe.png" alt="Representation example of a game board position." /><figcaption aria-hidden="true">Representation example of a game board position.</figcaption>
</figure>
<p>In this example <span class="math inline">\(-1\)</span> denotes an X, <span class="math inline">\(+1\)</span> an O, and zero an empty slot.</p>
<p>Let us imagine player 1 as the environment placing the X’s and the player 2 being the agent placing the O’s. The possible actions of the player could be modeled by <span class="math display">\[\begin{align}
  \mathcal A=\{1,2,\ldots,9\},
\end{align}\]</span> each number denoting a placement of an O in the enumerated slot according to the table:</p>
<figure>
<img src="11/tic-tac-toe_actions.png" alt="Representation of actions." /><figcaption aria-hidden="true">Representation of actions.</figcaption>
</figure>
<p>Depending on the action <span class="math inline">\(A_t\)</span> chosen by the agent following the game policy that also may incorporate restrictions, as for example occupied slots cannot receive another O, the state <span class="math inline">\(S_t\)</span> changes to <span class="math inline">\(S_{t+1}\)</span> as the game either stops or a new slot will be occupied by an X of player 1, in other words, in response of the environment and according to an unknown transition probability <span class="math inline">\(p(s&#39;|s,a)\)</span> for <span class="math inline">\(s=S_t\)</span> and <span class="math inline">\(a=A_t\)</span>.</p>
<p>In scenarios in which the environment comprises other players, it is often convenient to let more than one MDP play against each other by interpreting the actions of each player as the environment of the respective others.</p>
<p>As discussed, the reward function must be chosen to be meaningful with respect to the overall goal, and hence, involves a priori knowledge. Our choice for the Tic-Tac-Toe game could be as follows:</p>
<ul>
<li>For every triple <span class="math inline">\((s,a,s&#39;)\in\mathcal S\times\mathcal A\times\mathcal S\)</span> such that <span class="math inline">\(s&#39;\)</span> implies a victory of the game, our agent receives a <span class="math inline">\(r(s,a,s&#39;)=+1\)</span>.</li>
<li>For <span class="math inline">\(s&#39;\)</span> implying a loss of the game, our agent receives a <span class="math inline">\(r(s,a,s&#39;)=-1\)</span>.</li>
<li>In every other case, i.e., a tie or a continuing game, the agent receives <span class="math inline">\(r(s,a,s&#39;)=0\)</span>.</li>
</ul>
<p>Adapting the reward function to <span class="math inline">\(r(s,a,s&#39;)=-\frac 12\)</span> in case of a tie would drive the agent’s policy towards avoiding ties as long as it does not mean a loss of the game since the latter receives an even lower reward <span class="math inline">\(r(s,a,s&#39;)=-1\)</span>. As it maybe known to the reader, in our case, this choice does not play a big role for the game, as the complexity is so low that there is an algorithm how to avoid a loss. However, it illustrates how the choice of a local reward may influence the search of the agent for an optimal policy that maximizes the overall accumulate reward.</p>
<p>After this example, let us make the term policy precise, which shall drive the mechanism by means of which the agent chooses actions <span class="math inline">\(A_{t+1}\in\mathcal A\)</span> based on the state <span class="math inline">\(S_{t+1}\)</span>:</p>
<dl>
<dt>Definition:</dt>
<dd><h5 id="policy">(Policy)</h5>
We call a map <span class="math display">\[\begin{align}
\pi:\mathcal S\to\mathcal A
  \end{align}\]</span> a deterministic policy (or sometimes strategy) and say that an MDP <span class="math inline">\((S_t,A_t)_{t\in\mathbb N_0}\)</span> follows a policy <span class="math inline">\(\pi\)</span> if <span class="math display">\[\begin{align}
\forall t\in\mathbb N: \qquad A_t=\pi(S_t)
  \end{align}\]</span>
</dd>
</dl>
<p>This notion can be extended to depend on the entire history of the process or model actions stochastically, in which case <span class="math inline">\(\pi\)</span> takes values in the probability distributions over actions in <span class="math inline">\(\mathcal A\)</span> and the probability that the agents chooses an action <span class="math inline">\(a\in\mathcal A\)</span> given a state <span class="math inline">\(s\in\mathcal S\)</span> is then given by <span class="math inline">\((\pi(s))(a)\)</span>. Adding further generality, the policy may depend on time <span class="math inline">\(t\in\mathbb N_0\)</span>, i.e., <span class="math inline">\((\pi_t(s))(a)\)</span>. It turns out that MDPs that run only for finite times <span class="math inline">\(t\in\{0,1,\ldots,T\}\)</span> often even require a time-dependency of the policy in order to be optimal. We will however focus on the idealization of an “infinite time horizon”, i.e., <span class="math inline">\(t\in\mathbb N_0\)</span>, and stick to deterministic policies.</p>
<p>In order to evaluate a policy in view of the accumulated overall reward we define its value:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="policy-value">(Policy value)</h5>
Let <span class="math inline">\(\gamma\in[0,1]\)</span>, <span class="math inline">\((S_t,A_t)_{t\in\mathbb N_0}\)</span> an MDP, and <span class="math inline">\(\pi\)</span> a policy. We define the policy value for the MDP started at a state <span class="math inline">\(s\in\mathcal S\)</span> according to <span class="math display">\[\begin{align}
V_\pi(s) := E\left(\sum_{t=0}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1})\bigg|S_0=s\right),
\label{eq_2}
\tag{E2}
  \end{align}\]</span> in other words, the expected accumulated reward weighted with factors <span class="math inline">\(\gamma^t\)</span>.
</dd>
</dl>
<p>Should it be well-defined, one can quickly recast the right-hand side of <span class="math inline">\(\eqref{eq_2}\)</span> into the form, which will later be useful: <span class="math display">\[\begin{align}
  V_\pi(s)=E\left(
    R(s,\pi(s),S_1) + \sum_{t=1}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1}) \bigg| S_0=s
  \right).
\end{align}\]</span></p>
<p>Note that the value of a policy depends on all future states. The parameter <span class="math inline">\(\gamma\)</span> therefore fine-tunes how much influence possible future states may have. In a probabilistic setting, it could be problematic to set <span class="math inline">\(\gamma=1\)</span> as, e.g., due to an aggressive policy of the agent’s opponent in the game, most of these states may never be reached. In contrast, <span class="math inline">\(\gamma=0\)</span> would neglect all possible future rewards and only give value to the immediate reward. For practical purposes, the value of <span class="math inline">\(\gamma\)</span> is usually tuned close to but below 1.</p>
<p>Of course, one may ask why the weight was chosen in this special form of the decay <span class="math inline">\(\gamma^t\)</span> for <span class="math inline">\(t\to\infty\)</span>. The simple answer is that in great parts our strategy of proofs will require a certain decay in time <span class="math inline">\(t\)</span>. Slower decays may be possible will require more technology in the proofs and are often also practically not as easily implemented.</p>
<p>In a similar fashion one defines another helpful quantity, namely the state-action value:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="state-action-value">(State-action value)</h5>
Let <span class="math inline">\(\gamma\in[0,1]\)</span>, <span class="math inline">\((S_t,A_t)_{t\in\mathbb N_0}\)</span> an MDP, and <span class="math inline">\(\pi\)</span> a policy. We define the state-action value for the MDP started at a state <span class="math inline">\(s\in\mathcal S\)</span> with action <span class="math inline">\(a\in\mathcal A\)</span> according to <span class="math display">\[\begin{align}
Q_\pi(s,a):=E\left(
  R(s,a,S_1) + \sum_{t=1}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1}) \bigg| S_0=s, A_0=a
\right).
  \end{align}\]</span>
</dd>
</dl>
<p>Again presuming the well-undefinedness, a useful observation is that for all <span class="math inline">\(s\in\mathcal S\)</span> we have <span class="math display">\[\begin{align}
  V_\pi(s)=Q_\pi(s,\pi(s)).
\end{align}\]</span> Hence, both function contain the information regarding the accumulated reward. However, <span class="math inline">\(Q_\pi(s,a)\)</span> will prove convenient to understand how policy values should be adapted, e.g., how to choose another <span class="math inline">\(a\in\mathcal A\)</span> rather than <span class="math inline">\(a=\pi(s)\)</span> as a next action in order to optimize the strategy.</p>
<p>Ok, so far that was only definitions and vocabulary but we are over the hump now. Our program will be to construct the value function (next module), and afterwards, discuss optimality of a policy, and algorithms that approximate an optimal policy.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
