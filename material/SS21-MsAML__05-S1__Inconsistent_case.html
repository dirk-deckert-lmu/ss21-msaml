<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__05-S1__Inconsistent_case</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-05-s1-pdf">MsAML-05-S1 [<a href="SS21-MsAML__05-1__Inconsistent_case.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#inconsistent-case">Inconsistent case</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="inconsistent-case">Inconsistent case</h2>
<p>For more general cases, we may not hold on to our consistency assumption anymore, i.e., that for any concept <span class="math inline">\(c\)</span> and training data <span class="math inline">\(s\)</span> the algorithm <span class="math inline">\(\mathcal A\)</span> returns a consistent hypothesis <span class="math inline">\(h_s\)</span>, i.e., one fulfilling <span class="math inline">\(\widehat R_s(h_s)=0\)</span>.</p>
<p>One may rightfully expect that a failure of consistency will even typically be the case. First, before we can utilize the computer as optimizer we need to discretized the data which may lead to discrepancies. Next, the range of the algorithm <span class="math inline">\(\mathcal A\)</span> may only approximate a concept but not contain it. Furthermore, the optimization task carried out by the algorithm <span class="math inline">\(\mathcal A\)</span> might be rather complex and the particulars of the algorithm <span class="math inline">\(\mathcal A\)</span> may not guarantee a successful risk minimization; see our Adaline update rule for an example.</p>
<p>In turn, the actual risk <span class="math inline">\(R(h_S)\)</span> may even be bounded away from zero due to the constraints above. Nevertheless, an inconsistent hypothesis having a small number of classification errors can still be useful, and under certain assumptions we may still prove learning guarantees that allow to estimate the difference between actual and empirical risk. Here is a first example in the setting of last module but without requiring consistency:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="general-case-for-finite-hypothesis-spaces">(General case for finite hypothesis spaces)</h5>
Let <span class="math inline">\(\mathcal H\)</span> be any finite hypothesis set. Then, for any <span class="math inline">\(\delta&gt;0\)</span>, the following inequality holds true with probability of a t least <span class="math inline">\(1-\delta\)</span>: <span class="math display">\[\begin{align}
\forall h\in\mathcal H:
\left|\widehat R_S(h)-R(h)\right|
&lt; 
\sqrt{
  \frac{
    \log\frac{2|\mathcal H|}{\delta}}
  {2N}
}.
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> Since <span class="math inline">\(\mathcal H\)</span> is finite, we may label the hypotheses, for example like so <span class="math inline">\(\mathcal H=\{h_1, h_2, h_3,\ldots,h_M\}\)</span> such that <span class="math inline">\(|\mathcal H|=M\)</span>.</p>
<p>For an <span class="math inline">\(\epsilon&gt;0\)</span>, let us regard the expression <span class="math display">\[\begin{align}
  &amp;P_{S\sim P^N}\left(
    \exists h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \geq
    \epsilon
  \right)
  \\
  &amp;= P_{S\sim P^N}
  \left(
    \bigcup_{j=1}^M
    \left\{
      S=s \,\big|\,
      \left|
        \widehat R_s(h_j)-R(h_j)
      \right|
      \geq \epsilon
    \right\}
  \right)
  \\
  &amp;\leq
  \sum_{j=1}^M 
  P_{S\sim P^N}
  \left(
    \left|
      \widehat R_S(h_j)-R(h_j)
    \right|
    \geq \epsilon
  \right),
\end{align}\]</span> where we simply have rewrote the definition of the event in the argument of the measure in the first line and then exploited the union bound.</p>
<p>Now we recall the tool we have prepared in the last module about <a href="SS21-MsAML__04-S2__Concentration_inequalities.html">concentration inequalities</a>, i.e., Hoeffding’s inequality:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="hoeffdings-inequality">(Hoeffding’s inequality)</h5>
Let <span class="math inline">\(X_1,\ldots,X_N\)</span> be independent random variables with <span class="math inline">\(\text{range}X_i\subseteq [a_i,b_i]\)</span> for reals <span class="math inline">\(a_i&lt;b_i\)</span>, <span class="math inline">\(i=1,\ldots, N\)</span>. Then, for the empirical expectation <span class="math inline">\(\widehat S_N=\frac1N\sum_{i=1}^N X_i\)</span> and any <span class="math inline">\(\epsilon&gt;0\)</span> we find: <span class="math display">\[\begin{align}
P\left(\left| \widehat S_N- E\widehat S_N\right| \geq \epsilon\right) 
\leq 
2\exp\left(-\frac{2\epsilon^2N^2}{\sum_{i=1}^N(b_i-a_i)^2}\right).
  \end{align}\]</span>
</dd>
</dl>
<p>Setting <span class="math inline">\(X_i\equiv 1_{Y^{(i)}\neq h(X^{(i)})}\)</span>, observing that the corresponding range fulfills <span class="math inline">\(\text{range}X_i\subseteq [0,1]\)</span>, and recalling <span class="math display">\[\begin{align}
  R(h)=E_{S\sim P^N}\widehat R_S(h),
\end{align}\]</span> we find the estimate <span class="math display">\[\begin{align}
  P_{S\sim P^N}
  \left(
    \left|
      \widehat R_S(h_j)-R(h_j)
    \right|
    \geq \epsilon
  \right)
  \leq 2e^{-2\epsilon^2 N}.
\end{align}\]</span></p>
<p>Hence, we may conclude <span class="math display">\[\begin{align}
  &amp;P_{S\sim P^N}\left(
    \exists h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \geq
    \epsilon
  \right)
  \leq 2 |\mathcal H| e^{-2\epsilon^2 N}.
\end{align}\]</span></p>
<p>Setting the right-hand side to equal <span class="math inline">\(\delta\)</span> gives <span class="math display">\[\begin{align}
  2|\mathcal H|e^{-2\epsilon^2N} &amp;= \delta \\
  \log2|\mathcal H|-2\epsilon^2N &amp;= \log\delta,
\end{align}\]</span> from which we read off that <span class="math display">\[\begin{align}
  \epsilon = \sqrt{
    \frac{\log\frac{2|\mathcal H|}{\delta}}
    {2N}
  }.
\end{align}\]</span> This allows to conclude <span class="math display">\[\begin{align}
  &amp;P_{S\sim P^N}\left(
    \exists h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    \geq
    \sqrt{
      \frac{\log\frac{2|\mathcal H|}{\delta}}
      {2N}
    }
  \right)
  \leq \delta
\end{align}\]</span> or equivalently <span class="math display">\[\begin{align}
  &amp;P_{S\sim P^N}\left(
    \forall h\in\mathcal H:
    \left|
      \widehat R_S(h)-R(h)
    \right|
    &lt;
    \sqrt{
      \frac{\log\frac{2|\mathcal H|}{\delta}}
      {2N}
    }
  \right)
  \geq 1- \delta,
\end{align}\]</span> and in other words, <span class="math display">\[\begin{align}
  \forall h\in\mathcal H:
  \left|\widehat R_S(h)-R(h)\right|
  &lt; 
  \sqrt{
    \frac{
      \log\frac{2|\mathcal H|}{\delta}}
    {2N}
  }
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\delta\)</span>.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>Recall that, in principle, we are still in the deterministic case discussed in module <a href="SS21-MsAML__04-S1__PAC_learning_framework.html">PAC learning framework</a> so that <span class="math inline">\(P\)</span> is actually the product of a distribution <span class="math inline">\(D\)</span> on <span class="math inline">\(\mathcal X\)</span> and the one corresponding to a Dirac-delta measure <span class="math inline">\(\delta_{y=c(x)}\)</span>. This fact was not used in the above proof and the result will also hold in the more general case of <span class="math inline">\(P\)</span> having a distribution on <span class="math inline">\(\mathcal X\times\mathcal Y\)</span>, the latter of which are useful to model stochastic relations between the feature vectors and labels.</p>
<p>Compare this bound to the generalization bound we found for the consistent case. Here, as in the consistent case, we still observe the relevant ratio <span class="math display">\[\begin{align}
  \frac{\log\frac{|\mathcal H|}{\delta}}{N}.
\end{align}\]</span> However, this time under a square root. But this is not such a big price to pay, as for fixed <span class="math inline">\(\mathcal H\)</span> and <span class="math inline">\(\delta\)</span>, it means we need an only quadratically larger training data set to achieve the same performance.</p>
<p>Observe further that there is again a trade-off involved in reducing the risk:</p>
<ul>
<li>A richer (and then potentially larger) hypothesis set may help to reduce the empirical risk as the algorithm can better approximate the concept underlying the learning task.</li>
<li>But, for a similar empirical error, one should try to keep <span class="math inline">\(\mathcal H\)</span> as simple (and this means small) as possible.</li>
</ul>
<p>In order to focus on the performance of the algorithm in perspective of its capabilities it makes sense to slightly adapt our PAC learning definition. Instead of comparing to zero risk, let us compare the risk to the best in class risk considering only hypotheses in <span class="math inline">\(\mathcal F\)</span>, e.g., in the range of the algorithm <span class="math inline">\(\mathcal A\)</span>.</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="pac-learnable-in-the-inconsistent-case">(PAC-learnable in the inconsistent case)</h5>
Let <span class="math inline">\(\mathcal F\)</span> be a relevant set of hypotheses and <span class="math display">\[\begin{align}
R_{\mathcal F} := \inf_{h\in\mathcal F}R(h).
  \end{align}\]</span> A concept class <span class="math inline">\(\mathcal C\)</span> is said to be <em><span class="math inline">\(\mathcal  F\)</span>-agnostic PAC-learnable</em>, if there exists an algorithm <span class="math inline">\(\mathcal A\)</span> and a polynomial <span class="math inline">\(p\)</span> such that <span class="math display">\[\begin{align}
\forall \quad &amp;\epsilon, \delta\in (0,1]\\
\forall \quad &amp;\text{distributions $D$ on $\mathcal X$}\\
\forall \quad &amp;\text{concepts $c$ in $\mathcal C$}\\
\forall \quad &amp;N\geq p\left(\frac1\epsilon,\frac1\delta\right):\\
&amp;\\
&amp; P_{S\sim P^N}
\left(
  R(h_S) - R_{\mathcal F}\leq \epsilon
\right) \geq 1-\delta.
  \end{align}\]</span> Furthermore, if <span class="math inline">\(\mathcal A\)</span> runs in <span class="math inline">\(p(\frac1\epsilon,\frac1\delta)\)</span> polynomial time, then <span class="math inline">\(\mathcal C\)</span> is called <em>efficiently</em> <span class="math inline">\(\mathcal  F\)</span>-agnostic PAC-learnable.
</dd>
</dl>
<p>Note that, the larger <span class="math inline">\(R_{\mathcal F}\)</span>, e.g., due to a too small or ill-conditioned choice of <span class="math inline">\(\mathcal F\)</span>, the weaker is the notion of <span class="math inline">\(\mathcal F\)</span>-agnostic PAC learnability and the less useful it is. We will briefly look at two extreme examples in the next module. The learnability assertion is not absolute but relative to the best in class performance <span class="math inline">\(R_{\mathcal F}\)</span> that may be achieved by the algorithm under consideration.</p>
<dl>
<dt>👀</dt>
<dd>Exploit the bounds of our <a href="SS21-MsAML__02-S1__Error_decomposition.html">error decomposition</a> discussion together with the above theorem to formulate and show a first result for this notion of <span class="math inline">\(\mathcal F\)</span>-agnostic PAC learnability: As long as <span class="math inline">\(\mathcal F\)</span> is finite and our algorithm is always successful in carrying out the empirical risk minimization on <span class="math inline">\(\mathcal F\)</span>, then any concept class <span class="math inline">\(\mathcal C\subseteq  \mathcal F\)</span> is efficiently <span class="math inline">\(\mathcal F\)</span>-agnostic PAC learnable.
</dd>
</dl>
<p>We have already mentioned that the theorem above does not depend on the assumed property that, in the deterministic case, the distribution of <span class="math inline">\(P\)</span> was given by a product of a distribution <span class="math inline">\(D\)</span> on <span class="math inline">\(\mathcal X\)</span> and the one corresponding to a Dirac-delta measure <span class="math inline">\(\delta_{y=c(x)}\)</span>. Hence, it is not be much trouble to generalize PAC-learnability and state a similar first result for general distributions of <span class="math inline">\(P\)</span>, including also a stochastic dependence between the feature vectors and labels. This we shall do next.</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
