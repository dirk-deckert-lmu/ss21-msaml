# MsAML-13-S1 [[PDF](SS21-MsAML__13-1__Optimal_policies.pdf)]

1. [Optimal strategies](#optimal-strategies)

Back to [index](index.md).


## Optimal strategies

In our last module we understood how to construct the value functions given the
transition probabilities $\mathcal P=(p(s'|s,a))_{s,s'\in S,a\in\mathcal A}$,
the policy $\pi$, and the reward function $R$. Next we want to turn to the
question of optimality of a policy. 

Relying on the meaningfulness of the reward function, would consider a policy
$\pi$ a good one should it maximize the policy value $V_\pi(s)$ preferably for
many $s\in\mathcal S$. This gives rise to the following definition:

Definition
: ##### (Optimal value functions)
  We define the optimal policy value and state-action value functions
  \begin{align}
    V^*:\mathcal S\to\mathbb R, 
    s \mapsto V^*(s)
    &:=\max_{\pi\in{\mathcal A}^{\mathcal S}} V_\pi(s),
    \\
    Q^*:\mathcal S\times\mathcal A\to\mathbb R, 
    (s,a)\mapsto Q^*(s,a)
    &:=\max_{\pi\in{\mathcal A}^{\mathcal S}} Q_\pi(s,a),
  \end{align}
  and furthermore, we call a policy $\pi^*:\mathcal S\to\mathcal A$ fulfilling
  \begin{align}
    \forall s\in\mathcal S: V_{\pi^*}(s)=V^*(s)
  \end{align}
  optimal.

Note that, owing to the finiteness of $\mathcal S$ and $\mathcal A$, for
every $s\in\mathcal S$ and pair $s\in\mathcal S,a\in\mathcal A$ it is
always possible to find a maximizer of $V$ and $Q$, respectively, implying that 
$V^*$ and $Q^*$ are well-defined.

The definition of $\pi^*$ however demands that this maximum is achieved by the
same policy $\pi^*$. The question remains whether such $\pi^*$ actually
exist.

In order to answer this question, we will again make use of the
$\gamma$-contraction properties of the previously defined transport operators
\begin{align}
  (T_\pi^{\mathcal V}V)(s)
  &= 
  \sum_{s'\in\mathcal S}
  p(s'|s,\pi(s))
  \left[
    R(s,\pi(s),s')+\gamma V(s')
  \right],
  \\
  (T_\pi^{\mathcal Q}Q)(s,a)
  &=
  \sum_{s'\in\mathcal S}
  p(s'|s,a)
  \left[
    R(s,a,s')+\gamma Q(s',\pi(s'))
  \right],
\end{align}
and additionally introduce two new one as references
\begin{align}
  (T^{\mathcal V}_*V)(s)
  :=
  \max_{a\in\mathcal A}\sum_{s'\in\mathcal S}
  p(s'|s,\pi(s))
  \left[
    R(s,a,s')+\gamma V(s')
  \right],\\
  (T^{\mathcal Q}_*Q)(s,a)
  :=
  \sum_{s'\in\mathcal S}
  p(s'|s,a)
  \left[
    R(s,a,s')+\gamma \max_{a'\in\mathcal A} Q(s',a')
  \right],\\
\end{align}
which include the maximization over all actions $a\in\mathcal A$.

Our first step towards the guarantee of existence of optimal policies is to
characterize it in terms of the existence of fixed-points of the map
\begin{align}
  T^{\mathcal V}_* V = V
  \qquad
  :\Leftrightarrow
  \qquad
  \forall s\in\mathcal S:(T^{\mathcal V}_*V)(s)=V(s).
\end{align}
In a second step, we will study the existence of such fixed-points $V$, and
employing similar methods as in our last module, we will conclude that there
actually is a unique one.

Theorem
: ##### (Criterion for optimality) 
  Let $\gamma\in[0,1)$ and $V:\mathcal S\to\mathcal A$ a policy value function. Then:
  \begin{align}
    T^{\mathcal V}_*V=V
    &\qquad
    \Leftrightarrow
    \qquad
    V=V^*
    \;
    \wedge
    \;
    \exists \text{ optimal } \pi^*\in{\mathcal A}^{\mathcal S}.
  \end{align}
  Furthermore, given such a solution $V$, every
  \begin{align}
    \tilde\pi(s) 
    :\in \underset{a\in\mathcal A}{\operatorname{argmax}}
    \sum_{s'\in\mathcal S} p(s'|s,a)\left[R(s,a,s')+\gamma V(s')\right]
    \label{eq_1}
    \tag{E1}
  \end{align}
  is optimal, where the $:\in$ symbol reminds us the that the right-hand side
  is a set that may contain more than one element, in which case a choice has
  to be made to define a $\tilde \pi(s)$.

**Proof:** The central ingredient in this proof is a control of the difference
in transporting different initial policy value functions by $T^{\mathcal
V}_\pi$ for a given policy $\pi$. For this purpose, let us regard two policy
value functions $V,W$ and define the difference
\begin{align}
  R^n_\pi(V,W)
  :=
  (T^{\mathcal V}_\pi)^n V -
  (T^{\mathcal V}_\pi)^n W,
\end{align}
where the superscript $n$ again denotes the $n$-times application of the map.

As we have observed in the last module, due to the there proven
$\gamma$-contraction property of $T^{\mathcal V}_\pi$ for $\gamma$ in the
allowed range $[0,1)$, the limit of $(T^{\mathcal V}_\pi)^n V$ forgets its 
initial value $V$. Hence, for the difference of two such limits we find
\begin{align}
  \forall s\in\mathcal S: R^n_\pi(V,W)(s)\underset{n\to\infty}{\to}0,
  \label{eq_R}
  \tag{R}
\end{align}
which will turn out to be the central ingredient of the proof of the
equivalence.

"$\Leftarrow$": Assume there is a policy value function $V$ such that
\begin{align}
  \forall s\in\mathcal S: V(s)=V^*(s):=\max_{\pi\in{\mathcal A}^{\mathcal S}}
  V_\pi(s)
\end{align}
and that there is a policy $\pi^*$ that is optimal, i.e., $V^*=V_{\pi^*}$.

Recalling the definition of $\pi^*$, $V^*$, and $T^{\mathcal V}_*$, this implies
\begin{align}
  V^*(s)
  &=
  V_{\pi^*}(s)
  =
  \sum_{s'\in\mathcal S} p(s'|s,\pi^*(s))
  \left[
    R(s,\pi^*(s),s')+\gamma V_{\pi^*}(s')
  \right]
  \\
  &\leq
  \max_{a\in\mathcal A}
  \sum_{s'\in\mathcal S} p(s'|s,a)
  \left[
    R(s,\pi^*(s),s')+\gamma V^*(s')
  \right]
  \\
  &\qquad=
  (T^{\mathcal V}_* V^*)(s).
\end{align}

Say $\tilde \pi$ is of the form of $\eqref{eq_1}$, then we actually have
\begin{align}
  T^{\mathcal V}_*(V^*) = T^{\mathcal V}_{\tilde \pi}(V^*),
  \label{eq_2}
  \tag{E2}
\end{align}
and hence, for all $s\in\mathcal S$
\begin{align}
  V^*(s) \leq (T^{\mathcal V}_{\tilde \pi}V^*)(s).
  \label{eq_3}
  \tag{E3}
\end{align}

By definition of $T^{\mathcal V}_{\tilde\pi}$, relation $\eqref{eq_3}$, allows to compute
for all $s\in\mathcal S$
\begin{align}
  V_{\tilde\pi}(s)
  &=
  (T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi})(s)
  \\
  &=
  (T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi})^2(s)
  \\
  &\vdots
  \\
  &=
  (T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi})^n(s)
  \\
  &=
  ((T^{\mathcal V}_{\tilde\pi})^n V^*)(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s)
  \\
  &\geq
  ((T^{\mathcal V}_{\tilde\pi})^{n-1} V^*)(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s)
  \\
  &\vdots
  \\
  &\geq
  (T^{\mathcal V}_{\tilde\pi} V^*)(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s)
  \\
  &\geq
  V^*(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s),
\end{align}
where the vertical dots $\vdots$ denote respective inductions for any given 
$n\in\mathbb N$.

Taking the limit $n\to\infty$ with the help of $\eqref{eq_R}$, for all
$s\in\mathcal S$, we find
\begin{align}
  V^*(s) \leq V_{\tilde\pi}(s).
\end{align}

Suppose further that there is a state $\tilde s\in\mathcal S$ such that
\begin{align}
  V(\tilde s) < (T^{\mathcal V}_{\tilde\pi}V)(\tilde s),
\end{align}
i.e., for which the inequality in $\eqref{eq_2}$ holds strictly. Then we could
carry out the above argument analogously but, for this particular $\tilde
s\in\mathcal S$, yield the strict inequality
\begin{align}
  V^*(\tilde s) < V_{\tilde \pi}(\tilde s)=(T^{\mathcal V}_{\tilde\pi}V^*)(\tilde s).
\end{align}
However, due to $V^*=V_{\pi^*}$, this would imply that $\tilde \pi$ is a better
policy than $\pi^*$, and hence, contradicts the optimality of policy $\pi^*$.
Therefore, we must have 
\begin{align}
  V^*=V_{\tilde \pi}
\end{align}
and, using $\eqref{eq_2}$, we can conclude that
\begin{align}
  V^*=V_{\tilde \pi}=(T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi}) = (T^{\mathcal V}_{\tilde\pi} V^*) = T^{\mathcal V}_* V^*.
\end{align}
Moreover, any such $\tilde\pi$ fulfilling $\eqref{eq_1}$ is by definition optimal.

"$\Rightarrow$": Assume a $V:\mathcal S\to\mathbb R$ solves the equation
\begin{align}
  T^{\mathcal V}_* V = V.
\end{align}

Given some policy $\pi:\mathcal S\to\mathcal A$ and any $s\in\mathcal S$ we observe
\begin{align}
  V_\pi(s) 
  &=(T^{\mathcal V}_\pi V_\pi)(s)
  \\
  &\vdots
  \\
  &=((T^{\mathcal V}_\pi)^n V_\pi)(s)
  \\
  &=
  ((T^{\mathcal V}_\pi)^n V)(s)
  +
  R^n_\pi(V, V_\pi)(s)
  \\
  &\vdots
  \\
  &=V(s)
  +
  R^n_\pi(V, V_\pi)(s)
\end{align}
by induction for any given $n\in\mathbb N$. Again, with the help of
$\eqref{eq_R}$, for all $s\in\mathcal S$, the limit $n\to\infty$ results into
\begin{align}
  V_\pi(s) \leq V(s).
\end{align}
As this estimate is independent of the choice of policy $\pi$, we can take the
maximum over $\pi$ to find, for all $s\in\mathcal S$,
\begin{align}
  V^*:=\max_{\pi\in{\mathcal S}^{\mathcal A}} V_\pi(s) \leq V(s).
  \label{eq_4}
  \tag{E4}
\end{align}

Furthermore, for $\tilde \pi$ of the special form $\eqref{eq_1}$, however, we
find for all $s\in\mathcal S$
\begin{align}
  V_{\tilde\pi}(s) = ((T^{\mathcal V}_{\tilde\pi})^n V_{\tilde\pi})(s)\geq V(s) + R^n_{\tilde \pi}(V_{\tilde\pi},V)(s)
\end{align}
using an analogous argument as above. Again, taking the limit $n\to\infty$ with
the help of $\eqref{eq_R}$, for $s\in\mathcal S$, we get
\begin{align}
  V^*(s):=\max_{\pi\in{\mathcal A}^{\mathcal S}}V_\pi(s)\geq V_{\tilde\pi}\geq V(s).
  \label{eq_5}
  \tag{E5}
\end{align}

In conclusion, for all $s\in\mathcal S$, $\eqref{eq_4}$ and $\eqref{eq_5}$
allow to conclude
\begin{align}
  V^*(s) \leq V(s) \leq V_{\tilde\pi}(s) \leq  V^*(s).
\end{align}
Hence, $V=V^*$ solves equation $T^{\mathcal V}_* V=V$ and $\tilde \pi$ of form
$\eqref{eq_1}$ is by definition an optimal policy.

$\square$

With this criterion for optimality at hand the second step is to ensure the
existence of the fixed-point of $T^{\mathcal V}_* V=V$.

Theorem
: ##### (Existence of optimal policies)
  Given $\gamma\in[0,1)$, the following statements hold true:

    1. The optimal policy value function $V^*$ fufills
       \begin{align}
         T^{\mathcal V}_*V^*=V^*
       \end{align}
       and every policy of the form, for $s\in\mathcal S$,
       \begin{align}
         \tilde\pi(s) :\in \underset{a\in\mathcal A}{\operatorname{argmax}}
         \sum_{s'\in\mathcal S} p(s'|s,a)\left[R(s,a,s')+\gamma V^*(s')\right]
         \label{eq_6}
         \tag{E6}
       \end{align}
       is optimal.
    2. The optimal state-action value function $Q^*$ fulfills
       \begin{align}
          T^{\mathcal Q}_* Q^*=Q^* 
       \end{align}
       and every policy of the form, for $s\in\mathcal S$,
       \begin{align}
         \tilde\pi(s) :\in \underset{a\in\mathcal A}{\operatorname{argmax}}
         Q^*(s,a)
       \end{align}
       is optimal.

**Proof:** The estimates we have established in the last module allow to show
that, for $\gamma\in[0,1)$, also $T^{\mathcal V}_*$ is a $\gamma$-contraction.
By Banach's fixed-point arguments we are therefore guaranteed a unique solution
$V$ of $T^{\mathcal V}_* V=V$. Applying our previous theorem, the existence of
such a $V$ is equivalent to both $V=V^*$ and the optimally of any $\tilde \pi$
fulfilling $\eqref{eq_6}$. This proves the statement 1.

Regarding statement 2, it suffices to observe that, for $s\in\mathcal
S,a\in\mathcal A$,
\begin{align}
  Q^*(s,a)&=Q_{\tilde\pi}(s,a)
  \\
  &=
  \sum_{s'\in\mathcal S} p(s'|s,a)\left[R(s,a,s')+\gamma V_{\tilde\pi}(s')\right]
  \\
  &=
  \sum_{s'\in\mathcal S} p(s'|s,a)\left[R(s,a,s')+\gamma V^*(s')\right],
\end{align}
where we used $V_{\tilde\pi}=V^*=\max_{a\in\mathcal A}Q^*(\cdot,a)$.

$\square$

Let us summarize. So far we have constructed the value functions
and defined $V^*$ and $Q^*$. Therefore the $\operatorname{argmax}$ in the last
theorem is guaranteed to return a non-empty set, which ensures that there is at
least one optimal policy $\pi^*$.  Obviously, the next step is to provide an
algorithm for the agent to find a good approximation of $\pi^*$ during the
learning process.

Our last theorem provides an ansatz for this purpose through statement 2,
i.e., by maximization of $Q^*(s,a)$ with respect to actions $a\in\mathcal A$.
However, to do this, we either need to know $Q^*$ or at least the transition
probabilities $\mathcal P=(p(s'|s,a))_{s,s'\in\mathcal S,a\in\mathcal A}$ in
order to compute $Q^*$.

Both quantities, however, are inaccessible to the agent, and hence, we need an
algorithm to also approximate them. There are two routes:

* Approximation of $\mathcal P$, the so-called "model-based" approach, which can 
  be difficult without a good a priori knowledge of both the environment and the
  agent due to the $|\mathcal S\times\mathcal S\times \mathcal A|$ many
  parameters.
* Approximation of $Q^*$, so-called "model-free" approach, which requires some
  understanding of "only" $|\mathcal S\times\mathcal A|$ many parameters. It is
  usually referred to as *Q-learning*. The approximation of $Q^*$ can also be
  attempted by an additional supervised learning algorithm. A remarkable
  example of success for such an approach of reinforcement learning is the
  [2013 experiment by 
  DeepMind](https://deepmind.com/research/publications/playing-atari-deep-reinforcement-learning)
  in which an agent was trained to play Atari games by simply playing them --
  knowing only the control commands and having as input only the pixels on the
  computer screen and the game score.

In our next and last module we will conclude our reinforcement excursion with
an introduction to Q-learning.

➢ Next session!

