# Exercise 07-S1: VC-dimension examples [[PDF](SS21-MsAML__07-XS1__VC_dimension_examples.pdf)]

a) What is the VC-dimension of axis-aligned squares in $\mathbb{R}^2$?

b) What about intersections of two axis-aligned squares?

c) What is the VC-dimension of a union of $k$ intervals on $\mathbb{R}$, i.e. 

$$\mathcal{F} = \{x \to \mathbf{1}_{[a_1,b_1]\cup [a_2,b_2]\cup\dots [a_k,b_k]} \ | \ a_i,b_i\in \mathbb{R}\ \forall i \in 1,\dots k\}?$$

d) What is the VC-dimension of linear classification with zero bias in $\mathbb{R}^2$, i.e.

$$\mathcal{F} = \{x\to \text{sign}(w\cdot x) \ | \ w\in\mathbb{R}^2\}?$$

e) What is the VC-dimension of linear classification with non-zero bias in $\mathbb{R}^2$, i.e.

$$\mathcal{F} = \{x\to \text{sign}(w\cdot x +b) \ | \ w\in\mathbb{R}^2, b\in\mathbb{R}\}?$$