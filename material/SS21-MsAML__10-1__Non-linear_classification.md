# MsAML-10-1 [[PDF](SS21-MsAML__10-1__Non-linear_classification.pdf)]


1. [Non-linear classification](#non-linear-classification)
     * [Non-linear mappings to achieve linear separability of training data](#non-linear-mappings-to-achieve-linear-separability-of-training-data)
   * [Multi-layered Adaline neurons](#multi-layered-adaline-neurons)

Back to [index](index.md).


## Non-linear classification

The implementations we have considered so far, i.e., those for the class of
linear classification models, always assumed linearly separable training data
-- or at least training data close to linear separability since the Adaline
neuron and the soft-margin support vector machines can also handle a small
fraction of violations of linear separability reasonably well. After having
investigated a substantial amount of time in studying this class of models, the
next natural question is how to extend it to also treat the case of training
data that is not linearly separable. In short, we will refer to this type of
classification task as *non-linear classification*.

There are at least two successful routes:

1. Mapping of the input features of the training data to a sufficiently high
   dimensional space in which the training becomes linearly separable, then
   applying one of the models in the linear classification model class;
2. Approximation of non-linear activation outputs by layering two or more
   Adaline neurons on top of each other and placing a non-linear activation
   function in between between them.

In both cases we would like to achieve a certain generality of our models that
does not require too much a priori knowledge about the particular non-linearity
inherent in the corresponding training data, i.e., learning task. 

The first route will lead us to an extension of the support vector machines via
the so-called *kernel trick*, which will require some preparation based on
results from convex optimization. Therefore, we will only explain the basic
idea here and come back to this topic later. For their robustness and ease in
training, these kernel-equipped support vector machines have been a kind of
industry standard for the last decades.

The second route will lead us to the so-called multi-layer Adaline neuron
models that were already mentioned several times in our discussions of the
various activation and loss functions. These models are the building blocks of
most applications in today's field of *deep learning*.

Before going into the details, let us discuss the basic ideas behind these
routes here.


#### Non-linear mappings to achieve linear separability of training data

As a simple example for training data that is not linearly separable and a
map under which the image of the training features becomes linear
separable, let us consider the following setting:

* A family $s=(x^{(i)},y^{(i)})_{1,\ldots,N}$ of training data points in $(\mathcal X,\mathcal Y)$;
* A conveniently chosen map $\Phi:\mathcal X \to \mathcal X'$ from feature
  space $\mathcal X$ to a sufficiently high dimensional space $\mathcal X'$. 

![Mapping of training data to higher dimensional space to ensure linear separability.](10/non-linear_map.png)

Applying the map $\Phi$ to our training data features generates new family
\begin{align}
  s'=\left(\Phi\left(x^{(i)}\right),y^{(i)}\right)_{i=1,\ldots,N}
\end{align}
of training data points, this time in $\mathcal X'\times\mathcal Y$.

In this simple example, the square of the radius already discriminates well the
red and blue clusters of training data points so that a separating hyperplane
can be found.

After application of the conveniently chosen map $\Phi$, we can simply apply one
of our linear classification models, e.g., a regular Adaline neuron with some
activation function $\alpha$ and loss function $L$ such that the empirical loss
is given by
\begin{align}
  \widehat L'(b',w') &= \frac1N\sum_{i=1}^N L\left(y^{(i)},\alpha\left(w'\cdot\Phi(x)+b'\right))\right)
\end{align}
and the resulting classifier by
\begin{align}
  h'(x) &= \operatorname{sign}\left(\alpha\left(w'\cdot\Phi(x)+b'\right)\right).
\end{align}

The empirical loss function is almost the same as before, except for the fact
that we now compute the activation output parametrized by the higher
dimensional $b'$ and $w'$. Note that the model parameters $w'$ have to change
their type accordingly because now the inner product $\cdot$ is the one of
$\mathcal X'$.  The update rules for $w'$ and $b'$ can then again be computed
by gradient descent, as we are already used to. Furthermore, the same method can
be applied to the optimization programs of the support vector machines.

The basic idea being clear, the question remains how general such models can
ever be as the non-linear map $\Phi$ that we simply slipped in in our example
was obviously tailored to our needs -- in other words, already contained the a
priori knowledge of the nature of or learning task.

In our future discussion of the kernel trick we shall see that this ad hoc
introduction of a convenient non-linear map $\Phi$ can be replace by a quite
general choice that yields good generalization results in applications without
much a priori knowledge of the learning task at hand.

👀
: Implement an Adaline model for such an example setting as considered above,
  i.e., that involves training data that is not linearly separable. Choose a
  convenient map $\Phi$ and conduct the training.


### Multi-layered Adaline neurons

By definition, the Adaline neuron learns to divide training data into
half-spaces by adjusting one separating hyperplane, applying the non-linear
activation function $\alpha(z)$, and ultimately the discretization function
$\operatorname{sign(z)}$, at least in case of binary classification, in order to
select a particular label. In view of non-linear classification, it may seem
natural to divide the classification task into several such divisions into
half-spaces.

Recall, for instance, the failure of the linear models to learn the XOR-gate
using one hyperplane. Suppose we were allowed to employ two hyperplanes as
depicted here:

![XOR-gate training data and two dividing hyperplanes.](10/two_hyperplanes.png)

In this case, we would infer two classifiers
\begin{align}
  h_i(x)
  =
  \operatorname{sign}\left(w_i\cdot x+b_i\right)
  \qquad
  \text{for }i=1,2,
\end{align}
which we can employed to build a derived classifier
\begin{align}
  h(x) =
  \begin{cases}
    +1 & \text{for } h_1(x)+h_2(x)\in\{-2,+2\} \\
    -1 & \text{for } h_1(x)+h_2(x)=0
  \end{cases}.
\end{align}

One can readily imagine, how this method could also be applied to approximate
the circular decision boundaries in our first example above, or even much more
complicated non-linear decision boundaries, provided we were allowed to use
sufficiently many such half-space classifiers. Furthermore, there is no reason
to only restrict to half-space classifiers. 

The parametrization of hypotheses space for such more complicated
classification tasks, are given by so-called *neural networks* for which we
have to specify:

* Number of layers $D\in\mathbb N$, each layer $i=1,\ldots,D$ having

  * Input feature dimensions $n_{i-1}\in\mathbb N$;
  * activation output dimension $n_{i}\in\mathbb N$;

* Activation functions $\alpha_i:\mathbb R^{n_i}\to\mathbb R^{n_{i}}$ for
  $i=1,\ldots D$.

The layers for $i=0$ and $i=D$ are called *input* and *output layers*,
respectively, and the layers in between are sometime referred to as *hidden*.
Let us collect theses parameters with the tuple notation 
\begin{align}
  n=(n_i)_{i=0,\ldots N}, 
  \qquad \alpha=(\alpha_i)_{i=1,\ldots N},
\end{align}
and define the set of so-called *second-generation artificial neural networks* by
\begin{align}
  \mathcal N^{D}_{n,\alpha}
  :=
  \Big\{
    x_{n_D}:\mathbb R^{n_0}\to\mathbb R^{n_D}
    \,\Big|\,
    & \forall x\in\mathbb R^{n_0}: x_0 = x,
    \\
    & z_i = W_i\cdot x_{i-1}+b_i
    \\
    & x_i = \alpha_i(z_i),
    \\
    & i=1,\ldots, D,
    \\
    & \text{for any } W_i\in\mathbb R^{n_i\times n_{i-1}}, b_i\in\mathbb R^{n_i}
  \Big\}.
\end{align}
The number $D$ is called the *depth* of the network while the numbers
in the tuple $n$ are referred to as layer widths.  Any function $f\in\mathcal
N^D_{n,m,\alpha}$ is therefore recursively defined from input layer $i=0$ to
output layer $i=D$ and is a function $f:\mathbb R^{n_0}\to\mathbb R^{n_D}$
that models the activation output of $D$ connected multi-class Adaline
neurons. 

Typically, one uses the same activation function in each component of
a layer so that the following slight abuse of notation comes in handy: For
exampe, for a given sigmoid activation $\sigma:\mathbb R\to\mathbb R$ we will
employ the same symbol $\sigma$ to also denote the map
\begin{align}
  \mathbb R^{n_i}\to\mathbb R^{n_i},
  \qquad
  \sigma(z_i,\ldots,z_{n_i}):=
  (\sigma(z_i),\ldots,\sigma(z_{n_i})),
\end{align}
which in other words, is the component-wise application of $\sigma$. Unless
otherwise noted, we will use this shorthand notation.

**Example:** The possible activation output functions of the already discussed
single-layer multi-class Adaline neuron with sigmoid activation $\sigma$ for
$n$-dimensional input and $c$ classes are in the set $\mathcal N^1_{(n,c),
(\sigma)}$.

Hypotheses spaces for classification problems with $\mathcal X=\mathbb R^{n_0}$
and $\mathcal Y=\{1,\ldots, m_D\}$ could then be derived from $\mathcal
N^D_{n,\alpha}$ by, e.g., applying an $\operatorname{argmax}$ to the output function:
\begin{align}
  \mathcal H^D_{n,m,\alpha}
  =
  \left\{ 
    h:\mathcal X\to\mathcal Y
    \,\big|\,
    f\in\mathcal N^D_{n,m,\alpha}:
    \forall x\in\mathbb R^{n_0}:
    h(x)=\underset{k=1,\ldots, n_D}{\operatorname{argmax}} \left(f(x)\right)_k
  \right\} 
\end{align}

The set $\mathcal N$ can be visualized as follows:

![Neural network representation of $\mathcal N^D_{n,\alpha}$ and $\mathcal H^D_{n,\alpha}$.](10/neural_network.png)

Each node $x_{i,j}$ in this graph, i.e, the $j$-th component of the vector
$x_i\in\mathbb R^{n_i}$, represents the activation output 
\begin{align}
  x_{i,j} = \alpha_{i,j}(z_i)
\end{align}
of the so-called *neuron* $j$ of layer $i$.  Here, $\alpha_{i,j}(z_i)$ denotes
the $j-th$ component of the vector $\alpha_i(z_i)$.  Moreover, the edges denote
the *activation flow*
\begin{align}
  z_i = W_i\cdot x_{i-1}+b_i
\end{align}
from the previous layer $(i-1)$ to target layer $i$ given the weights
$W_i\in\mathbb R^{n_i\times n_{i-1}}$ and the bias vector $b_i\in\mathbb
R^{n_i}$. If, for every layer, all neuron of the previous layer are connected
to neurons on the next layer with potentially non-zero weights, one calls the network
*densely connected*.

In principle, each activation function $\alpha_i$ could be chosen individually.
Here, it is only important to observe that they need to be non-linear in order
to result in a nonlinear function $f\in\mathcal N^D_{n,\alpha}$. As long as the
activations are sigmoidal, the resulting networks can usually be shown to
approximate any target hypotheses provided $D=2$ and the width of this layer
$i=1$ is sufficiently large. In practice, however, the architectural choice of
the network can have a great impact on the training performance. In particular,
architectures which are deeper but not too wide tend to be easier to train that
shallow and brought ones. Deeper neural networks can also be structured into
chucks of layers, each of which is supposed to conduct a certain partial
learning task.  For example, in modern object recognition architectures, the
first chucks of layers are typically *convolution layers*, which are sparsely
connected layers with batches of neurons that even shared the same weights.
Those layers are well adapted to recognize relief features, e.g., by learning
of discrete derivatives. Their outputs are then collected and fed into a
densely connected network that is supposed to learn the logical implications of
the detected relief features.

In order to come back to our XOR-gate example, we will close our discussion by
showing that a sufficiently brought neural network with $D=2$ layers is
expressive enough to represent any boolean function.

Theorem
: ##### (Representation of boolean functions by neural networks)
  Let $n_0\in\mathbb N$ suppose we are given a concept
  $c:\{0,1\}^{n_0}\to\{0,1\}$ to learn. Then, there is a $h\in\mathcal
  N^D_{n,\alpha}$ for $D=2$, $n=(n_0,n_1,1)$ with $n_1\in\mathbb N$, and
  $\alpha=(\theta,\theta)$, given
  \begin{align}
    \theta(z):=\begin{cases}
      1 & \text{for } z\geq 0\\
      0 & \text{for } z < 0
    \end{cases}
    \,,
  \end{align}
  such that for all $x\in\{0,1\}^{n_0}$ we have $h(x)=c(x)$. Furthermore, the
  bound $n_1\leq 2^{n_0}$ holds true.

**Proof:** Let us denote the image of $\{1\}$ under $c$ as
\begin{align}
  c^{-1}(\{1\})=\{a_1,a_2,\ldots, a_k\}.
\end{align}
Hence, for all $x\in\{0,1\}^{n_0}$:
\begin{align}
  c(x) = \theta\left(
    -1 + \sum_{j=1}^k 1_{x=a_j}
  \right),
  \tag{E1}
  \label{eq_1}
\end{align}
and it suffices to show that we can find a representation of
\begin{align}
  1_{x=x_j}
  =
  \begin{cases}
    1 & \text{for } x=a_j\\
    0 & \text{for }  x\neq a_j
  \end{cases}
\end{align}
so that the right-hand side of $\eqref{eq_1}$ can be recast into the form of an
element in $\mathcal N^D_{n,\alpha}$.

For this purpose, let us consider the value table

| $a$ | $b$ | $(2ab-a-b)$ |
|:---:|:---:|:-----------:|
| 0   | 0   | 0           |
| 0   | 1   | -1          |
| 1   | 0   | -1          |
| 1   | 1   | 0           |

which implies
\begin{align}
  1_{a=b} = \theta(2ab-a-b).
\end{align}

Hence, for $x,a_j\in\{0,1\}^{n_0}$ we have
\begin{align}
  1_{x=a_j} = \theta\left(
    \sum_{i=1}^{n_0}\left(
      2x_i a_{j,i}-x_i-a_{j,i}
    \right)
  \right),
  \tag{E2}
  \label{eq_2}
\end{align}
denoting the $i$-th component of the vectors $x$ and $a_j$ by $x_i$ and
$a_{j,i}$, respectively.

From $\eqref{eq_2}$ we may now read off the required layer activations in a neural network.
First, we observe
\begin{align}
  z_{1,j}
  =\left(W_1\cdot x+b_i\right)_j 
  &\overset{!}{=}
  \sum_{i=1}^{n_0}\left(2x_ia_{j,i}-x_i-a_{j,i}\right),
\end{align}
which implies
\begin{align}
  \begin{split}
  W_{1,ji} &= 2a_{j,i}-1,\\
  b_{1,j} &= -\sum_{i=1}^{n_0} a_{j,k}
  \end{split}
  \qquad
  \text{for} \quad i=1,\ldots,n_0,
  \quad
  j=1,\ldots, k.
\end{align}
Second, we would define
\begin{align}
  x_{i,j} = \theta(z_{1,j}).
\end{align}
so that from $\eqref{eq_2}$ we can read off
\begin{align}
  z_2 = W_2\cdot x_1+b_2
  &
  \overset{!}{=}
  -1+\sum_{j=1}^k x_{1,j},
\end{align}
which implies
\begin{align}
  W_{2,i} &= 1 
  \qquad
  \text{for}
  \quad
  i=1,\ldots, k,\\
  b_2 &= -1.
\end{align}
And third, we would define
\begin{align}
  x_2 = \theta(z_2).
\end{align}

In summary, and setting the number of neurons in the middle layer to $n_1=k$,
we have shown that there are
\begin{align}
  W_1\in\mathbb R^{n_1\times n_0}, b_1\in\mathbb R^{n_1}\\
  W_2\in\mathbb R^{1\times n_1}, b_2\in\mathbb R^1
\end{align}
such that the map
\begin{align}
  h(x) 
  & = \theta\left(z_2\right)\\
  & = \theta\left(W_2\cdot x_1+b_2\right)\\
  & = \theta\left(W_2\cdot \left(\theta\left(z_1\right)\right)+b_2\right)\\
  & = \theta\left(W_2\cdot \left(\theta\left(W_1 \cdot x + b_1\right)\right)+b_2\right)
\end{align}
fulfills
\begin{align}
  h\in\mathcal N^2_{(n_0,n_1,1),(\theta,\theta)}
\end{align}
and for all $x\in\{0,1\}^{n_0}$
\begin{align}
  h(x)=c(x).
\end{align}

Finally, the image of $\{1\}$ under $c$ is a subset of $\{0,1\}^{n_0}$, and
hence, $k=n_1\leq 2^{n_0}$.

$\square$

Remark
: This construction requires an exponential increase of neurons in the 
  hidden layer, which renders the result academic. In practice, it is often
  easier to increase the depth of the network opposed to its width while still
  keeping the training efforts reasonably low. Historically, this was very
  likely one of the reasons for the name "deep learning". Nowadays, however, it
  is used differently and one refers to deep learning in the sense that
  learning is done on the basis of very primitive features fed to the input
  layer of neurons, such as pixels of an image, instead of manually engineered
  features such as the above mentioned non-linear map $\Phi$.  Furthermore, the
  bound $n_1\leq 2^{n_0}$ can, of course, be improved to
  \begin{align}
    k\leq \min\left\{\left|c^{-1}(\{0\}\right|, \left|c^{-1}(\{1\}\right|\right\},
  \end{align}
  and by the same method one can show that also networks with layers $D\geq 2$
  are able to represent boolean functions. 

In the next module we will look a bit deeper into the representation and
approximation capabilities of neural networks while for next week's exercises
we will take our time to finally start an implementation of a deeper network.

➢ Next session!

👀 
: For a two-layer Adaline network of your choice, compute the update rule, and
  implement a training for a boolean-type learning task of your choice.

