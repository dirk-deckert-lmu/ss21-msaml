# Exercise 01-X1: Perceptron convergence [[PDF](SS21-MsAML__03-X1__Perceptron_convergence.pdf)]

Assume we have are given any affine-linearly separable and finite training data $(x^{(i)}, y^{(i)})_{i \in \{1,\dots, N\}}$ , i.e. 

$$\exists \ w^{*}, b^{*}: \forall i \in \{1,\dots, N\},\ y_i (w^{*}\cdot x^{(i)}+ b^{*})>0$$.

(a) Prove that for any learning rate $\eta>0$, the Perceptron succeeds to separate the training data after only finitely many steps. (This is the generalization of the statement proven in the lecture notes for normalized data and $\eta=1/2$.)

(b) Set the parameters at initialization to zero: $w^{(0)}=0$. What is the bound on the maximum number of steps until the Perceptron converges in this case? How does the convergence depend on the learning rate $\eta$?