% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-12-1-pdf}{%
\section{\texorpdfstring{MsAML-12-1
{[}\href{SS21-MsAML__12-1__Short_review_optimization.pdf}{PDF}{]}}{MsAML-12-1 {[}PDF{]}}}\label{msaml-12-1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{a-short-review-of-optimization}{A short review of
  optimization}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{a-short-review-of-optimization}{%
\subsection{A short review of
optimization}\label{a-short-review-of-optimization}}

In the MAML course we have mainly discussed the implementation of
algorithms, their potential convergence, and the richness in
representation and approximation capabilities of the models. In the
MSAML course we focused on the statistical aspect and made a short
excursion into control of complexity by means of model selection. There
is at least one topic that, so far, we have only treated as a
prerequisite and left aside, namely optimization. Given optimization
programs the like of which we have seen so far, the leading questions
are:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  do optimal solutions exist?
\item
  And what are their properties?
\end{enumerate}

This topic can be introduced nicely with the rather clear but still
non-trivial example of the support vector machine in mind. The following
results of the next module will point towards a slight adaption of the
model by means of the kernel trick that allows to also treat non-linear
learning tasks very robustly.

Let us recall the SVM minimization program for the hard margin case:
\begin{align}
  \min_{\substack{w\in\mathbb R^d\\b\in\mathbb R}} \frac12|w|^2
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    y^{(i)}(w\cdot x^{(i)}+b)\geq 1\\
    \text{for } i=1,\ldots, N
  \end{cases}
  \,.
\end{align}

This program is an example of what is called a \emph{constrainted
optimization program}. In general, these optimization programs are
expressed as \begin{align}
  \min_{x\in D} f(x)
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    g_i(x)\leq 0\\
    i=1,\ldots, N
  \end{cases}
  \,,
  \label{eq_P}
  \tag{P}
\end{align} where for \(k\in\mathbb N\), the domain of interest is given
by a \(D\subseteq \mathbb R^k\), the objective function
\(f:\mathbb R^k\to\mathbb R\), and the constraints by \(g_i(x)\leq 0\)
for \(g:\mathbb R^k\to\mathbb R\) or \(i=1,\ldots, N\). We will use the
short-hand notation \begin{align}
  g(x)\leq 0
  \qquad
  :\Leftrightarrow
  \qquad
  \forall \, i=1,\ldots,N: g_i(x)\leq 0.
\end{align}

To any constraint optimization program, one can associate a so-called
\emph{Lagrangian}, which will turn out as a convenient tool for our
analysis.

\begin{description}
\item[Definition] ~ 
\hypertarget{lagrangian}{%
\subparagraph{(Lagrangian)}\label{lagrangian}}

The Lagrangian associated to constraint program \(\eqref{eq_P}\) is
given as follows: \begin{align}
\mathcal L: D\times (\mathbb R^+_0)^N &\to \mathbb R
\\
(x,\alpha) &\mapsto \mathcal L(x,\alpha):=f(x)+\sum_{i=1}^N \alpha_i g_i(x).
  \end{align} The variables \(\alpha_i\) are usually referred to as
Lagrange or dual variables.
\end{description}

This way also equality constraints can be specified by requiring both
constraints \(g_i(x)\leq 0\) and \(-g_i(x)\leq 0\) to hold
simultaneously. Let \(\alpha_i^+,\alpha_i^-\in\mathbb R^+_0\) be the
corresponding dual variables of these two constraints, respectively,
then the corresponding term in the Lagrangian takes the form
\((\alpha_i^+-\alpha^-)g_i(x)\) or equivalently one may introduce an
\(\alpha\in\mathbb R\) to replace \((\alpha^+-\alpha_-)\) instead.

In order to develop an intuition for the Lagrangian, let us consider the
simple program \begin{align}
  \min_{x\in D} f(x)
  \qquad
  \text{subject to}
  \qquad 
  g(x)\leq 0
\end{align} for some \(D\subseteq \mathbb R\) and
\(f,g:\mathbb R\to\mathbb R\). An optimal solution of the program
\(x^*\in D\), should it exist, therefore has the value \begin{align}
  f(x^*)=\min\{f(x)\,|\,x\in D, g(x)\leq 0\}.
\end{align} In order to study this program, we may look at the family of
auxiliary programs to minimize the following function for given
\(\alpha\in\mathbb R^+_0\): \begin{align}
  f(x)+\alpha g(x).
\end{align} The auxiliary variable \(\alpha\) introduces a ``weighting''
of the constraint \(g(x)\leq 0\). Let \(x^*(\alpha)\in D\) denote the
optimal solution for the unconstrained auxiliary program with given
\(\alpha\). Should these optima exists, we may expect the following
behavior:

\begin{itemize}
\tightlist
\item
  The function attains in \(x^*(0)\) its smallest value. If also
  \(g(x^*(0))\) holds, fine. However, since \(\alpha=0\), the program is
  unaware of the constraint and it may well happen that \(g(x^*(0))>0\).
\item
  In turn, for large \(\alpha\), one may have found \(x^*(\alpha)\) such
  that \(g(x^*(\alpha))\leq 0\) but the corresponding value
  \(f(x^*(\alpha))\) may tends to be be large and there might be points
  in \(D\) with smaller values.
\item
  One may then hope to find an intermediate value \(\bar\alpha\), e.g.,
  at the boundaries \(g(x^*(\bar\alpha))=0\).
\end{itemize}

The Lagrangian \(\mathcal L\) allows to simultaneously study all of
those \(\alpha\)-parametrized scenarios by means of saddle points:

\begin{description}
\item[Definition] ~ 
\hypertarget{saddle-point}{%
\subparagraph{(Saddle point)}\label{saddle-point}}

Given program \(\eqref{eq_P}\), we call
\((x^*,\alpha^*)\in \mathbb R^k\times(\mathbb  R^+_0)^N\) a saddle point
of the Lagrangian \(\mathcal L\), provided: \begin{align}
\forall x\in\mathbb R^k, \alpha \in (\mathbb R^+_0)^N:
\qquad
\mathcal L(x^*,\alpha)\leq \mathcal L(x^*,\alpha^*)\leq\mathcal L(x,\alpha^*).
  \end{align}
\end{description}

This definition reflects the intuition we have introduced above and we
readily get our first result in terms of the fact that saddle points
indicate solutions of the optimization program \(\eqref{eq_P}\):

\begin{description}
\item[Theorem] ~ 
\hypertarget{solutions-to-eqrefeq_p}{%
\subparagraph{\texorpdfstring{(Solutions to
\(\eqref{eq_P}\))}{(Solutions to \textbackslash eqref\{eq\_P\})}}\label{solutions-to-eqrefeq_p}}

Given program \(\eqref{eq_P}\) and \((x^*,\alpha^*\)) let
\((x^*,\alpha^*)\in\mathbb  R^k\times(\mathbb R^+_0)^N\) be a saddle
point of Lagrangian \(\mathcal L\). Then, \(x^*\) is a solution of the
program \(\eqref{eq_P}\).
\end{description}

\textbf{Proof:} By definition, we have \begin{align}
  \forall x\in\mathbb R^k,\alpha\in(\mathbb R^+_0)^N:
  \qquad
    \mathcal L(x^*,\alpha)\overset{\fbox{1}}{\leq} \mathcal
    L(x^*,\alpha^*)\overset{\fbox{2}}{\leq}\mathcal L(x,\alpha^*).
\end{align}

Based on relation \(\fbox{1}\), we find \begin{align}
  \sum_{i=1}^N \alpha_i g_i(x^*)
  \leq
  \sum_{i=1}^N \alpha_i^* g_i(x^*).
\end{align}

Now, \(\alpha_i\to+\infty\) implies \(g_i(x^*)\leq 0\),
\(i=1,\ldots, N\), or in other words, \(x^*\) satisfies the constraints.

Furthermore, \(\alpha_i\to 0\) implies \begin{align}
  0\leq \sum_{i=1}^N \alpha_i^* g_i(x^*).
\end{align} and since we already confirmed that \(g_i(x^*)\leq 0\) we
get \begin{align}
 \forall \, i=1,\ldots, N: \alpha_i^* g_i(x^*) = 0.
\end{align}

Based on this and relation \(\fbox{2}\), we find \begin{align}
  \forall x\in\mathbb R^k:
  \qquad
  f(x^*)+\underbrace{\sum_{i=1}^N \alpha^*_i g_i(x^*)}_{=0}
  \leq 
  f(x)+\sum_{i=1}^N \alpha^*_i g_i(x).
\end{align}

In other words, for all \(x\in\mathbb R^k\) satisfying the constraints
\(g_i(x)\leq 0\), \(i=1,\ldots, N\), the relation \begin{align}
  f(x^*) \leq f(x)
\end{align} holds true.

In conclusion, \(\fbox{1}\) and \(\fbox{2}\) imply that \(x^*\) is a
solution of \(\eqref{eq_P}\).

\(\square\).

In convex optimization, there is a well-developed theory to guarantee
the existence of saddle points and infer their properties. Let us recall
some results that we shall need later. Observing our time constraints,
and since it is usually the content of an optimization course, we will
omit the proofs. If you are new to the topic, have a look into the book
\emph{Convex optimization} by Jarre and Stoer.

As the name suggests, the main ingredient is convexity:

\begin{description}
\item[Definition] ~ 
\hypertarget{convexity}{%
\subparagraph{(Convexity)}\label{convexity}}

For \(D\subseteq \mathbb R^k\), \(f:\mathbb R^k\to \mathbb R\) is called
convex if \(D\) is convex and for all \(\lambda\in[0,1], x,y\in D\) the
relation \begin{align}   
f(\lambda x+(1\lambda)y) \leq \alpha f(x) + (1-\lambda) f(y)
  \end{align} holds true. The function \(f\) is called strictly convex,
if the relation holds even with a strict inequality.
\end{description}

Furthermore, from Analysis II we may remember:

\begin{description}
\item[Theorem] ~ 
\hypertarget{convexity-criterion}{%
\subparagraph{(Convexity criterion)}\label{convexity-criterion}}

Let \(D\subseteq \mathbb R^k\) convex and \(f:\mathbb R^k\to\mathbb R\).
Then:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  If \(f\) is differentiable on \(D\): \begin{align}
    \forall x,y\in D:
    f(y)-f(x)
    \geq
    \nabla f(x)\cdot(x-y)
    \qquad
    \Leftrightarrow
    \qquad
    f \text{ is convex}.
  \end{align}
\item
  If \(f\) is twice differentiable on \(D\): \begin{align}
    \forall x\in D:
    H_f(x)
    \geq 0
    \qquad
    \Leftrightarrow
    \qquad
    f \text{ is convex},
  \end{align} where \(H_f(x)\) is the Hessian matrix \begin{align}
    \left(H_f(x)\right)_{ij} 
    := 
    \frac{\partial^2}{\partial_{x_i}\partial_{x_j}}f(x)
    \qquad
    \text{for } i,j = 1,\ldots,k,
  \end{align} and the relation denotes that it must be positive
  semi-definite.
\end{enumerate}
\end{description}

An intuitive illustration for a convex function that is differentiable
is given in in the following:

\begin{figure}
\centering
\includegraphics{12/intuition_convex_diff.png}
\caption{Illustration of a convex and differentiable function and its
linear approximation.}
\end{figure}

Recall that a matrix \(M\in\mathbb R^{k\times k}\) is called positive
semi-definite if and only if \begin{align}
  \forall x\in\mathbb R^k:
  \langle x, Mx\rangle\geq 0,
\end{align} where \(\langle\cdot,\cdot,\rangle\) denotes the inner
product in \(\mathbb R^k\), or equivalently, if all of its eigenvalues
are non-negative.

Given convexity, one can prove the following theorem base on the
so-called hyperplane separation theorems.

\begin{description}
\item[Theorem] ~ 
\hypertarget{saddle-point-criterion}{%
\subparagraph{(Saddle point criterion)}\label{saddle-point-criterion}}

Let \(f,g_1,\ldots,g_n\) on \(D\) be convex, and \(x^*\in D\) a solution
of the now convex constraint program \(\eqref{eq_P}\). Under the
following conditions, there is an \(\alpha^*\geq 0\) such that
\((x^*,\alpha^*)\) is a saddle point of the corresponding Lagrangian
\(\mathcal L\):

\begin{itemize}
\item
  If the so-called \emph{strong Slater condition} holds:

  \begin{itemize}
  \tightlist
  \item
    The interior of \(D\), i.e., \(D^0\), is not empty.
  \item
    And there is a \(\bar x\in D^0\) such that \begin{align}
      \forall i=1,\ldots, N:
      \quad
      g_i(\bar x)< 0
    \end{align}
  \end{itemize}
\item
  Or if \(f\) is differentiable and the so-called \emph{weak Slater
  condition} holds

  \begin{itemize}
  \tightlist
  \item
    The interior of \(D\), i.e., \(D^0\), is not empty.
  \item
    And there is a \(\bar x\in D^0\) such that \begin{align}
      \forall i=1,\ldots, N:
      \quad
      g_i(\bar x)< 0
      \quad
      \vee
      \quad
      \left(
      g_i(\bar x)= 0
      \wedge
      \text{$g_i$ affine}
      \right).
    \end{align}
  \end{itemize}
\end{itemize}
\end{description}

This theorem lies at the heart of convex optimization, and it is a pity
that time does not permit going into the proof here. The heuristics
behind the, at first sight maybe cryptic, Slater condition is to find at
least one point in the interior of \(D\) which fulfills the constraints,
from which the search of an optimal solution can be conducted -- the
interested reader is again referred to the book \emph{Convex
optimization} by Jarre and Stoer.

In case of sufficient regularity, a necessary and sufficient criterion
for the solutions can be given by the famous Karush-Kuhn-Tucker theory:

\begin{description}
\item[Theorem] ~ 
\hypertarget{kkt-criterion}{%
\subparagraph{(KKT criterion)}\label{kkt-criterion}}

Let \(f,g_1,\ldots, g_N\) on \(D\) be convex and differentiable and let
the weak Slater condition be satisfied. Then, \(x^*\in D\) is a solution
to \(\eqref{eq_P}\) if and only if: \begin{align}
\exists \alpha^* \geq 0: \quad
&\nabla_x\mathcal L(x^*,\alpha^*)=\nabla_x f(x^*)+\sum_{i=1}^N\alpha_i^* \nabla_x g_i(x^*)=0
&
\fbox{KKT1}
\\
&\wedge \quad \nabla_\alpha\mathcal L(x^*,\alpha^*)=g(x)\leq 0
&
\fbox{KKT2}
\\
&\wedge \quad \sum_{i=1}^N \alpha_i^* g_i(x^*)=0,
& \fbox{KKT3}
  \end{align} where \(\nabla_x\) and \(\nabla_\alpha\) denote the
gradients with respect to first and second vector-valued argument,
respectively.
\end{description}

Note that \(\fbox{KKT3}\) is equivalent to \begin{align}
    g(x^*)\leq 0 \wedge \left(\forall\,i=1,\ldots,N: \alpha_i^* g_i(x^*)=0\right),
\end{align}

\textbf{Proof:} ``\(\Rightarrow\)'': The conditions were formulated so
that the necessary condition for
\protect\hyperlink{saddle-point-criterion}{Theorem (Saddle point
criterion)} is already fulfilled. Hence, there exists a
\(\alpha^*\geq 0\) such that \((x^*,\alpha^*)\) is a saddle point of the
Lagrangian. Let us define the function \begin{align}
  \phi(x) := f(x)-f(x^*)+\sum_{i=1}^N \alpha_i^* g_i(x),
  \label{eq_1}
  \tag{E1}
\end{align} which, by definition, fulfills \(\phi(x)\geq 0\).

Furthermore, we have \begin{align}
  \phi(x^*)=\sum_{i=1}^N \alpha^*_i g_i(x) \leq 0
  \label{eq_2}
  \tag{E2}
\end{align} because \(\alpha^*_i\geq 0\) and \(g_i(x^*)\leq 0\).

Hence, \(x^*\) needs to be a global minimum so that, thank to
differentiability and by Fermat's theorem, \begin{align}
  \nabla_x \phi(x^*)=0
\end{align} holds true, which is equivalent to \(\fbox{KKT1}\).

The condition \(\fbox{KKT2}\) and \(\fbox{KKT3}\) follow from a similar
argument as in \protect\hyperlink{solutions-to-eqrefeq_p}{Theorem
(Solutions to \(\eqref{eq_P}\))}.

``\(\Leftarrow\)'': For \(x\in D\) such that \(g(x)\leq 0\), and
exploiting the convexity of \(f\) to apply
\protect\hyperlink{convexity-criterion}{Theorem (Convexity criterion)},
we find \begin{align}
  f(x)-f(x^*)
  \geq \nabla_x f(x^*)\cdot (x-x^*).
  \tag{E3}
  \label{eq_3}
\end{align} Applying \(\fbox{KKT1}\) we get for the right-hand side
\begin{align}
  \eqref{eq_3} = -\sum_{i=1}^N\alpha^*_i \nabla_x g_i(x^*)\cdot (x-x^*).
  \tag{E4}
  \label{eq_4}
\end{align} Next, we also exploit the convexity of the \(g_i\)'s, which
results in the following recast of the right-hand side \begin{align}
  \eqref{eq_4} \geq -\sum_{i=1}^N\alpha^*_i \cdot (g_i(x)-g_i(x^*)).
  \tag{E5}
  \label{eq_5}
\end{align} Finally, when supplying \(\fbox{KKT3}\) and afterwards
\(\fbox{KKT2}\), we conclude \begin{align}
  \eqref{eq_5}
  =
  -\sum_{i=1}^N \alpha_i^* g(x) \geq 0.
\end{align}

Hence, \(f(x^*)\leq f(x)\) for all \(x\in D\) that satisfy the
constraints \(g(x)\leq 0\). In other words, we have shown that \(x^*\)
solves the program \(\eqref{eq_P}\).

\(\square\)

\begin{description}
\tightlist
\item[Remark]
Note that this theorem is a generalization of the theorems of Fermat (no
constraints) and Lagrange (equality constraints), which are contained as
special cases. All these theorems reduce the number of candidate points
in \(D\) that have to be checked for potential minima by means of extra
relations that such minima would have to fulfill.
\end{description}

Let us apply this theorem to our SVM optimization program:

👀: Show that the hard-margin SVM optimization program has a unique
solution. Set up the corresponding Lagrangian, check if the KKT Theorem
can be applied and infer the properties \(\fbox{KKT1-3}\). In
particular, show that the optimal \(w^*\) can be given as linear
combination of the support vectors, i.e., of those training data
features \(x^{(i)}\) that fulfill \(y^{(i)}(w^*\cdot x^{(i)}+b)=1\).

The properties inferred from \(\fbox{KKT1-3}\) will help us to formulate
a so-called dual optimization problem for the SVM, which will prove
particular convenient to generalize for non-linear classification tasks.

➢ Next session!

\end{document}
