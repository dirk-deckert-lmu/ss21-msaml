# Exercise 10-1: Non-linear classification [[PDF](SS21-MsAML__10-X1__Non-linear_classification.pdf)]

a) We saw in the previous exercises that the Iris dataset is not linearly separable if we consider all the three classes. Choose a convenient non-linear map $\Phi: \mathcal{X}\to \mathcal{X}^{'}$ for this dataset and train one of our Adaline models using the mapped data to find a classifier that performs well on the three-class problem.

b) Consider a two-layer neural network with trainable parameters $(W^1,b^1)$ and $(W^2,b^2)$ in the input and the output layers respectively. Assume we train the network with a given loss function $L$ and activation function $\alpha$ in both layers. Write the gradient descent update rule of the network's parameters.