# Exercise 08-1: Standardization and initial weights [[PDF](SS21-MsAML__08-X1__Standardization_and_initial_weights.pdf)]

a) Suppose we are given i.i.d random variables $X_i,\ i =1,\dots ,N$. Define 

$$\mu = E X_1, \qquad \sigma^2 = E (X_1-\mu)^2, $$
$$\hat{\mu} = \frac{1}{N}\sum_{i=1}^N X_i, \qquad \hat{\sigma}^2=\frac{1}{N-1}\sum_{i=1}^N (X_i - \hat{\mu})^2$$

and show 

$$E(X_1-\hat{\mu})=0,$$
$$E(\hat{\mu} - \mu)^2 = \frac{\sigma^2}{N},$$
$$E\hat{\sigma}^2 = \sigma^2.$$

b) Suppose $w=(w_1,\dots,w_d), x=(x_1,\dots,x_d)$ and $b$ are initialized randomly as follows:

$$w_i \sim \mathcal{N}(0,\frac{1}{d+1}),$$
$$x_i \sim \mathcal{N}(0,1),$$
$$b \sim \mathcal{N}(0,\frac{1}{d+1}),$$

where all the variables are pairwise independent. Show that 

$$E(w\cdot x+b) = 0,$$
$$E(w\cdot x+b)^2 = 1.$$