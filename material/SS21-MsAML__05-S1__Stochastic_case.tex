% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-05-s2-pdf}{%
\section{\texorpdfstring{MsAML-05-S2
{[}\href{SS21-MsAML__05-S1__Stochastic_case.pdf}{PDF}{]}}{MsAML-05-S2 {[}PDF{]}}}\label{msaml-05-s2-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{stochastic-case}{Stochastic case}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{stochastic-case}{%
\subsection{Stochastic case}\label{stochastic-case}}

In the last two modules we have been focussing on the deterministic
case, in which the distribution of \(P\) was given by the product of a
distribution in \(D\) and the one of the Dirac-delta measure
\(\delta_{y=c(x)}\). This implied that our realizations of training data
were of the form \begin{align}
  s=(x^{(i)},y{(i)})_{i=1,\ldots, N}
  =(x^{(i)},c(x{(i)}))_{i=1,\ldots, N}.
\end{align}

There are however many scenarios in which the feature vectors
\(x\in\mathcal X\) may be assigned more than one label
\(y\in\mathcal Y\):

\begin{itemize}
\tightlist
\item
  For instance, this allows us to anticipate noise in the training data.
\item
  But such scenarios may also naturally arise due to the type of
  questioned asked. Considering the example of a learning task to
  predict the gender by means of the height of the human body, clearly,
  in a sufficiently large training data set, there will features that
  will assign different labels to the same feature.
\end{itemize}

This implies that the best performance we may expect from a certain
algorithm \(\mathcal A\) is not only restricted by its capabilities,
e.g., its range \(\mathcal F\), but also by means of the Bayes risk.

Gladly, as mention already in the discussion of the inconsistent case,
the tools employed in the inconsistent case do not depend on special
form of the distribution of \(P\). In the corresponding definition we
can simple drop the reference to the concept class and the corresponding
special distribution and simple address general distributions \(P\) on
\(\mathcal X\times\mathcal Y\):

\begin{description}
\item[Definition] ~ 
\hypertarget{pac-learnable-in-the-general-case}{%
\subparagraph{(PAC-learnable in the general
case)}\label{pac-learnable-in-the-general-case}}

Let \(\mathcal F\) be a relevant set of hypotheses, e.g., the range of
\(\mathcal A\) and \begin{align}
R_{\mathcal F} := \inf_{h\in\mathcal F}R(h).
  \end{align} We call \(\mathcal A\) an \emph{\(\mathcal F\)-agnostic
PAC-learning} algorithm, if there exists a polynomial \(p\) such that
\begin{align}
\forall \quad &\epsilon, \delta\in (0,1]\\
\forall \quad &\text{distributions $P$ on $\mathcal X\times \mathcal Y$}\\
\forall \quad &N\geq p\left(\frac1\epsilon,\frac1\delta\right):\\
&\\
& P_{S\sim P^N}
\left(
  R(h_S) - R_{\mathcal F}\leq \epsilon
\right) \geq 1-\delta.
  \end{align} Furthermore, if \(\mathcal A\) runs in
\(p(\frac1\epsilon,\frac1\delta)\) polynomial time, we call
\(\mathcal A\) an \emph{efficient} \(\mathcal F\)-agnositic PAC-learner.
\end{description}

As mentioned earlier, the \(\mathcal F\)-agnostic PAC learnability is as
strong as the rich the set hypotheses \(\mathcal F\) is as we only
compare against the best in class risk \(R_{\mathcal F}\). For example,
taking

\begin{itemize}
\tightlist
\item
  some \(h^*\in{\mathcal Y}^{\mathcal X}\),
\item
  \(\mathcal F=\{h^*\}\),
\item
  \(\text{range}\mathcal A=\mathcal F\)
\end{itemize}

implies \(R(h_s)-R_{\mathcal F} = 0\) and has little meaning. The other
extreme, arranging \(\mathcal F={\mathcal Y}^{\mathcal X}\), would
compare against the Bayes risk; recall our discussion about the
\href{SS21-MsAML__01-S1__Statistical_framework.pdf}{statistical
framework}.

\begin{description}
\tightlist
\item[👀]
Check how your result concerning the inconsistent case can directly be
generalized to this stochastic setting, and show
\(\mathcal  F\)-agnostic learnability under the assumption that the
algorithm always succeeds in empirical risk minimization and
\(\mathcal F\) is finite.
\end{description}

Passing through the way points of realizablility/consistency,
inconsistency, as well as the deterministic and stochastic setting, we
have now made precise a quite general notion of learnability through
supervised learners. Furthermore, relying on the finiteness of
\(\mathcal F\), your learnability result shows that the corresponding
learning tasks are efficiently learnable provided there is an algorithm
that always succeeds in the optimization tasks of empirical risk
minimization. The latter is always guaranteed by a brute force search in
the finite space of hypotheses \(\mathcal F\), although this can be
awfully slow depending on the size of \(\mathcal F\) since the latter is
typically exorbitantly large in relation any imaginable computation
power. More precise estimations on the complexity of implementations of
learning algorithms are then an entire topic of its own, however, given
the typical exorbitantly large sizes of \(\mathcal F\), the given bounds
are rather academic -- they show the expected behavior of decrease of
risk for increase in \(N\) but are of no use to practical applications
as the required training data sets are simply too large. Therefore, in
the next modules, we want to focus on the statistical aspect of the
learning task, rather than its complexity, and see how we can relax the
finiteness assumption in our generalization bounds.

➢ Next session!

\end{document}
