% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-14-s1-pdf}{%
\section{\texorpdfstring{MsAML-14-S1
{[}\href{SS21-MsAML__14-S1__Q-value_iteration.pdf}{PDF}{]}}{MsAML-14-S1 {[}PDF{]}}}\label{msaml-14-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{q-value-iteration}{Q-value iteration}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{q-value-iteration}{%
\subsection{Q-value iteration}\label{q-value-iteration}}

After our discussion about the reinforcement learning frame work, the
construction of the value functions, and the existence of optimal
strategies, the question remains how to find approximations to optimal
policies by means of a learning algorithm.

The key ingredient in our discussion was the transport operator, e.g.,
for the state-action value function: \begin{align}
  T^{\mathcal Q}_*:\mathcal Q &\to \mathcal Q  
  \\
  (T^{\mathcal Q}_*Q)(s,a)&:=\sum_{s'\mathcal S} p(s'|s,a)\left[
    R(s,a,s')+\gamma\max_{a\in\mathcal A} Q(s',a')
  \right].
\end{align}

With the transition probabilities at hand, one can prove convergence:

\begin{description}
\item[Theorem] ~ 
\hypertarget{convergence-to-an-optimal-policy}{%
\subparagraph{(Convergence to an optimal
policy)}\label{convergence-to-an-optimal-policy}}

Let \(\gamma\in[0,1)\), \(Q^{(0)}\in\mathcal Q\), and define
\begin{align}
Q^{(k)}:=T^{\mathcal Q}_* Q^{(k-1)}
  \end{align} recursively for \(k\in\mathbb N\). Furthermore, for each
step \(k\in\mathbb N\), let \(\pi^{(k)}:\mathcal S\to\mathcal A\) be
selected as follows \begin{align}
\pi^{(k)}(s) &:\in \underset{a\in\mathcal A}{\operatorname{argmax}}Q^{(k)}(s,a).
  \end{align} Then, for all \(s\in\mathcal S\), \begin{align}
\lim_{k\to\infty}\pi^{(k)}(s)=\pi^*(s),
  \end{align} i.e., the family of policies
\((\pi^{(k)})_{k\in\mathbb N}\) converges to the optimal policy
\(\pi^*\).
\end{description}

The convergence can again be shown by exploiting the
\(\gamma\)-contraction property of the transport operators, but we will
not go into the proof here. Although, this may live up to our
expectation, the result is rather academic as the transition
probabilities \(\mathcal P\) are not known to the agent. Thus, for
practical purposes we need to setup a different strategy in
approximating \(\pi^*\).

A successful algorithm is the so-called \emph{\(Q\) value iteration} or
\emph{Q-learning} algorithm, a first version of which we will outline
next:

\textbf{Input:} Fix the following input values:

\begin{itemize}
\tightlist
\item
  learning rate \(\alpha\in(0,1)\);
\item
  exploration probability \(\delta\in[0,1]\);
\item
  initial state-action value function \(Q^{(0)}\in\mathcal Q\);
\item
  total number of training epochs \(K\in\mathbb N\).
\end{itemize}

\textbf{Step 1:} Repeat for \(k=1,2,\ldots, K\) rounds:

\textbf{Step 2:} Choose state \(s\in\mathcal S\) with uniform
distribution.

\textbf{Step 3:} With probability \(\delta\) choose an action
\(a\in\mathcal A\), otherwise choose \begin{align}
  a :\in \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(k)}(s,a).
\end{align}

\textbf{Step 4:} Start the actual MDP with initial condition
\begin{align}
  S_0=s, \qquad A_0=a.
\end{align} to retrieve a realization of random variable \(S_1\), i.e.,
the ``response'' of the environment in view of initial state \(s\) and
selected action \(a\in\mathcal A\). In the following, let us denote the
realization of \(S_1\) by \(s'\in\mathcal S\).

\textbf{Step 5:} Update the state-action value function as follows:
\begin{align}
  Q^{(k+1)}(s,a) 
  := 
  (1-\alpha) Q^{(k)}(s,a)
  +
  \alpha \left[ R(s,a,s')+
    \gamma \max_{a\in\mathcal A} Q^{(k)}(s',a')
  \right]
\end{align} and go back to \textbf{Step 1}.

\textbf{Output:} The updated policy after the \(K\) epochs of learning:
\begin{align}
  \pi^{(K)}(s)
  :\in
  \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(K)}(s,a).
\end{align}

Let us discuss some of the steps qualitatively:

As in practice the state space is exorbitantly large, Step 2 is
obviously not the best we can do. Theoretically, however, it is needed
to show that \(\pi^{(K)}\) approximates the optimal \(\pi^*\). For
practical purposes, this has to be adapted, e.g., by introducing a
method that picks a most relevant states that even allow to gain a
reward at all.

Note that Step 3 ensures that we do not always act greedily and choose
the most promising immediate action according to our present knowledge,
that latter of which is encoded in the most recent iteration of
\(Q^{(k)}\). The probability to make a random choice is controlled by
\(\delta\) which allows to fine-tune the trade-off between exploration
of new states and exploitation of knowledge about previously seen ones.
As we have discussed previously, a purely greedy strategy may not lead
to the biggest reward in the long run. Hence, it is import to allow the
agent to explore new state-action combinations. It is usually efficient
to allow for more exploration in the beginning as the initial
\(Q^{(0)}\) may not be trustworthy. This helps the agent to explore its
options and acquire knowledge about the resulting responses of the
environment. At a later stage it may be wise to reduce the probability
for exploration and to rather fine-tune the acquired knowledge by
selecting well-proven state-action pairs. In practice one replaces the
fixed \(\delta\) in the definition of the algorithm above by some null
sequence \(\delta^{(k)}\underset{k\to \infty}{\to}0\).

Furthermore, Step 4 conducts the actual exploration and collects the
feedback from the environment. Given the state-action pair, it answers
the question of which was the resulting state in this ``trial run'' in
order to compute the immediate reward in Step 5. During the repetitions
for a sufficiently large number of total epochs \(K\) the agent will
therefore pick up the statistical frequencies of such combinations.

Observe that the above algorithm attempts to learn an optimal \(Q^*\) in
a ``backwards'' fashion: Say we start with \(Q^{(0)}=0\). Then, in the
iteration over \(k\), the adapted state-action value fulfills
\(Q^{(k+1)}\neq 0\) for a currently chosen state-action pair \((s,a)\)
only if either:

\begin{itemize}
\tightlist
\item
  State \(s\) leads to a state \(s'\) that earns the agent an immediate
  non-zero reward \(R(s,a,s')\).
\item
  or, for the next state \(s'\), we already have a non-zero
  \(Q^{(k)}(s',a')\) for some action \(a'\).
\end{itemize}

Typically, however, immediate rewards \(R(s,a,s')\) tend to appear in
the later rather than the early phases of the learning task, e.g.,
during playing a game. In turn, the first iterations will be spent
searching for such ``late in the game'' state-action combinations that
lead to an immediate reward. Only after finding some of them, it is
possible for the algorithm to adapt the state-action value function
\(Q^{(k)}\) such that these rewards can eventually be reached by
exploitation. In this sense, one could say the agent learns strategies
in a ``backwards'' fashion.

Finally, also note that \(\alpha\) moderates how much we adapt
\(Q^{(k+1)}\) with the newly found knowledge similarly as the learning
rates in our previously discussed supervised learning algorithms. In
order to allow for a convergence towards the final epochs, one usually
introduces a decrease in the learning rate by replacing \(\alpha\) with
a null sequence \(\alpha^{(k)}\underset{k\to\infty}{\to}0\).

Given this setting, it possible to prove:

\begin{description}
\item[Theorem] ~ 
\hypertarget{convergence-of-the-q-learning-algorithm}{%
\subparagraph{(Convergence of the Q-learning
algorithm)}\label{convergence-of-the-q-learning-algorithm}}

Provided all state-action pairs \((s,a)\in\mathcal S\times\mathcal A\)
are visited infinitely often and \begin{align}
\sum_{k\in\mathbb N}\alpha^{(k)}=\infty
\qquad
\wedge
\qquad
\sum_{k\in\mathbb N}\left(\alpha^{(k)}\right)^2<\infty,
  \end{align} the following convergence holds true almost surely
\begin{align}
 \lim_{k\to\infty}Q^{(k)}=Q^*
\qquad
\wedge
\qquad
 \lim_{k\to\infty}\pi^{(k)}=\pi^*.
  \end{align}
\end{description}

In case you are interested in the proof, an nice version of it can be
found
\href{http://users.isr.ist.utl.pt/~mtjspaan/readingGroup/ProofQlearning.pdf}{here}.

In practice, however, the given algorithm will have to be modified. Let
us therefore close our discussion of reinforcement learning with three
effective adaptions that have led to the recent advances:

\emph{Improval of Step 2 and 4:} Instead of choosing the states at
random, a relevant initial state is picked and the process is run for
not only one but for multiple iterations \(T\in\mathbb N\). However, if
we would only apply the Q-learning algorithm to states found along such
so-called ``episodes'', there is a strong correlation between the
corresponding \(S_0,S_1,S_2,\ldots\) and we would leave the setting of
the above theorem. In order to restore some independence, one stores the
last, say, \(M\in\mathbb N\) many, observations for \((s,a,s')\) in a
so-called \emph{replay memory} \(\mathcal M\). Later, when updating to
the next iteration of the state-action value function \(Q^{(k+1)}\), one
picks only batches of \(B\in\mathbb N\) many such observations in
\(\mathcal M\) at random. With these adaptions, the algorithm reads as
follows:

\textbf{Input:} Fix the following input values:

\begin{itemize}
\tightlist
\item
  learning rate sequence \(\alpha^{(k)}\in(0,1)\), \(k\in\mathbb N\);
\item
  exploration probability \(\delta^{(k)}\in[0,1]\), \(k\in\mathbb N\);
\item
  replay memory \(\mathcal M=\emptyset\) and size \(M\in\mathbb N\);
\item
  batch size \(B\in\mathbb N\) and episode length \(T\in\mathbb N\);
\item
  initial state-action value function \(Q^{(0)}\in\mathcal Q\);
\item
  total number of training epochs \(K\in\mathbb K\).
\end{itemize}

\textbf{Step 1:} Repeat for \(k=1,2,\ldots, K\) rounds:

\textbf{Adapted Steps 2-4:} Pick a relevant state \(s\in\mathcal S\) and
start the actual MDP with initial condition \begin{align}
  S_0=s, \qquad A_0=a
\end{align} to run for \(T\) steps. For each step \(t=1,2,\ldots, T\)
perform the substeps:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Set \(s:=S_t\).
\item
  With probability \(\delta^{(k)}\) choose action \(a\in\mathcal A\) at
  random or otherwise choose \begin{align}
    a :\in \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(k)}(s,a).
  \end{align}
\item
  Choose \(A_t=a\) and receive the response of the environment as
  realization \(s'\) of the random variable \(S_t\).
\item
  Add \((s,a,a')\) to the replay memory \(\mathcal M\) and, if
  necessary, delete the oldest entry to ensure \(|\mathcal M|=M\).
\end{enumerate}

\textbf{Step 5:} Repeat \(B\) times:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Pick a tuple \((s,a,s')\in\mathcal M\) at random.
\item
  Conduct the update \begin{align}
    Q^{(k+1)}(s,a) 
    := 
    (1-\alpha) Q^{(k)}(s,a)
    +
    \alpha \left[ R(s,a,s') +
      \gamma \max_{a\in\mathcal A} Q^{(k)}(s',a')
    \right].
  \end{align}
\end{enumerate}

Afterwards go back to \textbf{Step 1}.

\textbf{Output:} The updated policy after the \(K\) epochs of learning:
\begin{align}
  \pi^{(K)}(s)
  :\in
  \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(K)}(s,a).
\end{align}

Observe the similarities to stochastic gradient descent in which we also
used randomly chosen batches to reduce the correlations. The algorithm
can be improved further by storing observations \((s,a,a')\) that
actually lead to non-zero reward with higher frequency.

\emph{Improval of Step 4:} If the interaction with the actual
environment is not feasible, it has to be simulated. For example, in a
game setting, the environment is basically driven by the reactions of
another player obeying the rules of the game. Such an environment can be
simulated by letting the agent play against itself using the same
learning algorithm on both sides of the game. In order to synchronize
the states for both players, we need an additional function that
translates the state of Player 1 into the one of Player 2 and vice
versa. Let this map be denoted by \begin{align}
  \operatorname{inv}:\mathcal S\to\mathcal S.
\end{align}

Giving this map, we can adapt the inner loop in \textbf{Step 2-4} in the
algorithm above as follows:

\begin{itemize}
\tightlist
\item
  Given \(s:=S_t\), Player 1 chooses an action \(a\in\mathcal A\)
  according to substep 2.
\item
  This creates an intermediate state \(s_\text{int}\in\mathcal S\) which
  for Player 2 looks as \(\tilde s=\operatorname{inv}(s_\text{int})\).
\item
  Player 2 then picks an action \(\tilde a\in\mathcal A\) according to
  subsetp 2.
\item
  In turn, this create the final state \(\tilde s'\) of this round,
  which for Player 1 looks as \begin{align}
    s' = \operatorname{inv}^{-1}(\tilde s').
  \end{align}
\item
  In this way, we have generated an observation \((s,a,s')\) that can be
  stored in the replay memory of Player 1, and similarly, we will do for
  Player 2.
\end{itemize}

Of course, there is a multitude of other variations of such algorithms
that simulate the environment. An obvious variant is to give each player
an own \(Q\) an pick the best performing one.

\emph{Supervised approximation of \(Q\)}: A last variant to mention
before closing our discussion which led to recent advances is the
approximation of \(Q\) by means of another supervised learner, e.g., a
neural network. In order to fight the combinatorial complexity of
\(|\mathcal S\times\mathcal A|\) that must be addressed when
approximating \(Q\), we may use a classifier to predict a potentially
good choice of \(Q\) by means of the current state only.

Formally, we would need a hypotheses space \(\mathcal H\) with
hypotheses of the form \begin{align}
  h:\mathcal S &\to {\mathbb R}^{\mathcal A}
  \\
  s &\mapsto h(s)
\end{align} such that the approximated state-action value can be
retrieved from \begin{align}
  Q^{\approx}(s,a) := (h(s))(a),
  \qquad \text{for }s\in\mathcal S,a\in\mathcal S.
\end{align}

For practical purposes we could choose a sufficiently deep neural
network architecture such as the discussed densely connected ones
\(\mathcal H:=\mathcal N^D_{n,\alpha}\) for \(D\) layers, \(n_0\) input
neurons, \(n_D\) output neurons, and parametrize \(\mathcal S\) by
\(\mathbb R^{n_0}\) and \({\mathbb R}^{\mathcal A}\) by
\(\mathbb R^{n_D}\).

Each \(h\in\mathcal H\) would then be parametrized by means of the
corresponding weight matrices and bias vectors
\(p=(W_i,b_i)_{i=1,\ldots,D}\), which we denote as \(h=h_p\) in our
notation.

In order to train this neural network, we need training data. This can
be generated from the recorded observations \((s,a,s')\) as follows:

Each newly observed triple \((s,a,s')\) gives rise to a new feature
vector \begin{align}
  x^{(i)} = s \in \mathbb R^{n_0}.
\end{align} and corresponding label \begin{align}
  y^{(i)} \in {\mathbb R}^{\mathcal A}
\end{align} which is computed for all \(\tilde a\in\mathcal A\) as
follows \begin{align}
  y^{(i)}(\tilde a) 
  := 
  \begin{cases}
    R(s,a,s') + \gamma\max_{a'\in\mathcal A} (h_p(s'))(a) & \text{for } \tilde a=a
    \\
    (h_p(s))(\tilde a) & \text{otherwise}
  \end{cases}
  \,.
\end{align}

Collecting the observations in the replay memory results in the training
data \((x^{(i)},y^{(i)})_{i=1,\ldots,M}\), which, e.g., can be used for
a gradient descent training of the classifier.

Provided with such an additional classifier, we can adapt the above
algorithm as follows:

\begin{itemize}
\tightlist
\item
  Provide an initialization of the classifier by means of an initial
  hyperparameter \(p^{(0)}\).
\item
  In the \(k\)-th step, replace \(Q^{(k)}(s,a)\) with
  \((h_{p^{(k)}}(s))(a)\).
\item
  After acquiring \(M\) observations, one generates the corresponding
  training data as prescribed above and conducts the update rule to
  adjust \(p^{(k)}\) to \(p^{(k+1)}\), e.g., by gradient descent for a
  convenient empirical loss.
\end{itemize}

In this form, the corresponding algorithms is usually referred as
\emph{deep Q-learning}. Most prominently, such a method has been applied
in DeepMind's
\href{https://deepmind.com/research/case-studies/alphago-the-story-so-far}{\emph{AlphaGo}}
and, with the Player 1 \& 2 scenario discussed above, in
\href{https://deepmind.com/blog/article/alphazero-shedding-new-light-grand-games-chess-shogi-and-go}{\emph{AlphaZero}}.

This closes our introduction into reinforcement learning and likewise
the statistical part of our course. As already said in our this week's
\href{SS21-MsAML__14-1__Kernel_SVMs.pdf}{Monday lecture}, the field is
large and we could only cover so much. However, we can probably claim
that we have covered most of the key ideas in our discussions and now,
depending on your interest, you should be well-prepared to continue your
own journey into the field. Enjoy!

\end{document}
