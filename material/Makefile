MDS=$(shell find . -type f -name '*.md')
HTMLS=$(patsubst %.md,%.html, $(MDS))
PDFS=$(patsubst %.md,%.pdf, $(MDS))
TEXS=$(patsubst %.md,%.tex, $(MDS))

.PHONY: all clean pdfclean run serve

all: $(HTMLS) $(PDFS) $(TEXS)

%.html: %.md
	@echo
	@echo "### Converting '$<' to '$@'..."
	pandoc --template ./_template/template.html --css ./_template/styles.css -M author="Dirk - André Deckert" --mathjax --standalone -f markdown -t html -s $< -o $@
	sed -i '' 's/href="\(.*\)\.md"/href="\1\.html"/g' $@
	@echo

%.pdf: %.md
	@echo
	@echo "### Converting '$<' to '$@'..."
	sed 's/(\(.*\)\.md)/(\1\.pdf)/g' $< | pandoc --template _template/template.tex -M author="Dirk - André Deckert" -V papersize=a4 -V geometry:margin=3.5cm -V fontsize=12pt -V colorlinks -V linkcolor=blue -V urlcolor=blue -f markdown -t pdf -s --pdf-engine=lualatex -f markdown -t pdf -s -o $@
	@echo

%.tex: %.md
	@echo
	@echo "### Converting '$<' to '$@'..."
	sed 's/(\(.*\)\.md)/(\1\.pdf)/g' $< | pandoc --template _template/template.tex -M author="Dirk - André Deckert" -V papersize=a4 -V geometry:margin=3.5cm -V fontsize=12pt -V colorlinks -V linkcolor=blue -V urlcolor=blue -f markdown -t pdf -s --pdf-engine=lualatex -f markdown -t latex -s -o $@
	@echo

html: $(HTMLS)
	@echo
	@echo "### Done building HTMLs"
	@echo

pdf: $(PDFS)
	@echo
	@echo "### Done building PDFs"
	@echo

tex: $(TEXS)
	@echo
	@echo "### Done building TEXs"
	@echo

clean:
	@echo
	@echo "### Removing $(HTMLS)..."
	rm $(HTMLS)
	@echo

cleanpdf:
	@echo
	@echo "### Removing $(PDFS)..."
	rm $(PDFS)
	@echo

cleantex:
	@echo
	@echo "### Removing $(TEXS)..."
	rm $(TEXS)
	@echo

cleanall:
	@echo
	@echo "### Removing all $(HTMSL), $(TEXS), $(PDFS)..."
	rm $(HTMLS) $(TEXS) $(PDFS)
	@echo

run:
	@echo
	@echo "### Starting broswer..."
	$(BROWSER) index.html
	@echo

serve:
	@echo
	@echo "### Serving at 8000..."
	python3 -m http.server 8000
