<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__13-1__Optimal_policies</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-13-s1-pdf">MsAML-13-S1 [<a href="SS21-MsAML__13-1__Optimal_policies.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#optimal-strategies">Optimal strategies</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="optimal-strategies">Optimal strategies</h2>
<p>In our last module we understood how to construct the value functions given the transition probabilities <span class="math inline">\(\mathcal P=(p(s&#39;|s,a))_{s,s&#39;\in S,a\in\mathcal A}\)</span>, the policy <span class="math inline">\(\pi\)</span>, and the reward function <span class="math inline">\(R\)</span>. Next we want to turn to the question of optimality of a policy.</p>
<p>Relying on the meaningfulness of the reward function, would consider a policy <span class="math inline">\(\pi\)</span> a good one should it maximize the policy value <span class="math inline">\(V_\pi(s)\)</span> preferably for many <span class="math inline">\(s\in\mathcal S\)</span>. This gives rise to the following definition:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="optimal-value-functions">(Optimal value functions)</h5>
We define the optimal policy value and state-action value functions <span class="math display">\[\begin{align}
V^*:\mathcal S\to\mathbb R, 
s \mapsto V^*(s)
&amp;:=\max_{\pi\in{\mathcal A}^{\mathcal S}} V_\pi(s),
\\
Q^*:\mathcal S\times\mathcal A\to\mathbb R, 
(s,a)\mapsto Q^*(s,a)
&amp;:=\max_{\pi\in{\mathcal A}^{\mathcal S}} Q_\pi(s,a),
  \end{align}\]</span> and furthermore, we call a policy <span class="math inline">\(\pi^*:\mathcal S\to\mathcal A\)</span> fulfilling <span class="math display">\[\begin{align}
\forall s\in\mathcal S: V_{\pi^*}(s)=V^*(s)
  \end{align}\]</span> optimal.
</dd>
</dl>
<p>Note that, owing to the finiteness of <span class="math inline">\(\mathcal S\)</span> and <span class="math inline">\(\mathcal A\)</span>, for every <span class="math inline">\(s\in\mathcal S\)</span> and pair <span class="math inline">\(s\in\mathcal S,a\in\mathcal A\)</span> it is always possible to find a maximizer of <span class="math inline">\(V\)</span> and <span class="math inline">\(Q\)</span>, respectively, implying that <span class="math inline">\(V^*\)</span> and <span class="math inline">\(Q^*\)</span> are well-defined.</p>
<p>The definition of <span class="math inline">\(\pi^*\)</span> however demands that this maximum is achieved by the same policy <span class="math inline">\(\pi^*\)</span>. The question remains whether such <span class="math inline">\(\pi^*\)</span> actually exist.</p>
<p>In order to answer this question, we will again make use of the <span class="math inline">\(\gamma\)</span>-contraction properties of the previously defined transport operators <span class="math display">\[\begin{align}
  (T_\pi^{\mathcal V}V)(s)
  &amp;= 
  \sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,\pi(s))
  \left[
    R(s,\pi(s),s&#39;)+\gamma V(s&#39;)
  \right],
  \\
  (T_\pi^{\mathcal Q}Q)(s,a)
  &amp;=
  \sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,a)
  \left[
    R(s,a,s&#39;)+\gamma Q(s&#39;,\pi(s&#39;))
  \right],
\end{align}\]</span> and additionally introduce two new one as references <span class="math display">\[\begin{align}
  (T^{\mathcal V}_*V)(s)
  :=
  \max_{a\in\mathcal A}\sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,\pi(s))
  \left[
    R(s,a,s&#39;)+\gamma V(s&#39;)
  \right],\\
  (T^{\mathcal Q}_*Q)(s,a)
  :=
  \sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,a)
  \left[
    R(s,a,s&#39;)+\gamma \max_{a&#39;\in\mathcal A} Q(s&#39;,a&#39;)
  \right],\\
\end{align}\]</span> which include the maximization over all actions <span class="math inline">\(a\in\mathcal A\)</span>.</p>
<p>Our first step towards the guarantee of existence of optimal policies is to characterize it in terms of the existence of fixed-points of the map <span class="math display">\[\begin{align}
  T^{\mathcal V}_* V = V
  \qquad
  :\Leftrightarrow
  \qquad
  \forall s\in\mathcal S:(T^{\mathcal V}_*V)(s)=V(s).
\end{align}\]</span> In a second step, we will study the existence of such fixed-points <span class="math inline">\(V\)</span>, and employing similar methods as in our last module, we will conclude that there actually is a unique one.</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="criterion-for-optimality">(Criterion for optimality)</h5>
Let <span class="math inline">\(\gamma\in[0,1)\)</span> and <span class="math inline">\(V:\mathcal S\to\mathcal A\)</span> a policy value function. Then: <span class="math display">\[\begin{align}
T^{\mathcal V}_*V=V
&amp;\qquad
\Leftrightarrow
\qquad
V=V^*
\;
\wedge
\;
\exists \text{ optimal } \pi^*\in{\mathcal A}^{\mathcal S}.
  \end{align}\]</span> Furthermore, given such a solution <span class="math inline">\(V\)</span>, every <span class="math display">\[\begin{align}
\tilde\pi(s) 
:\in \underset{a\in\mathcal A}{\operatorname{argmax}}
\sum_{s&#39;\in\mathcal S} p(s&#39;|s,a)\left[R(s,a,s&#39;)+\gamma V(s&#39;)\right]
\label{eq_1}
\tag{E1}
  \end{align}\]</span> is optimal, where the <span class="math inline">\(:\in\)</span> symbol reminds us the that the right-hand side is a set that may contain more than one element, in which case a choice has to be made to define a <span class="math inline">\(\tilde \pi(s)\)</span>.
</dd>
</dl>
<p><strong>Proof:</strong> The central ingredient in this proof is a control of the difference in transporting different initial policy value functions by <span class="math inline">\(T^{\mathcal V}_\pi\)</span> for a given policy <span class="math inline">\(\pi\)</span>. For this purpose, let us regard two policy value functions <span class="math inline">\(V,W\)</span> and define the difference <span class="math display">\[\begin{align}
  R^n_\pi(V,W)
  :=
  (T^{\mathcal V}_\pi)^n V -
  (T^{\mathcal V}_\pi)^n W,
\end{align}\]</span> where the superscript <span class="math inline">\(n\)</span> again denotes the <span class="math inline">\(n\)</span>-times application of the map.</p>
<p>As we have observed in the last module, due to the there proven <span class="math inline">\(\gamma\)</span>-contraction property of <span class="math inline">\(T^{\mathcal V}_\pi\)</span> for <span class="math inline">\(\gamma\)</span> in the allowed range <span class="math inline">\([0,1)\)</span>, the limit of <span class="math inline">\((T^{\mathcal V}_\pi)^n V\)</span> forgets its initial value <span class="math inline">\(V\)</span>. Hence, for the difference of two such limits we find <span class="math display">\[\begin{align}
  \forall s\in\mathcal S: R^n_\pi(V,W)(s)\underset{n\to\infty}{\to}0,
  \label{eq_R}
  \tag{R}
\end{align}\]</span> which will turn out to be the central ingredient of the proof of the equivalence.</p>
<p>“<span class="math inline">\(\Leftarrow\)</span>”: Assume there is a policy value function <span class="math inline">\(V\)</span> such that <span class="math display">\[\begin{align}
  \forall s\in\mathcal S: V(s)=V^*(s):=\max_{\pi\in{\mathcal A}^{\mathcal S}}
  V_\pi(s)
\end{align}\]</span> and that there is a policy <span class="math inline">\(\pi^*\)</span> that is optimal, i.e., <span class="math inline">\(V^*=V_{\pi^*}\)</span>.</p>
<p>Recalling the definition of <span class="math inline">\(\pi^*\)</span>, <span class="math inline">\(V^*\)</span>, and <span class="math inline">\(T^{\mathcal V}_*\)</span>, this implies <span class="math display">\[\begin{align}
  V^*(s)
  &amp;=
  V_{\pi^*}(s)
  =
  \sum_{s&#39;\in\mathcal S} p(s&#39;|s,\pi^*(s))
  \left[
    R(s,\pi^*(s),s&#39;)+\gamma V_{\pi^*}(s&#39;)
  \right]
  \\
  &amp;\leq
  \max_{a\in\mathcal A}
  \sum_{s&#39;\in\mathcal S} p(s&#39;|s,a)
  \left[
    R(s,\pi^*(s),s&#39;)+\gamma V^*(s&#39;)
  \right]
  \\
  &amp;\qquad=
  (T^{\mathcal V}_* V^*)(s).
\end{align}\]</span></p>
<p>Say <span class="math inline">\(\tilde \pi\)</span> is of the form of <span class="math inline">\(\eqref{eq_1}\)</span>, then we actually have <span class="math display">\[\begin{align}
  T^{\mathcal V}_*(V^*) = T^{\mathcal V}_{\tilde \pi}(V^*),
  \label{eq_2}
  \tag{E2}
\end{align}\]</span> and hence, for all <span class="math inline">\(s\in\mathcal S\)</span> <span class="math display">\[\begin{align}
  V^*(s) \leq (T^{\mathcal V}_{\tilde \pi}V^*)(s).
  \label{eq_3}
  \tag{E3}
\end{align}\]</span></p>
<p>By definition of <span class="math inline">\(T^{\mathcal V}_{\tilde\pi}\)</span>, relation <span class="math inline">\(\eqref{eq_3}\)</span>, allows to compute for all <span class="math inline">\(s\in\mathcal S\)</span> <span class="math display">\[\begin{align}
  V_{\tilde\pi}(s)
  &amp;=
  (T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi})(s)
  \\
  &amp;=
  (T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi})^2(s)
  \\
  &amp;\vdots
  \\
  &amp;=
  (T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi})^n(s)
  \\
  &amp;=
  ((T^{\mathcal V}_{\tilde\pi})^n V^*)(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s)
  \\
  &amp;\geq
  ((T^{\mathcal V}_{\tilde\pi})^{n-1} V^*)(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s)
  \\
  &amp;\vdots
  \\
  &amp;\geq
  (T^{\mathcal V}_{\tilde\pi} V^*)(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s)
  \\
  &amp;\geq
  V^*(s)
  +
  R^n_{\tilde\pi}(V_{\tilde\pi},V^*)(s),
\end{align}\]</span> where the vertical dots <span class="math inline">\(\vdots\)</span> denote respective inductions for any given <span class="math inline">\(n\in\mathbb N\)</span>.</p>
<p>Taking the limit <span class="math inline">\(n\to\infty\)</span> with the help of <span class="math inline">\(\eqref{eq_R}\)</span>, for all <span class="math inline">\(s\in\mathcal S\)</span>, we find <span class="math display">\[\begin{align}
  V^*(s) \leq V_{\tilde\pi}(s).
\end{align}\]</span></p>
<p>Suppose further that there is a state <span class="math inline">\(\tilde s\in\mathcal S\)</span> such that <span class="math display">\[\begin{align}
  V(\tilde s) &lt; (T^{\mathcal V}_{\tilde\pi}V)(\tilde s),
\end{align}\]</span> i.e., for which the inequality in <span class="math inline">\(\eqref{eq_2}\)</span> holds strictly. Then we could carry out the above argument analogously but, for this particular <span class="math inline">\(\tilde s\in\mathcal S\)</span>, yield the strict inequality <span class="math display">\[\begin{align}
  V^*(\tilde s) &lt; V_{\tilde \pi}(\tilde s)=(T^{\mathcal V}_{\tilde\pi}V^*)(\tilde s).
\end{align}\]</span> However, due to <span class="math inline">\(V^*=V_{\pi^*}\)</span>, this would imply that <span class="math inline">\(\tilde \pi\)</span> is a better policy than <span class="math inline">\(\pi^*\)</span>, and hence, contradicts the optimality of policy <span class="math inline">\(\pi^*\)</span>. Therefore, we must have <span class="math display">\[\begin{align}
  V^*=V_{\tilde \pi}
\end{align}\]</span> and, using <span class="math inline">\(\eqref{eq_2}\)</span>, we can conclude that <span class="math display">\[\begin{align}
  V^*=V_{\tilde \pi}=(T^{\mathcal V}_{\tilde\pi} V_{\tilde\pi}) = (T^{\mathcal V}_{\tilde\pi} V^*) = T^{\mathcal V}_* V^*.
\end{align}\]</span> Moreover, any such <span class="math inline">\(\tilde\pi\)</span> fulfilling <span class="math inline">\(\eqref{eq_1}\)</span> is by definition optimal.</p>
<p>“<span class="math inline">\(\Rightarrow\)</span>”: Assume a <span class="math inline">\(V:\mathcal S\to\mathbb R\)</span> solves the equation <span class="math display">\[\begin{align}
  T^{\mathcal V}_* V = V.
\end{align}\]</span></p>
<p>Given some policy <span class="math inline">\(\pi:\mathcal S\to\mathcal A\)</span> and any <span class="math inline">\(s\in\mathcal S\)</span> we observe <span class="math display">\[\begin{align}
  V_\pi(s) 
  &amp;=(T^{\mathcal V}_\pi V_\pi)(s)
  \\
  &amp;\vdots
  \\
  &amp;=((T^{\mathcal V}_\pi)^n V_\pi)(s)
  \\
  &amp;=
  ((T^{\mathcal V}_\pi)^n V)(s)
  +
  R^n_\pi(V, V_\pi)(s)
  \\
  &amp;\vdots
  \\
  &amp;=V(s)
  +
  R^n_\pi(V, V_\pi)(s)
\end{align}\]</span> by induction for any given <span class="math inline">\(n\in\mathbb N\)</span>. Again, with the help of <span class="math inline">\(\eqref{eq_R}\)</span>, for all <span class="math inline">\(s\in\mathcal S\)</span>, the limit <span class="math inline">\(n\to\infty\)</span> results into <span class="math display">\[\begin{align}
  V_\pi(s) \leq V(s).
\end{align}\]</span> As this estimate is independent of the choice of policy <span class="math inline">\(\pi\)</span>, we can take the maximum over <span class="math inline">\(\pi\)</span> to find, for all <span class="math inline">\(s\in\mathcal S\)</span>, <span class="math display">\[\begin{align}
  V^*:=\max_{\pi\in{\mathcal S}^{\mathcal A}} V_\pi(s) \leq V(s).
  \label{eq_4}
  \tag{E4}
\end{align}\]</span></p>
<p>Furthermore, for <span class="math inline">\(\tilde \pi\)</span> of the special form <span class="math inline">\(\eqref{eq_1}\)</span>, however, we find for all <span class="math inline">\(s\in\mathcal S\)</span> <span class="math display">\[\begin{align}
  V_{\tilde\pi}(s) = ((T^{\mathcal V}_{\tilde\pi})^n V_{\tilde\pi})(s)\geq V(s) + R^n_{\tilde \pi}(V_{\tilde\pi},V)(s)
\end{align}\]</span> using an analogous argument as above. Again, taking the limit <span class="math inline">\(n\to\infty\)</span> with the help of <span class="math inline">\(\eqref{eq_R}\)</span>, for <span class="math inline">\(s\in\mathcal S\)</span>, we get <span class="math display">\[\begin{align}
  V^*(s):=\max_{\pi\in{\mathcal A}^{\mathcal S}}V_\pi(s)\geq V_{\tilde\pi}\geq V(s).
  \label{eq_5}
  \tag{E5}
\end{align}\]</span></p>
<p>In conclusion, for all <span class="math inline">\(s\in\mathcal S\)</span>, <span class="math inline">\(\eqref{eq_4}\)</span> and <span class="math inline">\(\eqref{eq_5}\)</span> allow to conclude <span class="math display">\[\begin{align}
  V^*(s) \leq V(s) \leq V_{\tilde\pi}(s) \leq  V^*(s).
\end{align}\]</span> Hence, <span class="math inline">\(V=V^*\)</span> solves equation <span class="math inline">\(T^{\mathcal V}_* V=V\)</span> and <span class="math inline">\(\tilde \pi\)</span> of form <span class="math inline">\(\eqref{eq_1}\)</span> is by definition an optimal policy.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>With this criterion for optimality at hand the second step is to ensure the existence of the fixed-point of <span class="math inline">\(T^{\mathcal V}_* V=V\)</span>.</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="existence-of-optimal-policies">(Existence of optimal policies)</h5>
<p>Given <span class="math inline">\(\gamma\in[0,1)\)</span>, the following statements hold true:</p>
<ol type="1">
<li>The optimal policy value function <span class="math inline">\(V^*\)</span> fufills <span class="math display">\[\begin{align}
  T^{\mathcal V}_*V^*=V^*
\end{align}\]</span> and every policy of the form, for <span class="math inline">\(s\in\mathcal S\)</span>, <span class="math display">\[\begin{align}
  \tilde\pi(s) :\in \underset{a\in\mathcal A}{\operatorname{argmax}}
  \sum_{s&#39;\in\mathcal S} p(s&#39;|s,a)\left[R(s,a,s&#39;)+\gamma V^*(s&#39;)\right]
  \label{eq_6}
  \tag{E6}
\end{align}\]</span> is optimal.</li>
<li>The optimal state-action value function <span class="math inline">\(Q^*\)</span> fulfills <span class="math display">\[\begin{align}
   T^{\mathcal Q}_* Q^*=Q^* 
\end{align}\]</span> and every policy of the form, for <span class="math inline">\(s\in\mathcal S\)</span>, <span class="math display">\[\begin{align}
  \tilde\pi(s) :\in \underset{a\in\mathcal A}{\operatorname{argmax}}
  Q^*(s,a)
\end{align}\]</span> is optimal.</li>
</ol>
</dd>
</dl>
<p><strong>Proof:</strong> The estimates we have established in the last module allow to show that, for <span class="math inline">\(\gamma\in[0,1)\)</span>, also <span class="math inline">\(T^{\mathcal V}_*\)</span> is a <span class="math inline">\(\gamma\)</span>-contraction. By Banach’s fixed-point arguments we are therefore guaranteed a unique solution <span class="math inline">\(V\)</span> of <span class="math inline">\(T^{\mathcal V}_* V=V\)</span>. Applying our previous theorem, the existence of such a <span class="math inline">\(V\)</span> is equivalent to both <span class="math inline">\(V=V^*\)</span> and the optimally of any <span class="math inline">\(\tilde \pi\)</span> fulfilling <span class="math inline">\(\eqref{eq_6}\)</span>. This proves the statement 1.</p>
<p>Regarding statement 2, it suffices to observe that, for <span class="math inline">\(s\in\mathcal S,a\in\mathcal A\)</span>, <span class="math display">\[\begin{align}
  Q^*(s,a)&amp;=Q_{\tilde\pi}(s,a)
  \\
  &amp;=
  \sum_{s&#39;\in\mathcal S} p(s&#39;|s,a)\left[R(s,a,s&#39;)+\gamma V_{\tilde\pi}(s&#39;)\right]
  \\
  &amp;=
  \sum_{s&#39;\in\mathcal S} p(s&#39;|s,a)\left[R(s,a,s&#39;)+\gamma V^*(s&#39;)\right],
\end{align}\]</span> where we used <span class="math inline">\(V_{\tilde\pi}=V^*=\max_{a\in\mathcal A}Q^*(\cdot,a)\)</span>.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>Let us summarize. So far we have constructed the value functions and defined <span class="math inline">\(V^*\)</span> and <span class="math inline">\(Q^*\)</span>. Therefore the <span class="math inline">\(\operatorname{argmax}\)</span> in the last theorem is guaranteed to return a non-empty set, which ensures that there is at least one optimal policy <span class="math inline">\(\pi^*\)</span>. Obviously, the next step is to provide an algorithm for the agent to find a good approximation of <span class="math inline">\(\pi^*\)</span> during the learning process.</p>
<p>Our last theorem provides an ansatz for this purpose through statement 2, i.e., by maximization of <span class="math inline">\(Q^*(s,a)\)</span> with respect to actions <span class="math inline">\(a\in\mathcal A\)</span>. However, to do this, we either need to know <span class="math inline">\(Q^*\)</span> or at least the transition probabilities <span class="math inline">\(\mathcal P=(p(s&#39;|s,a))_{s,s&#39;\in\mathcal S,a\in\mathcal A}\)</span> in order to compute <span class="math inline">\(Q^*\)</span>.</p>
<p>Both quantities, however, are inaccessible to the agent, and hence, we need an algorithm to also approximate them. There are two routes:</p>
<ul>
<li>Approximation of <span class="math inline">\(\mathcal P\)</span>, the so-called “model-based” approach, which can be difficult without a good a priori knowledge of both the environment and the agent due to the <span class="math inline">\(|\mathcal S\times\mathcal S\times \mathcal A|\)</span> many parameters.</li>
<li>Approximation of <span class="math inline">\(Q^*\)</span>, so-called “model-free” approach, which requires some understanding of “only” <span class="math inline">\(|\mathcal S\times\mathcal A|\)</span> many parameters. It is usually referred to as <em>Q-learning</em>. The approximation of <span class="math inline">\(Q^*\)</span> can also be attempted by an additional supervised learning algorithm. A remarkable example of success for such an approach of reinforcement learning is the <a href="https://deepmind.com/research/publications/playing-atari-deep-reinforcement-learning">2013 experiment by DeepMind</a> in which an agent was trained to play Atari games by simply playing them – knowing only the control commands and having as input only the pixels on the computer screen and the game score.</li>
</ul>
<p>In our next and last module we will conclude our reinforcement excursion with an introduction to Q-learning.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
