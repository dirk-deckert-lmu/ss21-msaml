# Exercise 04-XS2: Review of standard concentration inequalities [[PDF](SS21-MsAML__04-XS2__Review_of_standard_concentration_inequalities.pdf)]

As a review of some basic inequalities, let us regard the following cases for a real-valued random variable $X$, a measurable $f:\mathbb{R}\to\mathbb{R}^{+}$, and $\epsilon>0$:

(a) Show 

$$P(f(x)\geq\epsilon) \leq \frac{E f(x)}{\epsilon}$$.

(b) Show Markov's inequality 

$$P(|X|\geq \epsilon) \leq \frac{E|X|}{\epsilon}$$.

(c) Show Chebyshev's inequality 

$$P(|X-EX|\geq\epsilon)\leq \frac{E(X-EX)^2}{\epsilon^2}$$.

(d) Compare the conditions under which you can prove the weak law of large numbers using Chebyshev's or Hoeffding's inequality, i.e. for $X_1,\dots ,X_N$ i.i.d random variables and $\hat{S} := \frac{1}{N}\sum_{i=1}^N X_i$ that 

$$P(|\hat{S}_N-E\hat{S}_n|\geq \epsilon) \to_{N\to\infty} 0$$,

which gives a meaning to the mathematical object of the expectation.

(e) Show that for range $X_i \subseteq [a,b], i=1,\dots,N, \epsilon, \delta \in (0,1]$ we have for  

$$N>\frac{(b-a)^2}{2\epsilon^2}\text{log}\Bigl(\frac 2 \delta\Bigr)$$

$$|\hat{S}_N - E \hat{S_N}|< \epsilon$$

with probability of at least $1-\delta$.



