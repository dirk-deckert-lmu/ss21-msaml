# Exercise 01-X2: Adaline learning rate [[PDF](SS21-MsAML__03-X2__Adaline_learning_rate.pdf)]

Suppose we are given a linearly separable finite dataset $(x^{(i)},y^{(i)})_{i\in\{1,\dots,N\}}$ and run the Adaline algorithm with constant learning rate $\eta>0$  to separate the classes.

Is it possible that the algorithm does not converge? How does the learning rate $\eta$ impact the convergence?