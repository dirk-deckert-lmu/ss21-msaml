% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-07-s1-pdf}{%
\section{\texorpdfstring{MsAML-07-S1
{[}\href{SS21-MsAML__07-S1__Growth_function.pdf}{PDF}{]}}{MsAML-07-S1 {[}PDF{]}}}\label{msaml-07-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{growth-function}{Growth function}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{growth-function}{%
\subsection{Growth function}\label{growth-function}}

We have seen that the Rademacher complexity provides a first means to
better capture the ``richness'' of potentially infinite hypotheses
spaces. However, even already the empirical Rademacher complexity can
turn out to be computationally quite hard to access. Therefore, it makes
sense to look at for more accessible quantities on which it depends. In
order to proceed, we will restrict ourselves to the case of binary
classification only, i.e., \(|\mathcal Y|=2\). There are several strong
techniques to also treat the general multi-class classification or even
regression based on similar ideas but, due to our time constraints,
these go beyond the scope of our course.

Based on the
\href{SS21-MsAML__06-XS1__Rademacher_complexity.pdf}{exercises}, we have
found for the special case of binary classification and loss function
\begin{align}
  L(y,y')=1_{y\neq y'}  
\end{align} that the relation \begin{align}
  \widehat{\mathcal R}_S(\mathcal G_{\mathcal F})
  =
  \frac12 \widehat{\mathcal R}_{\pi_{\mathcal X}S}(\mathcal F)
  \label{eq_1}
  \tag{E1}
\end{align} holds true. Recall the notation \begin{align}
  S &= (X^{(i)},Y^{(i)})_{i=1,\ldots,N} \\
  \pi_{\mathcal X} S &= (X^{(i)})_{i=1,\ldots,N} \\
  \mathcal G_{\mathcal F} &= \left\{
    g:(x,y)\mapsto 1_{y\neq h(x)} \,\big|\, h \in\mathcal F 
  \right\}.
\end{align} In this notation, we can recast the bounds for the
\href{SS21-MsAML__06-S1__Infinite_hypotheses.pdf}{infinite hypotheses
case} as follows:

\begin{description}
\item[Corollary] ~ 
\hypertarget{rademacher-generalization-bounds}{%
\subparagraph{(Rademacher generalization
bounds)}\label{rademacher-generalization-bounds}}

Let \(\mathcal F\subseteq \mathcal H\) be a set of relevant hypotheses,
\(\mathcal  Y=\{-1,+1\}\) and \(S\) an i.i.d.-\(P\) random vector of
training data. Then, for all \(h\in\mathcal H\), \(\delta\in (0,1]\) we
have \begin{align}
R(h)
&\leq
\widehat R_S(h)
+ \mathcal R_N(\mathcal F)
+ \sqrt{
    \frac{\log\frac2\delta}{2N} 
  }
   \label{eq_2}
   \tag{E2}
\\
R(h)
&\leq
\widehat R_S(h)
+ \widehat{\mathcal R}_{\pi_{\mathcal X}S}(\mathcal F)
+ 3\sqrt{
    \frac{\log\frac2\delta}{2N}
  }
  \end{align} with probability of at least \(1-\delta\).
\end{description}

In order to control these bounds further, let us regard again the
Rademacher complexity \begin{align}
  \mathcal R_N(\mathcal F)
  =
  E_S E_\sigma \sup_{h\in\mathcal F}
    \frac1N\sum_{i=1}^N\sigma_i h(X^{(i)})
  \label{eq_3}
  \tag{E3}
\end{align} and observe that the crucial quantity influencing the
Rademacher complexity is the number of patterns that arise in
\begin{align}
  \mathcal F_{x^{(1)},\ldots,x^{(N)}}
  :=
  \left\{
    \left(h(x^{(1)}),\ldots, h(x^{(N)})\right)
    \,\big|\,
    h\in\mathcal F
  \right\}
\end{align}

The latter quantity deserves a name:

\begin{description}
\item[Definition] ~ 
\hypertarget{growth-function-1}{%
\subparagraph{(Growth function)}\label{growth-function-1}}

For given \(\mathcal F\subseteq \mathcal H\) of relevant hypotheses, we
define the so-called \emph{growth function}: \begin{align}
\Pi_{\mathcal F}:\mathbb N &\to \mathbb N\\
N&\mapsto \Pi_{\mathcal F}(N)
:=
\max_{x^{(1)},\ldots, x^{(N)}\in\mathcal X}
\left|
  \mathcal F\big|_{(x^{(1)},\ldots, x^{(N)})}
\right|.
  \end{align}
\end{description}

This function counts the maximum number of ``bit'' patterns of length
\(N\) that are generated by the functions in \(\mathcal F\) when
considering all configurations of \(N\) input features. Increasing
\(N\), the growth function measures the growth of such maximum number of
patterns.

In the \href{SS21-MsAML__06-XS1__Rademacher_complexity.pdf}{exercises}
we have found the relation \begin{align}
  E_\sigma \sup_{v\in\mathcal F_{\pi_{\mathcal X}s}} \frac1N 
  \sum_{i=1}^N \sigma_i v_i
  \leq
  \sup_{v\in\mathcal F_{\pi_{\mathcal X}s}}
  |v|\frac{\sqrt{2\log |\mathcal F_{\pi_{\mathcal X}s}|}}{N},
\end{align} for which the right-hand side can now be estimated by means
of \(\Pi_{\mathcal F}(N)\). In view of \(\eqref{eq_3}\), this implies
\begin{align}
  \mathcal R_N(\mathcal F) 
  \leq 
  \sqrt{N}
  \frac{\sqrt{2\log \Pi_{\mathcal F}(N)}}{N},
  \label{eq_4}
  \tag{E4}
\end{align} for which the first factor \(\sqrt N\) arises due the fact
that the components of the vectors are sequences of \(-1,+1\)'s.

Let us summarize these observations:

\begin{description}
\item[Theorem] ~ 
\hypertarget{growth-function-bound-for-binary-classification}{%
\subparagraph{(Growth function bound for binary
classification)}\label{growth-function-bound-for-binary-classification}}

Let \(|\mathcal Y|=2\) and \(\mathcal F\subseteq \mathcal H\) a space of
relevant hypotheses. Then: \begin{align}
\mathcal R_N(\mathcal F) \leq \sqrt{\frac{2\log\Pi_{\mathcal F}(N)}{N}}.
  \end{align}
\end{description}

Coming back to our generalization bounds, we can recast the Rademacher
generalization bound above by means of the growth function, observing
\(\eqref{eq_1}\), \(\eqref{eq_2}\), and \(\eqref{eq_4}\):

\begin{description}
\item[Corollary] ~ 
\hypertarget{growth-function-generalization-bounds}{%
\subparagraph{(Growth function generalization
bounds)}\label{growth-function-generalization-bounds}}

Let \(\mathcal F\subseteq \mathcal H\) be a set of relevant hypotheses,
\(|\mathcal  Y|=2\), and \(S\) an i.i.d.-\(P\) random vector of training
data. Then, for all \(h\in\mathcal F\) and \(\delta\in (0,1]\), we have
\begin{align}
R(h)
&\leq
\widehat R_S(h)
+
\sqrt{\frac{2\log\Pi_{\mathcal F}(N)}{N}}
+ \sqrt{
    \frac{\log\frac2\delta}{2N} 
  }
  \end{align} with probability of at least \(1-\delta\).
\end{description}

Of course, had we started with the special case of binary
classification, we might have derived this result more directly,
potentially also with tighter constants, but we might have missed the
other measures of ``richness'' of the hypotheses space \(\mathcal F\)
which are again important for the general cases.

In the special case of binary classification, this is a first
improvement with respect to the previous Rademacher generalization bound
since the growth function is easier to compute. Nevertheless, it will
require a new computation every time we vary \(N\). In the next module
we will therefore study how the ``growth'' of the growth function can be
characterized further.

\end{document}
