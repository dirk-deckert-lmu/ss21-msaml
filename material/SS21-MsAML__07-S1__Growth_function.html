<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__07-S1__Growth_function</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-07-s1-pdf">MsAML-07-S1 [<a href="SS21-MsAML__07-S1__Growth_function.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#growth-function">Growth function</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="growth-function">Growth function</h2>
<p>We have seen that the Rademacher complexity provides a first means to better capture the “richness” of potentially infinite hypotheses spaces. However, even already the empirical Rademacher complexity can turn out to be computationally quite hard to access. Therefore, it makes sense to look at for more accessible quantities on which it depends. In order to proceed, we will restrict ourselves to the case of binary classification only, i.e., <span class="math inline">\(|\mathcal Y|=2\)</span>. There are several strong techniques to also treat the general multi-class classification or even regression based on similar ideas but, due to our time constraints, these go beyond the scope of our course.</p>
<p>Based on the <a href="SS21-MsAML__06-XS1__Rademacher_complexity.html">exercises</a>, we have found for the special case of binary classification and loss function <span class="math display">\[\begin{align}
  L(y,y&#39;)=1_{y\neq y&#39;}  
\end{align}\]</span> that the relation <span class="math display">\[\begin{align}
  \widehat{\mathcal R}_S(\mathcal G_{\mathcal F})
  =
  \frac12 \widehat{\mathcal R}_{\pi_{\mathcal X}S}(\mathcal F)
  \label{eq_1}
  \tag{E1}
\end{align}\]</span> holds true. Recall the notation <span class="math display">\[\begin{align}
  S &amp;= (X^{(i)},Y^{(i)})_{i=1,\ldots,N} \\
  \pi_{\mathcal X} S &amp;= (X^{(i)})_{i=1,\ldots,N} \\
  \mathcal G_{\mathcal F} &amp;= \left\{
    g:(x,y)\mapsto 1_{y\neq h(x)} \,\big|\, h \in\mathcal F 
  \right\}.
\end{align}\]</span> In this notation, we can recast the bounds for the <a href="SS21-MsAML__06-S1__Infinite_hypotheses.html">infinite hypotheses case</a> as follows:</p>
<dl>
<dt>Corollary</dt>
<dd><h5 id="rademacher-generalization-bounds">(Rademacher generalization bounds)</h5>
Let <span class="math inline">\(\mathcal F\subseteq \mathcal H\)</span> be a set of relevant hypotheses, <span class="math inline">\(\mathcal  Y=\{-1,+1\}\)</span> and <span class="math inline">\(S\)</span> an i.i.d.-<span class="math inline">\(P\)</span> random vector of training data. Then, for all <span class="math inline">\(h\in\mathcal H\)</span>, <span class="math inline">\(\delta\in (0,1]\)</span> we have <span class="math display">\[\begin{align}
R(h)
&amp;\leq
\widehat R_S(h)
+ \mathcal R_N(\mathcal F)
+ \sqrt{
    \frac{\log\frac2\delta}{2N} 
  }
   \label{eq_2}
   \tag{E2}
\\
R(h)
&amp;\leq
\widehat R_S(h)
+ \widehat{\mathcal R}_{\pi_{\mathcal X}S}(\mathcal F)
+ 3\sqrt{
    \frac{\log\frac2\delta}{2N}
  }
  \end{align}\]</span> with probability of at least <span class="math inline">\(1-\delta\)</span>.
</dd>
</dl>
<p>In order to control these bounds further, let us regard again the Rademacher complexity <span class="math display">\[\begin{align}
  \mathcal R_N(\mathcal F)
  =
  E_S E_\sigma \sup_{h\in\mathcal F}
    \frac1N\sum_{i=1}^N\sigma_i h(X^{(i)})
  \label{eq_3}
  \tag{E3}
\end{align}\]</span> and observe that the crucial quantity influencing the Rademacher complexity is the number of patterns that arise in <span class="math display">\[\begin{align}
  \mathcal F_{x^{(1)},\ldots,x^{(N)}}
  :=
  \left\{
    \left(h(x^{(1)}),\ldots, h(x^{(N)})\right)
    \,\big|\,
    h\in\mathcal F
  \right\}
\end{align}\]</span></p>
<p>The latter quantity deserves a name:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="growth-function-1">(Growth function)</h5>
For given <span class="math inline">\(\mathcal F\subseteq \mathcal H\)</span> of relevant hypotheses, we define the so-called <em>growth function</em>: <span class="math display">\[\begin{align}
\Pi_{\mathcal F}:\mathbb N &amp;\to \mathbb N\\
N&amp;\mapsto \Pi_{\mathcal F}(N)
:=
\max_{x^{(1)},\ldots, x^{(N)}\in\mathcal X}
\left|
  \mathcal F\big|_{(x^{(1)},\ldots, x^{(N)})}
\right|.
  \end{align}\]</span>
</dd>
</dl>
<p>This function counts the maximum number of “bit” patterns of length <span class="math inline">\(N\)</span> that are generated by the functions in <span class="math inline">\(\mathcal F\)</span> when considering all configurations of <span class="math inline">\(N\)</span> input features. Increasing <span class="math inline">\(N\)</span>, the growth function measures the growth of such maximum number of patterns.</p>
<p>In the <a href="SS21-MsAML__06-XS1__Rademacher_complexity.html">exercises</a> we have found the relation <span class="math display">\[\begin{align}
  E_\sigma \sup_{v\in\mathcal F_{\pi_{\mathcal X}s}} \frac1N 
  \sum_{i=1}^N \sigma_i v_i
  \leq
  \sup_{v\in\mathcal F_{\pi_{\mathcal X}s}}
  |v|\frac{\sqrt{2\log |\mathcal F_{\pi_{\mathcal X}s}|}}{N},
\end{align}\]</span> for which the right-hand side can now be estimated by means of <span class="math inline">\(\Pi_{\mathcal F}(N)\)</span>. In view of <span class="math inline">\(\eqref{eq_3}\)</span>, this implies <span class="math display">\[\begin{align}
  \mathcal R_N(\mathcal F) 
  \leq 
  \sqrt{N}
  \frac{\sqrt{2\log \Pi_{\mathcal F}(N)}}{N},
  \label{eq_4}
  \tag{E4}
\end{align}\]</span> for which the first factor <span class="math inline">\(\sqrt N\)</span> arises due the fact that the components of the vectors are sequences of <span class="math inline">\(-1,+1\)</span>’s.</p>
<p>Let us summarize these observations:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="growth-function-bound-for-binary-classification">(Growth function bound for binary classification)</h5>
Let <span class="math inline">\(|\mathcal Y|=2\)</span> and <span class="math inline">\(\mathcal F\subseteq \mathcal H\)</span> a space of relevant hypotheses. Then: <span class="math display">\[\begin{align}
\mathcal R_N(\mathcal F) \leq \sqrt{\frac{2\log\Pi_{\mathcal F}(N)}{N}}.
  \end{align}\]</span>
</dd>
</dl>
<p>Coming back to our generalization bounds, we can recast the Rademacher generalization bound above by means of the growth function, observing <span class="math inline">\(\eqref{eq_1}\)</span>, <span class="math inline">\(\eqref{eq_2}\)</span>, and <span class="math inline">\(\eqref{eq_4}\)</span>:</p>
<dl>
<dt>Corollary</dt>
<dd><h5 id="growth-function-generalization-bounds">(Growth function generalization bounds)</h5>
Let <span class="math inline">\(\mathcal F\subseteq \mathcal H\)</span> be a set of relevant hypotheses, <span class="math inline">\(|\mathcal  Y|=2\)</span>, and <span class="math inline">\(S\)</span> an i.i.d.-<span class="math inline">\(P\)</span> random vector of training data. Then, for all <span class="math inline">\(h\in\mathcal F\)</span> and <span class="math inline">\(\delta\in (0,1]\)</span>, we have <span class="math display">\[\begin{align}
R(h)
&amp;\leq
\widehat R_S(h)
+
\sqrt{\frac{2\log\Pi_{\mathcal F}(N)}{N}}
+ \sqrt{
    \frac{\log\frac2\delta}{2N} 
  }
  \end{align}\]</span> with probability of at least <span class="math inline">\(1-\delta\)</span>.
</dd>
</dl>
<p>Of course, had we started with the special case of binary classification, we might have derived this result more directly, potentially also with tighter constants, but we might have missed the other measures of “richness” of the hypotheses space <span class="math inline">\(\mathcal F\)</span> which are again important for the general cases.</p>
<p>In the special case of binary classification, this is a first improvement with respect to the previous Rademacher generalization bound since the growth function is easier to compute. Nevertheless, it will require a new computation every time we vary <span class="math inline">\(N\)</span>. In the next module we will therefore study how the “growth” of the growth function can be characterized further.</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
