% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-04-s1-pdf}{%
\section{\texorpdfstring{MsAML-04-S1
{[}\href{SS21-MsAML__04-S1__PAC_learning_framework.pdf}{PDF}{]}}{MsAML-04-S1 {[}PDF{]}}}\label{msaml-04-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{pac-learning-framework}{PAC learning framework}

  \begin{itemize}
  \tightlist
  \item
    \protect\hyperlink{consistent-case}{Consistent case}
  \end{itemize}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{pac-learning-framework}{%
\subsection{PAC learning framework}\label{pac-learning-framework}}

So far we have discussed indicators to measure the learning performance
of supervised learners. The next natural questions may be:

\begin{itemize}
\tightlist
\item
  What can be learned ``efficiently''?
\item
  How many training data samples are needed to achieve a certain
  performance?
\end{itemize}

The first question calls for a definition of was is learnable, and in
particular, \emph{efficiently} so. The second question goes in the
direction of our the
\href{SS21-MsAML__03-X1__Perceptron_convergence.pdf}{simple learning
setting} discussed in the last module.

As discussed in the previous modules, the main difficulty of supervised
learning is that all we have are our disposal is the realization of a
training data sample \(s\) in order to pick a hypothesis \(h_s\). As we
have little control over which training sample \(s\) we are given in our
application, we model the given training data as chosen at random, i.e.,
by random variabel \(S\), with an unknown distribution \(P\).

We can always try to find hypotheses \(h_s\) that perfectly predicts the
class labels of the training data feature vectors of \(s\) by simply
recording them in a look-up table. However, we would like to study the
performance, not only for one particular realization \(s\) but for
fairly chosen training data \(S\), and at the same time, we would like
to also measure the performance on previously unseen samples. Hence, a
meaningful performance estimate is naturally probabilistic. The
so-called \emph{p}robably-\emph{a}pproximately-\emph{c}orrect or PAC
framework, we will introduce next, tries to capture this nature of
statistical learning.

In this modules, let us regard the deterministic case, for which there
is a function \begin{align}
  c\in{\mathcal Y}^{\mathcal= X},
\end{align} called \emph{concept}, which assigns any feature vector
\(x\in\mathcal X\) its actual label \(y=c(x)\in\mathcal Y\). The
training data sample then takes the form \begin{align}
  s=(x^{(i)},y^{(i)}\equiv c(x^{(i)}))_{i=1,\ldots,N}.
\end{align} We will always consider the case of i.i.d. feature vectors
\(X^{(i)}\) with corresponding distribution \(D\) on \(\mathcal X\) and
write \(X^N=(X^{(1)},\ldots,X^{(N)})\sim D^N\). In this case, the joint
distribution \(P\) of feature vector and class label tuples on
\((\mathcal X\times\mathcal Y)\) is given by a product of \(D\) with the
Dirac-delta measure \(\delta_{y=c(x)}\).

We call a set of such concepts a \emph{concept class} \(\mathcal C\).

\begin{description}
\tightlist
\item[Example:]
In our usual setting of binary classification, \(|\mathcal Y|=2\), we
could for example choose class labels \(\{0,1\}\) and for any set
\(M\subseteq X\), the characteristic function \(1_M\) would represent a
concept. For instance, for \(\mathcal X=\mathbb R^2\), the concept class
\(\mathcal C\) of indicator functions attaining value one on rectangular
shaped subsets of \(\mathcal X\) and zero on there complements is
results in the concept class of ``rectangles on \(\mathbb R^2\)''. This
concept class would be convenient to study the performance of a
supervised learner that should predict a rectangle, i.e., represented by
the corresponding indicator function, from a given set of training data
points
\((x^{(i)},y^{(i)})_{i=1,\ldots,N}\in(\mathbb R^2\times\{0,1\})^N\).
\end{description}

With these notions, the central definition of PAC-learning can be given
by:

\begin{description}
\item[Definition] ~ 
\hypertarget{pac-learnable-in-the-deterministic-case}{%
\subparagraph{(PAC-learnable in the deterministic
case)}\label{pac-learnable-in-the-deterministic-case}}

A concept class \(\mathcal C\) is said to be PAC-learnable, if there
exists an algorithm \(\mathcal A\) and a polynomial \(p\) such that
\begin{align}
\forall \quad &\epsilon, \delta\in (0,1]\\
\forall \quad &\text{distributions $D$ on $\mathcal X$}\\
\forall \quad &\text{concepts $c$ in $\mathcal C$}\\
\forall \quad &N\geq p\left(\frac1\epsilon,\frac1\delta\right):\\
&\\
& P_{S\sim P^N}(R(h_S)\leq \epsilon) \geq 1-\delta.
  \end{align} Furthermore, if \(\mathcal A\) runs in
\(p(\frac1\epsilon,\frac1\delta)\) polynomial time, then \(\mathcal C\)
is called \emph{efficiently} PAC-learnable.
\end{description}

Note that, as introduced earlier, the distribution \(P\) is given by the
product measure of \(D\) the Dirac-delta measure \(\delta_{y=c(x)}\).
Therefore, \(P_{S\sim P^N}\) can be read as \(P_{X^N\sim D^N}\) in this
context.

In other words, \(\mathcal C\) is PAC-learnable if there exists an
algorithm and polynomial \(p\) that, independent of the distribution
\(D\) and only after inspection of the order of
\(p(\frac1\epsilon,\frac 1\delta)\) many training data samples in \(S\),
returns a hypothesis \(h_S\) that is with

\begin{itemize}
\tightlist
\item
  \(\geq 1-\delta\) high \emph{p}robability
\item
  \(\epsilon\)-\emph{a}pproximately
\item
  \emph{c}orrect.
\end{itemize}

One usually calls \(1-\delta\) the confidence and \(1\epsilon\) the
accuracy.

\hypertarget{consistent-case}{%
\subsubsection{Consistent case}\label{consistent-case}}

Suppose now \(\mathcal A\) is an algorithm with finite range
\(\mathcal F\) such that, for all \(c\in\mathcal C\) and i.i.d. training
samples \begin{align}
   s=(x^{(i)},c(x^{(i)}))_{i=1,\ldots,N},
\end{align} returns a hypothesis \(h_s\) which fulfills \begin{align}
  \widehat R_s(h_s) = 0,
\end{align} which is often called a \emph{consistent} hypothesis. This
assumption readily puts us in the scenario of the previously studied
\href{SS21-MsAML__03-S1__A_simple_learning_setting.pdf}{simple learning
setting}, the result of which can now we summarized as follows:

\begin{description}
\item[Corollary] ~ 
\hypertarget{consistent-and-deterministic-case-for-finite-hypothesis-spaces}{%
\subparagraph{(Consistent and deterministic case for finite hypothesis
spaces)}\label{consistent-and-deterministic-case-for-finite-hypothesis-spaces}}

Let \(\mathcal A\) be an algorithm with finite range \(\mathcal F\)
that, for any concept \(c\in\mathcal F\) and training data sample \(s\)
returns a consistent hypothesis \(h_s\), then any concept class
\(\mathcal C\subseteq\mathcal F\) is PAC-learnable.
\end{description}

\textbf{Proof:} Observing that the consistency condition, i.e.,
\begin{align}
  \forall s\in(\mathcal X\times\mathcal Y)^N: \widehat R_s(h_s)=0
\end{align} guarantees the assumptions used in our
\href{SS21-MsAML__03-S1__A_simple_learning_setting.pdf}{simple learning
setting}. Our case considered here is even simpler as the we have a
deterministic concept, though the estimates of our previous computations
are not effected. The proven bound of the last module implies for any
\(\mathcal C\subseteq \mathcal F\): \begin{align}
  \forall \quad &\epsilon, \delta\in (0,1]\\
  \forall \quad &\text{distributions $D$ on $\mathcal X$}\\
  \forall \quad &\text{concepts $c$ in $\mathcal C$}\\
  \forall \quad &N\geq \frac1\epsilon\log\frac{|\mathcal F|}{\delta}:\\
  &\\
  & P_{S\sim P^N}(R(h_S)\leq \epsilon) \geq 1-\delta.
\end{align} Noting that
\(N\geq \frac1\epsilon\log\frac{|\mathcal F|}{\delta}\) is implied by
\begin{align}
  \frac1\epsilon\log\frac{|\mathcal F|}{\delta}
  &=\frac1\epsilon\left( \log\frac1\delta +\log|\mathcal F|  \right)\\
  &\leq \frac1\epsilon \left( \frac1\delta + \log|\mathcal F|\right)\\
  &\leq N,
\end{align} which is a polynomial \(p(\frac1\epsilon,\frac1\delta)\), we
are in the setting of \protect\hyperlink{pac-learnable}{Definition
(PAC-learnable)}, which concludes the proof.

\(\square\)

In other words, if in addition to consistency, the set of relevant
hypotheses is finite, the required sample size is dominated by a
polynomially increasing function in terms of \(\frac1\epsilon\) and
\(\frac1\delta\).

In applications, of course, several other factors influence the total
amount of computation steps the algorithm has to carry out, such as size
of the representation of the data, etc. However, if there is an
algorithm that, e.g., only requires finite many per computation steps,
per inspection of a training data sample to find the hypothesis, we
immediately infer that the concept class is efficiently PAC-learnable.
The latter often requires a thorough analysis of the algorithm at hand.
In this course we will however focus more on the statistical aspect of
PAC-learnability, e.g., simply assuming that there are algorithms that
succeed in the task of empirical risk minimization rather than spelling
out the algorithm itself, and we will not discuss efficiency in detail.

Yet, in other words, the generalization error decreases as a function of
the sample size \(N\)

As we have already discussed, this behavior substantiates our initial
hope that increasing the sample size allows to increase the performance
of our supervised learner.

However, to ensure consistency of the hypotheses selected by our
algorithm we would have to make sure it exploits a sufficiently large
range \(\mathcal F\), in particular, containing the concepts that we
want to learn.

In our next steps, we will try to relax the consistency assumption.

➢ Next session!

For this purpose, we will however need a new tool, commonly referred to
as \emph{concentration inequalities}, and which, which we will introduce
in the next chapter.

\end{document}
