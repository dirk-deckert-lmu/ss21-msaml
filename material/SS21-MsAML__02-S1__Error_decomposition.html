<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__02-S1__Error_decomposition</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-02-s1-pdf">MsAML-02-S1 [<a href="SS21-MsAML__02-S1__Error_decomposition.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#error-decomposition-and-generalization-bounds">Error decomposition and generalization bounds</a>
<ul>
<li><a href="#trade-off-considerations">Trade-off considerations</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="error-decomposition-and-generalization-bounds">Error decomposition and generalization bounds</h2>
<p>In the last module, we have introduced a statistical framework in which we can grasp the learning performance more precisely. However, we also realized that its study may become rather tricky in non-academic settings in which all we have at our expense is the empirical data. Hence, the actual risk <span class="math inline">\(R\)</span> is unaccessible directly.</p>
<p>Instead of being able to minimize the risk <span class="math inline">\(R(h)\)</span> over a certain space of hypotheses <span class="math inline">\(h\in\mathcal H\)</span> directly we can only access the empirical <span class="math inline">\(\widehat R_s(h)\)</span> average of it given our realization of training data <span class="math inline">\(s\in\cup_{N\in\mathbb N}(\mathcal X\times\mathcal Y)^N\)</span>. Hence, at best we may try to minimize the empirical risk <span class="math inline">\(\widehat R_s(h)\)</span> over a hypotheses <span class="math inline">\(h\in\mathcal H\)</span> that are accessible the supervised learning, i.e., the particular algorithm <span class="math inline">\(\mathcal A\)</span>, and hope that, for large samples sizes <span class="math inline">\(N=|s|\)</span> the obtained minimum of the empirical risk <span class="math inline">\(\widehat R(\widehat h)\)</span> for some minimizer <span class="math inline">\(\widehat h\)</span> is with larger probability not too far from the actual infimum of <span class="math inline">\(R\)</span>.</p>
<p>This approach is called <em>empirical risk minimization</em>.</p>
<p>To better understand the challenges in such an approach, it makes sense to identify the different sources for differences in <span class="math inline">\(\widehat R(\widehat h)\)</span> and <span class="math inline">\(R^*\)</span>, which goes by the name of <em>error decomposition</em>.</p>
<p>For our purposes in this module, let <span class="math inline">\(\mathcal H\)</span> be the largest set of sensible hypothesis candidates, i.e., <span class="math display">\[\begin{align}
  \mathcal H := \left\{h\in {\mathcal Y}^{\mathcal X} \,|\, h \text{ measurable}\right\},
\end{align}\]</span> and let us define <span class="math display">\[\begin{align}
   R^* := \inf_{h \in\mathcal H}R(h),
\end{align}\]</span> which is called the <em>Bayes risk</em>. A hypothesis <span class="math inline">\(h^*\)</span> with <span class="math inline">\(R^*=R(h^*)\)</span> is then called a Bayes classifier, which however may not need to exist.</p>
<p>Suppose further, the supervised learner, an algorithm <span class="math inline">\(\mathcal A\)</span>, has a range <span class="math display">\[\begin{align}
   \mathcal F:=\text{range}\mathcal A \subseteq \mathcal H,
\end{align}\]</span> say, due to the particularities and constraints of its implementation. We may then define the optimal risk on this subspace by <span class="math display">\[\begin{align}
   R_{\mathcal F} := \inf_{h\in\mathcal F}R(h).
\end{align}\]</span></p>
<p>Let <span class="math inline">\(\widehat h_s=\mathcal A(s)\)</span> denote a hypothesis learn by algorithm <span class="math inline">\(\mathcal A\)</span> by means of inspecting training <span class="math inline">\(s\)</span>. We may then go ahead and decompose the original difference of interest for a hypothesis <span class="math inline">\(h\in\mathcal F\)</span>: <span class="math display">\[\begin{align}
   R(h)-R^*
   =
   &amp; \phantom{-}\left(R(h)-R(\widehat h)\right)
   \tag{Optimization Error}
   \\
   &amp; +\left(R(\widehat h)-R_{\mathcal F}\right)
   \tag{Estimation Error}
   \\
   &amp; +\left(R_{\mathcal F}-R^*\right)
   \tag{Approximation Error}
\end{align}\]</span></p>
<p>This decomposition is useful because:</p>
<ul>
<li><span class="math inline">\(\widehat h\)</span> is the only entity that depends on the empirical sample of training data and is usually the only accessible object;</li>
<li><span class="math inline">\(R_{\mathcal F}\)</span> depends only the abilities of the algorithm <span class="math inline">\(\mathcal A\)</span>;</li>
<li><span class="math inline">\(R^*\)</span> depends only on the learning task.</li>
</ul>
<p>Therefore:</p>
<ul>
<li>the optimization error compares a hypothesis <span class="math inline">\(h\)</span> against the learned hypothesis <span class="math inline">\(\widehat h\)</span> in terms of their actual risk;</li>
<li>the estimation error reflects how well the actual risk of the learned hypothesis <span class="math inline">\(\widehat h\)</span> performs when compared against the theoretically optimal performance of the algorithm <span class="math inline">\(\mathcal A\)</span>, namely the minimizer of the risk over <span class="math inline">\(\mathcal F\)</span>;</li>
<li>the approximation error only depends on the learning task and the available hypotheses in <span class="math inline">\(\mathcal F\)</span> the algorithm <span class="math inline">\(\mathcal A\)</span> can reach.</li>
</ul>
<p>Clearly, when given a learning task and an algorithm, the approximation error is what it is. Nevertheless, the hope formulated above, i.e., that the right-hand side decreases when increasing the sample size of training data <span class="math inline">\(N\)</span>, due to the optimization and estimation error, and we will spend a large part of this course trying to justify this intuition in various settings.</p>
<p>Hence, let us assume that, for given training data <span class="math inline">\(s\)</span>, the algorithm <span class="math inline">\(\mathcal A\)</span> always succeeds in the empirical minimization and learns a hypothesis <span class="math inline">\(\widehat h_s\)</span> such that <span class="math display">\[\begin{align}
  \forall h\in\mathcal F: \, \widehat R_s(\widehat h_s)\leq \widehat R(h).
\end{align}\]</span> To a gain more control the remaining difference of interest, observe for any <span class="math inline">\(h\in\mathcal F\)</span>: <span class="math display">\[\begin{align}
  &amp; R(\widehat h) - R(h) 
  \\
  &amp;= 
    R(\widehat h)            - \widehat R(\widehat h)
                 + \underbrace{\widehat R(\widehat h) - \widehat R(h)}_{\leq 0 \text{ as $\widehat h$ minimizes $\widehat R$}}
        + \widehat R(h) - R(h)
  \\
  &amp;\leq
  R(\widehat h)  - \widehat R(\widehat h)
        + \widehat R(h) - R(h)
          \\
  &amp;\leq 
  |R(\widehat h)  - \widehat R(\widehat h)|
  + |\widehat R(h) - R(h)|
  \\
  &amp;\leq
  2\sup_{h&#39;\in\mathcal F}|\widehat R(h&#39;)-R(h&#39;)|
  \tag{Generalization error}
  \label{eq_generalization_error}
\end{align}\]</span></p>
<p>As this inequality holds for all <span class="math inline">\(h\in\mathcal F\)</span>, we can also take the infimum<br />
over <span class="math inline">\(h\in\mathcal F\)</span> on the left-hand side to find <span class="math display">\[\begin{align}
  &amp; R(\widehat h)-R_{\mathcal F} 
  \leq
  2\sup_{h&#39;\in\mathcal F}|\widehat R(h&#39;)-R(h&#39;)|.
\end{align}\]</span></p>
<p>Hence, in order to substantiate our hope it suffices to provide bounds for the right-hand side of <span class="math inline">\(\eqref{eq_generalization_error}\)</span> in terms of sample size <span class="math inline">\(N\)</span>. As these bounds provide information on how well a hypothesis <span class="math inline">\(h\)</span> generalizes from observed training data to unseen test data, they a usually referred as <em>generalization bounds</em>.</p>
<h3 id="trade-off-considerations">Trade-off considerations</h3>
<p>Neglecting the optimization error for a moment typically reveals the following trade-off:</p>
<ul>
<li>A smaller approximation error may require a more complex hypotheses space <span class="math inline">\(\mathcal F\)</span> that contains hypotheses which are better adapted to learning task at hand;</li>
<li>The estimation error typically grows with the complexity of <span class="math inline">\(\mathcal F\)</span> as there are more an more peculiarities to consider when choosing a hypothesis.</li>
</ul>
<p>To better understand such a trade-off let us regard a very similar setting as in our discussion of Theorem (Geometric nature of conditional expectation) in the <a href="SS21-MsAML__01-S1__Statistical_framework.html">Statistical framwork</a> module with the following ingredients:</p>
<ul>
<li>The random variables <span class="math inline">\((X,Y)\sim P\)</span> taking values in <span class="math inline">\(\mathcal X\times\mathcal Y\)</span>;</li>
<li>Training data <span class="math inline">\(S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}\)</span> as a random variable taking values in <span class="math inline">\((\mathcal X,\mathcal Y)^N\)</span> and being distributed according to <span class="math inline">\(P^N\)</span>.</li>
<li>The pair-wise independence of <span class="math inline">\((X,Y), (X^{(1)},Y^{(1)}), \ldots, (X^{(N)},Y^{(N)})\)</span>;</li>
<li>A hypothesis <span class="math display">\[\begin{align}
   h_S := \mathcal A(S)
\end{align}\]</span> that is learned by an algorithm <span class="math inline">\(\mathcal A\)</span> on the basis of training data <span class="math inline">\(S\)</span>.</li>
<li>The assumption that that <span class="math inline">\(E_{(X,Y)\sim P} h_S(X)^2, EY^2&lt;\infty\)</span> so that also the mean square error risk <span class="math inline">\(R(h_S):=E_{(X,Y)\sim P}|Y-h(X)|^2\)</span> to be well-defined;</li>
<li>The assumption <span class="math display">\[\begin{align}
  E_{S\sim P^N} R(h_S)&lt;\infty;
\end{align}\]</span></li>
<li>The expected prediction denoted by: <span class="math display">\[\begin{align}
   \bar h(\cdot) = E_{S\sim P^N} h_S(\cdot).
\end{align}\]</span> Note the dependence of <span class="math inline">\(h_S\)</span> on <span class="math inline">\(S\)</span> and the independence of <span class="math inline">\(\bar h\)</span> of <span class="math inline">\(S\)</span>.</li>
</ul>
<dl>
<dt>Theorem</dt>
<dd><h5 id="noise-bias-variance-decomposition">(Noise-bias-variance decomposition)</h5>
In the above setting we have <span class="math display">\[\begin{align}
 E_{S\sim P^N} R(h_S) &amp;\\
 = &amp; \underbrace{E_{(X,Y)\sim P} |Y-E(Y|X)|^2}_\text{expected noise}
 \\
 &amp; + \underbrace{E_{(X,Y)\sim P} |\bar h(X)-E(Y|X)|^2}_\text{expected bias}
 \\
 &amp; + \underbrace{E_{(X,Y)\sim P} E_{S\sim P^N}|h_S(X)-\bar h(X)|^2}_\text{expected variance}
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> Recall Theorem (Geometric nature of conditional expectation) for the <a href="SS21-MsAML__01-S1__Statistical_framework.html">Statistical framwork</a> module. For a realization of training data <span class="math inline">\(s\in(\mathcal X\times\mathcal Y)^N\)</span> it states: <span class="math display">\[\begin{align}
  R(h_s) = E|Y-E(Y|X)|^2 + E|h_s(X)-E(Y|X)|^2.
\end{align}\]</span> Replacing the realization <span class="math inline">\(s\)</span> with a random variable <span class="math inline">\(S\)</span> and taking the expectation with respect to <span class="math inline">\(S\)</span> yields: <span class="math display">\[\begin{align}
  E_{S\sim P^N} R(h_S) = &amp; E_{S\sim P^N} E_{(X,Y)\sim P} |Y-E(Y|X)|^2
  \tag{S1}
  \label{eq_first_summand}
  \\
  &amp; + E_{S\sim P^N} E_{(X,Y)\sim P} |h_S(X)-E(Y|X)|^2
  \tag{S2}
  \label{eq_second_summand}
\end{align}\]</span> The first summand is already to our liking. The second summand reads: <span class="math display">\[\begin{align}
  \eqref{eq_second_summand} = 
  &amp; E_{S\sim P^N} E_{(X,Y)\sim P} |h_S(X)-\bar h(X)+\bar h(X)-E(Y|X)|^2 
  \\
  = &amp; \phantom{+} E_{S\sim P^N} E_{(X,Y)\sim P} |h_S(X)-\bar h(X)|^2 
  \tag{S2.1}
  \label{eq_second_first_summand}
  \\
  &amp; + E_{S\sim P^N} E_{(X,Y)\sim P} |\bar h(X)-E(Y|X)|^2 
  \tag{S2.2}
  \label{eq_second_second_summand}
  \\
  &amp; + 2\underbrace{E_{S\sim P^N} E_{(X,Y)\sim P} 
                  \left(h_S(X)-\bar h(X)\right)
                  \left(\bar h(X)-E(Y|X)\right)}_{
                      =E_{(X,Y)\sim P}\left(\bar h(X)-E(Y|X)\right)
                      E_{S\sim P^N}\left(h_S(X)-\bar h(X)]\right)
                  }.
\end{align}\]</span> Note that by definition <span class="math inline">\(E_{S\sim P^N}\left(h_S(X)-\bar h(X)\right)=0\)</span>. Hence, only <span class="math inline">\(\eqref{eq_first_summand},\eqref{eq_second_first_summand},\eqref{eq_second_second_summand}\)</span> remain in the original expression of the expectation of the actual risk. Furthermore, we observe that in <span class="math inline">\(\eqref{eq_first_summand}\)</span> and <span class="math inline">\(\eqref{eq_second_second_summand}\)</span> we may drop the expectation over <span class="math inline">\(S\sim P^N\)</span> and in <span class="math inline">\(\eqref{eq_second_first_summand}\)</span> we may interchange the expectation values over <span class="math inline">\(S\sim P^N\)</span> and <span class="math inline">\((X,Y)\sim P\)</span> by Fubini, which concludes the proof.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>The noise term was already discussed in module <a href="SS21-MsAML__01-S1__Statistical_framework.html">Statistical framwork</a>. It vanished for a deterministic concept. In addition we have:</p>
<ul>
<li>The expected bias that reflects an inherent preference of the algorithm <span class="math inline">\(\mathcal A\)</span> in picking a hypothesis <span class="math inline">\(h_S\)</span> on the basis of training data <span class="math inline">\(S\)</span>, which may stem from the implementation of algorithm <span class="math inline">\(\mathcal A\)</span>, in particular, the richness in complexity of the hypotheses <span class="math inline">\(\mathcal F\)</span> accessible to algorithm <span class="math inline">\(\mathcal A\)</span>.</li>
<li>The expected variance reflects how much the computed <span class="math inline">\(h_S\)</span> depends on the training data sample <span class="math inline">\(S\)</span>, or put a bit more negatively, how much the algorithm <span class="math inline">\(\mathcal A\)</span> adapted the hypothesis <span class="math inline">\(h_S\)</span> to the particularities of the inspected training data at hand, which might however not be generally relevant features.</li>
</ul>
<p>To illustrate this further, consider the example of the well-known least-square polynomial fitting algorithm from your analysis classes that can be used to approximate a function <span class="math display">\[\begin{align}
  f:\mathbb R\to\mathbb R
\end{align}\]</span> given a set of potentially noisy sample points, i.e., random variables <span class="math display">\[\begin{align}
  S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N} \in (\mathbb R\times\mathbb R)^N,
\end{align}\]</span> by means of a polynomial <span class="math inline">\(h:\mathbb R\to\mathbb R\)</span> of degree <span class="math inline">\(\leq d\in\mathbb N_0\)</span> that minimizes the least square distance to the sample points at the positions <span class="math inline">\(X^{(i)}\)</span>, i.e., in our new jargon, the empirical risk: <span class="math display">\[\begin{align}
  \widehat R_S(h) = \frac{1}{N}\sum_{i=1}^N L(Y^{(i)},h(X^{(i)})), \qquad L(y,y&#39;)=|y-y&#39;|^2.
\end{align}\]</span></p>
<p>In order to decrease the expected bias of the algorithm <span class="math inline">\(\mathcal A\)</span> we may enrich the range of <span class="math inline">\(\mathcal A\)</span>, i.e., <span class="math inline">\(\mathcal F\)</span>, by increasing the degree <span class="math inline">\(d\)</span>. This allows the corresponding hypotheses <span class="math inline">\(h\in\mathcal F\)</span> to adapt more easily to the training data <span class="math inline">\(s\)</span>. A typical plot of two such hypotheses could look as follows:</p>
<figure>
<img src="02/High_polynomial_degree.png" alt="Two realizations of hypotheses for a high polynomial degree and two realizations of training data samples." /><figcaption aria-hidden="true">Two realizations of hypotheses for a high polynomial degree and two realizations of training data samples.</figcaption>
</figure>
<p>Note that in the illustration, due to the small amount of noise, the red and blue realizations of samples points almost do not differ. Thanks to the high degree in the polynomial both correspondingly learned hypotheses manage easily to hit all the sample points and the expected bias is correspondingly small. However, the illustration already hints at the potentially large variance in the hypotheses even though the noise is so small – the polynomials may even flip their asymptotic behavior due to a tiny change in one sample point!</p>
<p>Going to the other extreme by drastically decreasing the degree of the polynomial, one has the opposite effect:</p>
<figure>
<img src="02/Low_polynomial_degree.png" alt="Two realizations of hypotheses for a low polynomial degree and two realizations of training data samples." /><figcaption aria-hidden="true">Two realizations of hypotheses for a low polynomial degree and two realizations of training data samples.</figcaption>
</figure>
<p>Now the expected variance is reduced as we are left with straight lines in the vicinity of the sample points. This obviously comes at the cost of a large expected bias.</p>
<p>The takeaway message is that, if algorithms have a too large space of hypotheses with respect to the complexity of the learning task, there is the danger that they adapt too well to the features found in the inspected training data but generalize poorly to unseen data that might in general not express them. This phenomenon we have already mentioned by the name <em>overfitting</em>. In our framework, we can give it the following formal meaning:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="overfitting">(Overfitting)</h5>
Let <span class="math inline">\(\mathcal A\)</span> be an algorithm, a hypothesis <span class="math inline">\(h\in\text{range}\mathcal A\)</span> is said to be overfitted if: <span class="math display">\[\begin{align}
\exists h&#39;\in\text{range}\mathcal A: \widehat R(h) &lt; \widehat R(h&#39;) \quad \wedge \quad R(h)&gt;R(h&#39;).
  \end{align}\]</span>
</dd>
</dl>
<p>As we have already discussed, the tendency of an algorithm to choose an overly complex hypothesis is neither easy to monitor nor to control as <span class="math inline">\(R\)</span> cannot be computed without the actual distribution of the training data.</p>
<p>A first attempt to monitor the learning behavior of an algorithm is to implement a cross validation which we have briefly discussed in module <a href="SS21-MsAML__02-1__Linear_classification.html">Linear classification and Perceptron definition</a>: Repeatedly divide the available data randomly into a test data set <span class="math inline">\(T\)</span> and training data set <span class="math inline">\(S\)</span> and evaluate the empirical risk of the learned hypothesis <span class="math inline">\(h_S\)</span> on both <span class="math inline">\(S\)</span> and <span class="math inline">\(T\)</span> denoted by <span class="math inline">\(\widehat R_S(h_S)\)</span> and <span class="math inline">\(\widehat R_T(h_S)\)</span>, respectively. Should <span class="math inline">\(\widehat R_S(h_S)\)</span> tend to be much lower than <span class="math inline">\(\widehat R_T(h_S)\)</span>, it is a good indication for overfitting, or in other words that the empirical risk is likely too optimistic with respect to the actual risk. Such monitoring efforts obviously depend on the availability of a large enough the fairness of the picked sample of data with respect to the distribution <span class="math inline">\(P\)</span>.</p>
<p>During the course we will review several techniques that help to study an optimal trade-off between bias and variation. However, before going into that, we first need to understand better how, without knowing the actual distribution of the training data <span class="math inline">\(P\)</span>, we even have a chance to derive inequalities such as the above mentioned generalization bounds <span class="math inline">\(\eqref{eq_generalization_error}\)</span> in terms of the sample size <span class="math inline">\(N\)</span>.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
