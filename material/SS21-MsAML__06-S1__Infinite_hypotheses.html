<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__06-S1__Infinite_hypotheses</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-06-s1-pdf">MsAML-06-S1 [<a href="SS21-MsAML__06-S1__Infinite_hypotheses.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#infinite-hypotheses-case">Infinite hypotheses case</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="infinite-hypotheses-case">Infinite hypotheses case</h2>
<p>Until now we have derived our generalization bounds from the assumption that our set of hypotheses consisted of a finite dictionary only, i.e., <span class="math inline">\(\mathcal H\subset \mathcal Y^{\mathcal X}\)</span> with <span class="math inline">\(|\mathcal H|&lt;\infty\)</span>.</p>
<p>Our generalization bounds that we have used for the given learning guarantees were of the type <span class="math display">\[\begin{align}
  |\widehat R_S(h) - R(H) |
  \leq 
  \text{some function of }
  \left(\log|\mathcal F|,\frac1\epsilon,\frac1\delta\right)
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\delta\)</span>.</p>
<p>First, one may argue that, for algorithms <span class="math inline">\(\mathcal A:s\to h_s\)</span> implemented on a computer, at least the range <span class="math inline">\(\mathcal F\subseteq \mathcal H\)</span> of <span class="math inline">\(\mathcal A\)</span> will naturally be finite due to the discretization and finiteness of the data storage resources. One may even interpret the quantity <span class="math display">\[\begin{align}
  \log_2|\mathcal F|
\end{align}\]</span> as the amount of bits needed to encode one hypothesis, and in turn, <span class="math inline">\(|\mathcal F|\)</span> as the storage space that has to be searched in order to find a good classifier. Thus, one could already tend to be satisfied with our previous assertions about learnability. However, even for the finite case, <span class="math inline">\(\log|\mathcal F|\)</span> will typically be extremely large and for practical purposes our bounds are not very useful quantitatively.</p>
<p>Furthermore, we have already seen an example in the exercises about the learnability of the concept of intervals which involved an infinitely large space of hypotheses and proved that we can do better.</p>
<p>In order to do better, it is important to observe that the complexity of the learning task is often much lower when compared to the bare number of possible hypotheses in <span class="math inline">\(\mathcal H\)</span> or <span class="math inline">\(\mathcal F\)</span>. Consider, for instance, the Adaline, which does not directly pick a classifier <span class="math inline">\(h:\mathcal X\to\mathcal Y\)</span> in <span class="math inline">\(\mathcal H\)</span> but instead picks only a <span class="math inline">\(w\in\mathbb R^d\)</span> and <span class="math inline">\(b\in\mathbb R\)</span>. Even the latter would make up infinite choices but, in principle, only those matter that give rise to some partition of the training data into two sets.</p>
<p>In conclusion, we need to find a better way a measuring the complexity of the learning task than just looking at <span class="math inline">\(|\mathcal F|\)</span>, and in our proofs, we have to replace the uniform sum of the elements of <span class="math inline">\(\mathcal F\)</span>. There are two routes. First, we could introduce a relevant metric structure on <span class="math inline">\(\mathcal H\)</span> and attempt to argue along the lines “If two hypotheses are close in this metric, then also the derived classification is close.” Second, we could employ a combinatorial argument, such as the one about the partitions mentioned above, reducing the infinite set of hypotheses to a finite set of hypotheses classes that reflect better the complexity of the learning task. The combinatorial route, although at times more coarse, has the advantage to be more independent of the learning algorithm itself since the latter may directly influence the choice of a relevant metric. This is the route we will follow next.</p>
<p>In the considered setting, let <span class="math inline">\(\mathcal H\)</span> be the potentially infinite set of hypotheses <span class="math inline">\(\mathcal Y^{\mathcal X}\)</span>, and in order to add even more generality to our analysis let us regard a general loss function <span class="math display">\[\begin{align}
  L:\mathcal Y\times\mathcal Y \to\mathbb R,
\end{align}\]</span> instead of only regarding the choice <span class="math inline">\(L(y,y&#39;)=1_{y\neq y&#39;}\)</span>. Furthermore, we define <span class="math display">\[\begin{align}
  \mathcal G \equiv \mathcal G^L_{\mathcal H}  
\end{align}\]</span> to consist of functions <span class="math display">\[\begin{align}
  g(x,y) := L(y,h(x))
\end{align}\]</span> for each <span class="math inline">\(h\in\mathcal H\)</span>. We will silently assume sufficiently nice properties so that functions of <span class="math inline">\(g\in\mathcal G\)</span> and their respective suprema over <span class="math inline">\(\mathcal G\)</span> are measurable, which in applications can typically be arranged for.</p>
<p>In order to shorten our notation, let us introduce <span class="math display">\[\begin{align}
  \mathcal Z:=\mathcal X\times\mathcal Y,
\end{align}\]</span> as well as, <span class="math inline">\(Z\)</span>, <span class="math inline">\(Z^{(i)}\)</span> for <span class="math inline">\(i=1,\ldots,N\)</span> i.i.d. and <span class="math inline">\(\mathcal Z\)</span>-valued random variables, such that we may denote our training data random variable by <span class="math inline">\(S=(Z^{(i)})_{i=1,\ldots N}\)</span>, and any of its realization by <span class="math inline">\(s=(z^{(i)})_{i=1,\ldots, N}\)</span>.</p>
<p>We start again from the top, e.g., from our discussion of the <a href="SS21-MsAML__02-S1__Error_decomposition.html">error decomposition</a>. The quantity we will attempt to control is <span class="math display">\[\begin{align}
  R(\widehat h_S) - R_{\mathcal F} 
  \leq 2\sup_{h\in\mathcal F}
  \left|
    \widehat R_S(h)-R(h)
  \right|,
\end{align}\]</span> where as before <span class="math inline">\(h_S=\mathcal A(S)\)</span>, <span class="math inline">\(\mathcal F\subseteq \mathcal H\)</span> is a relevant but now potentially infinite set of hypotheses, and <span class="math inline">\(\widehat h_S\)</span> is the minimizer of the empirical risk. Recall that for this to hold we have to assume that our algorithm <span class="math inline">\(\mathcal A\)</span> always succeeds in the task of empirical risk minimization as we did before.</p>
<p>For training data <span class="math inline">\(S\)</span>, <span class="math inline">\(h\in\mathcal F\subseteq \mathcal H\)</span>, and <span class="math display">\[\begin{align}
  g_h(x,y):=L(y,h(x))
\end{align}\]</span> in <span class="math inline">\(\mathcal G^L_{\mathcal F}\)</span> we coarse grain the dependence on a particular <span class="math inline">\(h\)</span> by means of the estimate <span class="math display">\[\begin{align}
    R(h)-\widehat R_S(h)
  &amp;=
    E_Z g_h(Z) - \frac1N\sum_{i=1}^N g_h(Z^{(i)})
  \\
  &amp;\leq
  \sup_{g\in\mathcal G^L_{\mathcal F}} 
    \left[\frac1N\sum_{i=1}^N g(Z^{(i)})-E_Z g(Z)\right]
  =: \Phi(S).
  \label{eq_phi}
  \tag{E1}
\end{align}\]</span> The goal will now be to derive a meaningful estimate fort <span class="math inline">\(\Phi(S)\)</span>.</p>
<p>For this purpose, there is a nice generalization of Hoeffding’s inequality, i.e., the so-called <em>bounded differences</em> inequality:</p>
<dl>
<dt>Lemma</dt>
<dd><h5 id="bounded-differences">(Bounded differences)</h5>
For an <span class="math inline">\(\mathbb R\)</span>-valued function <span class="math inline">\(f\)</span>, let there be constants <span class="math inline">\(c_i\)</span>, <span class="math inline">\(i=1,\ldots,n\)</span>, such that <span class="math display">\[\begin{align}
\sup_{x_1,\ldots,x_n, \tilde x_i}
\left|
  f(x_1,\ldots,x_i,\ldots,x_n)-f(x_1,\ldots, \tilde x_i,\ldots, x_n)
\right|
\leq c_i.
  \end{align}\]</span> Then, the following estimates hold true for all <span class="math inline">\(\epsilon&gt;0\)</span>: <span class="math display">\[\begin{align}
P_{X_1,\ldots, X_n}\left(
  \pm\left[
    f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
  \right] 
  \geq \epsilon
\right)
\\
\leq 
\exp\left(
  -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
\right),
  \end{align}\]</span> and in turn, <span class="math display">\[\begin{align}
P_{X_1,\ldots, X_n}\left(
  \left|
    f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
  \right| \geq \epsilon
\right)
&amp;\leq 
2\exp\left(
  -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
\right).
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> Define <span class="math inline">\(\mathcal F_i:=\sigma(X_1,\ldots, X_i)\)</span>, i.e., the filtration of <span class="math inline">\(\sigma\)</span>-algebras generated by the random variables <span class="math inline">\(X_1,\ldots, X_n\)</span>.</p>
<p>For the sake of compactness of the notation, let us suppress the subscripts in <span class="math inline">\(P_{X_1,\ldots, X_N}\)</span> and <span class="math inline">\(E_{X_1,\ldots, X_N}\)</span> in our notation.</p>
<p>We form the marginal differences <span class="math display">\[\begin{align}
  \Delta_i := 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_i\right) 
    - 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right)
\end{align}\]</span> which implies <span class="math display">\[\begin{align}
  &amp;E(\Delta_i\,|\, \mathcal F_{i-1})
  \\
  &amp;=
  E\left(
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_i\right) 
    - 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right)
  \,\Big|\, \mathcal F_{i-1}\right)
  \\
  &amp;=
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right) 
    - 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right)
    \\
  &amp;=
  0,
\end{align}\]</span> where we have use the linearity of the conditional expectation and the fact that expectation with respect to <span class="math inline">\(\mathcal F_{i-1}\subset\mathcal F_i\)</span> wins over the one with repsect to <span class="math inline">\(\mathcal F_i\)</span>, i.e., <span class="math inline">\(E(E(\cdot\,|\,\mathcal F_{i-1})\,|\,\mathcal F_i)=E(\cdot\,|\,\mathcal F_{i-1})=E(E(\cdot\,|\,\mathcal F_i)\,|\,\mathcal F_{i-1})\)</span>.</p>
<p>For <span class="math inline">\(\epsilon&gt;0\)</span> we find <span class="math display">\[\begin{align}
    P\left(
      \left|
        f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
      \right|
      \geq \epsilon
    \right)
    =
    P\left(\left|\sum_{i=1}^N\Delta_i\right|\geq \epsilon\right).
    \label{eq_2}
    \tag{E2}
\end{align}\]</span></p>
<p>Next, we observe that <span class="math display">\[\begin{align}
  \Delta_i 
  &amp;\leq
  E\left(
    \sup_{x_i}
    f(X_1,\ldots,x_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_i
  \right)
  \\
  &amp;\qquad\qquad -
  E\left(
    f(X_1,\ldots,X_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_{i-1}
  \right)
  \\
  &amp;=
  E\left(
    \sup_{x_i}
    f(X_1,\ldots,x_i,\ldots,X_n)
    -
    f(X_1,\ldots,X_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_{i-1}
  \right)
  \\
  &amp;=:B_i,
\end{align}\]</span> and similarly, <span class="math display">\[\begin{align}
  \Delta_i 
  &amp;\geq 
  E\left(
    \inf_{x_i}
    f(X_1,\ldots,x_i,\ldots,X_n)
    -
    f(X_1,\ldots,X_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_{i-1}
  \right)
  \\
  &amp;=:A_i.
\end{align}\]</span></p>
<p>Note that <span class="math inline">\(A_i, B_i\)</span> are <span class="math inline">\(\mathcal F_{i-1}\)</span> measurable random variables but we can readily bound their differences using our assumption on <span class="math inline">\(f\)</span> by means of <span class="math display">\[\begin{align}
  \left\|
    B_i-A_i
  \right\|_\infty
  \leq c_i.
\end{align}\]</span></p>
<p>Coming back to the right-hand side of <span class="math inline">\(\eqref{eq_2}\)</span>, we follow similar arguments as in our proof of Hoeffding’s inequality: <span class="math display">\[\begin{align}
  P\left( \sum_{i=1}^N\Delta_i \geq \epsilon\right)
  &amp;\leq
  e^{-t \epsilon} E e^{t\sum_{i=1}^N\Delta_i}
  \\
  &amp;=
  e^{-t \epsilon} E E\left(
    e^{t\sum_{i=1}^N\Delta_i}
    \,\big|\,
    \mathcal F_{N-1}
  \right)
  \\
  &amp;=
  e^{-t \epsilon} E e^{t\sum_{i=1}^{N-1}\Delta_i} E\left(
    e^{t \Delta_N}
    \,\big|\,
    \mathcal F_{N-1}
  \right)
\end{align}\]</span> since all <span class="math inline">\(\Delta_1,\ldots, \Delta_{N-1}\)</span> are <span class="math inline">\(\mathcal F_{N-1}\)</span> measurable.</p>
<p>We may now apply the bound <span class="math display">\[\begin{align}
  E e^{tZ} \leq \exp\left(\frac{(b-a)^2}{8}t^2\right)
  \qquad
  \text{for}
  \qquad a \leq Z\leq b,
\end{align}\]</span> which was derived as an intermediate result in our proof of Hoeffding’s inequality – therefore, often referred to as <em>Hoeffding’s lemma</em>. Observe that conditioning on <span class="math inline">\(\mathcal F_{n-1}\)</span> does not impact the strategy of proof since <span class="math inline">\(E(\Delta_i\,|\,\mathcal F_{n-1})=0\)</span>, under conditioning of <span class="math inline">\(\mathcal F_{n-1}\)</span> the random variables <span class="math inline">\(X_1,\ldots, X_{n-1}\)</span> can be regarded as fixed quantities, and we may choose <span class="math inline">\(a=A_n, b=B_n\)</span>.</p>
<p>This implies <span class="math display">\[\begin{align}
  P\left(\sum_{i=1}^N\Delta_i\geq \epsilon\right)
  &amp;\leq
  e^{-t\epsilon} E e^{t\sum_{i=1}^{n-1}\Delta_i} e^{\frac{(B_n-A_n)^2}{8}t^2}
\end{align}\]</span> and by induction <span class="math display">\[\begin{align}
  P\left(\sum_{i=1}^N\Delta_i\geq \epsilon\right)
  &amp;\leq
  e^{-t\epsilon} \exp\left(
    \sum_{i=1}^n \frac{\|B_i-A_i\|^2_\infty}{8} t^2
  \right)
  \\
  &amp;\leq
  e^{-t\epsilon} \exp\left(
    \sum_{i=1}^n \frac{c_i^2}{8} t^2
  \right)
\end{align}\]</span> The latter bound can again be sharpened by optimizing over <span class="math inline">\(t\)</span> so that we find <span class="math display">\[\begin{align}
  P\left(\sum_{i=1}^N\Delta_i\geq \epsilon\right)
  &amp;\leq
  \exp\left(
    -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
  \right).
\end{align}\]</span> Note that the same bound can be proven for <span class="math inline">\(P\left(-\sum_{i=1}^N\Delta_i\geq \epsilon\right)\)</span> so that we can exploiting the union bound to conclude <span class="math display">\[\begin{align}
  \eqref{eq_2}
  &amp;\leq
  2
  \exp\left(
    -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
  \right)
\end{align}\]</span> or equivalently <span class="math display">\[\begin{align}
    P_{X_1,\ldots, X_n}\left(
      \left|
        f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
      \right| \geq \epsilon
    \right)
  &amp;\leq
  2
  \exp\left(
    -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
  \right).
\end{align}\]</span></p>
<p><span class="math inline">\(\square\)</span></p>
<dl>
<dt>Remark</dt>
<dd>This slightly more general estimate, exploiting the marginal differences, is often referred as <em>Azuma-Hoeffding</em> inequality. The special case for uniformly bounded differences, as we have used above, is called <em>McDiarmid</em> inequality.
</dd>
</dl>
<p>After this excursion, we can return to our original expression of interest <span class="math inline">\(\Phi(S)\)</span> in <span class="math inline">\(\eqref{eq_phi}\)</span>. Let <span class="math inline">\(\tilde S=(\tilde Z^{(i)})_{i=1,\ldots,N}\)</span> be another random variable sampling the training data like <span class="math inline">\(S=(Z^{(i)})_{i=1,\ldots,N}\)</span> such that only the component <span class="math inline">\(\tilde Z^{(N)}\)</span> differs from <span class="math inline">\(Z^{(N)}\)</span> and the other components are equal, i.e., <span class="math inline">\(Z^{(i)}=\tilde Z^{(i)}\)</span> for <span class="math inline">\(i=1,\ldots,N\)</span>.</p>
<p>Regarding the following difference, we find <span class="math display">\[\begin{align}
\left|
  \Phi(S)-\Phi(\tilde S)
\right|
&amp;\leq
\sup_{g\in\mathcal G}
\frac{\left|
  g(Z^{(N)})
  -
  g(\tilde Z^{(N)})
\right|}{N}
\\
&amp;\leq
\sup_{g\in\mathcal G}
\sup_{z,\tilde z}
\frac{\left|
  g(z)
  -
  g(\tilde z)
\right|}{N}
=:\frac C N
\end{align}\]</span> as long as <span class="math inline">\(g\)</span> is bounded. This allows to apply McDiarmid’s inequality which states <span class="math display">\[\begin{align}
  P\left(
      \Phi(S)-E_S\Phi(S)
    \geq \epsilon
  \right)
  \leq
  \exp\left(
    -\frac{2\epsilon^2 N}{C^2}
  \right).
\end{align}\]</span></p>
<p>For <span class="math display">\[\begin{align}
  \exp\left(
    -\frac{2\epsilon^2 N}{C^2}
  \right)
  =
  \delta
\end{align}\]</span> we get <span class="math display">\[\begin{align}
  \epsilon= C\sqrt{
    \frac{\log\frac 1\delta}{2N}
  }
\end{align}\]</span> and we can recast our first intermediate result into the form <span class="math display">\[\begin{align}
  R(h)
  - \widehat R_S(h) 
  \leq
  \Phi(S)
  \leq E_S\Phi(S) + C\sqrt{
    \frac{\log\frac 1\delta}{2N}
  }
  \label{eq_intermediate}
  \tag{E3}
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\delta\)</span>.</p>
<p>The right-hand side is still implicit due to the implicit expectation <span class="math inline">\(E_S \Phi(S)\)</span>. To gain more explicit control on the right-hand side we apply a “symmetrization trick” often used in statistics or probability theory. For this purpose, let us introduce another random variable <span class="math inline">\(\tilde S\)</span> sampling potential training data in the same way as does <span class="math inline">\(S\)</span> and compute <span class="math display">\[\begin{align}
  E_S\Phi(S)
  &amp;=
  E_S\sup_{g\in\mathcal G}
  \left[
    E_Z g(Z)
    - \frac1N\sum_{i=1}^N g(Z^{(i)})
  \right]
  \\
  &amp;=
  E_S\sup_{g\in\mathcal G}
  \left[
    E_{\tilde S} \frac1N\sum_{i=1}^N g(\tilde Z^{(i)})
    - \frac1N\sum_{i=1}^N g(Z^{(i)})
  \right]
  \\
  &amp;=
  E_S\sup_{g\in\mathcal G}
  E_{\tilde S}
  \left(
    \frac1N\sum_{i=1}^N g(\tilde Z^{(i)})
    - \frac1N\sum_{i=1}^N g(Z^{(i)})
    \,\Bigg|\,
    S
    \right).
  \label{eq_4}
  \tag{E4}
\end{align}\]</span> Using the sub-additivity of the supremum gives <span class="math display">\[\begin{align}
  \eqref{eq_4}
  \leq
  E_{S,\tilde S} 
  \sup_{g\in\mathcal G}
  \frac1N 
    \sum_{i=1}^N\left[
      g(\tilde Z^{(i)})
      -
      g(Z^{(i)})
    \right].
  \label{eq_5}
  \tag{E5}
\end{align}\]</span> Thanks to the symmetry in interchanging <span class="math inline">\(S\)</span> and <span class="math inline">\(\tilde S\)</span>, which only changes the sign of the differences under the sum, we may simply also introduce another random variable <span class="math inline">\(\sigma=(\sigma_1,\ldots, \sigma_N)\)</span>, each <span class="math inline">\(\sigma_i\)</span> taking values in <span class="math inline">\(\{-1,+1\}\)</span> with distribution <span class="math display">\[\begin{align}
  P(\sigma_i=-1) = \frac 12 = P(\sigma_i=+1),
\end{align}\]</span> and recast the last right-hand side as <span class="math display">\[\begin{align}
  \eqref{eq_5}
  =
  E_{S,\tilde S,\sigma} 
  \sup_{g\in\mathcal G}
  \frac1N 
    \sum_{i=1}^N\sigma_i\left[
      g(\tilde Z^{(i)})
      -
      g(Z^{(i)})
  \right].
  \label{eq_6}
  \tag{E6}
\end{align}\]</span> The sub-additivity of the supremum and the fact that <span class="math inline">\(\sigma_i\)</span> has the same distribution as <span class="math inline">\(-\sigma_i\)</span> imply <span class="math display">\[\begin{align}
  \eqref{eq_6}
  \leq
  2E_S E_\sigma\sup_{g\in\mathcal G} \frac 1N
    \sum_{i=1}^N \sigma_i g(Z^{(i)}).
  \label{eq_7}
  \tag{E7}
\end{align}\]</span></p>
<p>The last expression under the expectation on the right-hand side <span class="math display">\[\begin{align}
  \widehat{\mathcal R}_S(\mathcal G_{\mathcal F}^L)
  :=
  E_\sigma\sup_{g\in\mathcal G^L_{\mathcal F}} \frac 1N 
    \sum_{i=1}^N \sigma_i g(Z^{(i)})
\end{align}\]</span> captures the “richness” of the hypotheses contained in <span class="math inline">\(\mathcal F\)</span>, which was used to generate the set <span class="math inline">\(\mathcal G^L_{\mathcal F}\)</span>, by measuring how well a hypothesis can “fit” the random noise: Writing <span class="math inline">\(g(S)\)</span> as a vector <span class="math inline">\(g(S)=(g(Z^{(1)}),\ldots, g(Z^{(N)}))\)</span> we may recast the latter by means of the scalar product <span class="math inline">\(\langle\cdot,\cdot\rangle\)</span> in <span class="math inline">\(\mathbb R^N\)</span> as follows <span class="math display">\[\begin{align}
  \widehat{\mathcal R}_S(\mathcal G_{\mathcal F}^L)
  :=
  E_\sigma\sup_{g\in\mathcal G^L_{\mathcal F}} \frac 1N \left\langle
    \sigma, g(S)
    \right\rangle.
\end{align}\]</span> The inner product encodes the correlation of the vector <span class="math inline">\(g(S)\)</span> to the noise <span class="math inline">\(\sigma\)</span>. Hence, given training data <span class="math inline">\(S\)</span>, the quantity <span class="math inline">\(\widehat{\mathcal R}_S(\mathcal G^L_{\mathcal F})\)</span> measures, in the empirical average, how well the functions in <span class="math inline">\(g\in\mathcal G^L_{\mathcal F}\)</span> correlate to random noise. The richer the class of functions in <span class="math inline">\(\mathcal F\)</span>, the richer the functions in <span class="math inline">\(\mathcal G^L_{\mathcal F}\)</span>, the better the noise can be fitted, and in turn, the larger the quantity <span class="math inline">\(\widehat{\mathcal R}_S(\mathcal G^L_{\mathcal F})\)</span>.</p>
<p>This quantity deserves a name:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="rademacher-complexity">(Rademacher complexity)</h5>
Let <span class="math inline">\(\mathcal G\)</span> be a family of bounded maps <span class="math inline">\(\mathcal X\times \mathcal  Y\to\mathbb R\)</span> and let the training data be of size <span class="math inline">\(N\)</span>. Then, we define the <em>Rademacher complexity</em> as <span class="math display">\[\begin{align}
\mathcal R_N(\mathcal G) = E_S\widehat{\mathcal R}_S(\mathcal G),
  \end{align}\]</span> where as <span class="math inline">\(\widehat{\mathcal R}_S(\mathcal G)\)</span> is called the empirical Rademacher complexity.
</dd>
</dl>
<p>Replacing <span class="math inline">\(\delta \mapsto \frac\delta 2\)</span> in <span class="math inline">\(\eqref{eq_intermediate}\)</span>, the bound <span class="math inline">\(\eqref{eq_7}\)</span> on <span class="math inline">\(E_S\Phi(S)\)</span> implies <span class="math display">\[\begin{align}
  R(h)
  \leq
  \widehat R_S(h)
  +2\mathcal R_N(\mathcal G^L_{\mathcal F})
  + C \sqrt{\frac{\log\frac2\delta}{2N}}
  \label{eq_8}
  \tag{E8}
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\frac\delta2\)</span>.</p>
<p>This bound can be made even more explicit employing McDiarmid’s inequality once again to find <span class="math display">\[\begin{align}
  P\left(
    \left|
      \widehat{\mathcal R}_N(\mathcal G)
      -
      \mathcal R_N(\mathcal G)
    \right|
    \geq C\sqrt{\frac{\log\frac2\delta}{2N}}
  \right)
  \leq
  \frac\delta2,
\end{align}\]</span> since the difference <span class="math display">\[\begin{align}
    \left|
      \widehat{\mathcal R}_S(\mathcal G)
      -
      \widehat{\mathcal R}_{\tilde S}(\mathcal G)
    \right|
    \leq \frac C N
\end{align}\]</span> is bounded in the case that <span class="math inline">\(S\)</span> and <span class="math inline">\(\tilde S\)</span> differ only in one component.</p>
<p>Combining the latter estimate with <span class="math inline">\(\eqref{eq_8}\)</span> and observing the union bound finally results in <span class="math display">\[\begin{align}
  R(h)
  \leq
  \widehat R_S(h)
  +2\widehat{\mathcal R}_S(\mathcal G^L_{\mathcal F})
  + 3C \sqrt{\frac{\log\frac2\delta}{2N}}
\end{align}\]</span> with probability of at least <span class="math inline">\(1-\delta\)</span>.</p>
<p>As the expectation was replaced by an empirical average, this bound is more accessible in applications. Note that the computation of <span class="math inline">\(\widehat R_S(\mathcal G)\)</span> can be rephrased in terms of a risk minimization problem of its own because <span class="math display">\[\begin{align}
  \widehat{\mathcal R}_S(\mathcal G)
  =
  -E_\sigma
  \inf_{g\in\mathcal G}
  \frac1N
    \sum_{i=1}^N \sigma_i g(Z^{(i)}),
\end{align}\]</span> which can be computed for every realization of <span class="math inline">\(\sigma\)</span> in <span class="math inline">\(\{-1,+1\}^N\)</span> and then averaged.</p>
<p>In practice, also this computational task can be quite expensive. However, in next module and in the special case of binary classification, we will be able to derive from it an even simpler bound that will allow us to characterize learnability of binary classification tasks in the general setting of infinite hypotheses sets.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
