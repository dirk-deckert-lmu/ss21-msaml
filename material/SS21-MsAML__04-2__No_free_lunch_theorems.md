# MsAML-04-2 [[PDF](SS21-MsAML__04-2__No_free_lunch_theorems.pdf)]

1. [No free lunch theorems](#no-free-lunch-theorems)

Back to [index](index.md).


## No free lunch theorems 

As discussed, the generalization performance of our models will depend on a
priori knowledge. With the following theorem, we will get a first impression to
what an extend this is the case. 

Let us consider the following statistical learning setting:

* finite feature and label spaces $\mathcal X,\mathcal Y$;
* given training data $s\in(\mathcal X\times\mathcal Y)^N$, our favorite
  algorithm $\mathcal A$ always succeeds to find a hypothesis $h_s=\mathcal
  A(s)$;
* we regard deterministic learning tasks only, for which there exists a
  so-called *concept* $c:\mathcal X\to\mathcal Y$ that, for any feature vector
  $x\in\mathcal X$, deterministically assigns the corresponding actual label
  $y=c(x)\in\mathcal Y$;
* as error probability (or generalization error) of a learned hypothesis $h_s$
  we regard a random variable of features $X$ uniformly distributed over
  $\mathcal X$ and define
  \begin{align}
    P_X(h_s(X)\neq c(X));
  \end{align}
* we represent the training data $S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}$ at hand
  by random feature tuples $X^N\equiv (X^{(1)},\ldots,X^{(N)})$ uniformly
  distributed over $\mathcal X^N$ for given $N\leq |\mathcal X$ and
  corresponding labels $Y^{(i)}=c(X^{(i)})$ determined by the underlying
  concept of the learning task;
* finally, as we want to regard not only one learning task but an average over
  all, we introduce another random variable $C$ which is uniformly distributed
  over the finite space of possible concepts ${\mathcal Y}^{\mathcal X}$.


Theorem
: ##### (No free lunch theorem)
  For any such algorithm in this setting, the bounds
  \begin{align}
    &\frac{N}{|\mathcal X|\,|\mathcal Y|}+\left(1-\frac{1}{|\mathcal Y|}\right)
    \\
    &\qquad\qquad
    \geq 
    E_C E_{X^N} P_X\left( h_S(X)\neq c(X)\right)
    \\
    &\qquad\qquad \qquad \geq 
    \left(1-\frac{N}{|\mathcal X|}\right)
    \left(1-\frac{1}{|\mathcal Y|}\right)
  \end{align}
  hold true.

Before we turn to the proof, let us review the meaning of the term sandwiched
between these upper and lower bounds. It is the average $E_C$ with respect to
uniformly chosen concepts $C$ in ${\mathcal Y}^{\mathcal X}$ of the average
$E_{X^N}$ with respect to uniformly chosen training data features $X^N$ in
$\mathcal X^N$ of the error probability for the learned hypothesis $h_S$ and
underlying concept $C$. 

This quantity measures how well our favorite algorithm
\begin{align}
  \mathcal A:(\mathcal X,\mathcal Y)^N &\to{\mathcal Y}^{\mathcal X}, \\
  s &\mapsto \mathcal A(s)\equiv h_s
\end{align}
generalizes on average with respect to all possible deterministic learning
tasks in finding the this underlying concept $C$ under fairly chosen training
data.

**Proof:** We begin by defining the set
\begin{align}
  \mathcal X_{X^N}:=\left\{ X^{(i)} \,|\, i=1,\ldots, N\right\}\subset \mathcal X,
\end{align}
i.e., the subset of feature vectors appearing in the training data.  In case,
we find only $|\mathcal X_{X^N}|<N$ many, we add further elements of $\mathcal
X$ until $|\mathcal X_{X^N}|=N$.

Let us write the innermost probability as an expectation:
\begin{align}
  &E_C E_{X^N} P_X\left(h_S(X)\neq C(X)\right)\\
  &=
  E_C E_{X^N} \frac{1}{|\mathcal X|}\sum_{x\in\mathcal X} 1_{h_S(x)\neq C(x)}
  \\
  &=: \fbox{1}.
\end{align}
As the characteristic function of the right-hand side is non-negative, this
quantity can be estimated from below by replacing $\mathcal X$ in the summation
by the complement of the subset $\mathcal X_{X^N}$:
\begin{align}
  \fbox{1} \geq 
  &\frac{1}{|\mathcal X|} E_C E_{X^N} \sum_{x\notin\mathcal X_S} 1_{h_S(x)\neq C(x)} 
  \\
  &=: \fbox{2}.
\end{align}
Note that, while we can move the constant $\frac{1}{|\mathcal X|}$ to the front
by linearity of the expectation, the domain of the summation itself depends on
the random training data features $X^N$.

However, we observe that, while for each $x\in\mathcal X_{X^N}$, the training
data $S$, being of function of $X^N$, and the value of $C(x)$ are generally 
dependent, for $x\notin \mathcal X_{X^N}$ all values of $C(x)$ in $\mathcal Y$
are independent of $S$, and thus, equally likely. In turn, for concepts $C$ that
do not change the training data $S$ and therewidth $h_S$, the event $h_S(x)\neq
C(x)$ for $x\notin \mathcal X_S$ amounts to picking one value out of $|\mathcal
Y|$ many, an event of probability of $\left(1-\frac{1}{|\mathcal Y|}\right)$.
The remaining sum has $(|\mathcal X|-N)$ summands so that we get:
\begin{align}
  \fbox{2} =
  &\frac{1}{|\mathcal X|} \left(|\mathcal X|-N\right) \left(1-\frac{1}{|\mathcal Y|}\right)
  \tag{T2}
  \label{eq_term2}
  \\
  &= \left(1-\frac{N}{|\mathcal X|}\right)\left(1-\frac{1}{|\mathcal Y|}\right),
\end{align}
which proves the lower bound.

The upper bound can be seen as follows:
\begin{align}
  &E_C E_{X^N} P_X\left(h_S(X)\neq C(X)\right)
  \\
  &=E_C  E_{X^N} \frac{1}{|\mathcal X|}\left(
    \sum_{x\notin\mathcal X_{X^N}}1_{h_S(X)\neq C(X)}
    + \sum_{x\in\mathcal X_{X^N}}1_{h_S(X)\neq C(X)}
  \right)
  \\
  &\leq E_C E_{X^N} \frac{1}{|\mathcal X|}\left(
    \sum_{x\notin\mathcal X_{X^N}}1_{h_S(X)\neq C(X)}
    + N
  \right)
  \\
  & =: \fbox{3},
\end{align}
where we can again apply equality $\eqref{eq_term2}$ to find
\begin{align}
  \fbox{3} = \left(1-\frac{1}{|\mathcal Y|}\right) + \frac{N}{|\mathcal
  X|\,|\mathcal Y|}.
\end{align}
This proves the upper bound and concludes the proof.

$\square$

What does this theorem entail? The factor $\left(1-\frac{1}{|\mathcal
Y|}\right)$ is the error probability of randomly guessing a label out of
$|\mathcal Y|$ possibilities with uniform distribution. Now the lower bound of
the theorem entails that, provided $N$ training data samples were inspected by
the algorithm during training, on average, there is only a factor
$\left(1-\frac{N}{|\mathcal X|}\right)$ left for room of improvement in the
error probability as compared to randomly guessing. This room of improvement is
entirely due to the possibility of simply recording the inspected training data
during training.

In applications, while the amount of class labels $|\mathcal Y|$ is usually
small, e.g., as low as two for binary classification, $|\mathcal X|$ is
typically very large, even, when compared to the amount of training data
samples $N$, so that the fraction $\frac{N}{|\mathcal X|}$ is very small.
Observing the upper and lower bounds in the limit of $\frac{N}{|\mathcal X|}\to
0$, we find that any algorithm will on average only perform a little bit better
than randomly guessing a class label independently of the presented feature
vector.

In other words, although our favorite algorithm may do very well on a specific
learning tasks, i.e., in the above context of learning a concept $c\in{\mathcal
Y}^{\mathcal X}$, but it is bound to do very poorly on another one. 

There are much more sophisticated version of such no free lunch theorems, in
particular, weakening the uniformity assumption in the distribution of concepts
as not all concepts may be equally important but the entailed spirit is the same.

On average, no supervised learner is better than another as regards unseen test
data. The only way to boost the generalization performance is to provide a
priori knowledge for the learning task, as for example the regularity
assumption in finding a continuation of sequence $2, 4, 8, 16, 32, \ldots$ or
the restriction to separating hyperplanes for linearly separable training data,
etc. In the rest of this course, we will discuss several ways to incorporate a
priori knowledge into our model definitions. In order to do so, there are two
main routes, one in putting a priori knowledge into the distribution of
training data $P$, e.g., with the help of a parametric model of $P$, and one in
putting restricting of the range of hypothesis selected by the resulting
algorithm $\mathcal A$. This latter approach is to some extend independent of
the unknown distribution $p$ of the training data and this will be the main one
studied in this course.
