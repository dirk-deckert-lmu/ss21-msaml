<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__04-S1__PAC_learning_framework</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-04-s1-pdf">MsAML-04-S1 [<a href="SS21-MsAML__04-S1__PAC_learning_framework.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#pac-learning-framework">PAC learning framework</a>
<ul>
<li><a href="#consistent-case">Consistent case</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="pac-learning-framework">PAC learning framework</h2>
<p>So far we have discussed indicators to measure the learning performance of supervised learners. The next natural questions may be:</p>
<ul>
<li>What can be learned “efficiently”?</li>
<li>How many training data samples are needed to achieve a certain performance?</li>
</ul>
<p>The first question calls for a definition of was is learnable, and in particular, <em>efficiently</em> so. The second question goes in the direction of our the <a href="SS21-MsAML__03-X1__Perceptron_convergence.html">simple learning setting</a> discussed in the last module.</p>
<p>As discussed in the previous modules, the main difficulty of supervised learning is that all we have are our disposal is the realization of a training data sample <span class="math inline">\(s\)</span> in order to pick a hypothesis <span class="math inline">\(h_s\)</span>. As we have little control over which training sample <span class="math inline">\(s\)</span> we are given in our application, we model the given training data as chosen at random, i.e., by random variabel <span class="math inline">\(S\)</span>, with an unknown distribution <span class="math inline">\(P\)</span>.</p>
<p>We can always try to find hypotheses <span class="math inline">\(h_s\)</span> that perfectly predicts the class labels of the training data feature vectors of <span class="math inline">\(s\)</span> by simply recording them in a look-up table. However, we would like to study the performance, not only for one particular realization <span class="math inline">\(s\)</span> but for fairly chosen training data <span class="math inline">\(S\)</span>, and at the same time, we would like to also measure the performance on previously unseen samples. Hence, a meaningful performance estimate is naturally probabilistic. The so-called <em>p</em>robably-<em>a</em>pproximately-<em>c</em>orrect or PAC framework, we will introduce next, tries to capture this nature of statistical learning.</p>
<p>In this modules, let us regard the deterministic case, for which there is a function <span class="math display">\[\begin{align}
  c\in{\mathcal Y}^{\mathcal= X},
\end{align}\]</span> called <em>concept</em>, which assigns any feature vector <span class="math inline">\(x\in\mathcal X\)</span> its actual label <span class="math inline">\(y=c(x)\in\mathcal Y\)</span>. The training data sample then takes the form <span class="math display">\[\begin{align}
  s=(x^{(i)},y^{(i)}\equiv c(x^{(i)}))_{i=1,\ldots,N}.
\end{align}\]</span> We will always consider the case of i.i.d. feature vectors <span class="math inline">\(X^{(i)}\)</span> with corresponding distribution <span class="math inline">\(D\)</span> on <span class="math inline">\(\mathcal X\)</span> and write <span class="math inline">\(X^N=(X^{(1)},\ldots,X^{(N)})\sim D^N\)</span>. In this case, the joint distribution <span class="math inline">\(P\)</span> of feature vector and class label tuples on <span class="math inline">\((\mathcal X\times\mathcal Y)\)</span> is given by a product of <span class="math inline">\(D\)</span> with the Dirac-delta measure <span class="math inline">\(\delta_{y=c(x)}\)</span>.</p>
<p>We call a set of such concepts a <em>concept class</em> <span class="math inline">\(\mathcal C\)</span>.</p>
<dl>
<dt>Example:</dt>
<dd>In our usual setting of binary classification, <span class="math inline">\(|\mathcal Y|=2\)</span>, we could for example choose class labels <span class="math inline">\(\{0,1\}\)</span> and for any set <span class="math inline">\(M\subseteq X\)</span>, the characteristic function <span class="math inline">\(1_M\)</span> would represent a concept. For instance, for <span class="math inline">\(\mathcal X=\mathbb R^2\)</span>, the concept class <span class="math inline">\(\mathcal C\)</span> of indicator functions attaining value one on rectangular shaped subsets of <span class="math inline">\(\mathcal X\)</span> and zero on there complements is results in the concept class of “rectangles on <span class="math inline">\(\mathbb R^2\)</span>”. This concept class would be convenient to study the performance of a supervised learner that should predict a rectangle, i.e., represented by the corresponding indicator function, from a given set of training data points <span class="math inline">\((x^{(i)},y^{(i)})_{i=1,\ldots,N}\in(\mathbb R^2\times\{0,1\})^N\)</span>.
</dd>
</dl>
<p>With these notions, the central definition of PAC-learning can be given by:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="pac-learnable-in-the-deterministic-case">(PAC-learnable in the deterministic case)</h5>
A concept class <span class="math inline">\(\mathcal C\)</span> is said to be PAC-learnable, if there exists an algorithm <span class="math inline">\(\mathcal A\)</span> and a polynomial <span class="math inline">\(p\)</span> such that <span class="math display">\[\begin{align}
\forall \quad &amp;\epsilon, \delta\in (0,1]\\
\forall \quad &amp;\text{distributions $D$ on $\mathcal X$}\\
\forall \quad &amp;\text{concepts $c$ in $\mathcal C$}\\
\forall \quad &amp;N\geq p\left(\frac1\epsilon,\frac1\delta\right):\\
&amp;\\
&amp; P_{S\sim P^N}(R(h_S)\leq \epsilon) \geq 1-\delta.
  \end{align}\]</span> Furthermore, if <span class="math inline">\(\mathcal A\)</span> runs in <span class="math inline">\(p(\frac1\epsilon,\frac1\delta)\)</span> polynomial time, then <span class="math inline">\(\mathcal C\)</span> is called <em>efficiently</em> PAC-learnable.
</dd>
</dl>
<p>Note that, as introduced earlier, the distribution <span class="math inline">\(P\)</span> is given by the product measure of <span class="math inline">\(D\)</span> the Dirac-delta measure <span class="math inline">\(\delta_{y=c(x)}\)</span>. Therefore, <span class="math inline">\(P_{S\sim P^N}\)</span> can be read as <span class="math inline">\(P_{X^N\sim D^N}\)</span> in this context.</p>
<p>In other words, <span class="math inline">\(\mathcal C\)</span> is PAC-learnable if there exists an algorithm and polynomial <span class="math inline">\(p\)</span> that, independent of the distribution <span class="math inline">\(D\)</span> and only after inspection of the order of <span class="math inline">\(p(\frac1\epsilon,\frac 1\delta)\)</span> many training data samples in <span class="math inline">\(S\)</span>, returns a hypothesis <span class="math inline">\(h_S\)</span> that is with</p>
<ul>
<li><span class="math inline">\(\geq 1-\delta\)</span> high <em>p</em>robability</li>
<li><span class="math inline">\(\epsilon\)</span>-<em>a</em>pproximately</li>
<li><em>c</em>orrect.</li>
</ul>
<p>One usually calls <span class="math inline">\(1-\delta\)</span> the confidence and <span class="math inline">\(1\epsilon\)</span> the accuracy.</p>
<h3 id="consistent-case">Consistent case</h3>
<p>Suppose now <span class="math inline">\(\mathcal A\)</span> is an algorithm with finite range <span class="math inline">\(\mathcal F\)</span> such that, for all <span class="math inline">\(c\in\mathcal C\)</span> and i.i.d. training samples <span class="math display">\[\begin{align}
   s=(x^{(i)},c(x^{(i)}))_{i=1,\ldots,N},
\end{align}\]</span> returns a hypothesis <span class="math inline">\(h_s\)</span> which fulfills <span class="math display">\[\begin{align}
  \widehat R_s(h_s) = 0,
\end{align}\]</span> which is often called a <em>consistent</em> hypothesis. This assumption readily puts us in the scenario of the previously studied <a href="SS21-MsAML__03-S1__A_simple_learning_setting.html">simple learning setting</a>, the result of which can now we summarized as follows:</p>
<dl>
<dt>Corollary</dt>
<dd><h5 id="consistent-and-deterministic-case-for-finite-hypothesis-spaces">(Consistent and deterministic case for finite hypothesis spaces)</h5>
Let <span class="math inline">\(\mathcal A\)</span> be an algorithm with finite range <span class="math inline">\(\mathcal F\)</span> that, for any concept <span class="math inline">\(c\in\mathcal F\)</span> and training data sample <span class="math inline">\(s\)</span> returns a consistent hypothesis <span class="math inline">\(h_s\)</span>, then any concept class <span class="math inline">\(\mathcal C\subseteq\mathcal F\)</span> is PAC-learnable.
</dd>
</dl>
<p><strong>Proof:</strong> Observing that the consistency condition, i.e., <span class="math display">\[\begin{align}
  \forall s\in(\mathcal X\times\mathcal Y)^N: \widehat R_s(h_s)=0
\end{align}\]</span> guarantees the assumptions used in our <a href="SS21-MsAML__03-S1__A_simple_learning_setting.html">simple learning setting</a>. Our case considered here is even simpler as the we have a deterministic concept, though the estimates of our previous computations are not effected. The proven bound of the last module implies for any <span class="math inline">\(\mathcal C\subseteq \mathcal F\)</span>: <span class="math display">\[\begin{align}
  \forall \quad &amp;\epsilon, \delta\in (0,1]\\
  \forall \quad &amp;\text{distributions $D$ on $\mathcal X$}\\
  \forall \quad &amp;\text{concepts $c$ in $\mathcal C$}\\
  \forall \quad &amp;N\geq \frac1\epsilon\log\frac{|\mathcal F|}{\delta}:\\
  &amp;\\
  &amp; P_{S\sim P^N}(R(h_S)\leq \epsilon) \geq 1-\delta.
\end{align}\]</span> Noting that <span class="math inline">\(N\geq \frac1\epsilon\log\frac{|\mathcal F|}{\delta}\)</span> is implied by <span class="math display">\[\begin{align}
  \frac1\epsilon\log\frac{|\mathcal F|}{\delta}
  &amp;=\frac1\epsilon\left( \log\frac1\delta +\log|\mathcal F|  \right)\\
  &amp;\leq \frac1\epsilon \left( \frac1\delta + \log|\mathcal F|\right)\\
  &amp;\leq N,
\end{align}\]</span> which is a polynomial <span class="math inline">\(p(\frac1\epsilon,\frac1\delta)\)</span>, we are in the setting of <a href="#pac-learnable">Definition (PAC-learnable)</a>, which concludes the proof.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>In other words, if in addition to consistency, the set of relevant hypotheses is finite, the required sample size is dominated by a polynomially increasing function in terms of <span class="math inline">\(\frac1\epsilon\)</span> and <span class="math inline">\(\frac1\delta\)</span>.</p>
<p>In applications, of course, several other factors influence the total amount of computation steps the algorithm has to carry out, such as size of the representation of the data, etc. However, if there is an algorithm that, e.g., only requires finite many per computation steps, per inspection of a training data sample to find the hypothesis, we immediately infer that the concept class is efficiently PAC-learnable. The latter often requires a thorough analysis of the algorithm at hand. In this course we will however focus more on the statistical aspect of PAC-learnability, e.g., simply assuming that there are algorithms that succeed in the task of empirical risk minimization rather than spelling out the algorithm itself, and we will not discuss efficiency in detail.</p>
<p>Yet, in other words, the generalization error decreases as a function of the sample size <span class="math inline">\(N\)</span></p>
<p>As we have already discussed, this behavior substantiates our initial hope that increasing the sample size allows to increase the performance of our supervised learner.</p>
<p>However, to ensure consistency of the hypotheses selected by our algorithm we would have to make sure it exploits a sufficiently large range <span class="math inline">\(\mathcal F\)</span>, in particular, containing the concepts that we want to learn.</p>
<p>In our next steps, we will try to relax the consistency assumption.</p>
<p>➢ Next session!</p>
<p>For this purpose, we will however need a new tool, commonly referred to as <em>concentration inequalities</em>, and which, which we will introduce in the next chapter.</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
