<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__12-1__Short_review_optimization</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-12-1-pdf">MsAML-12-1 [<a href="SS21-MsAML__12-1__Short_review_optimization.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#a-short-review-of-optimization">A short review of optimization</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="a-short-review-of-optimization">A short review of optimization</h2>
<p>In the MAML course we have mainly discussed the implementation of algorithms, their potential convergence, and the richness in representation and approximation capabilities of the models. In the MSAML course we focused on the statistical aspect and made a short excursion into control of complexity by means of model selection. There is at least one topic that, so far, we have only treated as a prerequisite and left aside, namely optimization. Given optimization programs the like of which we have seen so far, the leading questions are:</p>
<ol type="1">
<li>do optimal solutions exist?</li>
<li>And what are their properties?</li>
</ol>
<p>This topic can be introduced nicely with the rather clear but still non-trivial example of the support vector machine in mind. The following results of the next module will point towards a slight adaption of the model by means of the kernel trick that allows to also treat non-linear learning tasks very robustly.</p>
<p>Let us recall the SVM minimization program for the hard margin case: <span class="math display">\[\begin{align}
  \min_{\substack{w\in\mathbb R^d\\b\in\mathbb R}} \frac12|w|^2
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    y^{(i)}(w\cdot x^{(i)}+b)\geq 1\\
    \text{for } i=1,\ldots, N
  \end{cases}
  \,.
\end{align}\]</span></p>
<p>This program is an example of what is called a <em>constrainted optimization program</em>. In general, these optimization programs are expressed as <span class="math display">\[\begin{align}
  \min_{x\in D} f(x)
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    g_i(x)\leq 0\\
    i=1,\ldots, N
  \end{cases}
  \,,
  \label{eq_P}
  \tag{P}
\end{align}\]</span> where for <span class="math inline">\(k\in\mathbb N\)</span>, the domain of interest is given by a <span class="math inline">\(D\subseteq \mathbb R^k\)</span>, the objective function <span class="math inline">\(f:\mathbb R^k\to\mathbb R\)</span>, and the constraints by <span class="math inline">\(g_i(x)\leq 0\)</span> for <span class="math inline">\(g:\mathbb R^k\to\mathbb R\)</span> or <span class="math inline">\(i=1,\ldots, N\)</span>. We will use the short-hand notation <span class="math display">\[\begin{align}
  g(x)\leq 0
  \qquad
  :\Leftrightarrow
  \qquad
  \forall \, i=1,\ldots,N: g_i(x)\leq 0.
\end{align}\]</span></p>
<p>To any constraint optimization program, one can associate a so-called <em>Lagrangian</em>, which will turn out as a convenient tool for our analysis.</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="lagrangian">(Lagrangian)</h5>
The Lagrangian associated to constraint program <span class="math inline">\(\eqref{eq_P}\)</span> is given as follows: <span class="math display">\[\begin{align}
\mathcal L: D\times (\mathbb R^+_0)^N &amp;\to \mathbb R
\\
(x,\alpha) &amp;\mapsto \mathcal L(x,\alpha):=f(x)+\sum_{i=1}^N \alpha_i g_i(x).
  \end{align}\]</span> The variables <span class="math inline">\(\alpha_i\)</span> are usually referred to as Lagrange or dual variables.
</dd>
</dl>
<p>This way also equality constraints can be specified by requiring both constraints <span class="math inline">\(g_i(x)\leq 0\)</span> and <span class="math inline">\(-g_i(x)\leq 0\)</span> to hold simultaneously. Let <span class="math inline">\(\alpha_i^+,\alpha_i^-\in\mathbb R^+_0\)</span> be the corresponding dual variables of these two constraints, respectively, then the corresponding term in the Lagrangian takes the form <span class="math inline">\((\alpha_i^+-\alpha^-)g_i(x)\)</span> or equivalently one may introduce an <span class="math inline">\(\alpha\in\mathbb R\)</span> to replace <span class="math inline">\((\alpha^+-\alpha_-)\)</span> instead.</p>
<p>In order to develop an intuition for the Lagrangian, let us consider the simple program <span class="math display">\[\begin{align}
  \min_{x\in D} f(x)
  \qquad
  \text{subject to}
  \qquad 
  g(x)\leq 0
\end{align}\]</span> for some <span class="math inline">\(D\subseteq \mathbb R\)</span> and <span class="math inline">\(f,g:\mathbb R\to\mathbb R\)</span>. An optimal solution of the program <span class="math inline">\(x^*\in D\)</span>, should it exist, therefore has the value <span class="math display">\[\begin{align}
  f(x^*)=\min\{f(x)\,|\,x\in D, g(x)\leq 0\}.
\end{align}\]</span> In order to study this program, we may look at the family of auxiliary programs to minimize the following function for given <span class="math inline">\(\alpha\in\mathbb R^+_0\)</span>: <span class="math display">\[\begin{align}
  f(x)+\alpha g(x).
\end{align}\]</span> The auxiliary variable <span class="math inline">\(\alpha\)</span> introduces a “weighting” of the constraint <span class="math inline">\(g(x)\leq 0\)</span>. Let <span class="math inline">\(x^*(\alpha)\in D\)</span> denote the optimal solution for the unconstrained auxiliary program with given <span class="math inline">\(\alpha\)</span>. Should these optima exists, we may expect the following behavior:</p>
<ul>
<li>The function attains in <span class="math inline">\(x^*(0)\)</span> its smallest value. If also <span class="math inline">\(g(x^*(0))\)</span> holds, fine. However, since <span class="math inline">\(\alpha=0\)</span>, the program is unaware of the constraint and it may well happen that <span class="math inline">\(g(x^*(0))&gt;0\)</span>.</li>
<li>In turn, for large <span class="math inline">\(\alpha\)</span>, one may have found <span class="math inline">\(x^*(\alpha)\)</span> such that <span class="math inline">\(g(x^*(\alpha))\leq 0\)</span> but the corresponding value <span class="math inline">\(f(x^*(\alpha))\)</span> may tends to be be large and there might be points in <span class="math inline">\(D\)</span> with smaller values.</li>
<li>One may then hope to find an intermediate value <span class="math inline">\(\bar\alpha\)</span>, e.g., at the boundaries <span class="math inline">\(g(x^*(\bar\alpha))=0\)</span>.</li>
</ul>
<p>The Lagrangian <span class="math inline">\(\mathcal L\)</span> allows to simultaneously study all of those <span class="math inline">\(\alpha\)</span>-parametrized scenarios by means of saddle points:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="saddle-point">(Saddle point)</h5>
Given program <span class="math inline">\(\eqref{eq_P}\)</span>, we call <span class="math inline">\((x^*,\alpha^*)\in \mathbb R^k\times(\mathbb  R^+_0)^N\)</span> a saddle point of the Lagrangian <span class="math inline">\(\mathcal L\)</span>, provided: <span class="math display">\[\begin{align}
\forall x\in\mathbb R^k, \alpha \in (\mathbb R^+_0)^N:
\qquad
\mathcal L(x^*,\alpha)\leq \mathcal L(x^*,\alpha^*)\leq\mathcal L(x,\alpha^*).
  \end{align}\]</span>
</dd>
</dl>
<p>This definition reflects the intuition we have introduced above and we readily get our first result in terms of the fact that saddle points indicate solutions of the optimization program <span class="math inline">\(\eqref{eq_P}\)</span>:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="solutions-to-eqrefeq_p">(Solutions to <span class="math inline">\(\eqref{eq_P}\)</span>)</h5>
Given program <span class="math inline">\(\eqref{eq_P}\)</span> and <span class="math inline">\((x^*,\alpha^*\)</span>) let <span class="math inline">\((x^*,\alpha^*)\in\mathbb  R^k\times(\mathbb R^+_0)^N\)</span> be a saddle point of Lagrangian <span class="math inline">\(\mathcal L\)</span>. Then, <span class="math inline">\(x^*\)</span> is a solution of the program <span class="math inline">\(\eqref{eq_P}\)</span>.
</dd>
</dl>
<p><strong>Proof:</strong> By definition, we have <span class="math display">\[\begin{align}
  \forall x\in\mathbb R^k,\alpha\in(\mathbb R^+_0)^N:
  \qquad
    \mathcal L(x^*,\alpha)\overset{\fbox{1}}{\leq} \mathcal
    L(x^*,\alpha^*)\overset{\fbox{2}}{\leq}\mathcal L(x,\alpha^*).
\end{align}\]</span></p>
<p>Based on relation <span class="math inline">\(\fbox{1}\)</span>, we find <span class="math display">\[\begin{align}
  \sum_{i=1}^N \alpha_i g_i(x^*)
  \leq
  \sum_{i=1}^N \alpha_i^* g_i(x^*).
\end{align}\]</span></p>
<p>Now, <span class="math inline">\(\alpha_i\to+\infty\)</span> implies <span class="math inline">\(g_i(x^*)\leq 0\)</span>, <span class="math inline">\(i=1,\ldots, N\)</span>, or in other words, <span class="math inline">\(x^*\)</span> satisfies the constraints.</p>
<p>Furthermore, <span class="math inline">\(\alpha_i\to 0\)</span> implies <span class="math display">\[\begin{align}
  0\leq \sum_{i=1}^N \alpha_i^* g_i(x^*).
\end{align}\]</span> and since we already confirmed that <span class="math inline">\(g_i(x^*)\leq 0\)</span> we get <span class="math display">\[\begin{align}
 \forall \, i=1,\ldots, N: \alpha_i^* g_i(x^*) = 0.
\end{align}\]</span></p>
<p>Based on this and relation <span class="math inline">\(\fbox{2}\)</span>, we find <span class="math display">\[\begin{align}
  \forall x\in\mathbb R^k:
  \qquad
  f(x^*)+\underbrace{\sum_{i=1}^N \alpha^*_i g_i(x^*)}_{=0}
  \leq 
  f(x)+\sum_{i=1}^N \alpha^*_i g_i(x).
\end{align}\]</span></p>
<p>In other words, for all <span class="math inline">\(x\in\mathbb R^k\)</span> satisfying the constraints <span class="math inline">\(g_i(x)\leq 0\)</span>, <span class="math inline">\(i=1,\ldots, N\)</span>, the relation <span class="math display">\[\begin{align}
  f(x^*) \leq f(x)
\end{align}\]</span> holds true.</p>
<p>In conclusion, <span class="math inline">\(\fbox{1}\)</span> and <span class="math inline">\(\fbox{2}\)</span> imply that <span class="math inline">\(x^*\)</span> is a solution of <span class="math inline">\(\eqref{eq_P}\)</span>.</p>
<p><span class="math inline">\(\square\)</span>.</p>
<p>In convex optimization, there is a well-developed theory to guarantee the existence of saddle points and infer their properties. Let us recall some results that we shall need later. Observing our time constraints, and since it is usually the content of an optimization course, we will omit the proofs. If you are new to the topic, have a look into the book <em>Convex optimization</em> by Jarre and Stoer.</p>
<p>As the name suggests, the main ingredient is convexity:</p>
<dl>
<dt>Definition</dt>
<dd><h5 id="convexity">(Convexity)</h5>
For <span class="math inline">\(D\subseteq \mathbb R^k\)</span>, <span class="math inline">\(f:\mathbb R^k\to \mathbb R\)</span> is called convex if <span class="math inline">\(D\)</span> is convex and for all <span class="math inline">\(\lambda\in[0,1], x,y\in D\)</span> the relation <span class="math display">\[\begin{align}   
f(\lambda x+(1\lambda)y) \leq \alpha f(x) + (1-\lambda) f(y)
  \end{align}\]</span> holds true. The function <span class="math inline">\(f\)</span> is called strictly convex, if the relation holds even with a strict inequality.
</dd>
</dl>
<p>Furthermore, from Analysis II we may remember:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="convexity-criterion">(Convexity criterion)</h5>
<p>Let <span class="math inline">\(D\subseteq \mathbb R^k\)</span> convex and <span class="math inline">\(f:\mathbb R^k\to\mathbb R\)</span>. Then:</p>
<ol type="1">
<li>If <span class="math inline">\(f\)</span> is differentiable on <span class="math inline">\(D\)</span>: <span class="math display">\[\begin{align}
  \forall x,y\in D:
  f(y)-f(x)
  \geq
  \nabla f(x)\cdot(x-y)
  \qquad
  \Leftrightarrow
  \qquad
  f \text{ is convex}.
\end{align}\]</span></li>
<li>If <span class="math inline">\(f\)</span> is twice differentiable on <span class="math inline">\(D\)</span>: <span class="math display">\[\begin{align}
  \forall x\in D:
  H_f(x)
  \geq 0
  \qquad
  \Leftrightarrow
  \qquad
  f \text{ is convex},
\end{align}\]</span> where <span class="math inline">\(H_f(x)\)</span> is the Hessian matrix <span class="math display">\[\begin{align}
  \left(H_f(x)\right)_{ij} 
  := 
  \frac{\partial^2}{\partial_{x_i}\partial_{x_j}}f(x)
  \qquad
  \text{for } i,j = 1,\ldots,k,
\end{align}\]</span> and the relation denotes that it must be positive semi-definite.</li>
</ol>
</dd>
</dl>
<p>An intuitive illustration for a convex function that is differentiable is given in in the following:</p>
<figure>
<img src="12/intuition_convex_diff.png" alt="Illustration of a convex and differentiable function and its linear approximation." /><figcaption aria-hidden="true">Illustration of a convex and differentiable function and its linear approximation.</figcaption>
</figure>
<p>Recall that a matrix <span class="math inline">\(M\in\mathbb R^{k\times k}\)</span> is called positive semi-definite if and only if <span class="math display">\[\begin{align}
  \forall x\in\mathbb R^k:
  \langle x, Mx\rangle\geq 0,
\end{align}\]</span> where <span class="math inline">\(\langle\cdot,\cdot,\rangle\)</span> denotes the inner product in <span class="math inline">\(\mathbb R^k\)</span>, or equivalently, if all of its eigenvalues are non-negative.</p>
<p>Given convexity, one can prove the following theorem base on the so-called hyperplane separation theorems.</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="saddle-point-criterion">(Saddle point criterion)</h5>
<p>Let <span class="math inline">\(f,g_1,\ldots,g_n\)</span> on <span class="math inline">\(D\)</span> be convex, and <span class="math inline">\(x^*\in D\)</span> a solution of the now convex constraint program <span class="math inline">\(\eqref{eq_P}\)</span>. Under the following conditions, there is an <span class="math inline">\(\alpha^*\geq 0\)</span> such that <span class="math inline">\((x^*,\alpha^*)\)</span> is a saddle point of the corresponding Lagrangian <span class="math inline">\(\mathcal L\)</span>:</p>
<ul>
<li><p>If the so-called <em>strong Slater condition</em> holds:</p>
<ul>
<li>The interior of <span class="math inline">\(D\)</span>, i.e., <span class="math inline">\(D^0\)</span>, is not empty.</li>
<li>And there is a <span class="math inline">\(\bar x\in D^0\)</span> such that <span class="math display">\[\begin{align}
  \forall i=1,\ldots, N:
  \quad
  g_i(\bar x)&lt; 0
\end{align}\]</span></li>
</ul></li>
<li><p>Or if <span class="math inline">\(f\)</span> is differentiable and the so-called <em>weak Slater condition</em> holds</p>
<ul>
<li>The interior of <span class="math inline">\(D\)</span>, i.e., <span class="math inline">\(D^0\)</span>, is not empty.</li>
<li>And there is a <span class="math inline">\(\bar x\in D^0\)</span> such that <span class="math display">\[\begin{align}
  \forall i=1,\ldots, N:
  \quad
  g_i(\bar x)&lt; 0
  \quad
  \vee
  \quad
  \left(
  g_i(\bar x)= 0
  \wedge
  \text{$g_i$ affine}
  \right).
\end{align}\]</span></li>
</ul></li>
</ul>
</dd>
</dl>
<p>This theorem lies at the heart of convex optimization, and it is a pity that time does not permit going into the proof here. The heuristics behind the, at first sight maybe cryptic, Slater condition is to find at least one point in the interior of <span class="math inline">\(D\)</span> which fulfills the constraints, from which the search of an optimal solution can be conducted – the interested reader is again referred to the book <em>Convex optimization</em> by Jarre and Stoer.</p>
<p>In case of sufficient regularity, a necessary and sufficient criterion for the solutions can be given by the famous Karush-Kuhn-Tucker theory:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="kkt-criterion">(KKT criterion)</h5>
Let <span class="math inline">\(f,g_1,\ldots, g_N\)</span> on <span class="math inline">\(D\)</span> be convex and differentiable and let the weak Slater condition be satisfied. Then, <span class="math inline">\(x^*\in D\)</span> is a solution to <span class="math inline">\(\eqref{eq_P}\)</span> if and only if: <span class="math display">\[\begin{align}
\exists \alpha^* \geq 0: \quad
&amp;\nabla_x\mathcal L(x^*,\alpha^*)=\nabla_x f(x^*)+\sum_{i=1}^N\alpha_i^* \nabla_x g_i(x^*)=0
&amp;
\fbox{KKT1}
\\
&amp;\wedge \quad \nabla_\alpha\mathcal L(x^*,\alpha^*)=g(x)\leq 0
&amp;
\fbox{KKT2}
\\
&amp;\wedge \quad \sum_{i=1}^N \alpha_i^* g_i(x^*)=0,
&amp; \fbox{KKT3}
  \end{align}\]</span> where <span class="math inline">\(\nabla_x\)</span> and <span class="math inline">\(\nabla_\alpha\)</span> denote the gradients with respect to first and second vector-valued argument, respectively.
</dd>
</dl>
<p>Note that <span class="math inline">\(\fbox{KKT3}\)</span> is equivalent to <span class="math display">\[\begin{align}
    g(x^*)\leq 0 \wedge \left(\forall\,i=1,\ldots,N: \alpha_i^* g_i(x^*)=0\right),
\end{align}\]</span></p>
<p><strong>Proof:</strong> “<span class="math inline">\(\Rightarrow\)</span>”: The conditions were formulated so that the necessary condition for <a href="#saddle-point-criterion">Theorem (Saddle point criterion)</a> is already fulfilled. Hence, there exists a <span class="math inline">\(\alpha^*\geq 0\)</span> such that <span class="math inline">\((x^*,\alpha^*)\)</span> is a saddle point of the Lagrangian. Let us define the function <span class="math display">\[\begin{align}
  \phi(x) := f(x)-f(x^*)+\sum_{i=1}^N \alpha_i^* g_i(x),
  \label{eq_1}
  \tag{E1}
\end{align}\]</span> which, by definition, fulfills <span class="math inline">\(\phi(x)\geq 0\)</span>.</p>
<p>Furthermore, we have <span class="math display">\[\begin{align}
  \phi(x^*)=\sum_{i=1}^N \alpha^*_i g_i(x) \leq 0
  \label{eq_2}
  \tag{E2}
\end{align}\]</span> because <span class="math inline">\(\alpha^*_i\geq 0\)</span> and <span class="math inline">\(g_i(x^*)\leq 0\)</span>.</p>
<p>Hence, <span class="math inline">\(x^*\)</span> needs to be a global minimum so that, thank to differentiability and by Fermat’s theorem, <span class="math display">\[\begin{align}
  \nabla_x \phi(x^*)=0
\end{align}\]</span> holds true, which is equivalent to <span class="math inline">\(\fbox{KKT1}\)</span>.</p>
<p>The condition <span class="math inline">\(\fbox{KKT2}\)</span> and <span class="math inline">\(\fbox{KKT3}\)</span> follow from a similar argument as in <a href="#solutions-to-eqrefeq_p">Theorem (Solutions to <span class="math inline">\(\eqref{eq_P}\)</span>)</a>.</p>
<p>“<span class="math inline">\(\Leftarrow\)</span>”: For <span class="math inline">\(x\in D\)</span> such that <span class="math inline">\(g(x)\leq 0\)</span>, and exploiting the convexity of <span class="math inline">\(f\)</span> to apply <a href="#convexity-criterion">Theorem (Convexity criterion)</a>, we find <span class="math display">\[\begin{align}
  f(x)-f(x^*)
  \geq \nabla_x f(x^*)\cdot (x-x^*).
  \tag{E3}
  \label{eq_3}
\end{align}\]</span> Applying <span class="math inline">\(\fbox{KKT1}\)</span> we get for the right-hand side <span class="math display">\[\begin{align}
  \eqref{eq_3} = -\sum_{i=1}^N\alpha^*_i \nabla_x g_i(x^*)\cdot (x-x^*).
  \tag{E4}
  \label{eq_4}
\end{align}\]</span> Next, we also exploit the convexity of the <span class="math inline">\(g_i\)</span>’s, which results in the following recast of the right-hand side <span class="math display">\[\begin{align}
  \eqref{eq_4} \geq -\sum_{i=1}^N\alpha^*_i \cdot (g_i(x)-g_i(x^*)).
  \tag{E5}
  \label{eq_5}
\end{align}\]</span> Finally, when supplying <span class="math inline">\(\fbox{KKT3}\)</span> and afterwards <span class="math inline">\(\fbox{KKT2}\)</span>, we conclude <span class="math display">\[\begin{align}
  \eqref{eq_5}
  =
  -\sum_{i=1}^N \alpha_i^* g(x) \geq 0.
\end{align}\]</span></p>
<p>Hence, <span class="math inline">\(f(x^*)\leq f(x)\)</span> for all <span class="math inline">\(x\in D\)</span> that satisfy the constraints <span class="math inline">\(g(x)\leq 0\)</span>. In other words, we have shown that <span class="math inline">\(x^*\)</span> solves the program <span class="math inline">\(\eqref{eq_P}\)</span>.</p>
<p><span class="math inline">\(\square\)</span></p>
<dl>
<dt>Remark</dt>
<dd>Note that this theorem is a generalization of the theorems of Fermat (no constraints) and Lagrange (equality constraints), which are contained as special cases. All these theorems reduce the number of candidate points in <span class="math inline">\(D\)</span> that have to be checked for potential minima by means of extra relations that such minima would have to fulfill.
</dd>
</dl>
<p>Let us apply this theorem to our SVM optimization program:</p>
<p>👀: Show that the hard-margin SVM optimization program has a unique solution. Set up the corresponding Lagrangian, check if the KKT Theorem can be applied and infer the properties <span class="math inline">\(\fbox{KKT1-3}\)</span>. In particular, show that the optimal <span class="math inline">\(w^*\)</span> can be given as linear combination of the support vectors, i.e., of those training data features <span class="math inline">\(x^{(i)}\)</span> that fulfill <span class="math inline">\(y^{(i)}(w^*\cdot x^{(i)}+b)=1\)</span>.</p>
<p>The properties inferred from <span class="math inline">\(\fbox{KKT1-3}\)</span> will help us to formulate a so-called dual optimization problem for the SVM, which will prove particular convenient to generalize for non-linear classification tasks.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
