# Exercise 06-S1: Rademacher complexity [[PDF](SS21-MsAML__06-XS1__Rademacher_complexity.pdf)]

Consider for a set $G\subseteq \mathbb{R}^N$

$$\widetilde{R}_N(G) = E_\sigma \sup_{g\in G} \frac{\langle \sigma, g \rangle}{N} $$

for $\sigma=(\sigma_1,\dots,\sigma_N)$ i.i.d Rademacher random variables, i.e. $P(\sigma_i=-1)=\frac{1}{2} = P(\sigma_i=1)$.

a) Compute $\widetilde{R}_N(G)$ for an $N$-dimensional cube of edge length $2d$, i.e. $G=[-d,d]^N$.

b) Compute $\widetilde{R}_N(G)$ for 

$$G_k = \{g \in [-d,d]^N \ | \text{  only } k \text{ of terms } g_1,\dots,g_N \text{ are non-zero} \}$$

c) Show that $\forall G\subset \mathbb{R}^N$ s.t. $|G|<\infty$ we have the estimate

$$\widetilde{R}_N(G)\leq \sup_{g\in G}|g|\frac{\sqrt{2\log|G|}}{N}$$

For this apply Hoeffding's lemma to the random variable $Z_g = \sum_{i=1}^N \sigma_ig_i$ and estimate $E\exp(tZ_g)$. Employ Jensen's inequality and sum over all possible $g\in G$ to obtain the cardinality $|G|$.

d) For binary classification $Y=\{-1,1\}$, relevant hypotheses $\mathcal{F} \subseteq \mathcal{H} = \mathcal{Y}^\mathcal{X}$, and

$$\mathcal{G}_\mathcal{F} = \{g:(x,y)\to \mathbf{1}_{y \neq h(x)} \ |\ h \in \mathcal{F} \},$$

show that the Rademacher complexity fulfills 

$$R_N(\mathcal{G}_\mathcal{F})=\frac{1}{2}E_{S,\sigma}\sup_{h\in\mathcal{F}}\frac{1}{N}\sum_{i=1}^N\sigma_i h(x^{(i)}) = \frac{1}{2}R_N(\mathcal{F})$$

To do this, first consider training data $s=(x^{(i)},y^{(i)})_{i=1,\dots,N}$ and its projection onto $\mathcal{X}$ denoted $\pi_{\mathcal{X}}s = (x^{(i)})_{i=1,\dots,N}$ and show 

$$\hat{R}_S(\mathcal{G}_\mathcal{F}) = \frac{1}{2} \hat{R}_{\pi_{\mathcal{X}}S }(\mathcal{F}).$$



