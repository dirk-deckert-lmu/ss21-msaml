% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-06-1-pdf}{%
\section{\texorpdfstring{MsAML-06-1
{[}\href{SS21-MsAML__06-1__Cross_entropy.pdf}{PDF}{]}}{MsAML-06-1 {[}PDF{]}}}\label{msaml-06-1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{entropy-measures}{Entropy measures}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{entropy-measures}{%
\subsection{Entropy measures}\label{entropy-measures}}

In the last module, we have found a good hint for a new type of loss
function, i.e., we identified its derivative that would allow to
compensate the slow learning problem in case the Adaline neuron is in a
region of saturation. We could simply leave it at that and accept the
new form of loss function as a good choice in combination with the
logistic activation function (and also the \(\tanh\) when converting the
labels correspondingly). However, in this module, we will take it as
excuse to make a brief excursion into information theory in order to
better understand under which conditions the new type of loss function
can be expected to work and also reflect the misclassification errors
well.

For sake of simplicity, let us restrict our discussion to a discrete
probability space \((\Omega,P)\) and consider the following
communication setting between Alice and Bob:

Suppose, in their communication, Alice and Bob want to distinguish a
number of \(|\Omega|=n\) events using a communication channel that
allows to send only one bit per communication exchange. In order to
reduce the number of communication exchanges between Alice and Bob, they
would need to find a coding scheme, i.e., an encoding table relating bit
sequences to the respective events, which still allows to encode all the
\(N\) events but at least on average reduces the number of
communications exchanges. As an analogy, if Bob intends to report to
Alice the color of each car passing by a highway intersection, it would
be advantageous that the word ``blue'' has less letters than
``magenta'', in case, there are more blue than magenta cars passing by,
and there is no need to use the same amount of letters for each word
denoting a color. Clearly, to achieve this, the more frequent events
\(\omega\in\Omega\), i.e., those with respectively large
\(P(\{\omega\})\), would need to be assigned a smaller bit sequence than
the less frequent ones.

Such a strategy provides a gain in communication efficiency only if
there are events that are indeed more frequent than others. In order to
predict the average minimum number of communication exchanges required
by Alice and Bob for any encoding scheme given the probability measure
\(P\), one defines the so-called \emph{information entropy}:

\begin{description}
\item[Definition] ~ 
\hypertarget{entropy}{%
\subparagraph{(Entropy)}\label{entropy}}

\begin{align}
H(P) := \sum_{\substack{\omega\in\Omega\\
                        P(\{\omega\})>0}}
        P(\{\omega\}) 
        (-1)\log_2 P(\{\omega\})
  \end{align}
\end{description}

The term \((-1)\log_2 P(\{\omega\})\) can be interpreted as number of
bits that shall be allocated in order to encode the event \(\omega\) --
few bits for events that are likely to occur and more for those that
will occur less likely. In this computation we can, of course, drop
those events that surely do not occur, i.e., with \(P(\{\omega\})=0\),
as they do not have to be encoded at all and we will do this for
convenience as otherwise the \(\log_2\) is not always well-defined.

To get familiar with this quantity, let us look at a few examples:

\textbf{Example 1) A fair coin:}

To model a fair coin, we set \begin{align}
  \Omega=\{0,1\} \qquad \text{and} \qquad P(\{0\})=\frac12=P(\{1\})
\end{align} and find \begin{align}
  H(P) = -\log_2\frac12=1.
\end{align} Hence, on average, we will need one bit for each event as we
might already expect. For example, we could represent \(\omega=0\) by
bit value 0 and \(\omega=1\) by bit value \(1\).

\textbf{Example 2) An unfair coin:}

Let us load the coin, e.g., such that \begin{align}
  \Omega=\{0,1\}, \qquad P(\{0\})=\frac14, \qquad \text{and} \qquad P(\{1\})=\frac34.
\end{align} Then the entropy evaluates to \begin{align}
  H(P) = \frac14 \log_2 4 + \frac34 \log_2\frac43 \approx 0.81\ldots .
\end{align} Of course, the number of bits, or number of communication
exchanges, in our example of Alice and Bob is an integer. Even though,
\(H(P)\) indicates that some events are much frequent than others, we
would still need \(1\) bit per event. A reduction in communication
exchanges can therefore only be achieved in case of more events.

\textbf{Example 3) A fair and unfair dice:}

To model a fair dice, we set \begin{align}
  \Omega=\{1,2,3,4,5,6\}, \qquad \text{and} \qquad \forall \omega\in\Omega: P(\{w\})=\frac16,
\end{align} which results in \begin{align}
  H(P) \approx 2.85\ldots .
\end{align} This implies that we would have to use a \(3\) bit coding
scheme, which would even suffice to encode \(8=2^3\) events. However,
for a loaded dice, e.g., \begin{align}
  P(\{1\})=\frac14, \qquad P(\{2\}),\ldots,P(\{5\})=\frac{1}{16},\qquad P(\{6\})=\frac12,
\end{align} we find \begin{align}
  H(P)=2,
\end{align} which implies that, in an optimal coding scheme, on average
only \(2\) bits would be need to be send per event.

\begin{description}
\tightlist
\item[👀]
Find such a coding scheme.
\end{description}

In general, \(H(P)\) is larger the more uniform \(P\) is distributed and
for a uniform distributed \(P\) one finds \(H(P)=\log_2 N\), i.e., the
minimum number of bits to encode \(N\) events with bit sequences of same
length.

Given \(P\), one could now assign bit sequences to events to approximate
the optimal coding scheme bandwith (taking in account that the number of
bits per communication exchange sequence is an integer, of course).

However, in the typical situation in statistics, the measure \(P\) is
unknown and the objective is to find a good ``estimated'' measure \(Q\)
based on the empirical evidence. A possible means to evaluate how good a
``guess'' \(Q\) actually is, when compared to the actual measure \(P\),
is to compute the so-called \emph{cross-entropy}:

\begin{description}
\item[Definition] ~ 
\hypertarget{cross-entropy}{%
\subparagraph{(Cross entropy)}\label{cross-entropy}}

Given another measure \(Q\) on \(\Omega\) we define: \begin{align}
H(P,Q) = \sum_{\omega \in\Omega}
         P(\{\omega\})(-1)\underbrace{\log_2 Q(\{\omega\})}_{\text{convention: $\log_2 0 = -\infty$}}
  \end{align}
\end{description}

One may now interpret \(H(P,Q)\) as follows: According our guess \(Q\)
of how likely any event \(\omega\) occurs, we would optimally assign a
bit sequence of length \(\log_2 Q(\{\omega\})\) to encode event
\(\omega\). Now, the cross-entropy \(H(P,Q)\) averages these assigned
bit-lengths over the actual distribution \(P\) of events
\(\omega\in\Omega\). If, according to \(Q\), we would enforce a
sub-optimal coding scheme with respect to \(P\), we will on average
assign longer bit sequences as would be required per event:

\begin{description}
\tightlist
\item[Theorem]
The cross-entropy is bounded from below by the entropy, i.e.,
\begin{align}
H(P,Q)\geq H(P).
  \end{align}
\end{description}

We leave the proof as an exercise.

\begin{description}
\tightlist
\item[👀]
Proof the above theorem. One strategy could be to regard the difference
\(H(P,Q)-H(P)\), confirm the inequality \(\log x \leq x -1\) for
\(x>0\), and apply it to show that the former difference is bounded
below by zero.
\end{description}

This property qualifies \(H(P,Q)\) to function as distance between
potential guess \(Q\) and actual \(P\).

\hypertarget{application-to-our-adaline-neuron}{%
\subsubsection{Application to our Adaline
neuron}\label{application-to-our-adaline-neuron}}

Next, we will line out an analogy to this optimal encoding question for
the Adaline setting of binary classification that motivates the use of
the cross-entropy as a loss function, at least in the case of the
logistic activation function \begin{align}
  \alpha(z) = \frac{1}{1+e^{-z}}.
\end{align} In such a setting, the Adaline neuron is sometimes referred
to as \emph{logistic Adaline} neuron.

Given the usual training data for binary classification \begin{align}
  s=(x^{(i)},y^{(i)})_{i=1,\ldots, N} \in (\mathcal X\times\mathcal Y)^N,
  \qquad \mathcal Y=\{0,1\},
\end{align} let us consider the following sample space \begin{align}
  \Omega=\left\{ \{x^{(i)}\text{ drawn and }y^{(i)}=y\}\,\Big|\, i=1,\ldots, N, y\in\{0,1\}
         \right\}
\end{align} with the following probability distribution \begin{align}
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=1
    \right\}
  \right)
  &=
  \frac{y^{(i)}}{N},\\
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=0
    \right\}
  \right)
  &=
  \frac{1-y^{(i)}}{N}.
\end{align} Note that this actual gives rise to a probability measure
because \begin{align}
  P(\Omega)
  =
  \sum_{\omega \in\Omega} P(\{\omega\})
  =
  \sum_{i=1}^N\sum_{y=0,1}
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=y
    \right\}
  \right)
  =1.
\end{align}

Hence, \((\Omega,P)\) models the probability of picking a feature vector
\(x^{(i)}\) of the training data with the correct label at random with
uniform distribution.

Now the Adaline's confidence for the same event to have one label of
another could be modeled by the following measure \(Q\), setting
\begin{align}
  Q\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=1
    \right\}
  \right)
  &=
  \frac{\alpha(w\cdot x^{(i)}+b)}{N},\\
  Q\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=0
    \right\}
  \right)
  &=
  \frac{1-\alpha(w\cdot x^{(i)}+b)}{N}.
\end{align} Note that also \(Q\) is a probability measure as
\(\text{range}\alpha\in(0,1)\) and \(Q(\Omega)=1\). Here, the emphasis
must lie on ``a measure'' as the nature of the activation obviously does
not need to reflect a relative frequency, not to mention a probability
-- although there are interesting classification models that try to
enforce this nature of relative frequencies.

Computing the respective cross entropy yields \begin{align}
  &H(P,Q)
  =
  -\sum_{\omega\in\Omega} \log_2 Q(\{\omega\})P(\{\omega\})\\
  &=
  -\sum_{i=1}^N \sum_{y=0,1} 
  P\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=y
    \right\}
  \right)
  \\
  &\qquad \qquad \qquad
  \times \log_2 
  Q\left(
    \left\{
      x^{(i)}\text{ drawn and }y^{(i)}=y
    \right\}
  \right)
  \\
  &=
  -\sum_{i=1}^N \Big[
    P\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=1
      \right\}
    \right)
    \\
    &\qquad\qquad\qquad\times
    \log_2
    Q\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=1
      \right\}
    \right)
    \\
    &\qquad\qquad
    +
    P\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=0
      \right\}
    \right)
    \\
    &\qquad\qquad\qquad\times
    \log_2
    Q\left(
      \left\{
        x^{(i)}\text{ drawn and }y^{(i)}=0
      \right\}
    \right)
  \Big]
  \\
  &=
  -\sum_{i=1}^N
  \left[
    \frac{y^{(i)}}{N}\log_2\frac{\alpha(w\cdot x^{(i)}+b)}{N}
    +
    \frac{1-y^{(i)}}{N}\log_2\frac{1-\alpha(w\cdot x^{(i)}+b)}{N}
  \right]
  \\
  &=
  \log_2 N - \frac1N
  \sum_{i=1}^N 
  \Big[
    y^{(i)}\log_2 \alpha(w\cdot x^{(i)}+b)
    \\
    &\qquad\qquad\qquad\qquad\qquad
    +
    (1-y^{(i)})\log_2(1-\alpha(w\cdot x^{(i)}+b))
  \Big].
\end{align} The constant \(\log_2 N\), as discussed, reflects the worst
case average for the case of a uniform distribution. Dropping this
constant (and also the base \(2\)), which is irrelevant for the task of
optimization, this computation motivates the loss function \begin{align}
  L_{\text{cross-entropy}}(y,y')
  =
  -\left(y\log y' + (1-y)\log (1-y')\right)
\end{align} and corresponding empirical loss \begin{align}
  \widehat L_{\text{cross-entropy}}(b,w)
  =
  \frac1N \sum_{i=1}^N L_{\text{cross-entropy}}\left(y^{(i)}, \alpha(w\cdot x^{(i)}+b)\right).
\end{align}

The gradient needed for the corresponding update rule can be computed as
follows: \begin{align}
  \frac{\partial \widehat L(\bar w)}{\partial\bar w}
  &= 
  -\frac1N\sum_{i=1}^N\left[
    \frac{y^{(i)}}{\alpha(\bar w\cdot \bar x^{(i)})}
    -
    \frac{1-y^{(i)}}{1-\alpha(\bar w\cdot \bar x^{(i)})}
  \right]
  \alpha'(\bar w\cdot \bar x^{(i)})\bar x^{(i)}\\
  &=
  -\frac1N
  \sum_{i=1}^N \left[y^{(i)}-\alpha(\bar w\cdot \bar x^{(i)})\right]
  \bar x^{(i)},
\end{align} recalling \begin{align}
  \alpha'(z)=\alpha(z)(1-\alpha(z))
\end{align} and using our short-hand notation \begin{align}
  \bar w=(b,w)^T \qquad\text{and}\qquad \bar x=(1,x)^T.
\end{align} This allows to specify the update rule by means of:
\begin{align}
  \bar w & \leftarrowtail \bar w^\text{new} := \bar w + \eta \frac1N
  \sum_{i=1}^N \left[y^{(i)}-\alpha(\bar w\cdot \bar x^{(i)})\right] \bar x^{(i)}.
\end{align}

In comparison to a setting of an logistic Adaline with a mean square
error loss, we observe the absence of the potentially small
\(\alpha'(z)\) despite the logistic activation function, as discussed in
the last module.

\begin{description}
\tightlist
\item[Remark]
As a rule of thumb, the Adaline neuron with logistic or \(tanh\)
activation function \(\alpha(z)\) will in general be easier to train
with respect to a cross-entropy than a mean square error loss, in
particular, for ``unfortunate'' choice of initial model parameters. That
is, unless the vanishing gradient effect is desired to model a certain
inertial during training. The latter turns out to be useful in the case
of deep networks of many layered Adaline neurons, in particular, for the
so-called \emph{convolution layers}. In their case, a popular choice for
an activation function is given by the rectified linear or ReLu unit:
\includegraphics{06/ReLu_activation.png}
\end{description}

If time permits, we will discuss these convolution layers and the
usefulness of the ReLu activation in more detail towards the end of the
course. For now, the take-aways message is that, often, the choice of
the activation functions and loss function have to be carefully adapted
to each other.

\begin{description}
\tightlist
\item[👀]
Since the ReLu is not differentiable. How could be define a sensible
update rule?
\end{description}

In the next model, we will touch upon another important topic in
conditioning the Adaline model well for optimal learning performance and
later generalize it to the case of more than two classes, which can be
seen as the first step towards deep networks.

➢ Next session!

\end{document}
