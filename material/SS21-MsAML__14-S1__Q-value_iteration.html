<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__14-S1__Q-value_iteration</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-14-s1-pdf">MsAML-14-S1 [<a href="SS21-MsAML__14-S1__Q-value_iteration.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#q-value-iteration">Q-value iteration</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="q-value-iteration">Q-value iteration</h2>
<p>After our discussion about the reinforcement learning frame work, the construction of the value functions, and the existence of optimal strategies, the question remains how to find approximations to optimal policies by means of a learning algorithm.</p>
<p>The key ingredient in our discussion was the transport operator, e.g., for the state-action value function: <span class="math display">\[\begin{align}
  T^{\mathcal Q}_*:\mathcal Q &amp;\to \mathcal Q  
  \\
  (T^{\mathcal Q}_*Q)(s,a)&amp;:=\sum_{s&#39;\mathcal S} p(s&#39;|s,a)\left[
    R(s,a,s&#39;)+\gamma\max_{a\in\mathcal A} Q(s&#39;,a&#39;)
  \right].
\end{align}\]</span></p>
<p>With the transition probabilities at hand, one can prove convergence:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="convergence-to-an-optimal-policy">(Convergence to an optimal policy)</h5>
Let <span class="math inline">\(\gamma\in[0,1)\)</span>, <span class="math inline">\(Q^{(0)}\in\mathcal Q\)</span>, and define <span class="math display">\[\begin{align}
Q^{(k)}:=T^{\mathcal Q}_* Q^{(k-1)}
  \end{align}\]</span> recursively for <span class="math inline">\(k\in\mathbb N\)</span>. Furthermore, for each step <span class="math inline">\(k\in\mathbb N\)</span>, let <span class="math inline">\(\pi^{(k)}:\mathcal S\to\mathcal A\)</span> be selected as follows <span class="math display">\[\begin{align}
\pi^{(k)}(s) &amp;:\in \underset{a\in\mathcal A}{\operatorname{argmax}}Q^{(k)}(s,a).
  \end{align}\]</span> Then, for all <span class="math inline">\(s\in\mathcal S\)</span>, <span class="math display">\[\begin{align}
\lim_{k\to\infty}\pi^{(k)}(s)=\pi^*(s),
  \end{align}\]</span> i.e., the family of policies <span class="math inline">\((\pi^{(k)})_{k\in\mathbb N}\)</span> converges to the optimal policy <span class="math inline">\(\pi^*\)</span>.
</dd>
</dl>
<p>The convergence can again be shown by exploiting the <span class="math inline">\(\gamma\)</span>-contraction property of the transport operators, but we will not go into the proof here. Although, this may live up to our expectation, the result is rather academic as the transition probabilities <span class="math inline">\(\mathcal P\)</span> are not known to the agent. Thus, for practical purposes we need to setup a different strategy in approximating <span class="math inline">\(\pi^*\)</span>.</p>
<p>A successful algorithm is the so-called <em><span class="math inline">\(Q\)</span> value iteration</em> or <em>Q-learning</em> algorithm, a first version of which we will outline next:</p>
<p><strong>Input:</strong> Fix the following input values:</p>
<ul>
<li>learning rate <span class="math inline">\(\alpha\in(0,1)\)</span>;</li>
<li>exploration probability <span class="math inline">\(\delta\in[0,1]\)</span>;</li>
<li>initial state-action value function <span class="math inline">\(Q^{(0)}\in\mathcal Q\)</span>;</li>
<li>total number of training epochs <span class="math inline">\(K\in\mathbb N\)</span>.</li>
</ul>
<p><strong>Step 1:</strong> Repeat for <span class="math inline">\(k=1,2,\ldots, K\)</span> rounds:</p>
<p><strong>Step 2:</strong> Choose state <span class="math inline">\(s\in\mathcal S\)</span> with uniform distribution.</p>
<p><strong>Step 3:</strong> With probability <span class="math inline">\(\delta\)</span> choose an action <span class="math inline">\(a\in\mathcal A\)</span>, otherwise choose <span class="math display">\[\begin{align}
  a :\in \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(k)}(s,a).
\end{align}\]</span></p>
<p><strong>Step 4:</strong> Start the actual MDP with initial condition <span class="math display">\[\begin{align}
  S_0=s, \qquad A_0=a.
\end{align}\]</span> to retrieve a realization of random variable <span class="math inline">\(S_1\)</span>, i.e., the “response” of the environment in view of initial state <span class="math inline">\(s\)</span> and selected action <span class="math inline">\(a\in\mathcal A\)</span>. In the following, let us denote the realization of <span class="math inline">\(S_1\)</span> by <span class="math inline">\(s&#39;\in\mathcal S\)</span>.</p>
<p><strong>Step 5:</strong> Update the state-action value function as follows: <span class="math display">\[\begin{align}
  Q^{(k+1)}(s,a) 
  := 
  (1-\alpha) Q^{(k)}(s,a)
  +
  \alpha \left[ R(s,a,s&#39;)+
    \gamma \max_{a\in\mathcal A} Q^{(k)}(s&#39;,a&#39;)
  \right]
\end{align}\]</span> and go back to <strong>Step 1</strong>.</p>
<p><strong>Output:</strong> The updated policy after the <span class="math inline">\(K\)</span> epochs of learning: <span class="math display">\[\begin{align}
  \pi^{(K)}(s)
  :\in
  \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(K)}(s,a).
\end{align}\]</span></p>
<p>Let us discuss some of the steps qualitatively:</p>
<p>As in practice the state space is exorbitantly large, Step 2 is obviously not the best we can do. Theoretically, however, it is needed to show that <span class="math inline">\(\pi^{(K)}\)</span> approximates the optimal <span class="math inline">\(\pi^*\)</span>. For practical purposes, this has to be adapted, e.g., by introducing a method that picks a most relevant states that even allow to gain a reward at all.</p>
<p>Note that Step 3 ensures that we do not always act greedily and choose the most promising immediate action according to our present knowledge, that latter of which is encoded in the most recent iteration of <span class="math inline">\(Q^{(k)}\)</span>. The probability to make a random choice is controlled by <span class="math inline">\(\delta\)</span> which allows to fine-tune the trade-off between exploration of new states and exploitation of knowledge about previously seen ones. As we have discussed previously, a purely greedy strategy may not lead to the biggest reward in the long run. Hence, it is import to allow the agent to explore new state-action combinations. It is usually efficient to allow for more exploration in the beginning as the initial <span class="math inline">\(Q^{(0)}\)</span> may not be trustworthy. This helps the agent to explore its options and acquire knowledge about the resulting responses of the environment. At a later stage it may be wise to reduce the probability for exploration and to rather fine-tune the acquired knowledge by selecting well-proven state-action pairs. In practice one replaces the fixed <span class="math inline">\(\delta\)</span> in the definition of the algorithm above by some null sequence <span class="math inline">\(\delta^{(k)}\underset{k\to \infty}{\to}0\)</span>.</p>
<p>Furthermore, Step 4 conducts the actual exploration and collects the feedback from the environment. Given the state-action pair, it answers the question of which was the resulting state in this “trial run” in order to compute the immediate reward in Step 5. During the repetitions for a sufficiently large number of total epochs <span class="math inline">\(K\)</span> the agent will therefore pick up the statistical frequencies of such combinations.</p>
<p>Observe that the above algorithm attempts to learn an optimal <span class="math inline">\(Q^*\)</span> in a “backwards” fashion: Say we start with <span class="math inline">\(Q^{(0)}=0\)</span>. Then, in the iteration over <span class="math inline">\(k\)</span>, the adapted state-action value fulfills <span class="math inline">\(Q^{(k+1)}\neq 0\)</span> for a currently chosen state-action pair <span class="math inline">\((s,a)\)</span> only if either:</p>
<ul>
<li>State <span class="math inline">\(s\)</span> leads to a state <span class="math inline">\(s&#39;\)</span> that earns the agent an immediate non-zero reward <span class="math inline">\(R(s,a,s&#39;)\)</span>.</li>
<li>or, for the next state <span class="math inline">\(s&#39;\)</span>, we already have a non-zero <span class="math inline">\(Q^{(k)}(s&#39;,a&#39;)\)</span> for some action <span class="math inline">\(a&#39;\)</span>.</li>
</ul>
<p>Typically, however, immediate rewards <span class="math inline">\(R(s,a,s&#39;)\)</span> tend to appear in the later rather than the early phases of the learning task, e.g., during playing a game. In turn, the first iterations will be spent searching for such “late in the game” state-action combinations that lead to an immediate reward. Only after finding some of them, it is possible for the algorithm to adapt the state-action value function <span class="math inline">\(Q^{(k)}\)</span> such that these rewards can eventually be reached by exploitation. In this sense, one could say the agent learns strategies in a “backwards” fashion.</p>
<p>Finally, also note that <span class="math inline">\(\alpha\)</span> moderates how much we adapt <span class="math inline">\(Q^{(k+1)}\)</span> with the newly found knowledge similarly as the learning rates in our previously discussed supervised learning algorithms. In order to allow for a convergence towards the final epochs, one usually introduces a decrease in the learning rate by replacing <span class="math inline">\(\alpha\)</span> with a null sequence <span class="math inline">\(\alpha^{(k)}\underset{k\to\infty}{\to}0\)</span>.</p>
<p>Given this setting, it possible to prove:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="convergence-of-the-q-learning-algorithm">(Convergence of the Q-learning algorithm)</h5>
Provided all state-action pairs <span class="math inline">\((s,a)\in\mathcal S\times\mathcal A\)</span> are visited infinitely often and <span class="math display">\[\begin{align}
\sum_{k\in\mathbb N}\alpha^{(k)}=\infty
\qquad
\wedge
\qquad
\sum_{k\in\mathbb N}\left(\alpha^{(k)}\right)^2&lt;\infty,
  \end{align}\]</span> the following convergence holds true almost surely <span class="math display">\[\begin{align}
 \lim_{k\to\infty}Q^{(k)}=Q^*
\qquad
\wedge
\qquad
 \lim_{k\to\infty}\pi^{(k)}=\pi^*.
  \end{align}\]</span>
</dd>
</dl>
<p>In case you are interested in the proof, an nice version of it can be found <a href="http://users.isr.ist.utl.pt/~mtjspaan/readingGroup/ProofQlearning.pdf">here</a>.</p>
<p>In practice, however, the given algorithm will have to be modified. Let us therefore close our discussion of reinforcement learning with three effective adaptions that have led to the recent advances:</p>
<p><em>Improval of Step 2 and 4:</em> Instead of choosing the states at random, a relevant initial state is picked and the process is run for not only one but for multiple iterations <span class="math inline">\(T\in\mathbb N\)</span>. However, if we would only apply the Q-learning algorithm to states found along such so-called “episodes”, there is a strong correlation between the corresponding <span class="math inline">\(S_0,S_1,S_2,\ldots\)</span> and we would leave the setting of the above theorem. In order to restore some independence, one stores the last, say, <span class="math inline">\(M\in\mathbb N\)</span> many, observations for <span class="math inline">\((s,a,s&#39;)\)</span> in a so-called <em>replay memory</em> <span class="math inline">\(\mathcal M\)</span>. Later, when updating to the next iteration of the state-action value function <span class="math inline">\(Q^{(k+1)}\)</span>, one picks only batches of <span class="math inline">\(B\in\mathbb N\)</span> many such observations in <span class="math inline">\(\mathcal M\)</span> at random. With these adaptions, the algorithm reads as follows:</p>
<p><strong>Input:</strong> Fix the following input values:</p>
<ul>
<li>learning rate sequence <span class="math inline">\(\alpha^{(k)}\in(0,1)\)</span>, <span class="math inline">\(k\in\mathbb N\)</span>;</li>
<li>exploration probability <span class="math inline">\(\delta^{(k)}\in[0,1]\)</span>, <span class="math inline">\(k\in\mathbb N\)</span>;</li>
<li>replay memory <span class="math inline">\(\mathcal M=\emptyset\)</span> and size <span class="math inline">\(M\in\mathbb N\)</span>;</li>
<li>batch size <span class="math inline">\(B\in\mathbb N\)</span> and episode length <span class="math inline">\(T\in\mathbb N\)</span>;</li>
<li>initial state-action value function <span class="math inline">\(Q^{(0)}\in\mathcal Q\)</span>;</li>
<li>total number of training epochs <span class="math inline">\(K\in\mathbb K\)</span>.</li>
</ul>
<p><strong>Step 1:</strong> Repeat for <span class="math inline">\(k=1,2,\ldots, K\)</span> rounds:</p>
<p><strong>Adapted Steps 2-4:</strong> Pick a relevant state <span class="math inline">\(s\in\mathcal S\)</span> and start the actual MDP with initial condition <span class="math display">\[\begin{align}
  S_0=s, \qquad A_0=a
\end{align}\]</span> to run for <span class="math inline">\(T\)</span> steps. For each step <span class="math inline">\(t=1,2,\ldots, T\)</span> perform the substeps:</p>
<ol type="1">
<li>Set <span class="math inline">\(s:=S_t\)</span>.</li>
<li>With probability <span class="math inline">\(\delta^{(k)}\)</span> choose action <span class="math inline">\(a\in\mathcal A\)</span> at random or otherwise choose <span class="math display">\[\begin{align}
  a :\in \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(k)}(s,a).
\end{align}\]</span></li>
<li>Choose <span class="math inline">\(A_t=a\)</span> and receive the response of the environment as realization <span class="math inline">\(s&#39;\)</span> of the random variable <span class="math inline">\(S_t\)</span>.</li>
<li>Add <span class="math inline">\((s,a,a&#39;)\)</span> to the replay memory <span class="math inline">\(\mathcal M\)</span> and, if necessary, delete the oldest entry to ensure <span class="math inline">\(|\mathcal M|=M\)</span>.</li>
</ol>
<p><strong>Step 5:</strong> Repeat <span class="math inline">\(B\)</span> times:</p>
<ol type="1">
<li>Pick a tuple <span class="math inline">\((s,a,s&#39;)\in\mathcal M\)</span> at random.</li>
<li>Conduct the update <span class="math display">\[\begin{align}
  Q^{(k+1)}(s,a) 
  := 
  (1-\alpha) Q^{(k)}(s,a)
  +
  \alpha \left[ R(s,a,s&#39;) +
    \gamma \max_{a\in\mathcal A} Q^{(k)}(s&#39;,a&#39;)
  \right].
\end{align}\]</span></li>
</ol>
<p>Afterwards go back to <strong>Step 1</strong>.</p>
<p><strong>Output:</strong> The updated policy after the <span class="math inline">\(K\)</span> epochs of learning: <span class="math display">\[\begin{align}
  \pi^{(K)}(s)
  :\in
  \underset{a\in\mathcal A}{\operatorname{argmax}} Q^{(K)}(s,a).
\end{align}\]</span></p>
<p>Observe the similarities to stochastic gradient descent in which we also used randomly chosen batches to reduce the correlations. The algorithm can be improved further by storing observations <span class="math inline">\((s,a,a&#39;)\)</span> that actually lead to non-zero reward with higher frequency.</p>
<p><em>Improval of Step 4:</em> If the interaction with the actual environment is not feasible, it has to be simulated. For example, in a game setting, the environment is basically driven by the reactions of another player obeying the rules of the game. Such an environment can be simulated by letting the agent play against itself using the same learning algorithm on both sides of the game. In order to synchronize the states for both players, we need an additional function that translates the state of Player 1 into the one of Player 2 and vice versa. Let this map be denoted by <span class="math display">\[\begin{align}
  \operatorname{inv}:\mathcal S\to\mathcal S.
\end{align}\]</span></p>
<p>Giving this map, we can adapt the inner loop in <strong>Step 2-4</strong> in the algorithm above as follows:</p>
<ul>
<li>Given <span class="math inline">\(s:=S_t\)</span>, Player 1 chooses an action <span class="math inline">\(a\in\mathcal A\)</span> according to substep 2.</li>
<li>This creates an intermediate state <span class="math inline">\(s_\text{int}\in\mathcal S\)</span> which for Player 2 looks as <span class="math inline">\(\tilde s=\operatorname{inv}(s_\text{int})\)</span>.</li>
<li>Player 2 then picks an action <span class="math inline">\(\tilde a\in\mathcal A\)</span> according to subsetp 2.</li>
<li>In turn, this create the final state <span class="math inline">\(\tilde s&#39;\)</span> of this round, which for Player 1 looks as <span class="math display">\[\begin{align}
  s&#39; = \operatorname{inv}^{-1}(\tilde s&#39;).
\end{align}\]</span></li>
<li>In this way, we have generated an observation <span class="math inline">\((s,a,s&#39;)\)</span> that can be stored in the replay memory of Player 1, and similarly, we will do for Player 2.</li>
</ul>
<p>Of course, there is a multitude of other variations of such algorithms that simulate the environment. An obvious variant is to give each player an own <span class="math inline">\(Q\)</span> an pick the best performing one.</p>
<p><em>Supervised approximation of <span class="math inline">\(Q\)</span></em>: A last variant to mention before closing our discussion which led to recent advances is the approximation of <span class="math inline">\(Q\)</span> by means of another supervised learner, e.g., a neural network. In order to fight the combinatorial complexity of <span class="math inline">\(|\mathcal S\times\mathcal A|\)</span> that must be addressed when approximating <span class="math inline">\(Q\)</span>, we may use a classifier to predict a potentially good choice of <span class="math inline">\(Q\)</span> by means of the current state only.</p>
<p>Formally, we would need a hypotheses space <span class="math inline">\(\mathcal H\)</span> with hypotheses of the form <span class="math display">\[\begin{align}
  h:\mathcal S &amp;\to {\mathbb R}^{\mathcal A}
  \\
  s &amp;\mapsto h(s)
\end{align}\]</span> such that the approximated state-action value can be retrieved from <span class="math display">\[\begin{align}
  Q^{\approx}(s,a) := (h(s))(a),
  \qquad \text{for }s\in\mathcal S,a\in\mathcal S.
\end{align}\]</span></p>
<p>For practical purposes we could choose a sufficiently deep neural network architecture such as the discussed densely connected ones <span class="math inline">\(\mathcal H:=\mathcal N^D_{n,\alpha}\)</span> for <span class="math inline">\(D\)</span> layers, <span class="math inline">\(n_0\)</span> input neurons, <span class="math inline">\(n_D\)</span> output neurons, and parametrize <span class="math inline">\(\mathcal S\)</span> by <span class="math inline">\(\mathbb R^{n_0}\)</span> and <span class="math inline">\({\mathbb R}^{\mathcal A}\)</span> by <span class="math inline">\(\mathbb R^{n_D}\)</span>.</p>
<p>Each <span class="math inline">\(h\in\mathcal H\)</span> would then be parametrized by means of the corresponding weight matrices and bias vectors <span class="math inline">\(p=(W_i,b_i)_{i=1,\ldots,D}\)</span>, which we denote as <span class="math inline">\(h=h_p\)</span> in our notation.</p>
<p>In order to train this neural network, we need training data. This can be generated from the recorded observations <span class="math inline">\((s,a,s&#39;)\)</span> as follows:</p>
<p>Each newly observed triple <span class="math inline">\((s,a,s&#39;)\)</span> gives rise to a new feature vector <span class="math display">\[\begin{align}
  x^{(i)} = s \in \mathbb R^{n_0}.
\end{align}\]</span> and corresponding label <span class="math display">\[\begin{align}
  y^{(i)} \in {\mathbb R}^{\mathcal A}
\end{align}\]</span> which is computed for all <span class="math inline">\(\tilde a\in\mathcal A\)</span> as follows <span class="math display">\[\begin{align}
  y^{(i)}(\tilde a) 
  := 
  \begin{cases}
    R(s,a,s&#39;) + \gamma\max_{a&#39;\in\mathcal A} (h_p(s&#39;))(a) &amp; \text{for } \tilde a=a
    \\
    (h_p(s))(\tilde a) &amp; \text{otherwise}
  \end{cases}
  \,.
\end{align}\]</span></p>
<p>Collecting the observations in the replay memory results in the training data <span class="math inline">\((x^{(i)},y^{(i)})_{i=1,\ldots,M}\)</span>, which, e.g., can be used for a gradient descent training of the classifier.</p>
<p>Provided with such an additional classifier, we can adapt the above algorithm as follows:</p>
<ul>
<li>Provide an initialization of the classifier by means of an initial hyperparameter <span class="math inline">\(p^{(0)}\)</span>.</li>
<li>In the <span class="math inline">\(k\)</span>-th step, replace <span class="math inline">\(Q^{(k)}(s,a)\)</span> with <span class="math inline">\((h_{p^{(k)}}(s))(a)\)</span>.</li>
<li>After acquiring <span class="math inline">\(M\)</span> observations, one generates the corresponding training data as prescribed above and conducts the update rule to adjust <span class="math inline">\(p^{(k)}\)</span> to <span class="math inline">\(p^{(k+1)}\)</span>, e.g., by gradient descent for a convenient empirical loss.</li>
</ul>
<p>In this form, the corresponding algorithms is usually referred as <em>deep Q-learning</em>. Most prominently, such a method has been applied in DeepMind’s <a href="https://deepmind.com/research/case-studies/alphago-the-story-so-far"><em>AlphaGo</em></a> and, with the Player 1 &amp; 2 scenario discussed above, in <a href="https://deepmind.com/blog/article/alphazero-shedding-new-light-grand-games-chess-shogi-and-go"><em>AlphaZero</em></a>.</p>
<p>This closes our introduction into reinforcement learning and likewise the statistical part of our course. As already said in our this week’s <a href="SS21-MsAML__14-1__Kernel_SVMs.html">Monday lecture</a>, the field is large and we could only cover so much. However, we can probably claim that we have covered most of the key ideas in our discussions and now, depending on your interest, you should be well-prepared to continue your own journey into the field. Enjoy!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
