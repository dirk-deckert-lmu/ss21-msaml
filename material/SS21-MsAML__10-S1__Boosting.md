# MsAML-10-S1 [[PDF](SS21-MsAML__10-S1__Boosting.pdf)]

1. [Boosting](#boosting)
2. [Outlook](#outlook)

Back to [index](index.md).

This module turned out a bit longer than usual. Therefore, consider it as both
lecture and exercise. There are many of maybe not so difficult but still
tedious computations involved, which you are encourage to follow with pencil
and paper -- and then, please do not forget to report the typos! Thanks!


## Boosting 

Boosting provides another means to address the bias-complexity tradeoff that we
have discussed in view of the our [model selection
exercise](SS21-MsAML__09-XS2__Model_selection.md).
The general idea is to approach a learning task by training (or usuage of
pre-trained or even engineered) simpler classifiers, which individually might
only deliver a weak classification performance, but also at a potentially
lower computational cost, and combine several of them to build a stronger
classifier. The main goal of this module is to discuss the so-called *adaptive
boosting* algorithm, or *AdaBoost* for short, which is widely used and
well-known for its application in fast object detection in real-time video on
systems with little computational power, see for example, [Viola-Jones object
detection
framework](https://en.wikipedia.org/wiki/Viola%E2%80%93Jones_object_detection_framework)
and the implementation at
[OpenCV](https://docs.opencv.org/3.4/db/d28/tutorial_cascade_classifier.html),
in case you want to play with it.

The road map for this module is, first, to make precise what we mean by a weak
classifier (or learner), second, see how these can be combined to form stronger
ones, and third, define and analyze the AdaBoost algorithm.

For the sake of simplicity, let us assume:

* The case of binary classification, i.e., $|\mathcal Y|=2$;
* $\mathcal F=\mathcal H$, i.e., we do not distinguish between relevant
  hypotheses in $\mathcal F$ and all possible hypotheses in $\mathcal H$;
* A deterministic target concept $c\in{\mathcal Y}^{\mathcal X}$ that we want
  to learn, i.e., the features $(X^{(i)})_{i=1,\ldots,N}$ are i.i.d. according
  to a distribution $D$ but the corresponding labels are given deterministically by
  $\left(Y^{(i)}=c(X^{(i)})\right)_{i=1,\ldots,N}$;
* And finally, the corresponding realizability assumption:
  \begin{align}
    \exists \, h^*\in\mathcal H:\, R(h^*)=0,
  \end{align}
  which we recall to imply for all training data $s\in \bigcup_{N\in\mathbb
  N}(\mathcal X\times\mathcal Y)^n$ that the empirical risk fulfills $\widehat
  R_s(h^*)=0$.

Given this setting, we called a hypotheses class $\mathcal H$ PAC-learnable 
if there exists an algorithm $\mathcal A:s\mapsto h_s$ and a polynomial $p$ such that
\begin{align}
  \forall \quad &\epsilon, \delta\in (0,1]\\
  \forall \quad &\text{distributions $D$ on $\mathcal X$}\\
  \forall \quad &\text{concepts $c$ in $\mathcal H$}\\
  \forall \quad &N\geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  &\\
  & P_{S\sim P^N}(R(h_S)\leq \epsilon) \geq 1-\delta.
\end{align}
where again $P$ is build from the distribution $D$ on $\mathcal X$ times the
Dirac-delta measure $\delta_{Y=c(X)}$ on $\mathcal Y$.

This definition later allowed to give a characterization of PAC-learnability in
terms of the VC-dimension and featured empirical risk minimization as a prime
example for such algorithms. However, it is completely ignorant of the
computational complexity of the algorithm -- somewhat intentionally, as a
detailed treatment thereof would go beyond the scope of this lecture.

Nevertheless, without going too deep into the details of complexity bounds, on
a higher level, we could hope to be able to trade computational complexity for
the accuracy of the learner and then also observe such an behavior in our
derived estimates. Let us therefore assume that even if empirical risk
minimization is a computationally hard task in our setting of interests, it
might still be possible to efficiently find a hypothesis that performs at least
slightly better than random guessing, which then could be combined to form
better performing ones. This gives us at least a qualitative handle on the
involved complexity and this line of thought motivates the following relaxation
of our definition of learnability:

Definition
: ##### ($\gamma$-weak PAC learnability)
  Given $\gamma\in(0,\frac12)$, an algorithm $\mathcal A$ is a $\gamma$-weak
  learner on $\mathcal H$ if there is a polynomial $p(\cdot)$ such that
  \begin{align}
    \forall \quad &\delta\in (0,1]\\
    \forall \quad &\text{distributions $D$ on $\mathcal X$}\\
    \forall \quad &\text{concepts $c$ in $\mathcal H$}\\
    \forall \quad &N\geq p\left(\frac1\delta\right):\\
    &\\
    & P_{S\sim P^N}\left(R(h_S)\leq \frac12-\gamma\right) \geq 1-\delta;
  \end{align}
  recall that, for simplicity, we operate under the assumption of realizability
  here. Furthermore, we say $\mathcal H$ is weakly learnable if there exists
  such a weak learner $\mathcal A$.

Likewise, we will refer to an algorithm $\mathcal A$ fulfilling the
former PAC-learnability definition above as a strong learner. The difference is
small but crucial:

Strong learners are able become arbitrarily good in terms of the actual risk or
error probability while $\gamma$-weak learners only guarantee to become better
than random guessing by an amount parametrized by $\gamma$.

Due to our fundamental theorem of binary classification, also weak learnability
requires $\dim_\text{VC}\mathcal H<\infty$, so that from the statistical
perspective, weak learning is as "hard' as strong learning. As discussed in the
introduction, however, the motivation to consider weak learners comes from the
complexity perspective.

Exploiting the potentially lower computational complexity of weak learners, we
can allow some $T>1$ efficient training runs on the training data $s$, which
provides $T$ hypotheses $h_{1},h_{2},\ldots,h_T$, however, each one only
guaranteeing better than random guessing performance. These found hypotheses
could then be combined to form a combined hypothesis, e.g., through a weighted
majority vote such as
\begin{align}
  h_{s}^T(x) := \operatorname{sign}\left(
    \sum_{t=1}^T w_t h_{t}(x)
  \right),
  \qquad
  \text{for}
  \quad
  x\in\mathcal X,
\end{align}
with conveniently chosen weights;

The obvious question is whether there is a good strategy to choose the weights
$w_t$ and therefore a combined hypothesis $h_s^T$ in order to guarantee a
better performing $h_s^T$ after increasing the number $T$ of iterations.

The idea for doing so is, in step $t$, to present the weak learner i.i.d.
sampled training data from the prescribed training data sample
$s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}$ with different discrete distributions
$d_t=\left(d^{(i)}_t\right)_{i=1,\ldots,N}$ such that samples for which a
preceding $h_{t-1}$ errs get higher weight, and therefore, will also receive more
attention of the weak learner during training of the next $h_{t}$. The weight
$w_t$ must then be chosen to reflect our confidence in the classifier $h_t$ in
some iversely proportional sense to the error probability w.r.t.  Distribution
$d_t$, i.e., 
\begin{align} 
  R_t = \sum_{i=1}^N d_t^{(i)} \, 1_{h_t(x^{(i)})\neq
  c(x^{(i)})}.  
\end{align}

The precise definition of the AdaBoost algorithm turns out as follows:

**Input:**

* Training data $s=\left(x^{(i)},y^{(i)}\right)_{i=1,\ldots,N}$;
* A $\gamma$-weak learner $\mathcal B$;
* An initially uniform distributions $d_1=(d_1^{(i)})_{i=1,\ldots,N}=\left(\frac1 N\right)_{i=1,\ldots,N}$.

**Inner Loop:** For $t\leftarrowtail 1 \text{ to } T$ do:

*(Step 1):* Form an i.i.d. sample $s_t$ from $s$ according to distribution $d_t$
and apply the weak learner $\mathcal B$ to it to find the hypothesis
\begin{align}
  h_t := \mathcal B(s_t).
\end{align}

*(Step 2)*: Compute the risk w.r.t. distribution $d_t$:
\begin{align}
  R_t := \sum_{i=1}^N d_t^{(i)} \, 1_{h_t(x^{(i)})\neq c(x^{(i)})}.
\end{align}

*(Step 3)*: Define the weight
\begin{align}
  w_t := \frac12 \log\left(\frac{1}{R_t}-1\right).
\end{align}

*(Step 4)*: Update the distribution
\begin{align}
  d^{(i)}_{t+1} := 
  \frac{
    d^{(i)}_t \exp\left(
      -w_t \, c(x^{(i)} \, h_t(x^{(i)})
    \right)
  }
  {
    \sum_{j=1}^N
    d^{(j)}_t \exp\left(
      -w_t \, c(x^{(j)} \, h_t(x^{(j)})
    \right)
  }.
  \label{eq_1}
  \tag{E1}
\end{align}

**Output:** The weighted majority vote classifier
\begin{align}
  h_s^T(\cdot) := \operatorname{sign}\left(
    \sum_{t=1}^T w_t h_t(\cdot)
  \right).
\end{align}

The choices for $w_t$ and $d_t$ are chosen carefully to allow for the following
control of the empirical risk:

Theorem
: ##### (AdaBoost empirical error bound)
  For the error loss function, for training data any $s\in(\mathcal
  X\times\mathcal Y)^N$, and number of iterations $T\in\mathbb N$, we have 
  \begin{align}
    \frac1N\sum_{i=1}^N 1_{h_s^T(x^{(i)})\neq c(x^{(i)})} 
    \equiv 
    \widehat R_s(h_s^T) \leq \exp\left(
      -2\sum_{t=1}^T\left(\frac12-R_t\right)^2
    \right).
  \end{align}

Note that, given $\mathcal B$ is a $\gamma$-weak learner, the bound
\begin{align}
  \forall t=1,\ldots, T: \, R_t \leq \frac12-\gamma
\end{align}
implies
\begin{align}
  \widehat R_s(h_s^T)\leq \exp(-2\gamma^2 T),
\end{align}
i.e., an exponential decrease in the number of runs $T$ of the empirical error
running the AdaBoost to provide $h_s^T$.

This decrease comes at the cost of a more complex $h_s^T$ since more and more
weak classifiers enter the weighted majority vote. In this sense, $T$ can be
understood as a fine tuning parameter that controls the bias-complexity
trade-off.

Note also that due to the definition of the $\gamma$-weak learner,
\begin{align}
  R_t \leq \frac12-\gamma
\end{align}
is only guaranteed with probability of at least $1-\delta$, or put
differently, may simply fail with probability smaller than $\delta$. Hence, by
the union bound, the probability that the weak learner does not fail a single
time is only bigger or equal
\begin{align}
  1-T\delta.
\end{align}
Therefore, the promised exponetial decrease relies on the smallness of $T\delta$. 

In practice, the AdaBoost algorithm performs rather well nonetheless. First,
the required sample complexity $N>p(1/\delta)$ is typically only logarithmic in
$1/\delta$ such that small $T\delta$ can often be arranged, and second, each
time the weak learner is applied to the same sample data $s$ only with a
different discrete distribution $d_t$ one can often reduce the probability of
failure $\delta$ to even zero.

Next, let us understand better the special choices made for $w_t$ and $d_t$:

**Proof of [Theorem (AdaBoost empirical error bound)](#adaboost-empirical-error-bound):**
Given the training data $s$ and number of iterations $T\in\mathbb N$, we start
by observing the bound
\begin{align}
  \forall x\in\mathcal X:
  \qquad
  1_{h_s^T(x)\neq c(x)} \leq \exp\left(-c(x)\,f_s^T(x)\right),
\end{align}
for
\begin{align}
  f_s^T(x):=\sum_{t=1}^T w_t h_t(x).
\end{align}
The latter can be employed to show that the empirical error is bound by
\begin{align}
  \widehat R_s(h_s^T)
  &\equiv
  \frac1N\sum_{i=1}^N 1_{h_s^T(x^{(i)})\neq c(x^{(i)})}\\
  &\leq 
  \frac1N\sum_{i=1}^N \exp\left(-c(x^{(i)})\,f_s^T(x^{(i)})\right) =: Z_T.
\end{align}
It remains to estimate $Z_T$. For this purpose, for $t=1,\ldots,T-1$, we regard
\begin{align}
  \frac{Z_{t+1}}{Z_t}
  =
  \frac{
    \sum_{i=1}^N \exp\left(-c(x^{(i)})\,f_s^{t+1}(x^{(i)})\right)
  }
  {
    \sum_{i=1}^N \exp\left(-c(x^{(i)})\,f_s^{t}(x^{(i)})\right)
  }
  .
  \label{eq_2}
  \tag{E2}
\end{align}

By definition, we have
\begin{align}
  f_s^{t+1}(\cdot) := f_s^t(\cdot) + w_{t+1}(\cdot) h_{t+1}(\cdot)
\end{align}
and by induction
\begin{align}
  d^{(i)}_{t+1} := 
  \frac{
    \exp\left(
      -c(x^{(i)}) \, f_s^t(x^{(i)})
    \right)
  }
  {
    \sum_{j=1}^N
    \exp\left(
      -c(x^{(j)}) \, f_s^t(x^{(j)})
    \right)
  }
  .
\end{align}

This implies
\begin{align}
  \eqref{eq_2}
  &=
  \frac{
    \sum_{i=1}^N 
    \exp\left(-c(x^{(i)})\,f_s^t(x^{(i)})\right)
    \exp\left(-c(x^{(i)})\,w_{t+1}\,h_{t+1}(x^{(i)})\right)
  }{
    \sum_{i=j}^N \exp\left(-c(x^{(j)})\,f_s^t(x^{(j)})\right)
  }
  \\
  &=
  \sum_{i=1}^N d_{t+1}^{(i)} 
  \exp\left(-c(x^{(i)})\,w_{t+1}\,h_{t+1}(x^{(i)})\right)
  \\
  &=
  \sum_{\substack{i=1,\ldots, N:\\c(x^{(i)}) h_{t+1}(x^{(i)}) = +1}} 
  e^{-w_{t+1}}
  d_{t+1}^{(i)} 
  +
  \sum_{\substack{i=1,\ldots, N:\\c(x^{(i)}) h_{t+1}(x^{(i)}) = -1}} 
  e^{+w_{t+1}}
  d_{t+1}^{(i)} 
  \label{eq_3}
  \tag{E3}
\end{align}

Recall that each $d_t$ is a probability distribution, and hence, fulfills
$\sum_{i=1}^Nd^{(i)}_t = 1$. Furthermore, we have
\begin{align}
  R_{t+1} 
  &= \sum_{i=1}^N d^{(i)}_t \, 1_{h_{t+1}(x^{(i)})\neq c(x^{(i)})}
  \\
  &= \sum_{i=1}^N d^{(i)}_t \, 1_{ c(x^{(i)})\, h_{t+1}(x^{(i)}) = -1}.
\end{align}

This allows to recast $\eqref{eq_3}$ as follows
\begin{align}
  \eqref{eq_3}
  = 
  e^{-w_{t+1}}
  \left(1-R_{t+1}\right)
  +
  e^{w_{t+1}}
  R_{t+1}.
  \label{eq_4}
  \tag{E4}
\end{align}

Finally, thanks to the definition of the weights
\begin{align}
  w_{t+1} = \frac12\log\left(\frac{1}{R_{t+1}}-1\right),
\end{align}
we find
\begin{align}
  \eqref{eq_4}
  &=
  \frac{1-R_{t+1}}
  {\left(\frac{1}{R_{t+1}}-1\right)^{1/2}}
  +
  \left(\frac{1}{R_{t+1}}-1\right)^{1/2}
  R_{t+1}
  \\
  &=
  \frac{
    1-R_{t+1}+\left(\frac{1}{R_{t+1}}-1\right)R_{t+1}
  }{
    \left(\frac{1}{R_{t+1}}-1\right)^{1/2}
  }
  \\
  &=
  2\sqrt{
    R_{t+1} (1 - R_{t+1})
  }.
\end{align}

This gives rise to the following bound
\begin{align}
  Z_T 
  &= \frac{Z_T}{Z_{T-1}}\frac{Z_{T-1}}{Z_{T-2}} \ldots \frac{Z_1}{Z_0}
  \\
  &= \prod_{t=1}^T 
  2\sqrt{
    R_{t} (1 - R_{t})
  }
  \\
  &= \prod_{t=1}^T 
  2\sqrt{
    \left(\frac12-\left(\frac12 - R_t\right)\right)
    \left(\frac12+\left(\frac12 - R_t\right)\right)
  }
  \\
  &=
  \prod_{t=1}^T 
  \sqrt{1-4\left(\frac12 - R_t \right)^2}
  \\
  &=
  \exp\left(
    -2\sum_{t=1}^T \left(\frac12 - R_{t}\right)^2
  \right),
\end{align}
where, for $x\in\mathbb R$, we have made use of $1-x\leq e^{-x}$.

$\square$

So far we can control the empirical error of the output classifier $h_s^T$ of
the AdaBoost algorithm. Next, we turn to the actual risk, for which we can
apply our VC-dimension machinery in order to derive learning guarantees.

Note that $h_s^T$ does not necessarily lie in $\mathcal H$. The latter is
rather to be considered a "base" class of hypotheses from which the output
classifier of the AdaBoost algorithm is then constructed. The range of the
AdaBoost algorithm is a subset of
\begin{align}
  \mathcal F^T_{\mathcal H}
  :=
  \left\{
    h(\cdot)=
    \operatorname{sign}
    \left(
      \sum_{t=1}^T
      w_t\, h_t(\cdot)
    \right)
    \,\Bigg|\,
    w_t\in\mathbb R, h_t\in\mathcal H, t=1,\ldots, T
  \right\}.
\end{align}

In terms of $T$, the VC-dimension of this set scales as follows:

Theorem
: ##### (VC-dimension bound for AdaBoost)
  Given a base class of hypotheses $\mathcal H$, $T\geq 3$, 
  $3\leq d=\dim_{\text{VC}}<\infty$, the following bound holds true:
  \begin{align}
    \dim_{\text{VC}} \mathcal F^T_{\mathcal H}
    \leq T(d+1)\left(3\log (T(d+1)) + 2\right).
  \end{align}

**Proof:** Say, $P=(x_1,\ldots,x_k)$ for $k$ distinct points $x_i\in\mathcal X$
is shattered by
$\mathcal F^T_{\mathcal H}$, i.e.,
\begin{align}
  \left|\mathcal F^T_{\mathcal H}|_P\right| = 2^{|P|},
\end{align}
where we again slightly abuse the set and tuple notations. Recall further
that, given the tuple $P$, the bit patterns generated by hypotheses in $\mathcal F^T_{\mathcal H}$ were defined by 
\begin{align}
  \mathcal F^T_{\mathcal H}|_P = \left\{
    (h(x_1),\ldots, h(x_k))
    \,\big|\,
    h\in\mathcal F^T_{\mathcal H}
  \right\}.
\end{align}

Moreover, $h$ is build by choosing $T$ hypotheses $h_1,\ldots, h_T$ by means of
a linear classifier, such as the ones used in our Adaline models,
\begin{align}
  h(x) = \operatorname{sign}\left(
    \sum_{t=1}^T w_t\, h_t(x)
  \right)
\end{align}
where the wights $(w_t)_{t=1,\ldots,T}\in\mathbb R^T$ can be interpreted as
hyperplane parameters that define the decision boundary.  Since
$\dim_\text{VC}\mathcal H=d$, [Sauer's
Lemma](SS21-MsAML__07-S2__VC-Dimension.md) implies that there are at most
\begin{align}
 \left(\frac{ek}{d}\right)^d,
 \qquad
 \text{for}
 \quad
 k>d
\end{align}
many patterns in $\mathcal H|_P$. In order to generate all patterns in
$\mathcal F^T_{\mathcal H}$, we need to choose $T$ hypotheses of at most
$\left(\frac{ek}{d}\right)^d$ many, i.e., at most
\begin{align}
 \left(\frac{ek}{d}\right)^{Td}
\end{align}
many.

The space of linear classifiers
\begin{align}
  \mathcal L^T=
  \left\{
    h(\cdot)=\operatorname{sign}(w\cdot x)
    \,\big|\,
    w\in\mathbb R^T
  \right\}
\end{align}
can be shown to have
\begin{align}
  \dim_\text{VC}\mathcal L^T \leq T,
\end{align}
following the [VC-dimension computation tools module](SS21-MsAML__08-S1__Computational_tools_VC-dimension.md).

This implies an upper bound on the total number of potential patterns given by
the product
\begin{align}
 \left(\frac{ek}{d}\right)^{d T}
 \cdot
 \left(\frac{ek}{T}\right)^{T}.
\end{align}

For $T,d\geq 3$ we can estimate this quantity by a more handy one:
\begin{align}
 \left(\frac{ek}{d}\right)^{d T}
 \cdot
 \left(\frac{ek}{T}\right)^{T}
 \leq 
 k^{T(d+1)}.
\end{align}

These estimates were based on the shattering condition of the $k$ points in $P$.
Recall the definition of the VC-dimension: In order to estimate the VC-dimension of
$\mathcal F^T_{\mathcal H}$, we need to see for which number of points $m$ in a
tuple $P'$, the shattering condition can be fulfilled, i.e., for which $m$ we have:
\begin{align}
  2^m \leq m^{T(d+1)}
  \qquad\Leftrightarrow 
  \qquad
  m\leq 
  \log\left(
    m\frac{T(d+1}{\log 2}
  \right).
  \label{eq_5}
  \tag{E5}
\end{align}

A sufficient condition for $\eqref{eq_5}$ to hold true is
\begin{align}
  m 
  &\leq
  2\frac{T(d+1)}{\log 2}
  \log\frac{T(d+1)}{\log2}
  \\
  &\leq
  T(d+1) \left(
    3\log\left(
    T(d+1)
    \right)
    +2
  \right)
  ,
\end{align}
which follows from similar arguments as were used in our [PAC learning review
exercises](SS21-MsAML__09-XS1__PAC_learning_review.md) and conludes our proof.

$\square$

In summary, we have shown that the VC-dimension of the space in which the range
of the AdaBoost algorithm lies only grows slightly faster that linearly with
$T$ while the empirical errors decrease exponentially.

👀
: Now we have all the ingredients needed to compute a generalization bound for
  the AdaBoost algorithm. Give such a generalization bound and discuss the
  fine-tuning between lower risk and larger the complexity by means of $T$.


## Outlook

This closes our survey on the PAC learning framework and supervised learning by
means of prescribed training data. We have only scratched the surface
but at least may claim that we saw several basic ideas and modes of thinking that
permit to derive rigorous statistical guarantees for learning in quite
sophisticated settings. 

For the rest of this course, we may now allow us some fun, and look into a
rather special case of supervised learning, the so-called *reinforcement
learning* case. The latter has the advantages that high quality training data
does not have to be provided before starting the learning loop but it is
generated by continuous feedback of environment in response to actions of a
learner.

➢ Next session!
