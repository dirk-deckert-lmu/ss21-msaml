# Exercise 04-XS1: Learning the concept of intervals [[PDF](SS21-MsAML__04-XS1__Learning_the_concept_of_intervals.pdf)]

Consider the following guessing game: 

* Alice picks i.i.d random variables $X_1,\dots X_N\in \mathbb{R}$ and fixes an interval $I=[a,b]\subset \mathbb{R}$ to define the concept $c:\mathbb{R}\to\{0,1\}$ that distinguishes points $x\in I$ from $x \notin I$, i.e. $c(x) = \mathbb{1}_{[a,b]}$.
* She lets Bob know the training data $S=(x^{(i)},c(x^{(i)}))_{i=1,\dots N}$ and asks him to guess the interval $I$.

Show that the concept class of all intervals is PAC-learnable by providing an explicit lower bound on $N$ to ensure desired accuracy and confidence.