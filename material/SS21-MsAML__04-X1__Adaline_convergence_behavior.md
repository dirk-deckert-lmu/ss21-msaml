# Exercise 04-X1: Adaline convergence behavior [[PDF](SS21-MsAML__04-X1__Adaline_convergence_behavior.pdf)]

We have already considered the convergence of Adaline informally in the last week's exercises. This exercise aims to make the previos discussion more formal.

Given a loss function $L$, the update rule for the Adaline algorithm is given by 

$$\bar{w} \to \bar{w}^{new} := \bar{w} - \eta \frac{\partial \hat{L}_{\bar{w}}}{\partial\bar{w}}$$

for empirical loss 
$\hat{L}_{\bar{w}} = \frac{1}{N}\sum_{i=1}^N L(y^{(i)}, h^{\alpha}_{\bar{w}}(x^{(i)}))$.

(a) For $L(y,y^{'})=\frac{1}{2}|y-y^{'}|^2$ and $\alpha(z)=z$, compute the update rule explicitly.

(b) Implement the Adaline algorithm for the choice of $L$ and $\alpha$ as given in (a).

Note that only the *train()* method of our Perceptron implementation needs to be adapted accordingly. Per epoch, compute the gradient $\dfrac{\hat{L}_{\bar{w}}}{\partial\bar{w}}$ using the explicit form derived in (a) and perform the update.

(c) Render a sequence of plots or an animation during the training as we did for the Perceptron for various initial model parameters $\bar{w}=(b,w)$ and learning rate $\eta$. Plot the value of $\hat{L}_{\bar{w}}$ as well as the number of errors per epoch. Observe the behavior for large and small $\eta$.

(d) Prove that in the setting of (a) and for training data $S=(x^{(i)},y^{(i)})_{i=1,\dots N}$ s.t. 

$$\forall \bar{a} \in \mathbb{R}^{d+1}\backslash\{0\} \  \exists i\in \{1,\dots ,N\}: \bar{a}\cdot\bar{x}^{(i)} \neq 0,$$

$\hat{L}_{\bar{w}}$ has a unique global minimum.

e) In the same setting as in (d) give a bound on $\eta$ that after the Adaline update $\bar{w} \to \bar{w}^{new}$ ensures 

$$\hat{L}(\bar{w}^{new}) < \hat{L}(\bar{w})$$.


(g) Later on $\alpha$ will be chosen a non-linear function and several Adalines will be concatenated to form neural networks. As a result $\hat{L}$ as a function of the model parameters will typically be much more complex, of course. 

Try to find an example for some loss function $L:\mathbb{R}\to\mathbb{R}, w\to L(w)=\dots?$ of your choice with two local mimima such that the update rule 

$$w \to w - \eta L^{'}(w)$$

for certain initial $w$ and $\eta$ oscillates between the two local minima.

This conclusively shows that convergence is not guaranteed using the gradient descent algorithm.
Nevertheless, gradient descent is what makes modern deep learning computationally possible as it is much less computationally expensive and, as we will see, much more generally applicable for varying network architectures as any other optimization algorithm.






















