% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-06-s1-pdf}{%
\section{\texorpdfstring{MsAML-06-S1
{[}\href{SS21-MsAML__06-S1__Infinite_hypotheses.pdf}{PDF}{]}}{MsAML-06-S1 {[}PDF{]}}}\label{msaml-06-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{infinite-hypotheses-case}{Infinite hypotheses case}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{infinite-hypotheses-case}{%
\subsection{Infinite hypotheses case}\label{infinite-hypotheses-case}}

Until now we have derived our generalization bounds from the assumption
that our set of hypotheses consisted of a finite dictionary only, i.e.,
\(\mathcal H\subset \mathcal Y^{\mathcal X}\) with
\(|\mathcal H|<\infty\).

Our generalization bounds that we have used for the given learning
guarantees were of the type \begin{align}
  |\widehat R_S(h) - R(H) |
  \leq 
  \text{some function of }
  \left(\log|\mathcal F|,\frac1\epsilon,\frac1\delta\right)
\end{align} with probability of at least \(1-\delta\).

First, one may argue that, for algorithms \(\mathcal A:s\to h_s\)
implemented on a computer, at least the range
\(\mathcal F\subseteq \mathcal H\) of \(\mathcal A\) will naturally be
finite due to the discretization and finiteness of the data storage
resources. One may even interpret the quantity \begin{align}
  \log_2|\mathcal F|
\end{align} as the amount of bits needed to encode one hypothesis, and
in turn, \(|\mathcal F|\) as the storage space that has to be searched
in order to find a good classifier. Thus, one could already tend to be
satisfied with our previous assertions about learnability. However, even
for the finite case, \(\log|\mathcal F|\) will typically be extremely
large and for practical purposes our bounds are not very useful
quantitatively.

Furthermore, we have already seen an example in the exercises about the
learnability of the concept of intervals which involved an infinitely
large space of hypotheses and proved that we can do better.

In order to do better, it is important to observe that the complexity of
the learning task is often much lower when compared to the bare number
of possible hypotheses in \(\mathcal H\) or \(\mathcal F\). Consider,
for instance, the Adaline, which does not directly pick a classifier
\(h:\mathcal X\to\mathcal Y\) in \(\mathcal H\) but instead picks only a
\(w\in\mathbb R^d\) and \(b\in\mathbb R\). Even the latter would make up
infinite choices but, in principle, only those matter that give rise to
some partition of the training data into two sets.

In conclusion, we need to find a better way a measuring the complexity
of the learning task than just looking at \(|\mathcal F|\), and in our
proofs, we have to replace the uniform sum of the elements of
\(\mathcal F\). There are two routes. First, we could introduce a
relevant metric structure on \(\mathcal H\) and attempt to argue along
the lines ``If two hypotheses are close in this metric, then also the
derived classification is close.'' Second, we could employ a
combinatorial argument, such as the one about the partitions mentioned
above, reducing the infinite set of hypotheses to a finite set of
hypotheses classes that reflect better the complexity of the learning
task. The combinatorial route, although at times more coarse, has the
advantage to be more independent of the learning algorithm itself since
the latter may directly influence the choice of a relevant metric. This
is the route we will follow next.

In the considered setting, let \(\mathcal H\) be the potentially
infinite set of hypotheses \(\mathcal Y^{\mathcal X}\), and in order to
add even more generality to our analysis let us regard a general loss
function \begin{align}
  L:\mathcal Y\times\mathcal Y \to\mathbb R,
\end{align} instead of only regarding the choice
\(L(y,y')=1_{y\neq y'}\). Furthermore, we define \begin{align}
  \mathcal G \equiv \mathcal G^L_{\mathcal H}  
\end{align} to consist of functions \begin{align}
  g(x,y) := L(y,h(x))
\end{align} for each \(h\in\mathcal H\). We will silently assume
sufficiently nice properties so that functions of \(g\in\mathcal G\) and
their respective suprema over \(\mathcal G\) are measurable, which in
applications can typically be arranged for.

In order to shorten our notation, let us introduce \begin{align}
  \mathcal Z:=\mathcal X\times\mathcal Y,
\end{align} as well as, \(Z\), \(Z^{(i)}\) for \(i=1,\ldots,N\)
i.i.d.~and \(\mathcal Z\)-valued random variables, such that we may
denote our training data random variable by
\(S=(Z^{(i)})_{i=1,\ldots N}\), and any of its realization by
\(s=(z^{(i)})_{i=1,\ldots, N}\).

We start again from the top, e.g., from our discussion of the
\href{SS21-MsAML__02-S1__Error_decomposition.pdf}{error decomposition}.
The quantity we will attempt to control is \begin{align}
  R(\widehat h_S) - R_{\mathcal F} 
  \leq 2\sup_{h\in\mathcal F}
  \left|
    \widehat R_S(h)-R(h)
  \right|,
\end{align} where as before \(h_S=\mathcal A(S)\),
\(\mathcal F\subseteq \mathcal H\) is a relevant but now potentially
infinite set of hypotheses, and \(\widehat h_S\) is the minimizer of the
empirical risk. Recall that for this to hold we have to assume that our
algorithm \(\mathcal A\) always succeeds in the task of empirical risk
minimization as we did before.

For training data \(S\), \(h\in\mathcal F\subseteq \mathcal H\), and
\begin{align}
  g_h(x,y):=L(y,h(x))
\end{align} in \(\mathcal G^L_{\mathcal F}\) we coarse grain the
dependence on a particular \(h\) by means of the estimate \begin{align}
    R(h)-\widehat R_S(h)
  &=
    E_Z g_h(Z) - \frac1N\sum_{i=1}^N g_h(Z^{(i)})
  \\
  &\leq
  \sup_{g\in\mathcal G^L_{\mathcal F}} 
    \left[\frac1N\sum_{i=1}^N g(Z^{(i)})-E_Z g(Z)\right]
  =: \Phi(S).
  \label{eq_phi}
  \tag{E1}
\end{align} The goal will now be to derive a meaningful estimate fort
\(\Phi(S)\).

For this purpose, there is a nice generalization of Hoeffding's
inequality, i.e., the so-called \emph{bounded differences} inequality:

\begin{description}
\item[Lemma] ~ 
\hypertarget{bounded-differences}{%
\subparagraph{(Bounded differences)}\label{bounded-differences}}

For an \(\mathbb R\)-valued function \(f\), let there be constants
\(c_i\), \(i=1,\ldots,n\), such that \begin{align}
\sup_{x_1,\ldots,x_n, \tilde x_i}
\left|
  f(x_1,\ldots,x_i,\ldots,x_n)-f(x_1,\ldots, \tilde x_i,\ldots, x_n)
\right|
\leq c_i.
  \end{align} Then, the following estimates hold true for all
\(\epsilon>0\): \begin{align}
P_{X_1,\ldots, X_n}\left(
  \pm\left[
    f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
  \right] 
  \geq \epsilon
\right)
\\
\leq 
\exp\left(
  -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
\right),
  \end{align} and in turn, \begin{align}
P_{X_1,\ldots, X_n}\left(
  \left|
    f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
  \right| \geq \epsilon
\right)
&\leq 
2\exp\left(
  -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
\right).
  \end{align}
\end{description}

\textbf{Proof:} Define \(\mathcal F_i:=\sigma(X_1,\ldots, X_i)\), i.e.,
the filtration of \(\sigma\)-algebras generated by the random variables
\(X_1,\ldots, X_n\).

For the sake of compactness of the notation, let us suppress the
subscripts in \(P_{X_1,\ldots, X_N}\) and \(E_{X_1,\ldots, X_N}\) in our
notation.

We form the marginal differences \begin{align}
  \Delta_i := 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_i\right) 
    - 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right)
\end{align} which implies \begin{align}
  &E(\Delta_i\,|\, \mathcal F_{i-1})
  \\
  &=
  E\left(
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_i\right) 
    - 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right)
  \,\Big|\, \mathcal F_{i-1}\right)
  \\
  &=
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right) 
    - 
    E\left(f(x_1,\ldots,x_n)\,\big|\, \mathcal F_{i-1}\right)
    \\
  &=
  0,
\end{align} where we have use the linearity of the conditional
expectation and the fact that expectation with respect to
\(\mathcal F_{i-1}\subset\mathcal F_i\) wins over the one with repsect
to \(\mathcal F_i\), i.e.,
\(E(E(\cdot\,|\,\mathcal F_{i-1})\,|\,\mathcal F_i)=E(\cdot\,|\,\mathcal F_{i-1})=E(E(\cdot\,|\,\mathcal F_i)\,|\,\mathcal F_{i-1})\).

For \(\epsilon>0\) we find \begin{align}
    P\left(
      \left|
        f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
      \right|
      \geq \epsilon
    \right)
    =
    P\left(\left|\sum_{i=1}^N\Delta_i\right|\geq \epsilon\right).
    \label{eq_2}
    \tag{E2}
\end{align}

Next, we observe that \begin{align}
  \Delta_i 
  &\leq
  E\left(
    \sup_{x_i}
    f(X_1,\ldots,x_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_i
  \right)
  \\
  &\qquad\qquad -
  E\left(
    f(X_1,\ldots,X_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_{i-1}
  \right)
  \\
  &=
  E\left(
    \sup_{x_i}
    f(X_1,\ldots,x_i,\ldots,X_n)
    -
    f(X_1,\ldots,X_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_{i-1}
  \right)
  \\
  &=:B_i,
\end{align} and similarly, \begin{align}
  \Delta_i 
  &\geq 
  E\left(
    \inf_{x_i}
    f(X_1,\ldots,x_i,\ldots,X_n)
    -
    f(X_1,\ldots,X_i,\ldots,X_n)
    \,\Big|\,
    \mathcal F_{i-1}
  \right)
  \\
  &=:A_i.
\end{align}

Note that \(A_i, B_i\) are \(\mathcal F_{i-1}\) measurable random
variables but we can readily bound their differences using our
assumption on \(f\) by means of \begin{align}
  \left\|
    B_i-A_i
  \right\|_\infty
  \leq c_i.
\end{align}

Coming back to the right-hand side of \(\eqref{eq_2}\), we follow
similar arguments as in our proof of Hoeffding's inequality:
\begin{align}
  P\left( \sum_{i=1}^N\Delta_i \geq \epsilon\right)
  &\leq
  e^{-t \epsilon} E e^{t\sum_{i=1}^N\Delta_i}
  \\
  &=
  e^{-t \epsilon} E E\left(
    e^{t\sum_{i=1}^N\Delta_i}
    \,\big|\,
    \mathcal F_{N-1}
  \right)
  \\
  &=
  e^{-t \epsilon} E e^{t\sum_{i=1}^{N-1}\Delta_i} E\left(
    e^{t \Delta_N}
    \,\big|\,
    \mathcal F_{N-1}
  \right)
\end{align} since all \(\Delta_1,\ldots, \Delta_{N-1}\) are
\(\mathcal F_{N-1}\) measurable.

We may now apply the bound \begin{align}
  E e^{tZ} \leq \exp\left(\frac{(b-a)^2}{8}t^2\right)
  \qquad
  \text{for}
  \qquad a \leq Z\leq b,
\end{align} which was derived as an intermediate result in our proof of
Hoeffding's inequality -- therefore, often referred to as
\emph{Hoeffding's lemma}. Observe that conditioning on
\(\mathcal F_{n-1}\) does not impact the strategy of proof since
\(E(\Delta_i\,|\,\mathcal F_{n-1})=0\), under conditioning of
\(\mathcal F_{n-1}\) the random variables \(X_1,\ldots, X_{n-1}\) can be
regarded as fixed quantities, and we may choose \(a=A_n, b=B_n\).

This implies \begin{align}
  P\left(\sum_{i=1}^N\Delta_i\geq \epsilon\right)
  &\leq
  e^{-t\epsilon} E e^{t\sum_{i=1}^{n-1}\Delta_i} e^{\frac{(B_n-A_n)^2}{8}t^2}
\end{align} and by induction \begin{align}
  P\left(\sum_{i=1}^N\Delta_i\geq \epsilon\right)
  &\leq
  e^{-t\epsilon} \exp\left(
    \sum_{i=1}^n \frac{\|B_i-A_i\|^2_\infty}{8} t^2
  \right)
  \\
  &\leq
  e^{-t\epsilon} \exp\left(
    \sum_{i=1}^n \frac{c_i^2}{8} t^2
  \right)
\end{align} The latter bound can again be sharpened by optimizing over
\(t\) so that we find \begin{align}
  P\left(\sum_{i=1}^N\Delta_i\geq \epsilon\right)
  &\leq
  \exp\left(
    -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
  \right).
\end{align} Note that the same bound can be proven for
\(P\left(-\sum_{i=1}^N\Delta_i\geq \epsilon\right)\) so that we can
exploiting the union bound to conclude \begin{align}
  \eqref{eq_2}
  &\leq
  2
  \exp\left(
    -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
  \right)
\end{align} or equivalently \begin{align}
    P_{X_1,\ldots, X_n}\left(
      \left|
        f(X_1,\ldots,X_N)-E f(X_1,\ldots, X_N)
      \right| \geq \epsilon
    \right)
  &\leq
  2
  \exp\left(
    -\frac{2\epsilon^2}{\sum_{i=1}^n c_i^2}
  \right).
\end{align}

\(\square\)

\begin{description}
\tightlist
\item[Remark]
This slightly more general estimate, exploiting the marginal
differences, is often referred as \emph{Azuma-Hoeffding} inequality. The
special case for uniformly bounded differences, as we have used above,
is called \emph{McDiarmid} inequality.
\end{description}

After this excursion, we can return to our original expression of
interest \(\Phi(S)\) in \(\eqref{eq_phi}\). Let
\(\tilde S=(\tilde Z^{(i)})_{i=1,\ldots,N}\) be another random variable
sampling the training data like \(S=(Z^{(i)})_{i=1,\ldots,N}\) such that
only the component \(\tilde Z^{(N)}\) differs from \(Z^{(N)}\) and the
other components are equal, i.e., \(Z^{(i)}=\tilde Z^{(i)}\) for
\(i=1,\ldots,N\).

Regarding the following difference, we find \begin{align}
\left|
  \Phi(S)-\Phi(\tilde S)
\right|
&\leq
\sup_{g\in\mathcal G}
\frac{\left|
  g(Z^{(N)})
  -
  g(\tilde Z^{(N)})
\right|}{N}
\\
&\leq
\sup_{g\in\mathcal G}
\sup_{z,\tilde z}
\frac{\left|
  g(z)
  -
  g(\tilde z)
\right|}{N}
=:\frac C N
\end{align} as long as \(g\) is bounded. This allows to apply
McDiarmid's inequality which states \begin{align}
  P\left(
      \Phi(S)-E_S\Phi(S)
    \geq \epsilon
  \right)
  \leq
  \exp\left(
    -\frac{2\epsilon^2 N}{C^2}
  \right).
\end{align}

For \begin{align}
  \exp\left(
    -\frac{2\epsilon^2 N}{C^2}
  \right)
  =
  \delta
\end{align} we get \begin{align}
  \epsilon= C\sqrt{
    \frac{\log\frac 1\delta}{2N}
  }
\end{align} and we can recast our first intermediate result into the
form \begin{align}
  R(h)
  - \widehat R_S(h) 
  \leq
  \Phi(S)
  \leq E_S\Phi(S) + C\sqrt{
    \frac{\log\frac 1\delta}{2N}
  }
  \label{eq_intermediate}
  \tag{E3}
\end{align} with probability of at least \(1-\delta\).

The right-hand side is still implicit due to the implicit expectation
\(E_S \Phi(S)\). To gain more explicit control on the right-hand side we
apply a ``symmetrization trick'' often used in statistics or probability
theory. For this purpose, let us introduce another random variable
\(\tilde S\) sampling potential training data in the same way as does
\(S\) and compute \begin{align}
  E_S\Phi(S)
  &=
  E_S\sup_{g\in\mathcal G}
  \left[
    E_Z g(Z)
    - \frac1N\sum_{i=1}^N g(Z^{(i)})
  \right]
  \\
  &=
  E_S\sup_{g\in\mathcal G}
  \left[
    E_{\tilde S} \frac1N\sum_{i=1}^N g(\tilde Z^{(i)})
    - \frac1N\sum_{i=1}^N g(Z^{(i)})
  \right]
  \\
  &=
  E_S\sup_{g\in\mathcal G}
  E_{\tilde S}
  \left(
    \frac1N\sum_{i=1}^N g(\tilde Z^{(i)})
    - \frac1N\sum_{i=1}^N g(Z^{(i)})
    \,\Bigg|\,
    S
    \right).
  \label{eq_4}
  \tag{E4}
\end{align} Using the sub-additivity of the supremum gives \begin{align}
  \eqref{eq_4}
  \leq
  E_{S,\tilde S} 
  \sup_{g\in\mathcal G}
  \frac1N 
    \sum_{i=1}^N\left[
      g(\tilde Z^{(i)})
      -
      g(Z^{(i)})
    \right].
  \label{eq_5}
  \tag{E5}
\end{align} Thanks to the symmetry in interchanging \(S\) and
\(\tilde S\), which only changes the sign of the differences under the
sum, we may simply also introduce another random variable
\(\sigma=(\sigma_1,\ldots, \sigma_N)\), each \(\sigma_i\) taking values
in \(\{-1,+1\}\) with distribution \begin{align}
  P(\sigma_i=-1) = \frac 12 = P(\sigma_i=+1),
\end{align} and recast the last right-hand side as \begin{align}
  \eqref{eq_5}
  =
  E_{S,\tilde S,\sigma} 
  \sup_{g\in\mathcal G}
  \frac1N 
    \sum_{i=1}^N\sigma_i\left[
      g(\tilde Z^{(i)})
      -
      g(Z^{(i)})
  \right].
  \label{eq_6}
  \tag{E6}
\end{align} The sub-additivity of the supremum and the fact that
\(\sigma_i\) has the same distribution as \(-\sigma_i\) imply
\begin{align}
  \eqref{eq_6}
  \leq
  2E_S E_\sigma\sup_{g\in\mathcal G} \frac 1N
    \sum_{i=1}^N \sigma_i g(Z^{(i)}).
  \label{eq_7}
  \tag{E7}
\end{align}

The last expression under the expectation on the right-hand side
\begin{align}
  \widehat{\mathcal R}_S(\mathcal G_{\mathcal F}^L)
  :=
  E_\sigma\sup_{g\in\mathcal G^L_{\mathcal F}} \frac 1N 
    \sum_{i=1}^N \sigma_i g(Z^{(i)})
\end{align} captures the ``richness'' of the hypotheses contained in
\(\mathcal F\), which was used to generate the set
\(\mathcal G^L_{\mathcal F}\), by measuring how well a hypothesis can
``fit'' the random noise: Writing \(g(S)\) as a vector
\(g(S)=(g(Z^{(1)}),\ldots, g(Z^{(N)}))\) we may recast the latter by
means of the scalar product \(\langle\cdot,\cdot\rangle\) in
\(\mathbb R^N\) as follows \begin{align}
  \widehat{\mathcal R}_S(\mathcal G_{\mathcal F}^L)
  :=
  E_\sigma\sup_{g\in\mathcal G^L_{\mathcal F}} \frac 1N \left\langle
    \sigma, g(S)
    \right\rangle.
\end{align} The inner product encodes the correlation of the vector
\(g(S)\) to the noise \(\sigma\). Hence, given training data \(S\), the
quantity \(\widehat{\mathcal R}_S(\mathcal G^L_{\mathcal F})\) measures,
in the empirical average, how well the functions in
\(g\in\mathcal G^L_{\mathcal F}\) correlate to random noise. The richer
the class of functions in \(\mathcal F\), the richer the functions in
\(\mathcal G^L_{\mathcal F}\), the better the noise can be fitted, and
in turn, the larger the quantity
\(\widehat{\mathcal R}_S(\mathcal G^L_{\mathcal F})\).

This quantity deserves a name:

\begin{description}
\item[Definition] ~ 
\hypertarget{rademacher-complexity}{%
\subparagraph{(Rademacher complexity)}\label{rademacher-complexity}}

Let \(\mathcal G\) be a family of bounded maps
\(\mathcal X\times \mathcal  Y\to\mathbb R\) and let the training data
be of size \(N\). Then, we define the \emph{Rademacher complexity} as
\begin{align}
\mathcal R_N(\mathcal G) = E_S\widehat{\mathcal R}_S(\mathcal G),
  \end{align} where as \(\widehat{\mathcal R}_S(\mathcal G)\) is called
the empirical Rademacher complexity.
\end{description}

Replacing \(\delta \mapsto \frac\delta 2\) in
\(\eqref{eq_intermediate}\), the bound \(\eqref{eq_7}\) on
\(E_S\Phi(S)\) implies \begin{align}
  R(h)
  \leq
  \widehat R_S(h)
  +2\mathcal R_N(\mathcal G^L_{\mathcal F})
  + C \sqrt{\frac{\log\frac2\delta}{2N}}
  \label{eq_8}
  \tag{E8}
\end{align} with probability of at least \(1-\frac\delta2\).

This bound can be made even more explicit employing McDiarmid's
inequality once again to find \begin{align}
  P\left(
    \left|
      \widehat{\mathcal R}_N(\mathcal G)
      -
      \mathcal R_N(\mathcal G)
    \right|
    \geq C\sqrt{\frac{\log\frac2\delta}{2N}}
  \right)
  \leq
  \frac\delta2,
\end{align} since the difference \begin{align}
    \left|
      \widehat{\mathcal R}_S(\mathcal G)
      -
      \widehat{\mathcal R}_{\tilde S}(\mathcal G)
    \right|
    \leq \frac C N
\end{align} is bounded in the case that \(S\) and \(\tilde S\) differ
only in one component.

Combining the latter estimate with \(\eqref{eq_8}\) and observing the
union bound finally results in \begin{align}
  R(h)
  \leq
  \widehat R_S(h)
  +2\widehat{\mathcal R}_S(\mathcal G^L_{\mathcal F})
  + 3C \sqrt{\frac{\log\frac2\delta}{2N}}
\end{align} with probability of at least \(1-\delta\).

As the expectation was replaced by an empirical average, this bound is
more accessible in applications. Note that the computation of
\(\widehat R_S(\mathcal G)\) can be rephrased in terms of a risk
minimization problem of its own because \begin{align}
  \widehat{\mathcal R}_S(\mathcal G)
  =
  -E_\sigma
  \inf_{g\in\mathcal G}
  \frac1N
    \sum_{i=1}^N \sigma_i g(Z^{(i)}),
\end{align} which can be computed for every realization of \(\sigma\) in
\(\{-1,+1\}^N\) and then averaged.

In practice, also this computational task can be quite expensive.
However, in next module and in the special case of binary
classification, we will be able to derive from it an even simpler bound
that will allow us to characterize learnability of binary classification
tasks in the general setting of infinite hypotheses sets.

➢ Next session!

\end{document}
