% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-04-s2-pdf}{%
\section{\texorpdfstring{MsAML-04-S2
{[}\href{SS21-MsAML__04-S2__Concentration_inequalities.pdf}{PDF}{]}}{MsAML-04-S2 {[}PDF{]}}}\label{msaml-04-s2-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{concentration-inequalities}{Concentration
  inequalities}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{concentration-inequalities}{%
\subsection{Concentration
inequalities}\label{concentration-inequalities}}

As announced, we will need a tool to treat the inconsistent PAC-learning
scenario, which we will find in:

\begin{description}
\item[Theorem] ~ 
\hypertarget{hoeffdings-inequality}{%
\subparagraph{(Hoeffding's inequality)}\label{hoeffdings-inequality}}

Let \(X_1,\ldots,X_N\) be independent random variables with
\(\text{range}X_i\subseteq [a_i,b_i]\) for reals \(a_i<b_i\),
\(i=1,\ldots, N\). Then, for the empirical expectation
\(\widehat S_N=\frac1N\sum_{i=1}^N X_i\) and any \(\epsilon>0\) we find:
\begin{align}
P\left(\left| \widehat S_N- E\widehat S_N\right| \geq \epsilon\right) 
\leq 
2\exp\left(-\frac{2\epsilon^2N^2}{\sum_{i=1}^N(b_i-a_i)^2}\right).
  \end{align}
\end{description}

\textbf{Proof:} Let \(\epsilon>0\). First, we regard the probability
\begin{align}
  P(\widehat S_N-E\widehat S_N\geq \epsilon).
\end{align} In order to make use of the moment generating function
\((t,Z)\mapsto \exp(tZ)\) for \(t> 0\) and a real valued random variable
\(Z\), we note that, for \(t>0\), \(z\mapsto \exp(tz)\) is strictly
increasing, and hence, an order preserving bijection. Therefore, for
\(t>0\) \begin{align}
  &P(\widehat S_N-E\widehat S_N\geq \epsilon)
  \\
  &P(N(\widehat S_N-E\widehat S_N)\geq \epsilon N)
  \\
  &=
  P\left(\exp(t(\widehat S_N-E\widehat S_N))\geq \exp(t\epsilon N)\right)
\end{align} holds true.

We apply Markov's inequality to find \begin{align}
  &P\left(\exp(t(\widehat S_N-E\widehat S_N))\geq \exp(t\epsilon N)\right)
  \\
  &\leq
  e^{-t\epsilon N} E\exp\left(tN(\widehat S_N-E\widehat S_N)\right)
  \\
  &=
  e^{-t\epsilon N} E \prod_{i=1}^N \exp\left(t(X_i-EX_i)\right),
\end{align} where in the last step we have only inserted the definition
of the empirical average \(\widehat S_N\) and exploited the properties
of the exponential function.

\begin{description}
\tightlist
\item[👀]
Recall Markov's inequality.
\end{description}

Next, we make use of the independence of the \(X_i\), \(i=1,\ldots,N\),
which allows to recast the right-hand side of the last equation into
\begin{align}
  e^{-t\epsilon} E \prod_{i=1}^N \exp\left(\frac t N(X_i-EX_i)\right)
  = e^{-t\epsilon} \prod_{i=1}^N E \exp\left(\frac t N(X_i-EX_i)\right). 
  \tag{E1}
  \label{eq_1}
\end{align}

We note that the factors are of the form \(E\exp(t Z)\) for a centered
random variable \(Z\), i.e., fulfilling \(EZ=0\). Let us therefore try
to estimate these factors \(E\exp(t Z)\) for a general random variable
\(X\) having \(EZ=0\) and \(\text{range}X\subseteq [a,b]\) fort reals
\(a<b\).

Since the exponential function is convex, for all \(\lambda \in [0,1]\)
we have \begin{align}
  \exp(\lambda z_1+(1-\lambda)z_2)
  \leq \lambda e^{x_1}+(1-\lambda)e^{\lambda z_2}.
\end{align} Setting \begin{align}
  z_1=a, \qquad z_2=b, \qquad \lambda = \frac{b-z}{b-a}
\end{align} implies \begin{align}
  &\exp(tZ)=\exp\left( t\left(t\frac{b-Z}{b-a} a + \frac{Z-a}{b-a} b\right) \right)
  \\
  &\leq
  \frac{b-Z}{b-a}e^{ta}+\frac{-a}{b-a}e^{tb}.
\end{align} Now we can make use of \(EZ=0\) and compute \begin{align}
  E\exp(tZ) \leq \frac{b}{b-a}e^{ta}+\frac{-a}{b-a}e^{tb} =: e^{\phi(t)}.
  \tag{E2}
  \label{eq_2}
\end{align} Let us approximate the right-hand side with a more tangible
expression. Therefore, we regard the function \begin{align}
  \phi(t) = \log\left((1-\alpha) e^{ta}+ \alpha e^{tb}\right)
\end{align} for \begin{align}
  \alpha = \frac{-a}{b-a}
\end{align} which implies \begin{align}
  1-\alpha = \frac{b}{b-a}.
\end{align} In order to expand this expression, let us compute the first
two derivatives \begin{align}
  \phi'(t) = a + \frac{\alpha(b-a)e^{t(b-a}}{(1-\alpha)e^{-t(b-a)}+\alpha}
\end{align} and \begin{align}
  \phi''(t) & = \frac{\alpha(b-a)(1-\alpha)(-1)(b-a)}
                    {\left[
                      (1-\alpha)e^{-t(b-a)}+\alpha
                     \right]^2}e^{-t(b-a)}
  \\
  & = -\frac{\alpha (1-\alpha)e^{-t(b-a)}}
        {\left[
          (1-\alpha)e^{-t(b-a)}+\alpha
         \right]^2}e^{-t(b-a)}
        (b-a)^2
  \\
  & = u(1-u)(b-a)^2
\end{align} for \begin{align}
  u := \frac{\alpha}{(1-\alpha)e^{-t(b-a)}+\alpha} \in [0,1].
\end{align} By Taylor's theorem, for any \(t>0\) there exists an
\(\theta\in(0,t)\) such that the following equality holds \begin{align}
  \phi(t) = \underbrace{\phi(0)}_{=0} + t\underbrace{\phi'(0)}_{=0} 
  + \frac{t^2}{2}\phi''(\theta).
\end{align} The second order derivative can however be bounded by
\begin{align}
  \phi''(\theta) \leq \sup_{u\in[0,1]} u(1-u)(b-a)^2 \leq \frac{(b-a)^2}{4}.
\end{align} Note that the latter bound is uniform in \(t\) and therefore
implies \begin{align}
  \phi(t) \leq \frac{(b-a)^2}{8} t^2.
\end{align}

Recalling our earlier inequality \(\eqref{eq_2}\) we have thus shown
\begin{align}
  E e^{tZ} \leq \exp\left(\frac{(b-a)^2}{8}t^2\right).
\end{align} With this ingredient we may return to our initial expression
\(\eqref{eq_1}\) and compute \begin{align}
  &P\left(\widehat S_N-E\widehat S_N\geq \epsilon\right)
  \\
  &\leq
  e^{-t\epsilon N} \prod_{i=1}^N E\exp\left( t(X_i-EX_i)\right)
  \\
  &\leq
  e^{-t\epsilon N} \prod_{i=1}^N \exp\left( \frac{(b_i-a_i)}{8} t^2 \right)
  \\
  &=
  \exp\left( t^2 \frac{\sum_{i=1}^N (b_i-a_i)}{8} - t \epsilon N\right)
  \tag{E3}
  \label{eq_3}
\end{align} Let us optimize this bound over the values of \(t\). For
this purpose, consider the function \begin{align}
  f(t) = t^2x- ty
\end{align} which is convex, and thus, attains the global minimum at
\begin{align}
  f'(t)=2tx-y=0 \qquad \Rightarrow \qquad t=\frac{y}{2x}.
\end{align} The corresponding value of the global minimum is given by
\begin{align}
  f\left(\frac{y}{2x}\right) = \frac{y^2}{4x}-\frac{y^2}{2x} = -\frac{y^2}{4x}.
\end{align}

Thanks to this computation, we see that \(\eqref{eq_3}\) can be
sharpened to read \begin{align}
  P\left(\widehat S_N-E\widehat S_N\geq \epsilon\right)
  \leq \exp\left(
    -\frac{2\epsilon^2 N^2}{\sum_{i=1}^N (b_i-a_i)^2}
  \right).
\end{align}

Note further that our argument holds true also for \(X_i \mapsto -X_i\)
so that we readily get the additional bound \begin{align}
  P\left(E\widehat S_N-\widehat S_N\geq \epsilon\right)
  \leq \exp\left(
    -\frac{2\epsilon^2 N^2}{\sum_{i=1}^N (b_i-a_i)^2}
  \right).
\end{align}

Putting both bounds together by exploiting the sub-additivity of \(P\)
results in \begin{align}
  P\left(\left|\widehat S_N-E\widehat S_N\right|\leq \epsilon\right)
  \leq
  2 \exp\left(
    -\frac{2\epsilon^2 N^2}{\sum_{i=1}^N (b_i-a_i)^2}
  \right)
\end{align} and concludes our proof.

\(\square\)

In the next session we will see that this is exactly what we need to
provide probabilistic bounds of the difference \(\widehat R_S(h)-R(h)\).

\end{document}
