% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{exercise-04-x1-adaline-convergence-behavior-pdf}{%
\section{\texorpdfstring{Exercise 04-X1: Adaline convergence behavior
{[}\href{SS21-MsAML__04-X1__Adaline_convergence_behavior.pdf}{PDF}{]}}{Exercise 04-X1: Adaline convergence behavior {[}PDF{]}}}\label{exercise-04-x1-adaline-convergence-behavior-pdf}}

We have already considered the convergence of Adaline informally in the
last week's exercises. This exercise aims to make the previos discussion
more formal.

Given a loss function \(L\), the update rule for the Adaline algorithm
is given by

\[\bar{w} \to \bar{w}^{new} := \bar{w} - \eta \frac{\partial \hat{L}_{\bar{w}}}{\partial\bar{w}}\]

for empirical loss
\(\hat{L}_{\bar{w}} = \frac{1}{N}\sum_{i=1}^N L(y^{(i)}, h^{\alpha}_{\bar{w}}(x^{(i)}))\).

\begin{enumerate}
\def\labelenumi{(\alph{enumi})}
\item
  For \(L(y,y^{'})=\frac{1}{2}|y-y^{'}|^2\) and \(\alpha(z)=z\), compute
  the update rule explicitly.
\item
  Implement the Adaline algorithm for the choice of \(L\) and \(\alpha\)
  as given in (a).
\end{enumerate}

Note that only the \emph{train()} method of our Perceptron
implementation needs to be adapted accordingly. Per epoch, compute the
gradient \(\dfrac{\hat{L}_{\bar{w}}}{\partial\bar{w}}\) using the
explicit form derived in (a) and perform the update.

\begin{enumerate}
\def\labelenumi{(\alph{enumi})}
\setcounter{enumi}{2}
\item
  Render a sequence of plots or an animation during the training as we
  did for the Perceptron for various initial model parameters
  \(\bar{w}=(b,w)\) and learning rate \(\eta\). Plot the value of
  \(\hat{L}_{\bar{w}}\) as well as the number of errors per epoch.
  Observe the behavior for large and small \(\eta\).
\item
  Prove that in the setting of (a) and for training data
  \(S=(x^{(i)},y^{(i)})_{i=1,\dots N}\) s.t.
\end{enumerate}

\[\forall \bar{a} \in \mathbb{R}^{d+1}\backslash\{0\} \  \exists i\in \{1,\dots ,N\}: \bar{a}\cdot\bar{x}^{(i)} \neq 0,\]

\(\hat{L}_{\bar{w}}\) has a unique global minimum.

\begin{enumerate}
\def\labelenumi{\alph{enumi})}
\setcounter{enumi}{4}
\tightlist
\item
  In the same setting as in (d) give a bound on \(\eta\) that after the
  Adaline update \(\bar{w} \to \bar{w}^{new}\) ensures
\end{enumerate}

\[\hat{L}(\bar{w}^{new}) < \hat{L}(\bar{w})\].

\begin{enumerate}
\def\labelenumi{(\alph{enumi})}
\setcounter{enumi}{6}
\tightlist
\item
  Later on \(\alpha\) will be chosen a non-linear function and several
  Adalines will be concatenated to form neural networks. As a result
  \(\hat{L}\) as a function of the model parameters will typically be
  much more complex, of course.
\end{enumerate}

Try to find an example for some loss function
\(L:\mathbb{R}\to\mathbb{R}, w\to L(w)=\dots?\) of your choice with two
local mimima such that the update rule

\[w \to w - \eta L^{'}(w)\]

for certain initial \(w\) and \(\eta\) oscillates between the two local
minima.

This conclusively shows that convergence is not guaranteed using the
gradient descent algorithm. Nevertheless, gradient descent is what makes
modern deep learning computationally possible as it is much less
computationally expensive and, as we will see, much more generally
applicable for varying network architectures as any other optimization
algorithm.

\end{document}
