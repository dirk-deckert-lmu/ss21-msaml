% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-13-1-pdf}{%
\section{\texorpdfstring{MsAML-13-1
{[}\href{SS21-MsAML__13-1__Dual_formulation_SVM.pdf}{PDF}{]}}{MsAML-13-1 {[}PDF{]}}}\label{msaml-13-1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{dual-formulation-of-the-svm}{Dual formulation of
  the SVM}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{dual-formulation-of-the-svm}{%
\subsection{Dual formulation of the
SVM}\label{dual-formulation-of-the-svm}}

Given a Lagrangian \begin{align}
  \mathcal L:D\times(\mathbb R^+_0)^N &\to \mathbb R\\
  (x,\alpha) &\mapsto \mathcal L(x,\alpha):=f(x)+\sum_{i=1}^N\alpha_i g_i(x)
\end{align} for the
\href{SS21-MsAML__12-1__Short_review_optimization.pdf}{previously
defined} general program \begin{align}
  \min_{x\in D} f(x)
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    g_i(x)\leq 0\\
    i=1,\ldots, N
  \end{cases}
  \,,
  \label{eq_P}
  \tag{P}
\end{align} we may associate a so-called \emph{Lagrange dual} function
by defining \begin{align}
  \mathcal D:(\mathbb R^+_0)^N &\to \mathbb R\\
  \alpha &\mapsto \mathcal D(\alpha):=\inf_{x\in D}\mathcal L(x,\alpha).
\end{align}

Due to the linearity of \(\mathcal L\) in \(\alpha\) and the properties
of the infimum, \(\mathcal D\) is always concave.

For every \(\tilde x\in D\) fulfilling the constraints, we have
\begin{align}
  \forall \alpha\in (\mathbb R^+_0)^N: \, \mathcal L(\tilde x,\alpha) \leq f(\tilde x).
\end{align} And in turn, for all \(\alpha\in (\mathbb R^+_0)^N\), we
find \begin{align}
  \mathcal D(\alpha) = \inf_{x\in D} \mathcal L(x,\alpha)
  \leq
  \mathcal L(\tilde x,\alpha)
  \leq f(\tilde x)
\end{align} so that also the optimal value \(p^*\) of \(\eqref{eq_P}\)
we have \begin{align}
  \forall \alpha\in(\mathbb R^+_0)^N:\, \mathcal D(\alpha)\leq p^*.
  \label{eq_1}
  \tag{E1}
\end{align}

\textbf{Example:} As a fictitious example (taken from the boook
\emph{Convex optimization} of Boyd \& Vandenberghe), let us consider
only one constraint \(g(x)\leq 0\), i.e., \(N=1\) and \(g=g_i\). The
following figure illustrates a possible objective function \(f\),
constraint function \(g\), and a family of plots of the Lagrange
function \(\mathcal L(x,\alpha)\) for different choices of \(\alpha\):

\begin{figure}
\centering
\includegraphics{13/objetive_constraint_lagrange.png}
\caption{Objective, constraint, and Lagrange functions for the example
of one constraint.}
\end{figure}

Although neither the objective function \(f\), nor the constraint
function \(g\) is convex, the Lagrange dual \(\mathcal D\) shown in the
next plot is concave:

\begin{figure}
\centering
\includegraphics{13/dual.png}
\caption{Lagrange dual function in the example above.}
\end{figure}

The Lagrange dual \(\mathcal D\) naturally leads to another optimization
problem:

\begin{align}
  \max_{\alpha\in\mathbb R^N} \mathcal D(\alpha)
  \qquad
  \text{subject to}
  \qquad
  \begin{cases}
    \alpha_i\leq 0\\
    i=1,\ldots, N
  \end{cases}
  \,.
  \label{eq_D}
  \tag{D}
\end{align}

One usually refers to \(\eqref{eq_P}\) as the \emph{primal} and
\(\eqref{eq_D}\) as the corresponding \emph{dual program}.

Note that thanks to concavity of \(\mathcal D\), the dual program is
always a convex optimization problem. For \(d^*\) denoting the optimal
value of \(\eqref{eq_D}\), relation \(\eqref{eq_1}\) implies
\begin{align}
  d^*\leq p^*
\end{align} and the difference \((p^*-d^*)\) is called duality gap.

It can be shown that a convex program fulfilling the Slater condition
also fulfills \begin{align}
  d^*=p^*.
\end{align} As a proof of this fact is the content of an optimization
course, we omit it here and refer the interest reader, e.g., to the
\href{SS21-MsAML__12-1__Short_review_optimization.pdf}{already
mentioned} standard literature.

Coming back to the minimization program of the support vector machine,
\begin{align}
  \begin{split}
    &\inf_{\bar w=(b,w)\in\mathbb R^{d+1}}
    \left[
      \frac12 |w|^2 + C\sum_{i=1}^N \xi^{(i)}
    \right]
    \\
    &\qquad \text{subject to}
    \quad
    \begin{cases}
      &y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1 - \xi^{(i)},\\
      &\xi^{(i)} \geq 0,\\
      &\text{for all } i=1,\ldots, N
    \end{cases}
    \,,
  \end{split}
  \label{eq_SVM}
  \tag{SVM}
\end{align} here in the soft-margin case with an additional constant
\(C\geq 0\) to fine-tune the penalty for larger slack variables, we can
now recast it into its dual formulation.

The Lagrangian reads \begin{align}
  &\mathcal L\left(
    \underbrace{w,b,\xi}_{\text{The "$x$" in previous notation.}},
    \underbrace{\alpha,\beta}_{\text{The "$\alpha$" in previous notation.}}
  \right)\\
  &=
  \underbrace{\frac12 |w|^2+C\sum_{i=1}^N\xi^{(i)}}_{\text{The "$f(x)$" in previous notation.}}\\
  &\qquad
  -\sum_{i=1} \underbrace{\alpha_i}_{\text{First part of the "$\alpha$" in previous notation.}}
  \quad
  \underbrace{\left(y^{(i)}(w\cdot x^{(i)}+b)-1+\xi^{(i)}\right)}_{\text{First type of constraints.}} 
  \\
  &\qquad
  -\sum_{i=1}^N \underbrace{\beta_i}_{\text{Second part of the "$\alpha$" in previous notation.}}\quad \underbrace{\xi^{(i)}}_{\text{Second type of constraints.}}.
\end{align}

Following the same steps as in
\href{SS21-MsAML__12-X1__KKT_criterion.pdf}{exercise 12-1}, in which the
hard-margin case was treated, we can apply the KKT criterion to find
\begin{align}
  \nabla_w\mathcal L = 0 &\Rightarrow w=\sum_{i=1}^N\alpha_i y^{(i)}x^{(i)},
  \label{eq_2}
  \tag{E2}
  \\
  \nabla_b\mathcal L = 0 &\Rightarrow \sum_{i=1}^N \alpha_i y^{(i)} = 0,
  \label{eq_3}
  \tag{E3}
  \\
  \nabla_{\xi^{(i)}}\mathcal L = 0 &\Rightarrow \alpha_i+\beta_i = C.
  \label{eq_4}
  \tag{E4}
\end{align} by means of \(\fbox{KKT1}\) and in addition for all
\(i=1,\ldots,N\) \begin{align}
  \alpha_i\left(
    y^{(i)}(w\cdot x^{(i)}+b)-1+\xi^{(i)}
  \right) 
  &= 0
  \\
  \beta_i\xi^{(i)} &= 0.
\end{align} based on \(\fbox{KKT2-3}\). It may strike the eye that the
KKT criterion for soft-margin case does not differ that much from the
hard-margin case.

In order to compute the Lagrange dual function for the soft-margin
support vector machine, we need to find the infimum in \((w,b,\xi)\) of
the Lagrange function. This can be done by exploiting Fermat's theorem,
where \(\eqref{eq_2}\), \(\eqref{eq_3}\), and \(\eqref{eq_4}\) come in
handy. In the first step, we plug in \(\eqref{eq_2}\) and observe
\begin{align}
  \mathcal L\Big|_{w=\sum_{i=1}^N\alpha_i y^{(i)} x^{(i)}}
  =
  &\frac12 \left|\sum_{i=1}^N \alpha_i y^{(i)} x^{(i)}\right|^2
  \\
  &+
  C\sum_{i=1}^N \xi^{(i)}
  \\
  &-
  \sum_{i=1}^N \alpha_i y^{(i)}\sum_{j=1}^N \alpha_j y^{(j)} \; x^{(j)}\cdot x^{(i)}
  \\
  &-
  \sum_{i=1}^N \alpha_i y^{(i)}b
  \\
  &-
  \sum_{i=1}^N \alpha_i \xi^{(i)}
  \\
  &+
  \sum_{i=1}^N \alpha_i 
\end{align} In a second step, we make use of \(\eqref{eq_3}\) and
collect the many summands to find \begin{align}
  \mathcal L\Big|_{w=\sum_{i=1}^N\alpha_i y^{(i)} x^{(i)}}
  =
  \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
  y^{(j)} \; x^{(i)} \cdot x^{(j)}.
\end{align} Since this is now a function of only the Lagrange
multipliers \((\alpha,\beta)\), notably even independent of \(\beta\),
we have already found our dual Lagrange function \begin{align}
  \mathcal D(\alpha, \beta) 
  = 
  \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
  y^{(j)} \; x^{(i)} \cdot x^{(j)}.
\end{align}

The dual formulation of corresponding to \(\eqref{eq_SVM}\) therefore
reads \begin{align}
  &\max_{\alpha\in \mathbb R^N}
  \left(
    \sum_{i=1}^N \alpha_i - \frac12 \sum_{i,j=1}^N \alpha_i\alpha_j y^{(i)}
    y^{(j)} \; x^{(i)} \cdot x^{(j)}.
  \right)
  \\
  &\qquad
  \text{subject to}
  \qquad
  \begin{cases}
    &0\leq \alpha_i\leq C,
    \\
    &\sum_{i=1}^N \alpha_i y^{(i)} = 0,
    \\
    &\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
  \label{eq_dSVM}
  \tag{dSVM}
\end{align}

As mentioned in our general discussion, this dual program is concave.
Furthermore, it is infinitely often differentiable and therefore a
smooth quadratic optimization program for which very efficient numerical
solvers are available. A very popular and efficient implementation can
be found here \href{https://en.wikipedia.org/wiki/LIBSVM}{LIBSVM}, which
has bindings to many languages including Python; see
\href{https://scikit-learn.org/0.15/modules/generated/sklearn.svm.libsvm.fit.html}{scikit
learn}.

Note that, given an optimal point \(\alpha^*\), we can directly compute
the hypothesis \begin{align}
  h(x) 
  &= 
  \text{sign} \left(w\cdot x+b\right)
  \\
  &= 
  \text{sign} \left(\sum_{i=1}^N\alpha_i y^{(i)}\, x^{(i)}\cdot x + b\right),
\end{align} where the bias term \(b\) can be inferred from any support
vector \(x^{(s)}\) with label \(y^{(s)}\), i.e., any \(s=1,\ldots,N\)
such that \(w\cdot x^{(i)}+b=y^{(i)}\), which results in \begin{align}
  b = y^{(s)} - \sum_{i=1}^N \alpha_i y^{(i)}\; x^{(i)}\cdot x^{(s)}.
\end{align}

This dual formulation not only allows for efficient solvers but has
another advantage, namely to readily generalize the support vector
machine to the case of non-linear classification problems. Recall our
initial discussion of
\href{SS21-MsAML__10-1__Non-linear_classification.pdf}{non-linear
classification problems}, where we introduced a non-linear map \(\Phi\)
on feature space \(\mathcal X\) that maps to a higher-dimensional one
\(\mathcal X'\) in order to render the corresponding image of the
training data linearly separable. A drawback of this route was the fact
that adding these further dimensions may increase the number of
computation steps during training substantially. Taking a look at
\(\eqref{eq_dSVM}\), it would require to replace the inner product
\(x^{(i)}\cdot x^{(j)}\) in \(\mathcal X\) by \begin{align}
  \Phi(x^{(i)})\cdot \Phi(x^{(j)})
\end{align} in \(\mathcal X'\), which roughly implies as many
computation steps as dimensions in \(\mathcal X'\). This is no news to
us but now this inner product structure of \(\eqref{eq_dSVM}\) allows
for a short-cut. Instead of simply equipping \(\mathcal X'\) with the
standard euclidean inner product, let us define the inner product by
means of an evaluation of a function
\(K:\mathcal X\times\mathcal X\to\mathbb R\), i.e., \begin{align}
  \Phi(x^{(i)})\cdot \Phi(x^{(j)}) := K(x^{(i)},x^{(j)}).
\end{align} Obviously, we need to choose \(K\) carefully in order to end
up defining an inner product on \(\mathcal X'\) and we will look into
this particular class of so-called \emph{kernel functions} in our last
module. However, to get into this mode of thinking, why defining the
inner product in terms of a kernel function is beneficial, consider,
e.g., the popular choice \begin{align}
  K(x,x') := \exp\left(-\frac{|x-x'|^2}{2\sigma^2}\right)
\end{align} for some with parameter \(\sigma>0\). The latter is called
\emph{gaussian kernel}. Note that the argument of the exponential
function involves only an inner product in \(\mathcal X\) and therefore
does not excessively blow up the computation steps. Roughly the only
additional computational effort is to evaluate the exponential for which
there are efficient algorithms and, in worst case, we could simply use a
look-up table. On the other hand, recall how the simple non-linear
classification example in
\href{SS21-MsAML__10-1__Non-linear_classification.pdf}{non-linear
classification} was made linearly separable by only adding a quadratic
polynomial. The gaussian kernel already comprises a limit of polynomials
of order \(N\) for \(N\to\infty\), and in a sense corresponds to an
infinite dimensional \(\mathcal X'\) in the above language. This
justifies the hope for another very general way to approach non-linear
classification by supplying a kernel function to the dual support vector
machine program.

In the last module, let us use conclude our first round into of
non-linear classification by a study of admissible kernel functions.

➢ Next session!

\end{document}
