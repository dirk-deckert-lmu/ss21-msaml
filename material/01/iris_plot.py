import matplotlib.pyplot as plt
from sklearn import datasets


iris = datasets.load_iris() # load Iris data set
X = iris.data[:, :2]  # only take the first two features
y = iris.target

x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5

fig, ax = plt.subplots()
scatter = ax.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Set1, edgecolor='k')

classes = ['Iris setosa', 'Iris versicolor', 'Iris virginica']
ax.legend(handles=scatter.legend_elements()[0], labels=classes)

ax.set_xlabel('sepal length [cm]')
ax.set_ylabel('sepal width [cm]')

ax.set_xlim(x_min, x_max)
ax.set_ylim(y_min, y_max)

plt.savefig('iris_plot.svg')

y[y == 0] = -1
y[y == 1] = 1
y[y == 2] = 1
classes = ['class label = -1 (Iris setosa)', 'class label = +1 (other)']

fig, ax = plt.subplots()
scatter = ax.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Set1, edgecolor='k')

ax.legend(handles=scatter.legend_elements()[0], labels=classes)

ax.set_xlabel('sepal length [cm]')
ax.set_ylabel('sepal width [cm]')

ax.set_xlim(x_min, x_max)
ax.set_ylim(y_min, y_max)


plt.savefig('iris_plot_binary.svg')
