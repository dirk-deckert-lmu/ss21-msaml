% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-11-1-pdf}{%
\section{\texorpdfstring{MsAML-11-1
{[}\href{SS21-MsAML__11-1__Representation_and_approximation.pdf}{PDF}{]}}{MsAML-11-1 {[}PDF{]}}}\label{msaml-11-1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{representation-and-approximation}{Representation
  and approximation}

  \begin{itemize}
  \tightlist
  \item
    \protect\hyperlink{representation-of-binary-classifiers-on-ux24ux2fmathbb-rux5cux255Edux24}{Representation
    of binary classifiers on \(\mathbb R^d\)}
  \item
    \protect\hyperlink{approximation-of-real-valued-functions}{Approximation
    of real-valued functions}
  \end{itemize}
\end{enumerate}

Back to \href{index.pdf}{index}.

\hypertarget{representation-and-approximation}{%
\subsection{Representation and
approximation}\label{representation-and-approximation}}

Recall our first result about representing any boolean function by a
two-layered neural network. In this module, we will extend this result
in the following directions:

\begin{itemize}
\tightlist
\item
  Representation of binary classifiers on \(\mathbb R^d\);
\item
  Approximation of real-valued functions by neural networks.
\end{itemize}

These are our first results that give answers to the question of how
rich the class of hypotheses parametrized by neural networks are. As one
can imaging, there is a whole field of research behind those
expressiveness-related questions for supervised learning models. In the
case of neural networks, the next two results show some typical ideas
behind the strategies of proof.

\hypertarget{representation-of-binary-classifiers-on-mathbb-rd}{%
\subsubsection{\texorpdfstring{Representation of binary classifiers on
\(\mathbb R^d\)}{Representation of binary classifiers on \textbackslash mathbb R\^{}d}}\label{representation-of-binary-classifiers-on-mathbb-rd}}

Let us set \(\mathcal X=\mathbb R^d\) and \(\mathcal Y=\{-1,+1\}\),
assume given test data \(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\), and as a
first step, let us try to understand whether there is always a
\(h\in\mathcal N^D_{n,\alpha}\) with \(n_D=1\) and the last activation
function being a \(\operatorname{sign}\), such that \begin{align}
  \forall i=1,\ldots,N:
  \quad
  h(x^{(i)})=y^{(i)},
  \label{eq_1}
  \tag{E1}
\end{align} or in other words, there is always a classifier generated by
a neural network that allows to classify the given test data flawlessly.

\begin{description}
\item[Theorem] ~ 
\hypertarget{representation-of-binary-classifiers}{%
\subparagraph{(Representation of binary
classifiers)}\label{representation-of-binary-classifiers}}

For input dimension \(d\in\mathbb N\), sample size \(N\in\mathbb N\),
and test data \(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\) in
\((\mathbb R^d\times\{-1,+1\})^N\) there is an \(n_1\leq 2N\) and a
\(h\in\mathcal  N^D_{n,\alpha}\) for \(D=2\), \(n=(n_0=d,n_1,1)\), and
\(\alpha=(\operatorname{sign},\operatorname{sign})\) that fulfills
\(\eqref{eq_1}\).
\end{description}

\textbf{Proof:} We shall start out similarly as in our previous result
on boolean functions and separate the feature vectors in the test data
\(s\) according their class labels. Without loss of generality, we may
assume that for some \(0\leq k\leq N\) we have \begin{align}
  y^{(i)}&=+1
  \qquad
  \text{for } 
  \qquad i=1,\ldots,k,
  \\
  y^{(i)}&=-1
  \qquad
  \text{for } 
  \qquad i=k+1,\ldots,N.
\end{align}

Since the test data is finite, for each \(i=1,\ldots,k\), we find a
separating hyperplane with parameters \(w^{(i)}\in\mathbb R^{n_0=d}\),
\(b^{(i)}\in\mathbb R\) such that \begin{align}
  f^{(i)}(x) = w^{(i)}\cdot x + b^{(i)}
\end{align} fulfills \begin{align}
  f^{(i)}(x^{(i)})=0
  \qquad
  \text{and}
  \qquad 
  \forall i\neq j: \quad h^{(i)}(x^{(j)})\neq 0,
\end{align} e.g., like such:

\begin{figure}
\centering
\includegraphics{11/hyperplanes.png}
\caption{Example of hyperplanes with the above property.}
\end{figure}

Furthermore, also due to finiteness of the test data, for each
\(i=1,\ldots,k\) there is an \(\epsilon^{(i)}>0\), e.g., half the
minimal distance of the hyperplane parametrized by \(w^{(i)}\) and
\(b^{(i)}\) to the nearest \(x^{(j)}\) for \(j\neq i\), such that:
\begin{align}
  I^{(i)}(x) 
  =
  \;
  &\operatorname{sign}\left(
    \epsilon^{(i)} + \left(w^{(i)}\cdot x+b^{(i)}\right)
  \right)
  \\
  +&
  \operatorname{sign}\left(
    \epsilon^{(i)} - \left(w^{(i)}\cdot x+b^{(i)}\right)
  \right)
\end{align} fulfills \begin{align}
  I^{(i)}(x^{(i)})=2
  \qquad
  \text{and}
  \qquad
  \forall i\neq j:\, I^{(i)}(x^{(j)})=0
\end{align}

The shifted hyperplane for \(i=1\) is depicted in orange in the
following illustration:

\begin{figure}
\centering
\includegraphics{11/epsilon_shift.png}
\caption{Example for the shifted hyperplane for \(i=1\).}
\end{figure}

This allows to construct \begin{align}
  h(x) = \operatorname{sign}\left(-1+\sum_{i=1}^k I^{(i)}(x)\right)
  \label{eq_2}
  \tag{E2}
\end{align} which then fulfills \begin{align}
  h(x^{(i)})&=+1
  \qquad
  \text{for}
  \qquad
  i=1,\ldots,k,
  \\
  h(x^{(i)})&=-1
  \qquad
  \text{for}
  \qquad
  i=k+1,\ldots,N.
\end{align} Note that his is implies the flawless classification of the
test data \(\eqref{eq_1}\).

Hence, it suffices to show that \(h\) actually lies in
\(\mathcal N^D_{n,\alpha}\) for \(D=2\), \(n=(n_0=d,n_1,1)\) given an
\(n_1\in\mathbb N\), and
\(\alpha=(\operatorname{sign},\operatorname{sign})\). In the following
we show that this is true by means of reading off the respective weights
and bias terms from \(\eqref{eq_2}\). The latter can be done
systematically by following the recursive definition of the feed forward
function of a neural network defined in \(\mathcal N^D_{n,\alpha}\):

\emph{Layer 0:} The input of the neural network is \begin{align}
  \mathbb R^{n_0=d} \ni x_0 = x.
\end{align}

\emph{Layer 1:} In the first layer we have \begin{align}
  \mathbb R^{n_1=2k} \ni z_1=W_1\cdot x_0 + b_1.
\end{align} Hence, for \(l=1,\ldots,k\), we could choose \begin{align}
  z_{1,2l-1} &= \epsilon^{(l)}+\left(w^{(l)}\cdot x_0+b^{(l)}\right),
  \\
  z_{1,2l}   &= \epsilon^{(l)}-\left(w^{(l)}\cdot x_0+b^{(l)}\right)
\end{align} so that the corresponding weight matrix \begin{align}
 \mathbb R^{n_1\times n_0}\ni W_1=(W_{1,(k,m)})_{\substack{1\leq k\leq n_1\\ 1\leq m\leq n_0}} 
\end{align} could be set up as follows \begin{align}
  W_{1,(2l-1,j)} := w^{(l)}_j,
  \qquad
  W_{1,(2l,j)} := -w^{(l)}_j,
\end{align} and the bias vectors \begin{align}
  b_1=(b_{1,k})_{1\leq k\leq n_1}
\end{align} could correspondingly be chosen as \begin{align}
  b_{1,2l-1} := b^{(l)}+\epsilon^{(l)},
  \qquad
  b_{1,2l} := -b^{(l)}+\epsilon^{(l)}.
\end{align} The output of this first layer is thus given by
\begin{align}
  \mathbb R^{n_1=2k}\ni x_1 = \operatorname{sign}(z_1),
\end{align} where we recall our agreed slight abuse of notation that the
\(\operatorname{sign}\) is applied to each vector component.

\emph{Layer 2:} For the second layer we have \begin{align}
  \mathbb R\ni z_2 = W_2\cdot x_1+b_2
\end{align} so that we could set the corresponding weights and biases
\begin{align}
  W_2\in\mathbb R^{(n_2=1)\times n_1},
  \qquad
  b_2\in\mathbb R^{n_2=1}
\end{align} as follows \begin{align}
  W_{2,j}&=1
  \qquad
  \text{for}
  \qquad 
  j=1,\ldots,2k,
  \\
  b_2&=-1.
\end{align} Finally, the output of this second layer is \begin{align}
  x_2 = \sigma(z_2)
\end{align} and by construction we have \begin{align}
  h(x)=x_2.
\end{align}

Due to the existence of \(W_1,b_1,W_2,b_2\) above we conclude that
\(h\in\mathcal N^D_{n,\alpha}\) for \(D=2\), \(n=(d,2k,1)\), and
\(\alpha=(\operatorname{sign},\operatorname{sign})\). Finally, we
observe the bound \(n_1=2k\leq 2N\), which concludes the proof.

\(\square\)

\begin{description}
\tightlist
\item[Remark]
Again, the bound can be given slightly improved by observing that we can
use twice the minimum of the total number of class \(+1\) and \(-1\)
feature vectors.
\end{description}

\hypertarget{approximation-of-real-valued-functions}{%
\subsubsection{Approximation of real-valued
functions}\label{approximation-of-real-valued-functions}}

Next we make an small excursion to the setting of a regression learning
task, for which it is important to control the approximation
capabilities of a neural network, e.g., to a given function
\begin{align}
  f\in\mathcal C^0([0,1],\mathbb R),
\end{align} i.e., a continuous real-valued function on interval
\([0,1]\subset\mathbb R\).

Unlike the cases considered earlier, in which we restricted our analysis
explicitly to heaviside or signum functions as activations, mostly for
the sake of simplicity, we will use this opportunity to treat more
general sigmoidal functions.

\begin{description}
\item[Theorem] ~ 
\hypertarget{approximation-of-real-valued-functions-1}{%
\subparagraph{(Approximation of real-valued
functions)}\label{approximation-of-real-valued-functions-1}}

Let \(\sigma:\mathbb R\to\mathbb R\) be bounded such that \begin{align}
\lim_{z\to +\infty} \sigma(z)=+1
\qquad
\text{and}
\qquad
\lim_{z\to -\infty} \sigma(z)=-1
  \end{align} and \begin{align}
\mathcal H_m:=\mathcal N^{D}_{n,\alpha},
\qquad \text{for}
\quad D=2, n=(n_0=1,n_1=m,1), (\sigma,1).
  \end{align} Then, there exists a constant \(C>0\) such that for
\(f\in\mathcal  C^0([0,1],\mathbb R)\) and all \(m\in\mathbb N\)
\begin{align}
\inf_{h\in\mathcal H_m} \left\|f-h\right\|_\infty 
\leq 
C\sup_{\substack{|x-y|<\frac1m\\x,y\in[0,1]}}
\left|f(x)-f(y)\right|.
\label{eq_3}
\tag{E3}
  \end{align}
\end{description}

Here, we have used the notation of the \(L^\infty([0,1],\mathbb R)\)
norm \begin{align}
  \|f(x)-g(x)\|_\infty := \sup_{x\in[0,1]}|f(x)-g(x)|.
\end{align} To understand the mode of approximation better, consider for
instances a Lipschitz-continuous function \(f\), in which case the
right-hand side in \(\eqref{eq_3}\) can be bounded by the
Lipschitz-constant divided by \(m\), which vanishes for \(m\to\infty\),
and hence, a two layered neural network is able to approximate the
function \(f\) arbitrarily well in the \(L^\infty\) norm.

\textbf{Proof:} Given \(m\in\mathbb N\), it suffices to construct a
tester, i.e., some \begin{align}
  h_m\in\mathcal H_m
\end{align} to estimate left-hand side of \(\eqref{eq_3}\) as
\begin{align}
  \inf_{h\in\mathcal H_m}\left\|h(x)-f(x)\right\|_\infty \leq \|h_m(x)-f(x)\|_\infty.
\end{align}

Define \(x^{(i)}=\frac i m\) for \(i=1,\ldots, m\) and, as
\(\widetilde h_m\), a step function with \begin{align}
  \forall i=1,\ldots,m \text{ and } x\in[x^{(i-1)},x^{(i)}):
  \qquad \widetilde h_m(x):=f(x^{(i)}), 
\end{align} and finally \(h_m(1)=f(x^{(m)})\). This procedure samples
the function \(f\) as for instance depicted here:

\begin{figure}
\centering
\includegraphics{11/sampling.png}
\caption{Sampling of a continuous function \(f\).}
\end{figure}

Let us recast \(\widetilde h_m\) as follows: \begin{align}
  \widetilde h_m(x)
  =
  f(x^{(1)}) 
  + 
  \sum_{i=1}^{m-1}\left(f(x^{(i+1)})-f(x^{(i)})\right) \, 1_{i\leq \lfloor mx\rfloor},
  \label{eq_4}
  \tag{E4}
\end{align} where we use the \emph{floor} notation
\(\lfloor\cdot\rfloor\), which truncates any float point values to their
smaller or equal integer values.

However, due to the characteristic function, \(\widetilde h_m\) as given
in \(\eqref{eq_4}\) is not in \(\mathcal H_m\). The characteristic
function needs to be approximated by the activation function \(\sigma\)
for which we will exploit the sigmoidal properties.

For this purpose, let us consider \begin{align}
  h^\lambda_m(x)
  =
  f(x^{(i)})\sigma(\lambda)
  +
  \sum_{i=1}^{m-1}\left(f(x^{(i+1)}-f(x^{(i)})\right) \, \sigma(\lambda(mx-i)),
\end{align} for given approximation parameter \(\lambda\in\mathbb R^+\).
Note that for large \(\lambda\) the function \(h^\lambda_m\) tends to
\(h_m\) at least in a point-wise sense as \(\sigma(\lambda)\to+1\) and
\(\sigma(\lambda(mx-i))\to1_{i\leq \lfloor mx\rfloor}\). However, we
need some uniformity.

For \(x\in[0,1]\), we make the following estimate \begin{align}
  \left|h^\lambda_m(x)-f(x)\right|
  \leq
  \left|h^\lambda_m(x)-\widetilde h_m(x)\right|
  +
  \left|h_m(x)-f(x)\right|
  =: \fbox{1} + \fbox{2}.
\end{align}

By construction of the sampling, we have \begin{align}
  \fbox{2} \leq \|h_m-f\|_\infty
  \leq \sum_{\substack{|x-y|<\frac 1m\\x,y\in[0,1]}} |f(x)-f(y)|.
\end{align}

Next, we observe that except maybe for \(i=\lfloor mx\rfloor\) and
\(i=\lfloor mx\rfloor +1\), given \(\epsilon>0\), there is always a
sufficiently large \(\lambda>0\) such that \begin{align}
  \left|
    1_{i\leq\lfloor mx\rfloor} - \sigma(\lambda(mx-i))
  \right|
  =
  \left|
    1_{0\leq\lfloor mx\rfloor-i} - \sigma(\lambda(mx-i))
  \right|
  \leq \frac{\epsilon}{m}.
\end{align} Similarly, for sufficiently large \(\lambda>0\) also
\begin{align}
  |1-\sigma(\lambda)|\leq \frac\epsilon m
\end{align} holds true.

This allows for the following estimate for \(\epsilon>0\) small enough,
e.g., \(\epsilon(1-2/m)\leq 1\), and \(\lambda>0\) sufficiently large:
\begin{align}
  \fbox{1}
  &= 
  \left|h^\lambda_m(x)-h_m(x)\right|
  \\
  &\leq
  \left|f(x^{(1)})(\sigma(\lambda)-1)\right|
  +
  \left|
    \sum_{i=1}^{m-1}\left(f(x^{(i+1)})-f(x^{(i)})\right)
    \left(
      \sigma(\lambda(mx-i))-1_{0\leq \lfloor mx\rfloor -1}
    \right)
  \right|
  \\
  &\leq \left|f(x^{(1)}\right|\frac\epsilon m
  \\
  &\qquad +
  (m-2)\sup_{\substack{|x-y|\leq \frac 1m\\x,y\in[0,1]}}|f(x)-f(y)| \frac\epsilon m
  \\
  &\qquad +
  \left|f(x^{\lfloor mx\rfloor+1}-f(x^{\lfloor mx\rfloor})\right| \left|\sigma(\lambda(mx-i))-1\right|
  \\
  &\qquad +
  \left|f(x^{\lfloor mx\rfloor+2}-f(x^{\lfloor mx\rfloor+1})\right| \left|\sigma(\lambda(mx-i))\right|
  \\
  &\leq
  \operatorname{constant}\frac{\epsilon}{m}
  \\
  &\qquad + (1+2\|\sigma\|_\infty)
  \sup_{\substack{|x-y|\leq \frac1 m\\x,y\in[0,1]}} |f(x)-f(y)|,
\end{align} where we have used the cancellation \((m+2)/m\) and the
smallness condition on \(\epsilon\) in the second term.

In conclusion, we have shown that for all sufficiently small
\(\epsilon>0\) there exists a \(\lambda>0\) such that \begin{align}
  \left|h^\lambda_m(x)-f(x)\right|
  &\leq 
  \fbox{1} + \fbox{2}
  \\
  &\leq
  \operatorname{constant} \frac\epsilon m
  + 2(1+\|\sigma\|_\infty) 
  \sup_{\substack{|x-y|\leq \frac1 m\\x,y\in[0,1]}} |f(x)-f(y)|.
\end{align} Sending \(\epsilon\) to zero then yields \begin{align}
  \left|h^\lambda_m(x)-f(x)\right|
  \leq 2(1+\|\sigma\|_\infty) 
  \sup_{\substack{|x-y|\leq \frac1 m\\x,y\in[0,1]}} |f(x)-f(y)|.
\end{align} Hence, provided \(\lambda>0\) is sufficiently large, the
family of function \(h^\lambda_m\) approximates \(f\) arbitrarily well
in the given notion of convergence.

It is left to show that these \(h^\lambda_m\) are elements of
\(\mathcal N^{D}_{n,\alpha}\) for \(D=2\), \(n=(n_0=1,n_1=m,n_2=1)\),
\(\alpha=(\sigma,1)\), which is left as an exercise:

\begin{description}
\tightlist
\item[👀]
Compute the explicit weights and biases \(W_1,b_1,W_2,b_2\) in order to
show that, for \(D=2\), \(n=(n_0=1,n_1=m,n_2=1)\),
\(\alpha=(\sigma,1)\), and any \(\lambda>0\) we have
\(h^\lambda_m\in\mathcal N^D_{n,\alpha}\).
\end{description}

\(\square\)

\end{document}
