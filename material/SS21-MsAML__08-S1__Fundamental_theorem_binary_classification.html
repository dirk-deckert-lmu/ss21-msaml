<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__08-S1__Fundamental_theorem_binary_classification</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-08-s2-pdf">MsAML-08-S2 [<a href="SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#the-fundamental-theorem-of-binary-classification">The fundamental theorem of binary classification</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="the-fundamental-theorem-of-binary-classification">The fundamental theorem of binary classification</h2>
<p>We have come a long way in treating also the setting of infinite hypotheses spaces, at least for binary classification, and it is a good time to harvest all our results and summarize them by means of one central theorem.</p>
<p>Let us recall the setting of binary classification:</p>
<ul>
<li>Binary labels: <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span>,</li>
<li>Relevant hypotheses: <span class="math inline">\(\mathcal F\subseteq {\mathcal Y}^{\mathcal X}\)</span>,</li>
<li>I.i.d. training data: <span class="math inline">\(S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}\)</span>,</li>
<li>Loss function, counting the misclassifications: <span class="math inline">\(L(y,y&#39;)=1_{y\neq y&#39;}\)</span>.</li>
</ul>
<dl>
<dt>Theorem:</dt>
<dd><h5 id="fundamental-theorem-of-binary-classification">(Fundamental theorem of binary classification)</h5>
In the above setting, the following statements (S1)-(S4) are equivalent.
</dd>
</dl>
<p><strong>Statement (S1):</strong> Finite VC-dimension of <span class="math inline">\(\mathcal F\)</span>: <span class="math inline">\(\dim_\text{VC}\mathcal F &lt; \infty\)</span>.</p>
<p><strong>Statement (S2):</strong> Uniform convergence property of <span class="math inline">\(\mathcal F\)</span>: <span class="math display">\[\begin{align}
  &amp; \exists \, \text{polynomial } p \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  &amp; \qquad \qquad 
  P_S\left(\exists h\in\mathcal F: \left|\widehat R_S(h)-R(h)\right| &gt; \epsilon \right) &lt; \delta.
\end{align}\]</span></p>
<p><strong>Statement (S3):</strong> PAC learnability of <span class="math inline">\(\mathcal F\)</span>: <span class="math display">\[\begin{align}
  &amp; \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  &amp; \qquad \qquad 
  P_S\left(\left|R(h_S)-R_{\mathcal F}\right| &gt; \epsilon \right) &lt; \delta.
\end{align}\]</span></p>
<p><strong>Statement (S4):</strong> PAC learnability of <span class="math inline">\(\mathcal F\)</span> by means of empirical risk minimization (ERM): <span class="math display">\[\begin{align}
  &amp; \exists \, \text{polynomial $p$ and empirical risk minimizer $\mathcal A^{\text{ERM}}:s\mapsto h_s^{ERM}\in\mathcal F$} \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  &amp; \qquad \qquad 
  P_S\left(\left|R(h_S)-R_{\mathcal F}\right| &gt; \epsilon \right) &lt; \delta.
\end{align}\]</span></p>
<p><strong>Proof:</strong> (S1) <span class="math inline">\(\Rightarrow\)</span> (S2) holds thanks to the last corollary in <a href="SS21-MsAML__07-S2__VC-Dimension.html">VC-dimension module</a>.</p>
<dl>
<dt>👀</dt>
<dd>Check the reference, recall the proof of the corollary, and convince yourself that it holds.
</dd>
</dl>
<p>(S2) <span class="math inline">\(\Rightarrow\)</span> (S4): Given (S2) there is a polynomial <span class="math inline">\(p\)</span> such that for all <span class="math inline">\(\epsilon,\delta\in(0,1]\)</span>, distributions <span class="math inline">\(P\)</span>, <span class="math inline">\(N\geq p\left(\frac2\epsilon,\frac1\delta\right)\)</span>, and all <span class="math inline">\(h\in\mathcal F\)</span> <span class="math display">\[\begin{align}
  |\widehat R_S(h)-R(h)|\leq \epsilon 
\end{align}\]</span> with probability of a t least <span class="math inline">\(1-\delta\)</span>.</p>
<p>Recalling our <a href="SS21-MsAML__02-S1__Error_decomposition.html">error decomposition module</a>, for empirical risk minimizers <span class="math inline">\(h^\text{ERM}_S\)</span> we get <span class="math display">\[\begin{align}
  |R(h^\text{ERM}_S)-R_{\mathcal F}| \leq 2 \sup_{h\in\mathcal F}|\widehat R_S(h)-R(h)| \leq 2\cdot \frac \epsilon 2 = \epsilon.
\end{align}\]</span></p>
<dl>
<dt>👀</dt>
<dd>Check the reference and convince yourself that it holds.
</dd>
</dl>
<p>(S4) <span class="math inline">\(\Rightarrow\)</span> (S3) holds as (S4) gives an example for such an algorithm.</p>
<p>(S3) <span class="math inline">\(\Rightarrow\)</span> (S1): This is the only new assertion that was not discussed before. Say, (S3) holds true, then <span class="math display">\[\begin{align}
  &amp; \exists \, \text{polynomial $p$ and an algorithm $\mathcal A:s\mapsto h_s\in\mathcal F$} \\
  &amp; \qquad \forall \, \epsilon,\delta \in (0,1]\\
  &amp; \qquad \forall \, \text{distributions $P$ over $\mathcal X\times\mathcal Y$}\\
  &amp; \qquad \forall \, N \geq p\left(\frac1\epsilon,\frac1\delta\right):\\
  &amp; \qquad \qquad 
  P_S\left(\left|R(h_S)-R_{\mathcal F}\right| &gt; \epsilon \right) &lt; \delta.
\end{align}\]</span> We shall now prove (S1) by contradiction and assume <span class="math display">\[\begin{align}
  \dim_\text{VC}\mathcal F = \infty.
\end{align}\]</span> This implies that for any <span class="math inline">\(k\in\mathbb N\)</span> there is a tuple <span class="math inline">\(\mathfrak X=(x_1,\ldots, x_k)\)</span> of points in <span class="math inline">\(\mathcal X\)</span> that is shattered by <span class="math inline">\(\mathcal F\)</span>, i.e., <span class="math inline">\(|\mathcal F|_{\mathfrak X}|=2^k\)</span>. Let us think of <span class="math inline">\(\mathfrak X\)</span> as feature space, and by abuse of notation also denote the set of points in <span class="math inline">\(\mathfrak X\)</span> by the same symbol <span class="math inline">\(\mathfrak X\)</span>. So put differently, for every bit pattern in <span class="math inline">\(\{-1,+1\}^k\)</span> there is a concept <span class="math inline">\(c:\mathfrak X\to\{-1,+1\}\)</span>, given by a hypothesis <span class="math inline">\(h\in\mathcal F\)</span> through <span class="math inline">\(c(x_i)=h(x_i)\)</span>, <span class="math inline">\(i=1,\ldots, k\)</span>, such that the bit pattern can be expressed by <span class="math inline">\(\big(c(x_1),\ldots, c(x_k)\big)\)</span>.</p>
<p>Applying our <a href="SS21-MsAML__04-2__No_free_lunch_theorems.html">no-free-lunch theorem</a> in which we replace <span class="math inline">\(\mathcal X\)</span> by the set of points <span class="math inline">\(\mathfrak X\)</span>, we find for any algorithm <span class="math inline">\(\mathcal A\)</span>, producing <span class="math inline">\(h_s=\mathcal A(s)\)</span>: <span class="math display">\[\begin{align}
  &amp;\frac12\left(1-\frac{N}{k}\right) 
  \leq E_C E_{X^N} P_X\left( h_S(X)\neq C(X)\right).
  \label{eq_1}
  \tag{E1}
\end{align}\]</span> The right-hand side can be bounded further by <span class="math display">\[\begin{align}
  \eqref{eq_1}
  \leq E_C \Bigg[
  &amp;
  1\cdot P_S\Big(
           P_X(h_S(X)\neq C(X)) &gt; \epsilon
         \Big)
  \\
  &amp;+ \epsilon \cdot \Big[
    1 - P_S\Big(
          P_X(h_S(X)\neq C(X) &gt; \epsilon)
        \Big)
    \Big]
  \Bigg]
\end{align}\]</span> since the error probability is less or equal one.</p>
<p>Hence, choosing <span class="math inline">\(\epsilon=\frac14\)</span>, for any <span class="math inline">\(k\in\mathbb N\)</span> and <span class="math inline">\(N&lt;k\)</span>, there must be a concept <span class="math inline">\(c:\mathfrak X\to\{-1,+1\}\)</span> such that <span class="math display">\[\begin{align}
  P_S\Big(
          P_X(h_S(X)\neq C(X) &gt; \epsilon)
        \Big) 
  \geq \frac13 - \frac23 \frac N k.
\end{align}\]</span></p>
<p>However, choosing further <span class="math inline">\(\delta=\frac 14\)</span> together with the distribution <span class="math display">\[\begin{align}
  P(X=x\wedge Y=y) = \frac{1}{k} 1_{x\in\mathfrak X} 1_{y=c(x)},
\end{align}\]</span> (S3) implies that for <span class="math inline">\(N&gt;p(\frac1\epsilon=4,\frac1\delta=4)\)</span> <span class="math display">\[\begin{align}
  \frac 14 
  &amp;&gt; 
  P_S\left(|R(h_S)-\underbrace{R_{\mathcal F}}_{=0 \text{ as }c\in\mathcal F}|&gt;\frac 1 4\right)
  \\
  &amp;=
  P_S\left(R(h_S)&gt;\frac 1 4\right)
  \\
  &amp;=
  P_X\left(P_X(h_S(X)\neq c(X))&gt;\frac 1 4\right)
\end{align}\]</span> This bound and the one of <span class="math inline">\(\eqref{eq_1}\)</span> render the following scenario:</p>
<figure>
<img src="08/bounds.png" alt="Illustration of the bounds on p=P_X\left(P_X(h_S(X)\neq c(X))&gt;\frac 1 4\right)." /><figcaption aria-hidden="true">Illustration of the bounds on <span class="math inline">\(p=P_X\left(P_X(h_S(X)\neq c(X))&gt;\frac 1 4\right)\)</span>.</figcaption>
</figure>
<p>For sufficiently large <span class="math inline">\(k\)</span>, this leads to a contradiction.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>As you can imagine, there are various generalizations to the case <span class="math inline">\(|\mathcal Y|&gt;2\)</span>, maybe most notably the graph and Natarajan dimension, which allow for a similar type of generalization bounds. Furthermore, there are similar complexity measures for regression tasks, i.e., in the continuous case, such as the fat-shattering dimension. For quadratic loss functions one can find an analogue of the above theorem employing the fat-shattering dimension in case of regression.</p>
<p>In view of our time constraints, we will however not proceed towards those generalizations but will rather consider the non-uniform learnability concept and related structural risk minimization and boosting methods, which led to many applications in machine learning.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
