<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__10-1__Non-linear_classification</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-10-1-pdf">MsAML-10-1 [<a href="SS21-MsAML__10-1__Non-linear_classification.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#non-linear-classification">Non-linear classification</a>
<ul>
<li><a href="#non-linear-mappings-to-achieve-linear-separability-of-training-data">Non-linear mappings to achieve linear separability of training data</a></li>
<li><a href="#multi-layered-adaline-neurons">Multi-layered Adaline neurons</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="non-linear-classification">Non-linear classification</h2>
<p>The implementations we have considered so far, i.e., those for the class of linear classification models, always assumed linearly separable training data – or at least training data close to linear separability since the Adaline neuron and the soft-margin support vector machines can also handle a small fraction of violations of linear separability reasonably well. After having investigated a substantial amount of time in studying this class of models, the next natural question is how to extend it to also treat the case of training data that is not linearly separable. In short, we will refer to this type of classification task as <em>non-linear classification</em>.</p>
<p>There are at least two successful routes:</p>
<ol type="1">
<li>Mapping of the input features of the training data to a sufficiently high dimensional space in which the training becomes linearly separable, then applying one of the models in the linear classification model class;</li>
<li>Approximation of non-linear activation outputs by layering two or more Adaline neurons on top of each other and placing a non-linear activation function in between between them.</li>
</ol>
<p>In both cases we would like to achieve a certain generality of our models that does not require too much a priori knowledge about the particular non-linearity inherent in the corresponding training data, i.e., learning task.</p>
<p>The first route will lead us to an extension of the support vector machines via the so-called <em>kernel trick</em>, which will require some preparation based on results from convex optimization. Therefore, we will only explain the basic idea here and come back to this topic later. For their robustness and ease in training, these kernel-equipped support vector machines have been a kind of industry standard for the last decades.</p>
<p>The second route will lead us to the so-called multi-layer Adaline neuron models that were already mentioned several times in our discussions of the various activation and loss functions. These models are the building blocks of most applications in today’s field of <em>deep learning</em>.</p>
<p>Before going into the details, let us discuss the basic ideas behind these routes here.</p>
<h4 id="non-linear-mappings-to-achieve-linear-separability-of-training-data">Non-linear mappings to achieve linear separability of training data</h4>
<p>As a simple example for training data that is not linearly separable and a map under which the image of the training features becomes linear separable, let us consider the following setting:</p>
<ul>
<li>A family <span class="math inline">\(s=(x^{(i)},y^{(i)})_{1,\ldots,N}\)</span> of training data points in <span class="math inline">\((\mathcal X,\mathcal Y)\)</span>;</li>
<li>A conveniently chosen map <span class="math inline">\(\Phi:\mathcal X \to \mathcal X&#39;\)</span> from feature space <span class="math inline">\(\mathcal X\)</span> to a sufficiently high dimensional space <span class="math inline">\(\mathcal X&#39;\)</span>.</li>
</ul>
<figure>
<img src="10/non-linear_map.png" alt="Mapping of training data to higher dimensional space to ensure linear separability." /><figcaption aria-hidden="true">Mapping of training data to higher dimensional space to ensure linear separability.</figcaption>
</figure>
<p>Applying the map <span class="math inline">\(\Phi\)</span> to our training data features generates new family <span class="math display">\[\begin{align}
  s&#39;=\left(\Phi\left(x^{(i)}\right),y^{(i)}\right)_{i=1,\ldots,N}
\end{align}\]</span> of training data points, this time in <span class="math inline">\(\mathcal X&#39;\times\mathcal Y\)</span>.</p>
<p>In this simple example, the square of the radius already discriminates well the red and blue clusters of training data points so that a separating hyperplane can be found.</p>
<p>After application of the conveniently chosen map <span class="math inline">\(\Phi\)</span>, we can simply apply one of our linear classification models, e.g., a regular Adaline neuron with some activation function <span class="math inline">\(\alpha\)</span> and loss function <span class="math inline">\(L\)</span> such that the empirical loss is given by <span class="math display">\[\begin{align}
  \widehat L&#39;(b&#39;,w&#39;) &amp;= \frac1N\sum_{i=1}^N L\left(y^{(i)},\alpha\left(w&#39;\cdot\Phi(x)+b&#39;\right))\right)
\end{align}\]</span> and the resulting classifier by <span class="math display">\[\begin{align}
  h&#39;(x) &amp;= \operatorname{sign}\left(\alpha\left(w&#39;\cdot\Phi(x)+b&#39;\right)\right).
\end{align}\]</span></p>
<p>The empirical loss function is almost the same as before, except for the fact that we now compute the activation output parametrized by the higher dimensional <span class="math inline">\(b&#39;\)</span> and <span class="math inline">\(w&#39;\)</span>. Note that the model parameters <span class="math inline">\(w&#39;\)</span> have to change their type accordingly because now the inner product <span class="math inline">\(\cdot\)</span> is the one of <span class="math inline">\(\mathcal X&#39;\)</span>. The update rules for <span class="math inline">\(w&#39;\)</span> and <span class="math inline">\(b&#39;\)</span> can then again be computed by gradient descent, as we are already used to. Furthermore, the same method can be applied to the optimization programs of the support vector machines.</p>
<p>The basic idea being clear, the question remains how general such models can ever be as the non-linear map <span class="math inline">\(\Phi\)</span> that we simply slipped in in our example was obviously tailored to our needs – in other words, already contained the a priori knowledge of the nature of or learning task.</p>
<p>In our future discussion of the kernel trick we shall see that this ad hoc introduction of a convenient non-linear map <span class="math inline">\(\Phi\)</span> can be replace by a quite general choice that yields good generalization results in applications without much a priori knowledge of the learning task at hand.</p>
<dl>
<dt>👀</dt>
<dd>Implement an Adaline model for such an example setting as considered above, i.e., that involves training data that is not linearly separable. Choose a convenient map <span class="math inline">\(\Phi\)</span> and conduct the training.
</dd>
</dl>
<h3 id="multi-layered-adaline-neurons">Multi-layered Adaline neurons</h3>
<p>By definition, the Adaline neuron learns to divide training data into half-spaces by adjusting one separating hyperplane, applying the non-linear activation function <span class="math inline">\(\alpha(z)\)</span>, and ultimately the discretization function <span class="math inline">\(\operatorname{sign(z)}\)</span>, at least in case of binary classification, in order to select a particular label. In view of non-linear classification, it may seem natural to divide the classification task into several such divisions into half-spaces.</p>
<p>Recall, for instance, the failure of the linear models to learn the XOR-gate using one hyperplane. Suppose we were allowed to employ two hyperplanes as depicted here:</p>
<figure>
<img src="10/two_hyperplanes.png" alt="XOR-gate training data and two dividing hyperplanes." /><figcaption aria-hidden="true">XOR-gate training data and two dividing hyperplanes.</figcaption>
</figure>
<p>In this case, we would infer two classifiers <span class="math display">\[\begin{align}
  h_i(x)
  =
  \operatorname{sign}\left(w_i\cdot x+b_i\right)
  \qquad
  \text{for }i=1,2,
\end{align}\]</span> which we can employed to build a derived classifier <span class="math display">\[\begin{align}
  h(x) =
  \begin{cases}
    +1 &amp; \text{for } h_1(x)+h_2(x)\in\{-2,+2\} \\
    -1 &amp; \text{for } h_1(x)+h_2(x)=0
  \end{cases}.
\end{align}\]</span></p>
<p>One can readily imagine, how this method could also be applied to approximate the circular decision boundaries in our first example above, or even much more complicated non-linear decision boundaries, provided we were allowed to use sufficiently many such half-space classifiers. Furthermore, there is no reason to only restrict to half-space classifiers.</p>
<p>The parametrization of hypotheses space for such more complicated classification tasks, are given by so-called <em>neural networks</em> for which we have to specify:</p>
<ul>
<li><p>Number of layers <span class="math inline">\(D\in\mathbb N\)</span>, each layer <span class="math inline">\(i=1,\ldots,D\)</span> having</p>
<ul>
<li>Input feature dimensions <span class="math inline">\(n_{i-1}\in\mathbb N\)</span>;</li>
<li>activation output dimension <span class="math inline">\(n_{i}\in\mathbb N\)</span>;</li>
</ul></li>
<li><p>Activation functions <span class="math inline">\(\alpha_i:\mathbb R^{n_i}\to\mathbb R^{n_{i}}\)</span> for <span class="math inline">\(i=1,\ldots D\)</span>.</p></li>
</ul>
<p>The layers for <span class="math inline">\(i=0\)</span> and <span class="math inline">\(i=D\)</span> are called <em>input</em> and <em>output layers</em>, respectively, and the layers in between are sometime referred to as <em>hidden</em>. Let us collect theses parameters with the tuple notation <span class="math display">\[\begin{align}
  n=(n_i)_{i=0,\ldots N}, 
  \qquad \alpha=(\alpha_i)_{i=1,\ldots N},
\end{align}\]</span> and define the set of so-called <em>second-generation artificial neural networks</em> by <span class="math display">\[\begin{align}
  \mathcal N^{D}_{n,\alpha}
  :=
  \Big\{
    x_{n_D}:\mathbb R^{n_0}\to\mathbb R^{n_D}
    \,\Big|\,
    &amp; \forall x\in\mathbb R^{n_0}: x_0 = x,
    \\
    &amp; z_i = W_i\cdot x_{i-1}+b_i
    \\
    &amp; x_i = \alpha_i(z_i),
    \\
    &amp; i=1,\ldots, D,
    \\
    &amp; \text{for any } W_i\in\mathbb R^{n_i\times n_{i-1}}, b_i\in\mathbb R^{n_i}
  \Big\}.
\end{align}\]</span> The number <span class="math inline">\(D\)</span> is called the <em>depth</em> of the network while the numbers in the tuple <span class="math inline">\(n\)</span> are referred to as layer widths. Any function <span class="math inline">\(f\in\mathcal N^D_{n,m,\alpha}\)</span> is therefore recursively defined from input layer <span class="math inline">\(i=0\)</span> to output layer <span class="math inline">\(i=D\)</span> and is a function <span class="math inline">\(f:\mathbb R^{n_0}\to\mathbb R^{n_D}\)</span> that models the activation output of <span class="math inline">\(D\)</span> connected multi-class Adaline neurons.</p>
<p>Typically, one uses the same activation function in each component of a layer so that the following slight abuse of notation comes in handy: For exampe, for a given sigmoid activation <span class="math inline">\(\sigma:\mathbb R\to\mathbb R\)</span> we will employ the same symbol <span class="math inline">\(\sigma\)</span> to also denote the map <span class="math display">\[\begin{align}
  \mathbb R^{n_i}\to\mathbb R^{n_i},
  \qquad
  \sigma(z_i,\ldots,z_{n_i}):=
  (\sigma(z_i),\ldots,\sigma(z_{n_i})),
\end{align}\]</span> which in other words, is the component-wise application of <span class="math inline">\(\sigma\)</span>. Unless otherwise noted, we will use this shorthand notation.</p>
<p><strong>Example:</strong> The possible activation output functions of the already discussed single-layer multi-class Adaline neuron with sigmoid activation <span class="math inline">\(\sigma\)</span> for <span class="math inline">\(n\)</span>-dimensional input and <span class="math inline">\(c\)</span> classes are in the set <span class="math inline">\(\mathcal N^1_{(n,c), (\sigma)}\)</span>.</p>
<p>Hypotheses spaces for classification problems with <span class="math inline">\(\mathcal X=\mathbb R^{n_0}\)</span> and <span class="math inline">\(\mathcal Y=\{1,\ldots, m_D\}\)</span> could then be derived from <span class="math inline">\(\mathcal N^D_{n,\alpha}\)</span> by, e.g., applying an <span class="math inline">\(\operatorname{argmax}\)</span> to the output function: <span class="math display">\[\begin{align}
  \mathcal H^D_{n,m,\alpha}
  =
  \left\{ 
    h:\mathcal X\to\mathcal Y
    \,\big|\,
    f\in\mathcal N^D_{n,m,\alpha}:
    \forall x\in\mathbb R^{n_0}:
    h(x)=\underset{k=1,\ldots, n_D}{\operatorname{argmax}} \left(f(x)\right)_k
  \right\} 
\end{align}\]</span></p>
<p>The set <span class="math inline">\(\mathcal N\)</span> can be visualized as follows:</p>
<figure>
<img src="10/neural_network.png" alt="Neural network representation of \mathcal N^D_{n,\alpha} and \mathcal H^D_{n,\alpha}." /><figcaption aria-hidden="true">Neural network representation of <span class="math inline">\(\mathcal N^D_{n,\alpha}\)</span> and <span class="math inline">\(\mathcal H^D_{n,\alpha}\)</span>.</figcaption>
</figure>
<p>Each node <span class="math inline">\(x_{i,j}\)</span> in this graph, i.e, the <span class="math inline">\(j\)</span>-th component of the vector <span class="math inline">\(x_i\in\mathbb R^{n_i}\)</span>, represents the activation output <span class="math display">\[\begin{align}
  x_{i,j} = \alpha_{i,j}(z_i)
\end{align}\]</span> of the so-called <em>neuron</em> <span class="math inline">\(j\)</span> of layer <span class="math inline">\(i\)</span>. Here, <span class="math inline">\(\alpha_{i,j}(z_i)\)</span> denotes the <span class="math inline">\(j-th\)</span> component of the vector <span class="math inline">\(\alpha_i(z_i)\)</span>. Moreover, the edges denote the <em>activation flow</em> <span class="math display">\[\begin{align}
  z_i = W_i\cdot x_{i-1}+b_i
\end{align}\]</span> from the previous layer <span class="math inline">\((i-1)\)</span> to target layer <span class="math inline">\(i\)</span> given the weights <span class="math inline">\(W_i\in\mathbb R^{n_i\times n_{i-1}}\)</span> and the bias vector <span class="math inline">\(b_i\in\mathbb R^{n_i}\)</span>. If, for every layer, all neuron of the previous layer are connected to neurons on the next layer with potentially non-zero weights, one calls the network <em>densely connected</em>.</p>
<p>In principle, each activation function <span class="math inline">\(\alpha_i\)</span> could be chosen individually. Here, it is only important to observe that they need to be non-linear in order to result in a nonlinear function <span class="math inline">\(f\in\mathcal N^D_{n,\alpha}\)</span>. As long as the activations are sigmoidal, the resulting networks can usually be shown to approximate any target hypotheses provided <span class="math inline">\(D=2\)</span> and the width of this layer <span class="math inline">\(i=1\)</span> is sufficiently large. In practice, however, the architectural choice of the network can have a great impact on the training performance. In particular, architectures which are deeper but not too wide tend to be easier to train that shallow and brought ones. Deeper neural networks can also be structured into chucks of layers, each of which is supposed to conduct a certain partial learning task. For example, in modern object recognition architectures, the first chucks of layers are typically <em>convolution layers</em>, which are sparsely connected layers with batches of neurons that even shared the same weights. Those layers are well adapted to recognize relief features, e.g., by learning of discrete derivatives. Their outputs are then collected and fed into a densely connected network that is supposed to learn the logical implications of the detected relief features.</p>
<p>In order to come back to our XOR-gate example, we will close our discussion by showing that a sufficiently brought neural network with <span class="math inline">\(D=2\)</span> layers is expressive enough to represent any boolean function.</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="representation-of-boolean-functions-by-neural-networks">(Representation of boolean functions by neural networks)</h5>
Let <span class="math inline">\(n_0\in\mathbb N\)</span> suppose we are given a concept <span class="math inline">\(c:\{0,1\}^{n_0}\to\{0,1\}\)</span> to learn. Then, there is a <span class="math inline">\(h\in\mathcal  N^D_{n,\alpha}\)</span> for <span class="math inline">\(D=2\)</span>, <span class="math inline">\(n=(n_0,n_1,1)\)</span> with <span class="math inline">\(n_1\in\mathbb N\)</span>, and <span class="math inline">\(\alpha=(\theta,\theta)\)</span>, given <span class="math display">\[\begin{align}
\theta(z):=\begin{cases}
  1 &amp; \text{for } z\geq 0\\
  0 &amp; \text{for } z &lt; 0
\end{cases}
\,,
  \end{align}\]</span> such that for all <span class="math inline">\(x\in\{0,1\}^{n_0}\)</span> we have <span class="math inline">\(h(x)=c(x)\)</span>. Furthermore, the bound <span class="math inline">\(n_1\leq 2^{n_0}\)</span> holds true.
</dd>
</dl>
<p><strong>Proof:</strong> Let us denote the image of <span class="math inline">\(\{1\}\)</span> under <span class="math inline">\(c\)</span> as <span class="math display">\[\begin{align}
  c^{-1}(\{1\})=\{a_1,a_2,\ldots, a_k\}.
\end{align}\]</span> Hence, for all <span class="math inline">\(x\in\{0,1\}^{n_0}\)</span>: <span class="math display">\[\begin{align}
  c(x) = \theta\left(
    -1 + \sum_{j=1}^k 1_{x=a_j}
  \right),
  \tag{E1}
  \label{eq_1}
\end{align}\]</span> and it suffices to show that we can find a representation of <span class="math display">\[\begin{align}
  1_{x=x_j}
  =
  \begin{cases}
    1 &amp; \text{for } x=a_j\\
    0 &amp; \text{for }  x\neq a_j
  \end{cases}
\end{align}\]</span> so that the right-hand side of <span class="math inline">\(\eqref{eq_1}\)</span> can be recast into the form of an element in <span class="math inline">\(\mathcal N^D_{n,\alpha}\)</span>.</p>
<p>For this purpose, let us consider the value table</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"><span class="math inline">\(a\)</span></th>
<th style="text-align: center;"><span class="math inline">\(b\)</span></th>
<th style="text-align: center;"><span class="math inline">\((2ab-a-b)\)</span></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
</tr>
<tr class="even">
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">-1</td>
</tr>
<tr class="odd">
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">-1</td>
</tr>
<tr class="even">
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
</tr>
</tbody>
</table>
<p>which implies <span class="math display">\[\begin{align}
  1_{a=b} = \theta(2ab-a-b).
\end{align}\]</span></p>
<p>Hence, for <span class="math inline">\(x,a_j\in\{0,1\}^{n_0}\)</span> we have <span class="math display">\[\begin{align}
  1_{x=a_j} = \theta\left(
    \sum_{i=1}^{n_0}\left(
      2x_i a_{j,i}-x_i-a_{j,i}
    \right)
  \right),
  \tag{E2}
  \label{eq_2}
\end{align}\]</span> denoting the <span class="math inline">\(i\)</span>-th component of the vectors <span class="math inline">\(x\)</span> and <span class="math inline">\(a_j\)</span> by <span class="math inline">\(x_i\)</span> and <span class="math inline">\(a_{j,i}\)</span>, respectively.</p>
<p>From <span class="math inline">\(\eqref{eq_2}\)</span> we may now read off the required layer activations in a neural network. First, we observe <span class="math display">\[\begin{align}
  z_{1,j}
  =\left(W_1\cdot x+b_i\right)_j 
  &amp;\overset{!}{=}
  \sum_{i=1}^{n_0}\left(2x_ia_{j,i}-x_i-a_{j,i}\right),
\end{align}\]</span> which implies <span class="math display">\[\begin{align}
  \begin{split}
  W_{1,ji} &amp;= 2a_{j,i}-1,\\
  b_{1,j} &amp;= -\sum_{i=1}^{n_0} a_{j,k}
  \end{split}
  \qquad
  \text{for} \quad i=1,\ldots,n_0,
  \quad
  j=1,\ldots, k.
\end{align}\]</span> Second, we would define <span class="math display">\[\begin{align}
  x_{i,j} = \theta(z_{1,j}).
\end{align}\]</span> so that from <span class="math inline">\(\eqref{eq_2}\)</span> we can read off <span class="math display">\[\begin{align}
  z_2 = W_2\cdot x_1+b_2
  &amp;
  \overset{!}{=}
  -1+\sum_{j=1}^k x_{1,j},
\end{align}\]</span> which implies <span class="math display">\[\begin{align}
  W_{2,i} &amp;= 1 
  \qquad
  \text{for}
  \quad
  i=1,\ldots, k,\\
  b_2 &amp;= -1.
\end{align}\]</span> And third, we would define <span class="math display">\[\begin{align}
  x_2 = \theta(z_2).
\end{align}\]</span></p>
<p>In summary, and setting the number of neurons in the middle layer to <span class="math inline">\(n_1=k\)</span>, we have shown that there are <span class="math display">\[\begin{align}
  W_1\in\mathbb R^{n_1\times n_0}, b_1\in\mathbb R^{n_1}\\
  W_2\in\mathbb R^{1\times n_1}, b_2\in\mathbb R^1
\end{align}\]</span> such that the map <span class="math display">\[\begin{align}
  h(x) 
  &amp; = \theta\left(z_2\right)\\
  &amp; = \theta\left(W_2\cdot x_1+b_2\right)\\
  &amp; = \theta\left(W_2\cdot \left(\theta\left(z_1\right)\right)+b_2\right)\\
  &amp; = \theta\left(W_2\cdot \left(\theta\left(W_1 \cdot x + b_1\right)\right)+b_2\right)
\end{align}\]</span> fulfills <span class="math display">\[\begin{align}
  h\in\mathcal N^2_{(n_0,n_1,1),(\theta,\theta)}
\end{align}\]</span> and for all <span class="math inline">\(x\in\{0,1\}^{n_0}\)</span> <span class="math display">\[\begin{align}
  h(x)=c(x).
\end{align}\]</span></p>
<p>Finally, the image of <span class="math inline">\(\{1\}\)</span> under <span class="math inline">\(c\)</span> is a subset of <span class="math inline">\(\{0,1\}^{n_0}\)</span>, and hence, <span class="math inline">\(k=n_1\leq 2^{n_0}\)</span>.</p>
<p><span class="math inline">\(\square\)</span></p>
<dl>
<dt>Remark</dt>
<dd>This construction requires an exponential increase of neurons in the hidden layer, which renders the result academic. In practice, it is often easier to increase the depth of the network opposed to its width while still keeping the training efforts reasonably low. Historically, this was very likely one of the reasons for the name “deep learning”. Nowadays, however, it is used differently and one refers to deep learning in the sense that learning is done on the basis of very primitive features fed to the input layer of neurons, such as pixels of an image, instead of manually engineered features such as the above mentioned non-linear map <span class="math inline">\(\Phi\)</span>. Furthermore, the bound <span class="math inline">\(n_1\leq 2^{n_0}\)</span> can, of course, be improved to <span class="math display">\[\begin{align}
k\leq \min\left\{\left|c^{-1}(\{0\}\right|, \left|c^{-1}(\{1\}\right|\right\},
  \end{align}\]</span> and by the same method one can show that also networks with layers <span class="math inline">\(D\geq 2\)</span> are able to represent boolean functions.
</dd>
</dl>
<p>In the next module we will look a bit deeper into the representation and approximation capabilities of neural networks while for next week’s exercises we will take our time to finally start an implementation of a deeper network.</p>
<p>➢ Next session!</p>
<dl>
<dt>👀</dt>
<dd>For a two-layer Adaline network of your choice, compute the update rule, and implement a training for a boolean-type learning task of your choice.
</dd>
</dl>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
