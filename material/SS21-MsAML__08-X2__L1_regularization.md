# Exercise 08-2: $L_1$-regularization [[PDF](SS21-MsAML__08-X2__L1_regularization.pdf)]

Given an empirical loss $\hat{L}(\bar{w})$, define the regularized empirical loss by 

$$\hat{L}_{reg}(\bar{w}) = \hat{L}(\bar{w}) + R(\bar{w}) \text{ for } R(\bar{w})=\frac{\lambda}{d}\sum_{i=1}^d|w_i|.$$

Propose an update rule for minimizing $\hat{L}_{reg}$, discuss properties of regularization induced by $R(\bar{w})$ and compare to $L_2$-regularization.

