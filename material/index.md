# Mathematic(al Statistic)s and Applications of Machine Learning [[PDF](index.pdf)]

* Lecture of summer semester 2021
* [Department of Mathematics](https://www.math.lmu.de) at the 
  [LMU Munich](https://www.en.uni-muenchen.de)
* Lecturer [D.-A. Deckert](http://www.mathematik.uni-muenchen.de/~deckert)
* Assistant [M. Seleznova](https://www.ai.math.uni-muenchen.de/members/phd_students/seleznova/index.html)
* [UNI2WORK Course Site](https://uni2work.ifi.lmu.de/course/S21/MI/M%28S%29AML)
* [GitLab repository](https://gitlab.com/dirk-deckert-lmu/ss21-msaml)

Table of contents

1. [Course material overviews](#course-material-overviews)
   * [Overview of MAML topics](#overview-of-maml-topics)
   * [Overview of MSAML topics](#overview-of-msaml-topics)
   * [Data challenge](#data-challenge)
2. [Weekly course material](#weekly-course-material)
   * [Week of April 12: Lecture 01](#week-of-april-12-lecture-01)
   * [Week of April 19: Lecture 02](#week-of-april-19-lecture-02)
   * [Week of April 26: Lecture 03](#week-of-april-26-lecture-03)
   * [Week of May 3: Lecture 04](#week-of-may-3-lecture-04)
   * [Week of May 10: Lecture 05](#week-of-may-10-lecture-05)
   * [Week of May 17: Lecture 06](#week-of-may-17-lecture-06)
   * [Week of May 24: Lecture 07](#week-of-may-24-lecture-07)
   * [Week of May 31: Lecture 08](#week-of-may-31-lecture-08)
   * [Week of June 7: Lecture 09](#week-of-june-7-lecture-09)
   * [Week of June 14: Lecture 10](#week-of-june-14-lecture-10)
   * [Week of June 21: Lecture 11](#week-of-june-21-lecture-11)
   * [Week of June 28: Lecture 12](#week-of-june-28-lecture-12)
   * [Week of July 5: Lecture 13](#week-of-july-5-lecture-13)
   * [Week of July 12: Lecture 14](#week-of-july-12-lecture-14)

Back to the [GitLab course site](https://gitlab.com/dirk-deckert-lmu/ss21-msaml).


## Course material overviews 


### Overview of MAML topics 

* [Introduction and overview](SS21-MsAML__01-2__Introduction_and_overview.md) 
* [Supervised learning setting](SS21-MsAML__01-3__Supervised_learning_setting.md)
* [Linear classification and the perceptron definition](SS21-MsAML__02-1__Linear_classification.md)
  * [Perceptron convergence](SS21-MsAML__03-1__Perceptron_convergence.md)
  * [Adaline definition](SS21-MsAML__03-2__Adaline_definition.md)
  * [Perceptron-Adaline comparison](SS21-MsAML__04-1__Perceptron-Adaline_comparison.md)
* [No free lunch theorems](SS21-MsAML__04-2__No_free_lunch_theorems.md)
* [General class of linear classification models](SS21-MsAML__05-1__Linear_classification_models.md)
  * [Cross entropy](SS21-MsAML__06-1__Cross_entropy.md)
  * [Further conditioning and tuning](SS21-MsAML__08-1__Conditioning_and_tuning.md)
  * [Multi-class classification](SS21-MsAML__08-2__Multi-class_classification.md)
  * [Support Vector Machines](SS21-MsAML__09-1__Support_vector_machines.md)
* [Non-linear classification](SS21-MsAML__10-1__Non-linear_classification.md)
  * [Representation and approximation](SS21-MsAML__11-1__Representation_and_approximation.md)
* [A short review of optimization](SS21-MsAML__12-1__Short_review_optimization.md)
  * [Dual formulation of the SVM](SS21-MsAML__13-1__Dual_formulation_SVM.md)
  * [Kernel support vector machines](SS21-MsAML__14-1__Kernel_SVMs.md)


### Overview of MSAML topics 

* \(S\) [Statistical framework](SS21-MsAML__01-S1__Statistical_framework.md)
* \(S\) [Error decomposition](SS21-MsAML__02-S1__Error_decomposition.md)
* \(S\) [A first simple learning setting](SS21-MsAML__03-S1__A_simple_learning_setting.md)
* \(S\) [PAC learning framework](SS21-MsAML__04-S1__PAC_learning_framework.md)
  * \(S\) [Concentration inequalities](SS21-MsAML__04-S2__Concentration_inequalities.md)
  * \(S\) [Inconsistent case](SS21-MsAML__05-S1__Inconsistent_case.md)
  * \(S\) [Stochastic case](SS21-MsAML__05-S1__Stochastic_case.md)
  * \(S\) [Infinite hypotheses case](SS21-MsAML__06-S1__Infinite_hypotheses.md)
  * \(S\) [Growth function](SS21-MsAML__07-S1__Growth_function.md)
  * \(S\) [VC-Dimension](SS21-MsAML__07-S2__VC-Dimension.md)
  * \(S\) [Computational tools for the VC-dimension](SS21-MsAML__08-S1__Computational_tools_VC-dimension.md)
  * \(S\) [Fundamental theorem of binary classification](SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.md)
  * \(S\) [Non-uniform learnability](SS21-MsAML__09-S1__Non-uniform_learnability.md)
* \(S\) [Reinforcement Learning](SS21-MsAML__11-S1__Reinforcement_learning.md)
  * \(S\) [Construction of the value function](SS21-MsAML__12-S1__Value_function.md)
  * \(S\) [Optimal policies](SS21-MsAML__13-1__Optimal_policies.md)
  * \(S\) [Q-value iteration](SS21-MsAML__14-S1__Q-value_iteration.md)


### Data challenge

The goal of the data challenge is to implement a neural network model for classification of the well-known MNIST dataset of handwritten digits. You can find implementation and description of our baseline model below:

* [Description](handwritten-digit-recognition/README.md)
* [Code folder](handwritten-digit-recognition/)

Your goal is to build your own model that performs better than the baseline. You can use/change/extend the above implementation any way you want within the following rules:

* External implementations of machine learning models/tools (sklearn, TensorFlow, PyTorch, etc.) are not allowed.
* You can use linear algebra packages (e.g. numpy), general scientific/mathematical packages (e.g. scipy), visualisation packages (e.g. matplotlib), etc.
* Model training should only use the training set, no information about the test set can be used.
* Models should be submitted as source code that performs training (not accuracy score, saved parameters, etc.)

The deadline to submit your model is Wednesday, July 7th. Afterwards, we will compare the results and determine the winners.


## Weekly course material 


### Week of April 12: Lecture 01

Organization:

* [Get-to-know session](SS21-MsAML__01-1__Get_to_know_session.pdf) [[Video](https://cast.itunes.uni-muenchen.de/clips/hcrhoalPU3/vod/online.html)]

Lectures notes:

* [Introduction and overview](SS21-MsAML__01-2__Introduction_and_overview.md)
* [Supervised learning setting](SS21-MsAML__01-3__Supervised_learning_setting.md)
* \(S\) [Statistical framework](SS21-MsAML__01-S1__Statistical_framework.md)

Exercises and solutions:

|       | Exercise                                                                                                       | Discussion | Solution                                                                                                                                                                                        |
|-------|----------------------------------------------------------------------------------------------------------------|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 01-0  | [Python warmup](SS21-MsAML__01-X0__Python_warmup.ipynb)                                                       | #5         | included                                                                                                                                                                                        |
| 01-1  | [Set up a Python development environment](SS21-MsAML__01-X1__Set_up_a_Python_development_environment.md)      | #6         | included                                                                                                                                                                                        |
| 01-2  | [First steps with Numpy](SS21_MsAML__01-X2__First_steps_with_numpy.ipynb)                                     | #7         | included                                                                                                                                                                                        |
| 01-3  | [Plotting and obtaining data](SS21_MsAML__01-X3__Plotting_and_obtaining_data.ipynb)                           | #8         | included                                                                                                                                                                                        |
| 01-M1 | [Histograms from Covid-19 time series data](SS21_MsAML__01-XM1__Histograms_from_Covid-19_time_series_data.md) | #9         | [IPython Notebook](SS21_MsAML__01-XM1__Histograms_from_Covid-19_time_series_data.ipynb)                                                                                                        |
| 01-S1 | [Probability warm up](SS21-MsAML__01-XS1__Probability_warm_up.md)                                             | #10        | [1](SS21-MsAML__01-XS1__Empirical_Risk_Expectation_SOLUTION.pdf), [2](SS21-MsAML__01-XS1__Quadratic_risk_decomposition_SOLUTION.pdf), [3](SS21-MsAML__01-XS1__Bayes_classifier_SOLUTION.pdf) |


### Week of April 19: Lecture 02

Lectures notes:

* [Linear classification and the perceptron definition](SS21-MsAML__02-1__Linear_classification.md)
* \(S\) [Error decomposition](SS21-MsAML__02-S1__Error_decomposition.md)

Exercises and solutions:

|       | Exercise                                                                                                                             | Discussion | Solution                                                                                       |
|-------|--------------------------------------------------------------------------------------------------------------------------------------|------------|------------------------------------------------------------------------------------------------|
| 02-1  | [Limits of linear classification](SS21-MsAML__02-X1__Limits_of_linear_classification.md)                                             | #16        | [Solution](SS21-MsAML__02-X1__Limits_of_linear_classification_SOL.pdf)                         |
| 02-2  | [Preparation of Python tools for linear classification](SS21-MsAML__02-X2__Preparation_of_Python_tools_for_linear_classification.md) | #17        | [Solution](SS21-MsAML__02-X2__Preparation_of_Python_tools_for_linear_classification_SOL.ipynb) |
| 02-3  | [Perceptron_implementation](SS21-MsAML__02-X3__Perceptron_implementation.md)                                                         | #18        | [Solution](SS21-MsAML__02-X3__Perceptron_implementation_SOL.ipynb)                             |
| 02-S1 | [Linear regression model](SS21-MsAML__02-XS1__Linear_regression_model.md)                                                            | #19        | [Solution](SS21-MsAML__02-XS1__Linear_regression_model_SOL.pdf)                                |


### Week of April 26: Lecture 03

Lectures notes:

* [Perceptron convergence](SS21-MsAML__03-1__Perceptron_convergence.md)
* [Adaline definition](SS21-MsAML__03-2__Adaline_definition.md)
* \(S\) [A first simple learning setting](SS21-MsAML__03-S1__A_simple_learning_setting.md)

Exercises and solutions:

|       | Exercise                                                                                     | Discussion | Solution                                                                                                                               |
|-------|----------------------------------------------------------------------------------------------|------------|----------------------------------------------------------------------------------------------------------------------------------------|
| 03-1  | [Perceptron convergence](SS21-MsAML__03-X1__Perceptron_convergence.md)                       | #21        | [Solution](SS21-MsAML__03-X1__Perceptron_convergence_SOL.pdf)                                                                          |
| 03-2  | [Adaline learning rate](SS21-MsAML__03-X2__Adaline_learning_rate.md)                         | #22        | [Solution](SS21-MsAML__03-X2__Adaline_learning_rate_SOL.pdf), [Jupyter Notebook](SS21-MsAML__03-X2__Adaline_learning_rate_SOL_1.ipynb) |
| 03-S1 | [A first simple learning setting](SS21-MsAML__03-S1__A_simple_learning_setting.md) Tasks 1-5 | #23        | included                                                                                                                               |
| 03-S2 | [Gradient descent with least squares](SS21-MsAML__03-XS2__Gradient_descent_least_squares.md) | #24        | [Solution](SS21-MsAML__03-XS2__Gradient_descent_least_squares_SOL.pdf)                                                                 |


### Week of May 3: Lecture 04

Lectures notes:

* [Perceptron-Adaline comparison](SS21-MsAML__04-1__Perceptron-Adaline_comparison.md)
* [No free lunch theorems](SS21-MsAML__04-2__No_free_lunch_theorems.md)
* \(S\) [PAC learning framework](SS21-MsAML__04-S1__PAC_learning_framework.md)
* \(S\) [Concentration inequalities](SS21-MsAML__04-S2__Concentration_inequalities.md)


Exercises and solutions:

|       | Exercise                                                                                                              | Discussion | Solution |
|-------|-----------------------------------------------------------------------------------------------------------------------|------------|----------|
| 04-1  | [Adaline convergence behavior](SS21-MsAML__04-X1__Adaline_convergence_behavior.md)                                    | #27        |   [Solution (pdf)](SS21-MsAML__04-X1__Adaline_convergence_behavior_SOL.pdf), [Jupyter Notebook (b,c)](SS21-MsAML__04-X1__Adaline_convergence_behavior_SOL_bc.ipynb), [Jupyter Notebook (f)](SS21-MsAML__04-X1__Adaline_convergence_behavior_SOL_f.ipynb)      |
| 04-S1 | [Learning the concept of intervals](SS21-MsAML__04-XS1__Learning_the_concept_of_intervals.md)                         | #28        |  [Solution](SS21-MsAML__04-XS1__Learning_the_concept_of_intervals_SOL.pdf)        |
| 04-S2 | [Review of standard concentration inequalities](SS21-MsAML__04-XS2__Review_of_standard_concentration_inequalities.md) | #29        |  [Solution](SS21-MsAML__04-XS2__Review_of_standard_concentration_inequalities_SOL.pdf)        |


### Week of May 10: Lecture 05

Organization:

* [First status meeting](SS21-MsAML__05-0__First_status_meeting.pdf) [[Video](https://cast.itunes.uni-muenchen.de/clips/K5VBHdGqVk/vod/online.html)]

Lectures notes:

* [General class of linear classification models](SS21-MsAML__05-1__Linear_classification_models.md)
* \(S\) [Inconsistent case](SS21-MsAML__05-S1__Inconsistent_case.md)
* \(S\) [Stochastic case](SS21-MsAML__05-S1__Stochastic_case.md)


Exercises and solutions:

|       | Exercise                                                                                             | Discussion | Solution |
|-------|------------------------------------------------------------------------------------------------------|------------|----------|
| 05-1 | [Linear classification models](SS21-MsAML__05-X1__Linear_classification_models.md)          |   #32         |   [Solution (a,d)](SS21-MsAML__05-X1__Linear_classification_models_SOL.pdf), [Jupyter (b,tanh)](SS21-MsAML__05-X1__Linear_classification_models_SOL_1.ipynb), [Jupyter (b,logistic)](SS21-MsAML__05-X1__Linear_classification_models_SOL_2.ipynb), [Jupyter (c)](SS21-MsAML__05-X1__Linear_classification_models_SOL_3.ipynb)       |
| 05-S1 | [Learning the concept of intervals with noise](SS21-MsAML__05-XS1__Learning_intervals_with_noise.md)                         |   #33    | [Solution](SS21-MsAML__05-XS1__Learning_intervals_with_noise_SOL.pdf)    |



### Week of May 17: Lecture 06

Lectures notes:

* [Cross entropy](SS21-MsAML__06-1__Cross_entropy.md)
* \(S\) [Infinite hypotheses case](SS21-MsAML__06-S1__Infinite_hypotheses.md)

Exercises and solutions:

|       | Exercise                                                              | Discussion | Solution                                                      |
|-------|-----------------------------------------------------------------------|------------|---------------------------------------------------------------|
| 06-1  | [Cross entropy](SS21-MsAML__06-X1__Cross_entropy.md)                  | #36        | [Solution](SS21-MsAML__06-X1__Cross_entropy_SOL.pdf)          |
| 06-S1 | [Rademacher complexity](SS21-MsAML__06-XS1__Rademacher_complexity.md) | #37        | [Solution](SS21-MsAML__06-XS1__Rademacher_complexity_SOL.pdf) |


### Week of May 24: Lecture 07

Lectures notes:

* holiday
* \(S\) [Growth function](SS21-MsAML__07-S1__Growth_function.md)
* \(S\) [VC-Dimension](SS21-MsAML__07-S2__VC-Dimension.md)

Exercises and solutions:

|       | Exercise                                                              | Discussion | Solution                                                      |
|-------|-----------------------------------------------------------------------|------------|---------------------------------------------------------------|
| 07-S1  | [VC dimension examples](SS21-MsAML__07-XS1__VC_dimension_examples.md)                  | #40        |  [Solution](SS21-MsAML__07-XS1__VC_dimension_examples_SOL.pdf)        |


### Week of May 31: Lecture 08

Lectures notes:

* [Further conditioning and tuning](SS21-MsAML__08-1__Conditioning_and_tuning.md)
* [Multi-class classification](SS21-MsAML__08-2__Multi-class_classification.md)
* \(S\) [Computational tools for the VC-dimension](SS21-MsAML__08-S1__Computational_tools_VC-dimension.md)
* \(S\) [Fundamental theorem of binary classification](SS21-MsAML__08-S1__Fundamental_theorem_binary_classification.md)

Exercises and solutions:

|       | Exercise                                                                                            | Discussion | Solution                                                                                                                                   |
|-------|-----------------------------------------------------------------------------------------------------|------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| 08-1  | [Standardization and initial weights](SS21-MsAML__08-X1__Standardization_and_initial_weights.md)    | #44        | [Solution](SS21-MsAML__08-X1__Standardization_and_initial_weights_SOL.pdf)                                                                 |
| 08-2  | [L1 regularization](SS21-MsAML__08-X2__L1_regularization.md)                                        | #45        | [Solution](SS21-MsAML__08-X2__L1_regularization_SOL.pdf)                                                                                   |
| 08-3  | [Multi-class classification](SS21-MsAML__08-X3__Multi-class-classification.md)                      | #46        | [Solution](SS21-MsAML__08-X3__Multi-class-classification_SOL.pdf), [Jupyter Notebook](SS21-MsAML__08-X3__Multi-class-classification.ipynb) |
| 08-S1 | [Fundamental theorem and VC-dimension](SS21-MsAML__08-XS1__Fundamental_theorem_and_VC_dimension.md) | #47        | [Solution](SS21-MsAML__08-XS1__Fundamental_theorem_and_VC_dimension_SOL.pdf)                                                               |


### Week of June 7: Lecture 09

Lectures notes:

* [Support Vector Machines](SS21-MsAML__09-1__Support_vector_machines.md)
* [Non-uniform learnability](SS21-MsAML__09-S1__Non-uniform_learnability.md)

Exercises and solutions:

|       | Exercise                                                                 | Discussion | Solution |
|-------|--------------------------------------------------------------------------|------------|----------|
| 09-1  | [Support vector machines](SS21-MsAML__09-X1__Support_vector_machines.md) | #51        | [Solution](SS21-MsAML__09-X1__Support_vector_machines_SOL.pdf), [Jupyter Notebook](SS21-MsAML__09-X1__Support_vector_machines.ipynb)         |
| 09-S1 | [PAC learning review](SS21-MsAML__09-XS1__PAC_learning_review.md)        | #52        |  [Solution](SS21-MsAML__09-XS1__PAC_learning_review_SOL.pdf)         |
| 09-S2 | [Model selection](SS21-MsAML__09-XS2__Model_selection.md)                | #53        |    [Solution](SS21-MsAML__09-XS2__Model_selection_SOL.pdf)      |


### Week of June 14: Lecture 10

Lectures notes:

* [Non-linear classification](SS21-MsAML__10-1__Non-linear_classification.md)
* \(S\) [Boosting](SS21-MsAML__10-S1__Boosting.md)

Exercises and solutions:

|      | Exercise                                                                     | Discussion | Solution |
|------|------------------------------------------------------------------------------|------------|----------|
| 10-1 | [Non-linear classification](SS21-MsAML__10-X1__Non-linear_classification.md) | #55        | [Jupyter notebook (a)](SS21_MsAML__10_X1__Non_linear_classification.ipynb),[Solution (b)](SS21-MsAML__10-X1__Non-linear_classification_SOL.pdf)         |


### Week of June 21: Lecture 11

Organization:

* [Second status meeting](SS21-MsAML__11-0__Second_Status.pdf) [[Video](https://cast.itunes.uni-muenchen.de/clips/OyAYFc3kcA/vod/online.html)]

Lectures notes:

* [Representation and approximation](SS21-MsAML__11-1__Representation_and_approximation.md)
* \(S\) [Reinforcement Learning](SS21-MsAML__11-S1__Reinforcement_learning.md)

Exercises and solutions:

|      | Exercise                                                  | Discussion | Solution |
|------|-----------------------------------------------------------|------------|----------|
| 11-1 | [Backpropagation](SS21-MsAML__11-X1__Backpropagation.pdf) | #59        | included |


### Week of June 28: Lecture 12

Lectures notes:

* [A short review of optimization](SS21-MsAML__12-1__Short_review_optimization.md)
* \(S\) [Construction of the value function](SS21-MsAML__12-S1__Value_function.md)

Exercises and solutions:

|      | Exercise                                              | Discussion | Solution |
|------|-------------------------------------------------------|------------|----------|
| 12-1 | [KKT criterion](SS21-MsAML__12-X1__KKT_criterion.pdf) | #60        | included |


### Week of July 5: Lecture 13

Lectures notes:

* [Dual formulation of the SVM](SS21-MsAML__13-1__Dual_formulation_SVM.md)
* \(S\) [Optimal policies](SS21-MsAML__13-1__Optimal_policies.md)

Exercises and solutions:

|  | Exercise       | Discussion | Solution |
|--|----------------|------------|----------|
|  | Data challenge |            |          |


### Week of July 12: Lecture 14

Lectures notes:

* [Kernel support vector machines](SS21-MsAML__14-1__Kernel_SVMs.md)
* \(S\) [Q-value iteration](SS21-MsAML__14-S1__Q-value_iteration.md)

Exercises and solutions:

|  | Exercise       | Discussion | Solution |
|--|----------------|------------|----------|
|  | Data challenge |            |          |
