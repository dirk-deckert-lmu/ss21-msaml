<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__12-S1__Value_function</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-12-s1-pdf">MsAML-12-S1 [<a href="SS21-MsAML__12-S1__Value_function.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#construction-of-the-value-function">Construction of the value function</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="construction-of-the-value-function">Construction of the value function</h2>
<p>Recall how our definitions of the policy and state-action value functions depended on the future rewards. It should be possible to compute expectations of such accumulated quantities on the basis of the transition probabilities <span class="math inline">\(\mathcal P\)</span> of the MDP. Indeed, based on our definition of the policy and state-action value, we find the so-called <em>Bellmann equation</em> given additionally the reward function <span class="math inline">\(R\)</span>, policy <span class="math inline">\(\pi\)</span>, and state-action pair <span class="math inline">\((s,a)\)</span>: <span class="math display">\[\begin{align}
  V_\pi(s)
  &amp;=
  E\left(
    \sum_{t=0}^\infty \gamma^t R(S_t,\pi(S_t),S_{t+1})
    \,\Big|\, 
    S_0=s
    \right)
  \\
  &amp;=
  E\left(
    R(s,\pi(s),S_1)+\gamma V_\pi(S_1) 
    \,\Big|\, 
    S_0=s
  \right)
  \\
  &amp;=
  \sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,\pi(s))
  \left[
    R(s,\pi(s),s&#39;)+\gamma V_\pi(s&#39;)
  \right],
  \label{eq_B1}
  \tag{B1}
\end{align}\]</span> and similarly, <span class="math display">\[\begin{align}
  Q_\pi(s,a)
  &amp;=
  \sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,a)
  \left[
    R(s,a,s&#39;)+\gamma Q_\pi(s&#39;,\pi(s&#39;))
  \right].
  \label{eq_B2}
  \tag{B2}
\end{align}\]</span> As before, observe the simple relation <span class="math inline">\(V_\pi(s)=Q_\pi(s,\pi(s))\)</span>.</p>
<p>It will strike the eye that the Bellman equations <span class="math inline">\(\eqref{eq_B1}\)</span> and <span class="math inline">\(\eqref{eq_B2}\)</span> are of a fixed-point form. Therefore, tuning <span class="math inline">\(\gamma&lt;1\)</span> one may construct solutions with the help of Banach’s fixed point argument.</p>
<p>For this purpose, consider the sets <span class="math display">\[\begin{align}
  \mathcal V
  &amp;:= 
  \left\{
    V:\mathcal S\to\mathcal R
    \,\big|\,
    V \text{ measurable}
  \right\},\\
  \mathcal Q
  &amp;:= 
  \left\{
    Q:\mathcal S\times \mathcal A\to\mathcal R
    \,\big|\,
    Q \text{ measurable}
  \right\}
\end{align}\]</span> on which we define the following self-maps in vein of <span class="math inline">\(\eqref{eq_B1}\)</span> and <span class="math inline">\(\eqref{eq_B2}\)</span>: <span class="math display">\[\begin{align}
  T_\pi^{\mathcal V}: \mathcal V &amp;\to \mathcal V
  \\
  V &amp;\mapsto 
  \left(
    s\mapsto \sum_{s&#39;\in\mathcal S}
    p(s&#39;|s,\pi(s))
    \left[
      R(s,\pi(s),s&#39;)+\gamma V(s&#39;)
    \right]
  \right),
  \\
  T_\pi^{\mathcal Q}: \mathcal Q &amp;\to \mathcal Q
  \\
  Q &amp;\mapsto 
  \left(
    (s,a)\mapsto
    \sum_{s&#39;\in\mathcal S}
    p(s&#39;|s,a)
    \left[
      R(s,a,s&#39;)+\gamma Q(s&#39;,\pi(s&#39;))
    \right]
  \right).
\end{align}\]</span> Recursively, for every <span class="math inline">\(k\in\mathbb N\)</span>, <span class="math inline">\(V\in\mathcal V\)</span>, <span class="math inline">\(Q\in\mathcal Q\)</span>, we define the repetitive application of these self-maps as <span class="math display">\[\begin{align}
  (T_\pi^{\mathcal V})^k(V)
  &amp;:=
  T^{\mathcal V}_\pi\left((T_\pi^{\mathcal V})^{k-1}(V)\right),
  \\
  T_\pi^{\mathcal Q})^k(Q)
  &amp;:=
  T^{\mathcal Q}_\pi\left((T_\pi^{\mathcal Q})^{k-1}(Q)\right)
\end{align}\]</span> together with <span class="math inline">\((T^{\mathcal V}_\pi)^0(V):=V\)</span> and <span class="math inline">\((T^{\mathcal Q}_\pi)^0(Q):=Q\)</span>.</p>
<p>The fixed-points of these self-maps are exactly the policy and state-action value functions as the following theorem shows.</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="construction-of-the-policy-and-state-action-functions">(Construction of the policy and state-action functions)</h5>
Let state space <span class="math inline">\(\mathcal S\)</span> and action space <span class="math inline">\(\mathcal A\)</span> be finite and <span class="math inline">\(\gamma\in[0,1)\)</span>. Then, for all <span class="math inline">\(V\in\mathcal V\)</span> and <span class="math inline">\(Q\in\mathcal Q\)</span>, the equalities <span class="math display">\[\begin{align}
\lim_{k\to\infty} \max_{s\in\mathcal S} 
\left|
V_\pi(s)-(T_\pi^{\mathcal V})^k(V)
\right|
&amp;=
0,\\
\lim_{k\to\infty} \max_{s\in\mathcal S,a\in\mathcal A} 
\left|
Q_\pi(s,a)-(T_\pi^{\mathcal Q})^k(Q)(s,a)
\right|
&amp;=
0
  \end{align}\]</span> hold true.
</dd>
</dl>
<p><strong>Proof:</strong> We only prove the convergence to the policy value function <span class="math inline">\(V_\pi\)</span>. The convergence of the state-action value function <span class="math inline">\(Q_\pi\)</span> can be shown analogously.</p>
<p>Since <span class="math inline">\(\mathcal S\)</span> is finite, for any <span class="math inline">\(V_1,V_2\in \mathcal V\)</span>, the following map is well-defined <span class="math display">\[\begin{align}
  d(V_1,V_2) := \max_{s\in\mathcal S}\left|V_1(s)-V_2(s)\right|.
\end{align}\]</span> and gives rise to a metric <span class="math inline">\(d\)</span> on <span class="math inline">\(\mathcal V\)</span>.</p>
<p>Moreover, <span class="math inline">\((\mathcal V,d)\)</span> is a complete metric space. To see this, take a Cauchy sequence <span class="math inline">\((V_n)_{n\in\mathbb N}\)</span> in <span class="math inline">\((\mathcal V,d)\)</span>, and observe that, thanks to the uniformity in <span class="math inline">\(\mathcal S\)</span> of the metric <span class="math inline">\(d\)</span>, we find for each <span class="math inline">\(s\in\mathcal S\)</span> that <span class="math inline">\((V_n(s))_{n\in\mathbb N}\)</span> is a Cauchy sequence in <span class="math inline">\((\mathbb R,|\cdot|)\)</span>. But <span class="math inline">\((\mathbb R,|\cdot|)\)</span> is complete, which implies that there exists a limit <span class="math inline">\(V(s):=\lim_{n\to\infty}\mathcal V_n(s)\)</span>. Exploiting the finiteness of <span class="math inline">\(\mathcal S\)</span> again, we find <span class="math display">\[\begin{align}
  d(V,V_n)
  =
  \max_{s\in\mathcal S}\left|V(s)-V_n(s)\right| 
  \leq 
  \sum_{s\in\mathcal S}\left|V(s)-V_n(s)\right|  
  \underset{n\to\infty}{\to} 0
\end{align}\]</span> and also that <span class="math inline">\(V\)</span> is measurable, hence <span class="math inline">\(V\in\mathcal V\)</span>. In conclusion, <span class="math inline">\((\mathcal V,d)\)</span> is a complete metric space.</p>
<p>Given a <span class="math inline">\(V\in\mathcal V\)</span> we introduce the short-hand notation <span class="math display">\[\begin{align}
  \qquad V_k:=(T_\pi^{\mathcal V})^k(V)
  \qquad
  \text{for} \qquad k\in\mathbb N_0
\end{align}\]</span> and turn to studying the possible convergence properties of this sequence in <span class="math inline">\(k\)</span>.</p>
<p>Based on the definition of <span class="math inline">\(V_{k+1}\)</span> and <span class="math inline">\(V_k\)</span> and the fact that <span class="math inline">\(\mathcal P\)</span> are probability distributions, we infer <span class="math display">\[\begin{align}
  d(V_{k+1},V_k)
  &amp;=
  \max_{s\in\mathcal S}\left|V_{k+1}(s)-V_k(s)\right|
  \\
  &amp;=
  \max_{s\in\mathcal S}\left|
    \sum_{s&#39;\in\mathcal S}
    p(s&#39;|s,\pi(s)) \gamma \left[V_k(s&#39;)-V_{k-1}(s&#39;)\right]
  \right|
  \\
  &amp;\leq
  \gamma   
  \max_{s\in\mathcal S} \left|V_k(s)-V_{k-1}(s)\right|
  \;
  \underbrace{\sum_{s&#39;\in\mathcal S} p(s&#39;|s,\pi(s))}_{=1}
  \\
  &amp;\leq
  \gamma
  \;
  d(V_k,V_{k-1}).
\end{align}\]</span> In other words, the self-map <span class="math inline">\(T^{\mathcal V}_\pi\)</span> is <span class="math inline">\(\gamma\)</span>-contractive on <span class="math inline">\((\mathcal V,d)\)</span>.</p>
<p>By induction, we find for all <span class="math inline">\(k\in\mathbb N\)</span> <span class="math display">\[\begin{align}
  d(V_{k+1},V_k) \leq \gamma^k\; d(V_1,V_0)
\end{align}\]</span> and, with Banach’s standard argument, conclude the <span class="math inline">\((\mathcal V,d)\)</span>-Cauchy property of the sequence <span class="math inline">\((V_n)_{n\in\mathbb N_0}\)</span> as follows. By exploiting the properties of the metric <span class="math inline">\(d\)</span>, for all <span class="math inline">\(n&gt;m\in\mathbb N\)</span> we get <span class="math display">\[\begin{align}
  d(V_n,V_m) 
  &amp;\leq 
  \sum_{k=m}^{n-1} d(V_{k+1},V_k)
  \\
  &amp;\leq 
  d(V_{1},V_0) \sum_{k=m}^{\infty} \gamma^k
  \underset{m\to\infty}{\to} 0
\end{align}\]</span> as the geometric sequence is convergent. Thanks to completeness of <span class="math inline">\((\mathcal V,d)\)</span> and the properties of the limit, there exists a unique <span class="math inline">\(V^*\in\mathcal V\)</span> such that <span class="math display">\[\begin{align}
  \lim_{n\to\infty}d(V^*,V_n) = 0.
\end{align}\]</span></p>
<p>Lastly, we need to ensure that this limit point also has the correct properties. Take <span class="math inline">\(n\in\mathbb N,s\in\mathcal S\)</span> and consider <span class="math display">\[\begin{align}
  V_{n+1}(s)
  &amp;=
  T_\pi^{\mathcal V}(V_n)(s)
  \\
  &amp;=
  \sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,\pi(s)) \left[R(s,\pi(s),s&#39;)+\gamma V_n(s&#39;)\right].
\end{align}\]</span> We can now take the limits of the left- and right-hand side to conclude <span class="math display">\[\begin{align}
  V^*(s)
  =
  \sum_{s&#39;\in\mathcal S}
  p(s&#39;|s,\pi(s)) \left[R(s,\pi(s),s&#39;)+\gamma V^*(s&#39;)\right].
\end{align}\]</span> This implies <span class="math inline">\(V^*=V_\pi^{\mathcal V}\)</span>, or in other words, the fixed-point <span class="math inline">\(V^*\)</span> is exactly our policy value function <span class="math inline">\(V_\pi^{\mathcal V}\)</span>.</p>
<p><span class="math inline">\(\square\)</span></p>
<p>Note that the above theorem does not only guarantee the existence and uniqueness of <span class="math inline">\(V_\pi\)</span> and <span class="math inline">\(Q_\pi\)</span> given a policy <span class="math inline">\(\pi\)</span> but also specifies a construction of these functions. The ingredients of the construction comprise the transition amplitudes <span class="math inline">\(\mathcal P\)</span> of the MDP, the reward functions <span class="math inline">\(R\)</span>, and the policy <span class="math inline">\(\pi\)</span>.</p>
<p>Obviously, our long-term goal is to maximize <span class="math inline">\(V_\pi\)</span> for as many states <span class="math inline">\(s\in\mathcal S\)</span> as possible. The question is how to learn a convenient strategy <span class="math inline">\(\pi\)</span> to accomplish this task. Typically, although finite, the combinatorial complexity does not allow a brute force search – recall the discussion about the number of position in a Go game. Another complication is that the transition probabilities <span class="math inline">\(\mathcal P\)</span> are typically unknown to us.</p>
<p>As next, steps we will therefore define optimal strategies and prove their existence. In a final step we will then attempt to approximate them by means of algorithms that, e.g., update given initial policies conveniently, and come back to the question of the unknown transition probabilities <span class="math inline">\(\mathcal P\)</span>.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
