<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__09-1__Support_vector_machines</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-09-1-pdf">MsAML-09-1 [<a href="SS21-MsAML__09-1__Support_vector_machines.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#support-vector-machines">Support vector machines</a>
<ul>
<li><a href="#hard-margin-case">Hard margin case</a></li>
<li><a href="#soft-margin-case">Soft margin case</a></li>
<li><a href="#implementation">Implementation</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="support-vector-machines">Support vector machines</h2>
<p>Recall the discussion we started in the module <a href="SS21-MsAML__05-1__Linear_classification_models.html">models of linear classification</a>. There, we introduced the class of linear classification models parametrized by the Adaline model as a basic representative for various choices of activation function <span class="math inline">\(\alpha(z)\)</span> and loss function <span class="math inline">\(L(y,y&#39;)\)</span>. The objective of this discussion was to better understand how these choices affect:</p>
<ol type="1">
<li>the efficiency of training with prescribed training data;</li>
<li>and the generalization capabilities of the model to unseen data.</li>
</ol>
<p>Regarding point 1. it was important to understand in which sense a minimization of the empirical loss implies a low empirical error probability and how this minimization procedure can be made more efficient, e.g., by avoiding the saturation of the neuron. We furthermore discussed that not only the choice of <span class="math inline">\(\alpha(z)\)</span> and <span class="math inline">\(L(y,y&#39;)\)</span> is important but also the preparation of the training data as well as the choice of initial weights impacts the performance; not to mention the purity of the manually annotated training data.</p>
<p>Regarding point 2., so far we only discussed that, in addition to the requirement that a minimization of the empirical loss should imply a low empirical error, we we could equip the empirical loss function <span class="math inline">\(\widehat L(b,w)\)</span> with a notion that influence the update rule directly, e.g., in order to prefer certain updates over others. This allows to encode a variety of desired generalization behavior for the respective model. In this module we close our discussion on linear classification models by introducing one of the most simple but, as it turns out, also very effective and “industry-proof” notions of generalization. The resulting model is the so-called <em>support vector machine</em>, which beside the multi-layered Adaline will later in our course provide a particular convenient way to tackle non-linear classification tasks.</p>
<h3 id="hard-margin-case">Hard margin case</h3>
<p>Recall that, for linearly separable training data, we proved that the Perceptron will succeed to find a separating hyperplane in finite time. Also we argued that, depending on the learning parameter tuning, the Adaline will quite efficiency find one – although we were not able to give a general convergence guarantee to a separating hyperplane. However, in the typical case of finite training data, the separating hyperplane is never unique. This would allow us to choose one that has good chances to generalize better than others to unseen data. As we discussed after the <a href="SS21-MsAML__04-2__No_free_lunch_theorems.html">no-free-lunch theorem</a>, this is only possible if certain regularities of the distribution of the test data are known a priori, and this a priori knowledge has to be supplied in addition to the training data. In the following, we shall explore such a simple notion that works well in many settings:</p>
<p>As we have done before, let us consider binary classification with labels <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span> and prescribed, linearly separable, and finite training data. Among all possible separating hyperplanes, one could now choose the one that maximizes the margin width of the separation as it is depicted here:</p>
<figure>
<img src="09/maximum_margin.png" alt="Margin width in our setting." /><figcaption aria-hidden="true">Margin width in our setting.</figcaption>
</figure>
<p>Now, our task is to find an optimization program that would penalizes both, a larger error probability as well as a larger margin width.</p>
<p>First, let us compute the distance between a feature vector of a training datum <span class="math inline">\(x^{(i)}\in\mathbb R^d\)</span> and a separating hyperplane given by means of the normal equation: <span class="math display">\[\begin{align}
  H_{(b,w)}
  :=
  \left\{
    x\in\mathbb R^d
    \,\big|\,
    \bar w\cdot\bar x = w\cdot x+b=0
  \right\}
\end{align}\]</span></p>
<p>In our <span class="math inline">\(\bar w=(b,w)\)</span> and <span class="math inline">\(\bar x^{(i)}=(1,x^{(i)})\)</span> notation, by definition, <span class="math inline">\(\bar w\)</span> is orthogonal to vectors <span class="math inline">\(\bar x=(1,x)\in H_{(b,w)}\)</span> on the hyperplane. Hence, the ray orthogonal to the hyperplane <span class="math inline">\(H_{(b,w)}\)</span> and emitted at point <span class="math inline">\(\bar x^{(i)}\)</span> can be parametrized as follows: <span class="math display">\[\begin{align}
  \bar x^{(i)}(\lambda)
  =
  \begin{pmatrix}
    1\\
    x^{(i)}(\lambda)
  \end{pmatrix}
  =
  \begin{pmatrix}
    1\\
    x^{(i)}
  \end{pmatrix}
  +
  \lambda
  \begin{pmatrix}
    0\\
    w 
  \end{pmatrix},
  \qquad
  \lambda\in\mathbb R.
\end{align}\]</span> In turn, the ray intersects the hyperplane at <span class="math inline">\(\lambda^*\)</span> fulfilling <span class="math display">\[\begin{align}
  0 = \bar w\cdot\bar x^{(i)}(\lambda^*)
  &amp;= b + w\cdot x^{(i)} + \lambda^* |w|^2\\
  &amp;= \bar w\cdot \bar x^{(i)} + \lambda^* |w|^2.
\end{align}\]</span> Note that on the right-hand side the bars <span class="math inline">\(|\cdot|\)</span> denote the euclidean norm and the overset bar <span class="math inline">\(\bar{\cdot}\)</span> on <span class="math inline">\(w\)</span> in the argument of the norm is missing on purpose.</p>
<p>The intersection point is therefore given by <span class="math inline">\(x^{(i)}(\lambda^*)\)</span> for <span class="math display">\[\begin{align}
  \lambda^* = - \frac{\bar w \cdot \bar x^{(i)}}{|w|^2},
\end{align}\]</span> which allows to compute the minimal distance between <span class="math inline">\(x^{(i)}\)</span> and the hyperplane as follows: <span class="math display">\[\begin{align}
  \operatorname{dist}_{(b,w)}(x^{(i)}) 
  &amp;:= 
  \left| x^{(i)}-x^{(i)}(\lambda^*)\right|
  =
  \frac{\left| \bar\omega\cdot \bar x^{(k)}\right|}{|w|}.
  \label{eq_1}
  \tag{E1}
\end{align}\]</span></p>
<p>This term would already allow to formulate a new notion of optimization, i.e., among all separating hyperplanes <span class="math inline">\(H_{(b,w)}\)</span>, i.e., those that achieve zero classification errors on the training data, choose the one that maximizes the distances <span class="math display">\[\begin{align}
  \operatorname{dist}_{(b,w)}(x^{(k)}),
\end{align}\]</span> for the points <span class="math inline">\(k\in\{1,\ldots,N\}\)</span> closest to the hyperplane <span class="math inline">\(H_{(b,w)}\)</span>.</p>
<p>In order to make this notion more concise, we may exploit the scalar freedom in the normal equations <span class="math display">\[\begin{align}
  \bar w\cdot \bar x = 0
  \qquad
  \Leftrightarrow
  \qquad
  \forall \gamma&gt;0: \gamma\bar w\cdot\bar x = 0,
\end{align}\]</span> and choose <span class="math inline">\(\gamma&gt;0\)</span> such that those vectors <span class="math inline">\(x^{(k)}\)</span> that lies closest to <span class="math inline">\(H_{(b,w)}\)</span>, the so-called <em>support vectors</em>, fulfill <span class="math display">\[\begin{align}
  \bar w\cdot\bar x^{(k)}=\pm 1.
\end{align}\]</span> After the scaling, the distances <span class="math inline">\(\eqref{eq_1}\)</span> read <span class="math display">\[\begin{align}
  \operatorname{dist}_{(b,w)}(x^{(k)}) 
  = 
  \frac{1}{|w|}
\end{align}\]</span> in the new units, we find ourselves in the following scenario:</p>
<figure>
<img src="09/after_rescaling.png" alt="A separating hyperplane after rescaling." /><figcaption aria-hidden="true">A separating hyperplane after rescaling.</figcaption>
</figure>
<p>Finally, we can recast this into the equivalent optimization program: <span class="math display">\[\begin{align}
  \underset{\bar w=(b,w)\in\mathbb R^{d+1}}{\sup} \frac{1}{|w|}
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &amp;y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1\\
    &amp;\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
\end{align}\]</span></p>
<p>In other words, during optimization, the objective is to maximize <span class="math inline">\(\frac{1}{|w|}\)</span> while restricting to those hyperplanes that correctly separate the training data, which is encoded in the constraints <span class="math display">\[\begin{align}
  y^{(i)}\,\bar w\cdot \bar x^{(i)}\geq 1,
  \qquad
  \text{for}
  \qquad
  i=1,\ldots,N.
\end{align}\]</span></p>
<p>If preferred, we can formulate the same maximization program as a minimization program: <span class="math display">\[\begin{align}
  \underset{\bar w=(b,w)\in\mathbb R^{d+1}}{\inf} |w|
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &amp;y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1\\
    &amp;\text{for all } i=1,\ldots, N
  \end{cases}
  \,,
  \label{eq_2}
  \tag{E2}
\end{align}\]</span> which we shall refer to as the optimization program for the <em>hard margin support vector machine</em>.</p>
<p>Note that compared to the previous Adaline optimization programs, for which we simply attempted to minimize the empirical loss <span class="math display">\[\begin{align}
  \underset{\bar w=(b,w)\in\mathbb R^{d+1}}{\inf} \widehat L(b,w),
\end{align}\]</span> in <span class="math inline">\(\eqref{eq_2}\)</span>, we minimize with respect to constraints that enforce zero misclassifications. After the discussion of the “soft margin case” we will give a formulation of the minimization program in terms of an empirical loss function as indicated in our introductory discussion above.</p>
<p>Of course, the generalization capabilities of the optimal hyperplane <span class="math inline">\(H_{(b,w)}\)</span> strongly depend on the question, whether the maximum margin is a good a priori choice to fit the test data for the learning task at hand. The latter depends on the regularities the underlying distribution of test data features. So far, we can only say that, if the empirical mean of the training data sample is already a good enough approximation of the actual mean and the test data typically clusters closely around this mean with respect to the euclidean norm, the above optimization program <span class="math inline">\(\eqref{eq_2}\)</span> is very likely a reasonable choice that has good chances to generalize well.</p>
<p>Later in our discussion of the optimization program, we will see that the optimal <span class="math inline">\(w\)</span> for program <span class="math inline">\(\eqref{eq_2}\)</span> will be given as a function of the support vectors. Thus, <span class="math inline">\(w\)</span> is we exposed to the fluctuations of the training data near the margin. In order to reduce this dependence, we extend the program in the next section.</p>
<h3 id="soft-margin-case">Soft margin case</h3>
<p>As a small generalization of the hard margin case, we could even allow for margin violations and even some misclassification to some extend and attempt to minimize them. Then, we cannot guarantee anymore to have zero misclassifications, however, the reduced dependence on strong fluctuations in the training data (or, e.g., even cope with some outlines of wrongly annotated training data) may be beneficial for the generalization capabilities, while one may still achieve a good, though not perfect, performance on the classification of the training data.</p>
<p>Say, a hyperplane <span class="math inline">\(H_{(b,w)}\)</span> was chosen and a training data feature vector <span class="math inline">\(x^{(i)}\)</span> violates the margin, then</p>
<ul>
<li>either, we have only a violation within the margin width <span class="math display">\[\begin{align}
  0 \leq y^{(i)} \, \bar w\cdot \bar x^{(i)} \leq 1,
\end{align}\]</span></li>
<li>or even a misclassification <span class="math display">\[\begin{align}
  y^{(i)}\, \bar w\cdot \bar x^{(i)} &lt; 0.
\end{align}\]</span></li>
</ul>
<p>Now, the setting looks as follows:</p>
<figure>
<img src="09/soft_margin.png" alt="The soft margin case." /><figcaption aria-hidden="true">The soft margin case.</figcaption>
</figure>
<p>The “depth” <span class="math inline">\(\eta^{(i)}\)</span> of the margin violation can be computed again by orthogonal projection of the feature vector of the training datum <span class="math inline">\(x^{(i)}\)</span> this time onto the respective margin boundaries <span class="math display">\[\begin{align}
  \bar w\cdot \bar x = y^{(i)};
\end{align}\]</span> recall again that we work in the setting <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span>.</p>
<p>Again, in order to compute the depth of a potential margin violation, we parametrize the respective ray emitted at <span class="math inline">\(x^{(i)}\)</span> and lying orthogonal to the margin boundary as follows: <span class="math display">\[\begin{align}
  \bar x^{(i)}(\lambda)
  =
  \begin{pmatrix}
    1\\
    x^{(i)}(\lambda)
  \end{pmatrix}
  =
  \begin{pmatrix}
    1\\
    x^{(i)}
  \end{pmatrix}
  +
  \lambda
  \begin{pmatrix}
    0\\
    w 
  \end{pmatrix},
  \qquad
  \lambda\in\mathbb R.
\end{align}\]</span> In consequence, the intersection of the ray and the margin boundary can be computed by solving for <span class="math inline">\(\lambda^*\)</span> in equation <span class="math display">\[\begin{align}
  \bar w \cdot \bar x^{(i)}(\lambda^*) = y^{(i)},
\end{align}\]</span> which yields <span class="math display">\[\begin{align}
  \lambda^* 
  = 
  \frac{\left|y^{(i)}-\bar w\cdot \bar x^{(i)}\right|}{|w|^2}
  = 
  \frac{\left|1 - y^{(i)} \, \bar w\cdot \bar x^{(i)}\right|}{|w|^2}.
\end{align}\]</span></p>
<p>In case of the margin violation, the depth is then given by <span class="math display">\[\begin{align}
  \eta^{(i)}
  =
  \left|x^{(i)}-x^{(i)}(\lambda^*)\right|
  =
  \frac{\left|1 - y^{(i)} \, \bar w\cdot \bar x^{(i)}\right|}{|w|}.
\end{align}\]</span></p>
<p>In analogy to the hard margin case optimization program <span class="math inline">\(\eqref{eq_2}\)</span>, we may set up its generalization as follows: <span class="math display">\[\begin{align}
  \underset{\substack{\bar w=(b,w)\in\mathbb R^{d+1}\\\eta^{(i)}\geq 0, i=1,\ldots, N}}{\inf} 
  \left[
    |w| + \sum_{i=1}^N \eta^{(i)}
  \right]
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &amp;y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1 - |w|\eta^{(i)}\\
    &amp;\text{for all } i=1,\ldots, N
  \end{cases}
  \,.
\end{align}\]</span> Note that the constraints give <span class="math inline">\(\eta^{(i)}\)</span> a meaning as depth of margin violation.</p>
<p>Again, exploiting the scalar freedom of the model, we may reformulate this equivalently as <span class="math display">\[\begin{align}
  \underset{\substack{\bar w=(b,w)\in\mathbb R^{d+1}\\\xi^{(i)}\geq 0, i=1,\ldots,N}}{\inf} 
  \left[
    |w|^2 + \sum_{i=1}^N \xi^{(i)}
  \right]
  \quad
  \text{subject to}
  \quad
  \begin{cases}
    &amp;y^{(i)} \, \bar w\cdot\bar x^{(i)}\geq 1 - \xi^{(i)}\\
    &amp;\text{for all } i=1,\ldots, N
  \end{cases}
  \label{eq_3}
  \tag{E3}
  \,,
\end{align}\]</span> which we shall refer to as the optimization program of the <em>soft margin support vector machine</em>.</p>
<h3 id="implementation">Implementation</h3>
<p>A first, maybe naive but working implementation can be made in the spirit of the Adaline model, minimizing the following empirical loss function <span class="math display">\[\begin{align}
  \widehat L(b,w) = 
  \frac12|w|^2 
  + \frac\mu N \sum_{i=1}^N 
    \max\left\{0, 1- y^{(i)}\, \bar w\cdot \bar x^{(i)}\right\}.
    \label{eq_4}
    \tag{E4}
\end{align}\]</span> The factor <span class="math inline">\(\frac12\)</span> and the parameter <span class="math inline">\(\mu\geq 0\)</span>, controlling the trade-off between a large margin vs. having only few and small margin violations during the updates, are introduced for convenience as done in many text-books. The update rule can be formulated by means of gradient descent employing a sub-differential for the <span class="math inline">\(\max\)</span> function.</p>
<p>There are far superior algorithms to carry out the optimization problem <span class="math inline">\(\eqref{eq_3}\)</span>, most notably as implemented in the C library <a href="https://www.csie.ntu.edu.tw/~cjlin/libsvm/">LIBSVM</a>, which has many bindings including Python-bindings through <a href="https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html">sklean</a>.</p>
<dl>
<dt>👀</dt>
<dd>Adapt one of your Adaline implementations to minimize the empirical loss <span class="math inline">\(\eqref{eq_4}\)</span> with the help of the gradient descent method employing a reasonable sub-differential. Discuss for which distributions of test data the model would indeed generalize well and in which situations it would not, despite the effort of the maximization of the margin width.
</dd>
</dl>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
