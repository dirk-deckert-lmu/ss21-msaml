% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  a4paper,
]{article}
\usepackage{amsmath,amssymb}
\usepackage{lmodern}
\usepackage{iftex}
\ifPDFTeX
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdfauthor={Dirk - André Deckert},
  colorlinks=true,
  linkcolor={blue},
  filecolor={Maroon},
  citecolor={Blue},
  urlcolor={blue},
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=3.5cm]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
  \usepackage{selnolig}  % disable illegal ligatures
\fi

\author{Dirk - André Deckert}
\date{}

\usepackage{fancyhdr}
\pagestyle{fancy}
\pagestyle{fancy}
\fancyhead{}
\fancyhead[LE]{\scriptsize\thepage\hfill\nouppercase\leftmark}
\fancyhead[RO]{\scriptsize \nouppercase\rightmark\hfill\thepage}
\fancyfoot[C]{\scriptsize By \href{https://www.mathematik.uni-muenchen.de/~deckert/}{D.-A.\ Deckert} licensed under a \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution
4.0 International License}.}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\renewcommand{\subparagraph}[1]{\textbf{#1}}

\begin{document}

\hypertarget{msaml-03-s1-pdf}{%
\section{\texorpdfstring{MsAML-03-S1
{[}\href{SS21-MsAML__03-S1__A_simple_learning_setting.pdf}{PDF}{]}}{MsAML-03-S1 {[}PDF{]}}}\label{msaml-03-s1-pdf}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \protect\hyperlink{a-simple-learning-setting}{A first simple learning
  setting}

  \begin{itemize}
  \tightlist
  \item
    \protect\hyperlink{solutions}{Solutions}
  \end{itemize}
\end{enumerate}

Back to \href{index.pdf}{index}.

Because we already have a several exercises concerning the Perceptron
and Adaline, this module was written as part lecture and part exercise
with most solutions included in the end.

\hypertarget{a-first-simple-learning-setting}{%
\subsection{A first simple learning
setting}\label{a-first-simple-learning-setting}}

In order to grasp the \emph{learnability} more formally, let us regard a
simple but non-trivial learning setting in the sense that it already
features all important ingredients we will discuss in more general
settings later on.

Suppose we have an algorithm \(\mathcal A\) whose range is given by a
finite space of hypotheses \(\mathcal F\). Let us assume that, given a
training data sample \(s\), the algorithm always produces a hypothesis
\(h_s=\mathcal A(s)\).

Furthermore, let us consider a setting such that \begin{align}
  \exists h^*\in\mathcal F: \, P_{(X,Y)\sim P}(Y\neq h^*(X)) = 0,
\end{align} a condition that is often called \emph{realizability}. In
other words, this means that there is a hypothesis \(h^*\), in principle
reachable by the algorithm as \(h^*\in\mathcal F\), which correctly
classifies all data points with probability \begin{align}
  P_{(X,Y)\sim P}(Y=h^*(X)) = 1 - P_{(X,Y)\sim P}(Y\neq h^*(X)) = 1.
  \tag{P1}
  \label{eq_prob}
\end{align}

As the actual risk, e.g., given in terms of the error probability,
\begin{align}
  R(h^*)=E_{(X,Y)\sim P} 1_{Y\neq h(X)},
\end{align} is not accessible without knowing \(P\), this is of course
academic. Under our general assumption that the training data random
variable \(S=(X^{(i)},Y^{(i)})_{i=1,\ldots,N}\) is i.i.d.-\(P\), the
corresponding empirical risk \begin{align}
  \forall h\in\mathcal F: \quad \widehat R_S(h)=\frac1N\sum_{i=1}^N 1_{Y^{(i)}\neq h(X^{(i)})}
\end{align} can however be seen to fulfill \begin{align}
  \widehat R_S(h^*) = 0
\end{align} with probability one (recall that it depends on random
variable \(S\), and in turn, is a random variable), or equivalently
\begin{align}
  P_{S\sim P^N}\left(\widehat R_S(h^*)=0\right)=1.
  \tag{P2}
  \label{eq_emp_risk}
\end{align}

\begin{description}
\item[👀] ~ 
\hypertarget{task-1}{%
\subparagraph{Task 1}\label{task-1}}

Prove \(\eqref{eq_emp_risk}\) using the properties of i.i.d. random
variables and assumption \(\eqref{eq_prob}\).
\end{description}

Even though the last proposition \(\eqref{eq_emp_risk}\) was about the
more accessible empirical risk, the reachability of \(h^*\) is still
academic. Recall that \(\mathcal A\) may only inspect the given
realization \(s\) of the random and i.i.d.-\(P\) distributed training
data \(S\). Hence, \(h^*\) and \(h_s\) may have little in common. Let us
therefore assume further that \(\mathcal A\) manages to complete the
task of empirical risk minimization for any given training data sample
\(s\), i.e., \begin{align}
  h_s \in \underset{h\in\mathcal F}{\text{argmin}} \widehat R_s(h).
  \tag{A}
  \label{eq_argmin}
\end{align}

\begin{description}
\item[👀] ~ 
\hypertarget{section}{%
\subparagraph{}\label{section}}

Why is the right-hand side of \(\eqref{eq_argmin}\) not empty?
\end{description}

This assumption is sometimes referred to as \emph{feasibility of the
empirical risk minimization}. As a consequence, also \begin{align}
  \widehat R_S(h_S)=0
\end{align} holds with probability one, i.e., \begin{align}
  P_{S\sim P^N}\left(\widehat R_S(h_S)=0\right) = 1.
  \tag{P3}
  \label{eq_emp_h}
\end{align}

\begin{description}
\item[👀] ~ 
\hypertarget{task-2}{%
\subparagraph{Task 2}\label{task-2}}

Prove \(\eqref{eq_emp_h}\) exploiting the existence of \(h^*\) and
\(\eqref{eq_argmin}\).
\end{description}

Hence, we are in a advantageous setting, in which \(\mathcal A\)
succeeds to perform the empirical risk minimization with probability
one. Next, it is natural to ask how well these found hypotheses
\(h_S=\mathcal A(S)\) generalize, or in other words, how their actual
risk behaves. We should therefore regard the difference \begin{align}
  R(h_S) - R(h^*),
\end{align} which in our simple setting amounts to \(R(h_S)\) as
\(R(h^*)=0\); though, we will still keep the zero quantity \(R(h^*)\) it
in our notation as it helps the eye. Let us therefore analyze the
probability \begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)>\epsilon\right)
  \tag{G1}
  \label{eq_gen}
\end{align} for some \(\epsilon>0\). This quantity is interesting as it
formalizes how well the found hypotheses \(h_S\) performs on unseen
data. In fact, the above term is the probability of such hypotheses to
generalize \(\epsilon\)-badly. For our analysis, let us define the
subset of hypotheses that, in our setting, can be considered
\(\epsilon\)-badly overfitted: \begin{align}
  \mathcal F_\epsilon := \left\{h \in\mathcal F \,|\, R(h)>\epsilon \right\}
\end{align} Let us write the event inside the argument of the measure
\(P\) in \(\eqref{eq_gen}\) as \begin{align}
  \left\{S=s\,|\, R(h_s) > \epsilon\right\},
\end{align} using the notation \(S=s\) to denote the realizations \(s\)
of the random variable \(S\). As \(h_s\) is in the range of
\(\mathcal A\), this event can only occur if there is a
\(h\in \mathcal F_\epsilon \cap \text{range}\mathcal A\), i.e.,
\begin{align}
  \left\{S=s\,\Big|\, R(h_s) > \epsilon\right\}
  \subseteq
  \left\{S=s\,\Big|\, \exists h \in\mathcal F_\epsilon \wedge \widehat R_s(h) = 0\right\}.
\end{align} This allows to find the bound \begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)>\epsilon\right)
  \leq 
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\widehat R_S(h)=0\right).
  \tag{G2}
  \label{eq_gen_2}
\end{align}

\begin{description}
\item[👀] ~ 
\hypertarget{task-3}{%
\subparagraph{Task 3}\label{task-3}}

Prove \(\eqref{eq_gen_2}\) using the union bound.
\end{description}

Of course, \(\eqref{eq_gen_2}\) is only a coarse bound counting all the
overfitted hypotheses. However, they are well-known to us in the sense
that, whatever the unknown \(P\) is, they fulfill \(R(h)>0\). Now we
have a chance to find a bound that is independent of the distribution
\(P\), namely: \begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)>\epsilon\right)
  \leq 
  |\mathcal F| (1-\epsilon)^N.
  \tag{G3}
  \label{eq_gen_3}
\end{align}

\begin{description}
\item[👀] ~ 
\hypertarget{task-4}{%
\subparagraph{Task 4}\label{task-4}}

Prove \(\eqref{eq_gen_3}\) by using the i.i.d.-\(P\) propoerty of \(S\),
the definition of \(\mathcal F_\epsilon\), and estimating the size
\(|\mathcal  F_\epsilon|\) even coarser by means of \(|\mathcal F|\).
\end{description}

Morally, this bound informs us that the probability of a generalization
error grows with the size of \(\mathcal F\) while it decreases with the
sample size \(N\). At least for this simple learning setting, our
initial hope that we used to motivate the method of supervised learning
is substantiated.

In a final step, let us rewrite \(\eqref{eq_gen_3}\) to gain information
on the magnitude of \(N\). For a given generalization error \(\epsilon\)
let us ask about the magnitude of \(N\) to guarantee a given target
probability of \(1-\delta\). Our bound \(\eqref{eq_gen_3}\) implies:

\begin{itemize}
\tightlist
\item
  \(\forall\delta > 0\)
\item
  \(\forall\epsilon > 0\)
\item
  \(\forall N > \frac1\epsilon \log \frac{|\mathcal F|}{\delta}\)
  \begin{align}
    P_{S\sim P^N}\left( R(h_S) - R(h^*) < \epsilon\right) \geq 1 - \delta,
    \tag{G4}
    \label{eq_gen_4}
  \end{align} or in other words \begin{align}
    R(h_S) - R(h^*)  < \epsilon
  \end{align} at least with probability \(1-\delta\).
\end{itemize}

\begin{description}
\item[👀] ~ 
\hypertarget{task-5}{%
\subparagraph{Task 5}\label{task-5}}

Prove \(\eqref{eq_gen_4}\).
\end{description}

Even though, in actual implementations \(|\mathcal F|\) is always finite
due to the way computers are build, the size of the \(\mathcal F\) can
become exorbitantly large even for simple problems. The latter renders
bounds as \(\eqref{eq_gen_3}\) rather meaningless for applications since
then, \(N\) would have to be chosen extremely large as well. The latter
is however not up to us as large samples of well-annotated training data
are typically very costly to produce. In the next modules we will
therefore go some distance to see how this simple learning setting can
be generalized, in particular, how we can avoid a reliance on
\(|\mathcal F|<\infty\).

➢ Next session!

\hypertarget{solutions}{%
\subsubsection{Solutions}\label{solutions}}

\textbf{Task 1:} Using the fact that \(S\) is i.i.d.-\(P^N\) we find
\begin{align}
  & P_{S\sim P^N}\left(\widehat R_S(h^*)=0\right)
  \\
  & = P_{S\sim P^N}\left(Y^{(1)}=h(X^{(1)})
                     \wedge Y^{(2)}=h(X^{(2)}) 
                     \wedge \ldots 
                     \wedge Y^{(N)}=h(X^{(N)})\right)
  \\
  & = 
  \prod_{i=1}^N P_{S\sim P^N}(Y^{(i)}=h(X^{(i)}))
  \\
  & =
  P_{(X,Y)\sim P}(Y=h(X))
  \\
  & =
  1
\end{align} by using independence, the identical distribution property,
and \(\eqref{eq_prob}\) in the last three steps, respectively.

\textbf{Task 2:} \begin{align}
  &
  P_{S\sim P^N}\Big(\widehat R_S(h_S)=0\Big)
  \\
  &=
  P_{S\sim P^N}\Big(\widehat R_S(\widetilde h)=0 
                     \text{ for } \widetilde h\in \underbrace{\underset{h\in\mathcal F}{\text{argmin}}\widehat R_S(h)}_{\ni h^*}
               \Big)
  \\
  &=
  P_{S\sim P^N}\Big(\widehat R_S(h^*)=0\Big) \overset{\eqref{eq_emp_risk}}{=} 1
\end{align}

\textbf{Task 3:} By the union bound we immediate get \begin{align}
  & P_{S\sim P^N}\left(R(h_S) - R(h^*)>\epsilon\right)
  \\
  &\leq
  P_{S\sim P^N}\left(\left\{S=s\,\Big|\, \exists h \in\mathcal F_\epsilon \wedge \widehat R(h_s) = 0\right\}\right)
  \\
  &=
  P_{S\sim P^N}\left(\bigcup_{h \in\mathcal F_\epsilon}\left\{S=s\,\Big|\, \widehat R(h_s) = 0\right\}\right)
  \\
  &\leq
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\left\{S=s\,\Big|\, \widehat R(h_s) = 0\right\}\right)
  \\
  &=
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\widehat R(h_S) = 0\right).
\end{align}

\textbf{Task 4:} Using \(\eqref{eq_gen_2}\) we have \begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)>\epsilon\right)
  \leq 
  \sum_{h\in\mathcal F_\epsilon} P_{S\sim P^N}\left(\widehat R_S(h)=0\right)
\end{align} and we note \begin{align}
  & P_{S\sim P^N}\left(\widehat R_S(h)=0\right)
  \\
  &= 
  P_{S\sim P^N}\left(\forall i=1,\ldots, N:\, Y^{(i)}=h(X^{(i)})\right).
\end{align} Again, exploiting the i.i.d.-\(P\) property, we find
\begin{align}
  &P_{S\sim P^N}\left(R(h_S) - R(h^*)>\epsilon\right)
  \\
  &\leq 
  \sum_{h\in\mathcal F_\epsilon} 
  \prod_{i=1}^N 
  P_{(X^{(i)},Y^{(i)})\sim P}
  \left(Y^{(i)}=h(X^{(i)}) \right)
  \\
  & = 
  \sum_{h\in\mathcal F_\epsilon} 
  \prod_{i=1}^N 
  P_{(X,Y)\sim P}
  \left(Y=h(X) \right).
\end{align} Using the definition of \(\mathcal F_\epsilon\) we find
\begin{align}
  P_{S\sim P^N}\left(R(h_S) - R(h^*)>\epsilon\right)
  & \leq 
  \sum_{h\in\mathcal F_\epsilon} 
  \prod_{i=1}^N 
  (1-\epsilon)
  \\
  & = |\mathcal F_\epsilon| (1-\epsilon)^N
  \\
  & \leq |\mathcal F| (1-\epsilon)^N.
\end{align}

\textbf{Task 5:} First, we observe \begin{align}
  & P_{S\sim P^N}\left( R(h_S) - R(h^*)\leq \epsilon\right) \geq 1-\delta
  \\
  & \Leftrightarrow
  P_{S\sim P^N}\left( R(h_S) - R(h^*)> \epsilon\right) \leq \delta.
\end{align} But from \(\eqref{eq_gen_3}\) we know already \begin{align}
  P_{S\sim P^N}\left( R(h_S) - R(h^*) > \epsilon\right) \leq |\mathcal F|(1-\epsilon)^N.
\end{align} Hence, solving \begin{align}
  |\mathcal F|\underbrace{(1-\epsilon)^N}_{\leq e^{-\epsilon N}} 
  \leq |\mathcal F|e^{-\epsilon N} 
  \leq \delta
\end{align} for \(N\) gives \begin{align}
  N \geq \frac1\epsilon \log\frac{|\mathcal F|}{\delta}.
\end{align}

\end{document}
