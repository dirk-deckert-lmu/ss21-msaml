# Exercise 05-S1: Learning intervals with noise [[PDF](SS21-MsAML__05-XS1__Learning_intervals_with_noise.pdf)]

Consider the guessing game with intervals learning from Exercise 03-2 again. However, now there is a random noise added to the setting. Before Bob receives the data, Alice randomly changes labels of data points inside the chosen interval from one to zero with probability $p$. She leaves the labels of all the points outside the interval untouched though, i.e. equal to zero.

Can Bob still PAC-learn the concept class of intervals using the same algorithm as before (i.e. choosing the smallest interval that covers the points with positive labels)? If so, express the bound on the number of samples in terms of $\epsilon, \delta$ and probability $p$. How does the bound change in comparison with the game setting without noise? 
